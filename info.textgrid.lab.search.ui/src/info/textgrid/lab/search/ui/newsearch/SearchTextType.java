package info.textgrid.lab.search.ui.newsearch;

import java.util.ArrayList;

public class SearchTextType {
	
	private SearchTextType root;
	private String textType;
	private String label;
	private ArrayList<SearchTextType> children = new ArrayList<SearchTextType>();
	
	public SearchTextType(String label, String textType, boolean init) {
		this.textType = textType;
		this.label = label;
		if (init) {
			this.root = this;
			init();
		}
	}
	
	public String toString() {
		return this.label;
	}
	
	public String getTextType() {
		return this.textType;
	}
	
	public void add(SearchTextType child) {
		children.add(child);
	}
	
	public SearchTextType[] getChildren() {
		return children.toArray(new SearchTextType[0]);
	}
	
	public int getChildCount() {
		return root.getChildren()[0].getChildren().length +1;
	}
	
	private void init() {
		SearchTextType textTypes = new SearchTextType(Messages.SearchTextType_TextTypes, "", false); 
		root.add(textTypes);
		
		SearchTextType drama = new SearchTextType(Messages.SearchTextType_Drama, "drama", false);
		SearchTextType prose = new SearchTextType(Messages.SearchTextType_Prose, "prose", false);
		SearchTextType verse = new SearchTextType(Messages.SearchTextType_Verse, "verse", false);
		SearchTextType referencework = new SearchTextType(Messages.SearchTextType_ReferenceWork, "referencework", false);
		SearchTextType nonfiction = new SearchTextType(Messages.SearchTextType_NonFiction, "none-fiction", false);
		SearchTextType nontext = new SearchTextType(Messages.SearchTextType_NonText, "none-text", false);
		SearchTextType other = new SearchTextType(Messages.SearchTextType_Other, "other", false);
		
		textTypes.add(drama);
		textTypes.add(prose);
		textTypes.add(verse);
		textTypes.add(referencework);
		textTypes.add(nonfiction);
		textTypes.add(nontext);
		textTypes.add(other);
		
		//The metadata schema doesn't support this breakdown at the moment
//		drama.add(new SearchTextType("Tradgedy", false));
//		drama.add(new SearchTextType("Comedy", false));
//		drama.add(new SearchTextType("Tradgedy", false));
//		drama.add(new SearchTextType("Tragicomedy", false));
//		drama.add(new SearchTextType("Opera", false));
//		drama.add(new SearchTextType("Operetta", false));
//		drama.add(new SearchTextType("Musical", false));
//		drama.add(new SearchTextType("Radio Play", false));
//		
//		prose.add(new SearchTextType("Anecdote", false));
//		prose.add(new SearchTextType("Short Story", false));
//		prose.add(new SearchTextType("Fairy Tale", false));
//		prose.add(new SearchTextType("Novel", false));
//		prose.add(new SearchTextType("Novella", false));
//		prose.add(new SearchTextType("Saga", false));
//		
//		verse.add(new SearchTextType("Epic Poem", false));
//		verse.add(new SearchTextType("Song", false));
//		verse.add(new SearchTextType("Lay", false));
//		verse.add(new SearchTextType("Lieder", false));
//		verse.add(new SearchTextType("Satire", false));
//		verse.add(new SearchTextType("Sonnet", false));
//		verse.add(new SearchTextType("Epigram", false));
//		
//		referencework.add(new SearchTextType("Monolingual Dictionary ", false));
//		referencework.add(new SearchTextType("Bilingual Dictionary", false));
//		referencework.add(new SearchTextType("Multilingual Dictionary", false));
//		referencework.add(new SearchTextType("Encyclopaedia", false));
	}


}
