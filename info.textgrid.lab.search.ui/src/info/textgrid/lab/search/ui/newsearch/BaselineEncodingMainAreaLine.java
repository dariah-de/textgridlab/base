package info.textgrid.lab.search.ui.newsearch;

import info.textgrid.lab.search.ui.Activator;
import info.textgrid.lab.search.ui.views.SearchView;

import java.util.ArrayList;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PlatformUI;

/**
 * Represents a line of the main baseline encoding area.
 * Such a line is composed of one ComboViewer to
 * select a parameter and an optional amount of 
 * text controls. These text controls are in an 
 * or-relationship with each other.
 * @author Frank Queens
 *
 */


public class BaselineEncodingMainAreaLine {
	
	private BaselineEncodingMainArea baselineEncodingMainArea;
//	private ComboViewer baselineEncodingViewer;
	private Text baselineEncodingElement;
	private ArrayList<Text> textList = new ArrayList<Text>();  
	private RowData rdText;
	private Composite mainComposite;
	private ScrolledComposite scrolledComposite;
	private Label deleteImage;
	
	private final static ArrayList<String> baselineEncodingList = new ArrayList<String>(){
		private static final long serialVersionUID = 1L;

		{
            add("letter (all)");
            add("--letter.top");
            add("--letter.center");
            add("--letter.bottom");
            add("drama");
            add("--drama.stage_direction");
            add("--drama.text");
            add("lyric");
            add("apostille");
        }
    };

	BaselineEncodingMainAreaLine(BaselineEncodingMainArea baselineEncodingMainArea, Composite composite, ScrolledComposite scrolledComposite, Boolean firstLine) {
		this.baselineEncodingMainArea = baselineEncodingMainArea;
		this.mainComposite = composite;
		this.scrolledComposite = scrolledComposite;
		RowLayout rl = new RowLayout();
		rl.wrap = false;

		composite.setLayout(rl);
//		baselineEncodingViewer = new ComboViewer(composite);
//		baselineEncodingViewer.setContentProvider(new ArrayContentProvider());
//		baselineEncodingViewer.setLabelProvider(new LabelProvider());
//		baselineEncodingViewer.setInput(baselineEncodingList);
//		GridDataFactory.defaultsFor(baselineEncodingViewer.getControl());
//		if (!firstLine) {
//			baselineEncodingViewer.setSelection(new StructuredSelection(baselineEncodingList.get(0)), true);
//			baselineEncodingViewer.getControl().setEnabled(false);
//		}	
		
		rdText = new RowData();
		rdText.width = 100;
		
		new Label(composite, SWT.NONE).setText("Element");
		baselineEncodingElement = new Text(composite, SWT.SINGLE | SWT.BORDER);
		baselineEncodingElement.setLayoutData(rdText);
		new Label(composite, SWT.NONE).setText("=");
		if (!firstLine) {
			baselineEncodingElement.setEnabled(false);
		}	
	
		
		
		addText(composite,firstLine);
		addLabel(composite);
		addText(composite,false);
		addImageLabel(composite, true);
		new Label(composite, SWT.CANCEL);
	}
	
	private void addText(Composite composite, Boolean enabled) {
		final Text text = new Text(composite, SWT.SINGLE | SWT.BORDER);
		text.setLayoutData(rdText);
		text.setEnabled(enabled);
		if (!enabled) 
			text.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		else 
			text.setBackground(null);
		textList.add(text);
		
		text.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR) {
					IViewPart searchView = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.findView("info.textgrid.lab.search.views.SearchView"); //$NON-NLS-1$
					((SearchView)searchView).startAdvancedSearch();
					return;
				}	
				Text nextText = textList.get((textList.indexOf(text))+1);
				if (!nextText.isEnabled()) {
					// enable next text control and add further control
					nextText.setEnabled(true);
					nextText.setBackground(null);
					Boolean visible = deleteImage.isVisible();
					deleteImage.dispose();
					addLabel(nextText.getParent());
					addText(nextText.getParent(), false);
					addImageLabel(nextText.getParent(), visible);
					 
					BaselineEncodingMainAreaLine nextLine = baselineEncodingMainArea.getNextLine(BaselineEncodingMainAreaLine.this); 
//					if (nextLine != null) {
//						if (!nextLine.getComboViewer().getControl().isEnabled()) {
//							// enable controls of NextLine and add further line
//							nextLine.getComboViewer().getControl().setEnabled(true);
//							ArrayList<Text> nextLineTextArray = nextLine.getTextList(); 
//							nextLineTextArray.get(0).setEnabled(true);
//							nextLineTextArray.get(0).setBackground(null);
//						}
//					} else {
//						// add new line
//						BaselineEncodingMainAreaLine newLine = baselineEncodingMainArea.addLine();
//						newLine.getComboViewer().getControl().setEnabled(true);
//						ArrayList<Text> newLineTextArray = newLine.getTextList(); 
//						newLineTextArray.get(0).setEnabled(true);
//						newLineTextArray.get(0).setBackground(null);
//					}
					if (nextLine != null) {
						if (!nextLine.getElementText().isEnabled()) {
							// enable controls of NextLine and add further line
							nextLine.getElementText().setEnabled(true);
							ArrayList<Text> nextLineTextArray = nextLine.getTextList(); 
							nextLineTextArray.get(0).setEnabled(true);
							nextLineTextArray.get(0).setBackground(null);
						}
					} else {
						// add new line
						BaselineEncodingMainAreaLine newLine = baselineEncodingMainArea.addLine();
						newLine.getElementText().setEnabled(true);
						ArrayList<Text> newLineTextArray = newLine.getTextList(); 
						newLineTextArray.get(0).setEnabled(true);
						newLineTextArray.get(0).setBackground(null);
					}
					refreshMainComposite();
				}
					
			}
		});
	}
	
	private void addLabel(Composite composite) {
		new Label(composite, SWT.LEFT).setText(Messages.BaselineEncodingMainAreaLine_OrLabel);
	}
	
	private void addImageLabel(Composite composite, Boolean visible) {
		deleteImage = new Label(composite, SWT.LEFT);
		deleteImage.setImage(Activator.getDefault().getImageRegistry().get(Activator.ROUND_CLOSE_IMAGE_ID));
		deleteImage.setVisible(visible);
		deleteImage.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				baselineEncodingMainArea.removeLine(BaselineEncodingMainAreaLine.this);
			}

			@Override
			public void mouseUp(MouseEvent e) {
			}
			
		});
	}
	
//	public ComboViewer getComboViewer() {
//		return baselineEncodingViewer;
//	}
	
	public Text getElementText() {
		return baselineEncodingElement;
	}
	
	public ArrayList<Text> getTextList() {
		return textList;
	}
	
	public Composite getMainComposite() {
		return mainComposite;
	}
	
	private void refreshMainComposite() {
		mainComposite.getParent().layout();
		mainComposite.getParent().getParent().layout();
		mainComposite.getParent().getParent().getParent().layout();
		scrolledComposite.setMinSize(scrolledComposite.getContent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}
	
	public Label getDeleteImage() {
		return this.deleteImage;
	}
	
}
