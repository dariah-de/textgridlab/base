package info.textgrid.lab.search.ui.newsearch;

import info.textgrid.lab.core.model.ProjectFileException;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectFile;
import info.textgrid.lab.search.ui.Activator;
import info.textgrid.lab.templateeditor.config.ConfigCreator;
import info.textgrid.lab.templateeditor.config.ConfigCustomElement;
import info.textgrid.namespaces.metadata.projectfile._2008_11_27.TgProjectFile;

import java.util.ArrayList;

import org.apache.axiom.om.OMElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;


/**
 * Class to control the lines of the main metadata area
 * in the search view. The different lines are in an 
 * and-relationship with each other.
 * @author Frank Queens
 *
 */
public class MetadataMainArea {
	
	private final class RetieveProjectMetadata extends Job {
		
		private TgProjectFile projectFileData = null;
		private final TextGridProjectFile projectFile; 
		
		public RetieveProjectMetadata(String name, TextGridProject project) {
			super(name);
			metadataProject = project;
			TextGridProjectFile projectFile = new TextGridProjectFile(project);
			this.projectFile = projectFile;
			metadataElements = null;
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			try {
				projectFileData = projectFile
						.getProjectFileData(false, true, new NullProgressMonitor());
			} catch (ProjectFileException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						e.getMessage(), e);
				Activator.getDefault().getLog().log(status);
			}

			OMElement[] appDataChildren = TextGridProjectFile.extractAppDataChildren(projectFileData);

			OMElement metaSection = null;
			if (appDataChildren != null && appDataChildren.length > 0) {
				for (OMElement child : appDataChildren) {
					if (child.getQName().equals(
							TextGridProjectFile.metadataSectionQName)) {
						metaSection = child;
						break;
					}
				}
			}

			if (metaSection != null) {
				OMElement config = metaSection
						.getFirstChildWithName(ConfigCreator.templateConfQname);

				if (config != null) {
					metadataElements = ConfigCreator
							.getConfigCustomElements(config);
				}
			}
			return Status.OK_STATUS;
		}
	}
	
	private Composite comMainArea;
	private ScrolledComposite scrolledComposite;
	private ArrayList<MetadataMainAreaLine>  metadataMainAreaLines = new ArrayList<MetadataMainAreaLine>();
	private ArrayList<Label>  metadataMainAreaLabels = new ArrayList<Label>();
	public ArrayList<ConfigCustomElement> metadataElements = null;
	private TextGridProject metadataProject;
	
	public MetadataMainArea(Composite comMainAra, ScrolledComposite scrolledComposite) {
		this.comMainArea = comMainAra;
		this.scrolledComposite = scrolledComposite;
		metadataMainAreaLines.add(addControlLine(true));
		metadataMainAreaLabels.add(addLabelLine());
		metadataMainAreaLines.add(addControlLine(false));
		setRemoveIconsVisible(false);
	}

	private MetadataMainAreaLine addControlLine(Boolean firstLine) {
		Composite composite = new Composite(comMainArea, SWT.None);
		return new MetadataMainAreaLine(this, composite, scrolledComposite, firstLine);
	}
	
	private Label addLabelLine() {
		Composite composite = new Composite(comMainArea, SWT.NONE);
		RowLayout rl = new RowLayout();
		rl.marginLeft = 10;
		composite.setLayout(rl);
		Label label = new Label(composite, SWT.NONE);
		label.setText(Messages.MetadataMainArea_AndLabel);
		return label;
	}
	
	public MetadataMainAreaLine addLine() {
		if (metadataMainAreaLines.size() == 2)
			setRemoveIconsVisible(true);
		metadataMainAreaLabels.add(addLabelLine());
		MetadataMainAreaLine newLine = addControlLine(false);
		metadataMainAreaLines.add(newLine);
		return newLine;
	}
	
	public void removeLine(MetadataMainAreaLine line) {
		// two lines should exist
		if (metadataMainAreaLines.size() <= 2) 
			return;
		
		int index = metadataMainAreaLines.indexOf(line);
		metadataMainAreaLines.remove(line);
		line.getMainComposite().dispose();
		if (index != -1) {
			//remove label
			if (index <= metadataMainAreaLabels.size()-1) {
				Label label = metadataMainAreaLabels.get(index);
				label.getParent().dispose();
				metadataMainAreaLabels.remove(index);
			} else {
				Label label = metadataMainAreaLabels.get(metadataMainAreaLabels.size()-1);
				label.getParent().dispose();
				metadataMainAreaLabels.remove(metadataMainAreaLabels.size()-1);
			}
		}
		comMainArea.layout();
		scrolledComposite.setMinSize(scrolledComposite.getContent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		if (metadataMainAreaLines.size() == 2)
			setRemoveIconsVisible(false);
	}
 	
	public MetadataMainAreaLine getNextLine(MetadataMainAreaLine line) {
		int index = metadataMainAreaLines.indexOf(line);
		if (index != -1) {
			try {
				return metadataMainAreaLines.get(index+1);
			} catch (IndexOutOfBoundsException  e) {
				return null;
			}
		}
		return null;
	}
	
	public Boolean isFirstLine(MetadataMainAreaLine line) {
		int index = metadataMainAreaLines.indexOf(line);
		if (index == 1) {
			return true;
		}
		return false;
	}
	
	private void setRemoveIconsVisible(Boolean state) {
		for (MetadataMainAreaLine line : metadataMainAreaLines) {
			line.getDeleteImage().setVisible(state);
		}
	}
	
	public String getMetadataMainSearchString() {
		String metadataString = "";
		String comboString = "";
		boolean setAND = false;
		for (MetadataMainAreaLine metaLine : metadataMainAreaLines) {
			
			//get metadata field
			ISelection sel = metaLine.getComboViewer().getSelection();
			Object obj = ((IStructuredSelection) sel).getFirstElement();
			if (obj == null)
				continue;
			comboString = MetadataMainAreaLine.standardMetadataMap.get(obj);
			if (comboString == null) {
				// project specific search
				if (obj.toString().matches(".*\\[.*\\]")) {
					int end = obj.toString().indexOf("[");
					comboString = obj.toString().substring(0, obj.toString().indexOf("[")).trim();
					comboString = "cns|" + comboString;
				}
			}
			
			//get metadata search values
			boolean firstRun = true;
			for (Text text : metaLine.getTextList()) {
				if (!text.getText().trim().equals("")) {
					if (setAND && firstRun)
						metadataString = metadataString + "AND ";
					if (!firstRun)
						metadataString = metadataString + "OR ";
					metadataString = metadataString + comboString + ":\"" + text.getText().trim() + "\" ";
					firstRun = false;
				}
			}
			
			setAND = true;
		}
		return metadataString;
	}
	
	public void setProjectSpecificSearchTags(ArrayList<TextGridProject> projectList) {
		MetadataMainAreaLine.initDynamicLists();
		for (TextGridProject project : projectList) {
			RetieveProjectMetadata metadataJob = new RetieveProjectMetadata("Retrieve project metadata...", project);
			try {
				metadataJob.schedule();
				metadataJob.join();
				if (metadataElements != null) {
					for (ConfigCustomElement element : metadataElements) {
						MetadataMainAreaLine.addMetadataItem(element.getName(), element.getType(), project.getName());
					}
				}
			} catch (InterruptedException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						e.getMessage(), e);
				Activator.getDefault().getLog().log(status);
			}
		}
		
		for (MetadataMainAreaLine line : metadataMainAreaLines) {
			line.initMetadataViewer();
		}
	}
	
}
