package info.textgrid.lab.search.ui.newsearch;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.search.ui.newsearch.messages"; //$NON-NLS-1
	public static String BaselineEncodingMainArea_AndLabel;
	public static String BaselineEncodingMainAreaLine_OrLabel;
	public static String MajorSearchLanguages_AncientGreek;
	public static String MajorSearchLanguages_AncientHebrew;
	public static String MajorSearchLanguages_Arabic;
	public static String MajorSearchLanguages_Belarusian;
	public static String MajorSearchLanguages_Bulgarian;
	public static String MajorSearchLanguages_Catalan;
	public static String MajorSearchLanguages_Croatian;
	public static String MajorSearchLanguages_Czech;
	public static String MajorSearchLanguages_Dutch;
	public static String MajorSearchLanguages_English;
	public static String MajorSearchLanguages_French;
	public static String MajorSearchLanguages_German;
	public static String MajorSearchLanguages_Hebrew;
	public static String MajorSearchLanguages_Hungarian;
	public static String MajorSearchLanguages_Italian;
	public static String MajorSearchLanguages_Languages;
	public static String MajorSearchLanguages_Latin;
	public static String MajorSearchLanguages_Macedonian;
	public static String MajorSearchLanguages_MiddleDutch;
	public static String MajorSearchLanguages_MiddleEnglish;
	public static String MajorSearchLanguages_MiddleFrench;
	public static String MajorSearchLanguages_MiddleHighGerman;
	public static String MajorSearchLanguages_MiddleLowGerman;
	public static String MajorSearchLanguages_ModernGreek;
	public static String MajorSearchLanguages_OldDutch;
	public static String MajorSearchLanguages_OldEnglish;
	public static String MajorSearchLanguages_OldFrench;
	public static String MajorSearchLanguages_OldHighGerman;
	public static String MajorSearchLanguages_OldHungarian;
	public static String MajorSearchLanguages_OldRussian;
	public static String MajorSearchLanguages_OldSpanish;
	public static String MajorSearchLanguages_Polish;
	public static String MajorSearchLanguages_Portuguese;
	public static String MajorSearchLanguages_Romanian;
	public static String MajorSearchLanguages_Russian;
	public static String MajorSearchLanguages_Serbian;
	public static String MajorSearchLanguages_Slovak;
	public static String MajorSearchLanguages_Spanish;
	public static String MajorSearchLanguages_Swedish;
	public static String MajorSearchLanguages_Ukrainian;
	public static String MetadataMainArea_AndLabel;
	public static String MetadataMainAreaLine_DataContributor;
	public static String MetadataMainAreaLine_Identifier;
	public static String MetadataMainAreaLine_Notes;
	public static String MetadataMainAreaLine_OrLabel;
	public static String MetadataMainAreaLine_Project;
	public static String MetadataMainAreaLine_RightsHolder;
	public static String MetadataMainAreaLine_TextGridURI;
	public static String MetadataMainAreaLine_Agent;
	public static String MetadataMainAreaLine_Title;
	public static String SearchContentType_ContentTypes;
	public static String SearchTextType_Drama;
	public static String SearchTextType_NonFiction;
	public static String SearchTextType_NonText;
	public static String SearchTextType_Other;
	public static String SearchTextType_Prose;
	public static String SearchTextType_ReferenceWork;
	public static String SearchTextType_TextTypes;
	public static String SearchTextType_Verse;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
