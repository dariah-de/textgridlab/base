package info.textgrid.lab.search.ui.newsearch;

import java.util.ArrayList;

public class DemoLanguage {
	private String language;
	private ArrayList<DemoLanguage> children = new ArrayList<DemoLanguage>();
	
	public DemoLanguage(String language) {
		this.language = language;
	}
	
	public String toString() {
		return this.language;
	}
	
	public void add(DemoLanguage child) {
		children.add(child);
	}
	
	public DemoLanguage[] getChildren() {
		return children.toArray(new DemoLanguage[0]);
	}
}
