package info.textgrid.lab.search.ui.newsearch;

import info.textgrid.lab.search.ui.Activator;

import java.util.ArrayList;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

/**
 * Represents a line of the main fulltext area.
 * Such a line is composed of two text controls, 
 * one ComboViewer to select a relational operator
 * and spinner to enter the word distance.
 * @author Frank Queens
 *
 */


public class FulltextMainAreaLine {
	
	private FulltextMainArea fulltextMainArea;
	private Text text1;
	private Text text2;
	private ComboViewer comboViewer;
	private Spinner spinner;
	private Label deleteImage;
	
	private Composite mainComposite;
	private ScrolledComposite scrolledComposite;
	
	
	private final static ArrayList<String> operatorList = new ArrayList<String>(){
		private static final long serialVersionUID = 1L;

		{
            add(">");
            add(">=");
            add("<");
            add("<=");
            add("=");
        }
    };

	FulltextMainAreaLine(FulltextMainArea fulltextMainArea, Composite composite, ScrolledComposite scrolledComposite, Boolean firstLine) {
		this.fulltextMainArea = fulltextMainArea;
		this.mainComposite = composite;
		this.scrolledComposite = scrolledComposite;
		GridLayout gl = new GridLayout(4, false);
		composite.setLayout(gl);
		addLine();		
	}
	
	private void addLine() {
		//create composites
		Composite c1 = new Composite(mainComposite, SWT.NULL);
		GridLayout gl1 = new GridLayout(1, false);
		gl1.marginTop = 3;
		c1.setLayout(gl1);
		GridData gd1 = new GridData(GridData.FILL, GridData.FILL, false, false);
		c1.setLayoutData(gd1);
		
		Composite c2 = new Composite(mainComposite, SWT.NULL);
		GridLayout gl2 = new GridLayout(1, false);
		c2.setLayout(gl2);
		Composite c21 = new Composite(c2, SWT.NULL);
		c21.setLayout(new FillLayout());
		c21.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		Composite c22 = new Composite(c2, SWT.NULL);
		c22.setLayout(new RowLayout());
		
		Composite c3 = new Composite(mainComposite, SWT.NULL);
		GridLayout gl3 = new GridLayout(1, false);
		gl3.marginLeft = 0;
		c3.setLayout(gl3);
		GridData gd3 = new GridData(GridData.BEGINNING, GridData.BEGINNING, true, true);
		c3.setLayoutData(gd3);
		Composite c31 = new Composite(c3, SWT.NULL);
		GridLayout gl31 = new GridLayout(2, false);
		gl31.marginHeight = 0; 
		c31.setLayout(gl31);
		Composite c32 = new Composite(c3, SWT.NULL);
		c32.setLayout(new GridLayout(1, false));
		c32.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		
		Composite c4 = new Composite(mainComposite, SWT.NULL);
		c4.setLayout(new FillLayout());
		
		RowData rdCombo = new RowData();
		rdCombo.width = 55;
		
		RowData rdSpinner = new RowData();
		rdSpinner.width = 50;
		
		// create controls
		Label labelBetween = new Label(c1, SWT.FILL);
		labelBetween.setText("Between");
		
		text1 = new Text(c21, SWT.SINGLE | SWT.FILL | SWT.BORDER);
		comboViewer = new ComboViewer(c22);
		comboViewer.setContentProvider(new ArrayContentProvider());
		comboViewer.setLabelProvider(new LabelProvider());
		comboViewer.setInput(operatorList);
		comboViewer.getControl().setLayoutData(rdCombo);
		comboViewer.setSelection(new StructuredSelection(operatorList.get(1)), true);
		spinner = new Spinner(c22, SWT.NONE);
		spinner.setValues(5, 1, 1000, 0, 1, 1);
		spinner.setLayoutData(rdSpinner);
		
		Label labelAnd = new Label(c31, SWT.NONE);
		labelAnd.setText("and     ");
		text2 = new Text(c31, SWT.SINGLE | SWT.FILL | SWT.BORDER);
		GridData gdText2 = new GridData(GridData.FILL, GridData.FILL, false, false);
		gdText2.widthHint = 115;
		text2.setLayoutData(gdText2);
		Label labelWords = new Label(c32, SWT.NONE);
		labelWords.setText("Words");

		addImageLabel(c4, true);
		
		addTextListener(text1);
		addTextListener(text2);
	}
	
	private void addTextListener(Text text) {
		text.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
					 
					FulltextMainAreaLine nextLine = fulltextMainArea.getNextLine(FulltextMainAreaLine.this); 
					if (nextLine == null) {
						// add new line
						fulltextMainArea.addLine();
					}
					refreshMainComposite();
			}
		});
	}
	
	private void addImageLabel(Composite composite, Boolean visible) {
		deleteImage = new Label(composite, SWT.LEFT);
		deleteImage.setImage(Activator.getDefault().getImageRegistry().get(Activator.ROUND_CLOSE_IMAGE_ID));
		deleteImage.setVisible(visible);
		deleteImage.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				fulltextMainArea.removeLine(FulltextMainAreaLine.this);
			}

			@Override
			public void mouseUp(MouseEvent e) {
			}
			
		});
	}
	
	
	public Composite getMainComposite() {
		return mainComposite;
	}
	
	private void refreshMainComposite() {
		mainComposite.getParent().layout();
		mainComposite.getParent().getParent().layout();
		mainComposite.getParent().getParent().getParent().layout();
		scrolledComposite.setMinSize(scrolledComposite.getContent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}
	
	public Label getDeleteImage() {
		return this.deleteImage;
	}
	
}
