package info.textgrid.lab.search.ui.newsearch;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;


/**
 * Class to control the lines of the main baseline
 * encoding area in the search view. The different 
 * lines are in an and-relationship with each other.
 * @author Frank Queens
 *
 */
public class BaselineEncodingMainArea {
	
	private Composite comMainArea;
	private ScrolledComposite scrolledComposite;
	private ArrayList<BaselineEncodingMainAreaLine>  baselineEncodingMainAreaLines = new ArrayList<BaselineEncodingMainAreaLine>();
	private ArrayList<Label>  baselineEncodingMainAreaLabels = new ArrayList<Label>();
	
	public BaselineEncodingMainArea(Composite comMainAra, ScrolledComposite scrolledComposite) {
		this.comMainArea = comMainAra;
		this.scrolledComposite = scrolledComposite;
		baselineEncodingMainAreaLines.add(addControlLine(true));
		baselineEncodingMainAreaLabels.add(addLabelLine());
		baselineEncodingMainAreaLines.add(addControlLine(false));
		setRemoveIconsVisible(false);
	}

	private BaselineEncodingMainAreaLine addControlLine(Boolean firstLine) {
		Composite composite = new Composite(comMainArea, SWT.None);
		return new BaselineEncodingMainAreaLine(this, composite, scrolledComposite, firstLine);
	}
	
	private Label addLabelLine() {
		Composite composite = new Composite(comMainArea, SWT.NONE);
		RowLayout rl = new RowLayout();
		rl.marginLeft = 10;
		composite.setLayout(rl);
		Label label = new Label(composite, SWT.NONE);
		label.setText(Messages.BaselineEncodingMainArea_AndLabel);
		return label;
	}
	
	public BaselineEncodingMainAreaLine addLine() {
		if (baselineEncodingMainAreaLines.size() == 2)
			setRemoveIconsVisible(true);
		baselineEncodingMainAreaLabels.add(addLabelLine());
		BaselineEncodingMainAreaLine newLine = addControlLine(false);
		baselineEncodingMainAreaLines.add(newLine);
		return newLine;
	}
	
	public void removeLine(BaselineEncodingMainAreaLine line) {
		// two lines should exist
		if (baselineEncodingMainAreaLines.size() <= 2) 
			return;
		
		int index = baselineEncodingMainAreaLines.indexOf(line);
		baselineEncodingMainAreaLines.remove(line);
		line.getMainComposite().dispose();
		if (index != -1) {
			//remove label
			if (index <= baselineEncodingMainAreaLabels.size()-1) {
				Label label = baselineEncodingMainAreaLabels.get(index);
				label.getParent().dispose();
				baselineEncodingMainAreaLabels.remove(index);
			} else {
				Label label = baselineEncodingMainAreaLabels.get(baselineEncodingMainAreaLabels.size()-1);
				label.getParent().dispose();
				baselineEncodingMainAreaLabels.remove(baselineEncodingMainAreaLabels.size()-1);
			}
		}
		comMainArea.layout();
		scrolledComposite.setMinSize(scrolledComposite.getContent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		if (baselineEncodingMainAreaLines.size() == 2)
			setRemoveIconsVisible(false);
	}
 	
	public BaselineEncodingMainAreaLine getNextLine(BaselineEncodingMainAreaLine line) {
		int index = baselineEncodingMainAreaLines.indexOf(line);
		if (index != -1) {
			try {
				return baselineEncodingMainAreaLines.get(index+1);
			} catch (IndexOutOfBoundsException  e) {
				return null;
			}
		}
		return null;
	}
	
	public Boolean isFirstLine(BaselineEncodingMainAreaLine line) {
		int index = baselineEncodingMainAreaLines.indexOf(line);
		if (index == 1) {
			return true;
		}
		return false;
	}
	
	private void setRemoveIconsVisible(Boolean state) {
		for (BaselineEncodingMainAreaLine line : baselineEncodingMainAreaLines) {
			line.getDeleteImage().setVisible(state);
		}
	}
	
	public String getBaselineEncodingMainSearchString() {
		String metadataString = "";
		String elementString = "";
		boolean setAND = false;
		for (BaselineEncodingMainAreaLine line : baselineEncodingMainAreaLines) {
			
			//get element field
			elementString = line.getElementText().getText().trim().toLowerCase();
			if (elementString.equals(""))
				continue;
			
			//get baseline encoding search values
			boolean firstRun = true;
			for (Text text : line.getTextList()) {
				if (!text.getText().trim().equals("")) {
					if (setAND && firstRun)
						metadataString = metadataString + "AND ";
					if (!firstRun)
						metadataString = metadataString + "OR ";
					metadataString = metadataString + elementString + ":\"" + text.getText().trim() + "\" ";
					firstRun = false;
				}
			}
			
			setAND = true;
		}
		return metadataString;
	}
	
	
}
