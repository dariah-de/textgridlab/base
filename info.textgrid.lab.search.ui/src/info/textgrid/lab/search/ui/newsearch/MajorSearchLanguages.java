package info.textgrid.lab.search.ui.newsearch;

import java.util.ArrayList;

public class MajorSearchLanguages {
	
	private MajorSearchLanguages root;
	private String language;
	private String code;
	private ArrayList<MajorSearchLanguages> children = new ArrayList<MajorSearchLanguages>();
	
	public MajorSearchLanguages(String language, String code, boolean init) {
		this.language = language;
		this.code = code;
		if (init) {
			this.root = this;
			init();
		}
	}
	
	public String toString() {
		return this.language;
	}
	
	public void add(MajorSearchLanguages child) {
		children.add(child);
	}
	
	public MajorSearchLanguages[] getChildren() {
		return children.toArray(new MajorSearchLanguages[0]);
	}
	
	public String getCode() {
		return this.code;
	}
	
	public int getChildCount() {
		return root.getChildren()[0].getChildren().length +1;
	}
	
	private void init() {
		MajorSearchLanguages languages = new MajorSearchLanguages(Messages.MajorSearchLanguages_Languages, "", false); 
		root.add(languages);
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_AncientGreek, "grc", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Bulgarian, "bul", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_German, "deu", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_MiddleHighGerman, "gmh", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_OldHighGerman, "goh", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_MiddleLowGerman, "gml", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_English, "eng", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_OldEnglish, "ang", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_MiddleEnglish, "enm", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_French, "fra", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_MiddleFrench, "frm", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_OldFrench, "fro", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Italian, "ita", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Catalan, "cat", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Croatian, "hrv", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Latin, "lat", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Macedonian, "mkd", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_ModernGreek, "ell", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Dutch, "nld", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_OldDutch, "odt", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_MiddleDutch, "dum", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Polish, "pol", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Portuguese, "por", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Romanian, "ron", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Russian, "rus", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_OldRussian, "orv", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Swedish, "swe", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Serbian, "srp", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Slovak, "slk", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Spanish, "spa", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_OldSpanish, "osp", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Czech, "ces", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Ukrainian, "ukr", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Hungarian, "hun", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_OldHungarian, "ohu", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Belarusian, "bel", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_AncientHebrew, "hbo", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Hebrew, "heb", false)); //$NON-NLS-2$
		languages.add(new MajorSearchLanguages(Messages.MajorSearchLanguages_Arabic, "ara", false)); //$NON-NLS-2$
	}

}
