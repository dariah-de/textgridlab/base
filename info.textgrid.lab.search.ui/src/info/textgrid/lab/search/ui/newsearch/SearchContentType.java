package info.textgrid.lab.search.ui.newsearch;

import info.textgrid.lab.core.model.TGContentType;

import java.util.ArrayList;

public class SearchContentType {
	private String name;
	private String format;
	private ArrayList<SearchContentType> children = new ArrayList<SearchContentType>();
	private static SearchContentType instance;
	
	private SearchContentType(String name, String format, boolean isRoot, boolean init) {
		this.name = name;
		this.format = format;
		if (isRoot) {
			add(new SearchContentType(Messages.SearchContentType_ContentTypes, "", false, true));
		} else if (init) {
			for (TGContentType ct : TGContentType.getContentTypes(false)) {
				add(new SearchContentType(ct.getDescription(), ct.getId(), false, false)); 
			}
		}
	}
	
	public synchronized static SearchContentType getInstance() {
        if (instance == null) {
            instance = new SearchContentType("Root", "", true, true); //$NON-NLS-1$
        }
        return instance;
    }
	
	public String toString() {
		String format = "";
		if (!this.format.equals("")) {
			format = " (" + this.format + ")";
		}
		return this.name + format;
	}
	
	public String getFormat() {
		return this.format;
	}
	
	public void add(SearchContentType child) {
		children.add(child);
	}
	
	public int getChildCount() {
		return this.getChildren()[0].getChildren().length +1;
	}
	
	public SearchContentType[] getChildren() {
		return children.toArray(new SearchContentType[0]);
	}
}
