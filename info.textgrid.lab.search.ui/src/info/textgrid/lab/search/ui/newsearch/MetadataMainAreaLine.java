package info.textgrid.lab.search.ui.newsearch;

import info.textgrid.lab.search.ui.Activator;
import info.textgrid.lab.search.ui.views.SearchView;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PlatformUI;

/**
 * Represents a line of the main metadata area.
 * Such a line is composed of one ComboViewer to
 * select a parameter and an optional amount of 
 * text controls. These text controls are in an 
 * or-relationship with each other.
 * @author Frank Queens
 *
 */


public class MetadataMainAreaLine {
	
	private MetadataMainArea metaDataMainArea;
	private ComboViewer metadataViewer;
	private ArrayList<Text> textList = new ArrayList<Text>();  
	private RowData rdText;
	private Composite mainComposite;
	private ScrolledComposite scrolledComposite;
	private Label deleteImage;
	
	private final static ArrayList<String> standardMetadataList = new ArrayList<String>(){
		private static final long serialVersionUID = 1L;

		{
            add(Messages.MetadataMainAreaLine_DataContributor);
            add(Messages.MetadataMainAreaLine_Identifier);
            add(Messages.MetadataMainAreaLine_Notes);
            add(Messages.MetadataMainAreaLine_Project);
            add(Messages.MetadataMainAreaLine_RightsHolder);
            add(Messages.MetadataMainAreaLine_TextGridURI);
            add(Messages.MetadataMainAreaLine_Agent);
            add(Messages.MetadataMainAreaLine_Title);
        }
    };
    
    public final static HashMap<String, String> standardMetadataMap = new HashMap<String, String>();
    {
    	standardMetadataMap.put(Messages.MetadataMainAreaLine_DataContributor, "dataContributor"); //$NON-NLS-2$
    	standardMetadataMap.put(Messages.MetadataMainAreaLine_Identifier, "identifier"); //$NON-NLS-2$
		standardMetadataMap.put(Messages.MetadataMainAreaLine_Notes, "notes"); //$NON-NLS-2$
		standardMetadataMap.put(Messages.MetadataMainAreaLine_Project, "project.value"); //$NON-NLS-2$
		standardMetadataMap.put(Messages.MetadataMainAreaLine_RightsHolder, "rightsHolder"); //$NON-NLS-2$
		standardMetadataMap.put(Messages.MetadataMainAreaLine_TextGridURI, "textgridUri"); //$NON-NLS-2$
		standardMetadataMap.put(Messages.MetadataMainAreaLine_Agent, "edition.agent.value"); //$NON-NLS-2$
		standardMetadataMap.put(Messages.MetadataMainAreaLine_Title, "title"); //$NON-NLS-2$
    }
    
    
	private static ArrayList<String> dynamicMetadataList = new ArrayList<String>();
    public static HashMap<String, String> dynamicMetadataMap = new HashMap<String, String>();
    

	MetadataMainAreaLine(MetadataMainArea metaDataMainArea, Composite composite, ScrolledComposite scrolledComposite, Boolean firstLine) {
		this.metaDataMainArea = metaDataMainArea;
		this.mainComposite = composite;
		this.scrolledComposite = scrolledComposite;
		RowLayout rl = new RowLayout();
		rl.wrap = false;
		initDynamicLists();

		composite.setLayout(rl);
		metadataViewer = new ComboViewer(composite);
		metadataViewer.setContentProvider(new ArrayContentProvider());
		metadataViewer.setLabelProvider(new LabelProvider());
		metadataViewer.setInput(standardMetadataList);
		//solved TG-1633
		if (firstLine) {
			//default (first)-item is "title"
			metadataViewer.getCombo().select(standardMetadataMap.size() - 1);
		} else {
			//default (second)-item is "rightsHolder"
			metadataViewer.getCombo().select(standardMetadataMap.size() - 3);
		}

		GridDataFactory.defaultsFor(metadataViewer.getControl());
		if (!firstLine) {
			//metadataViewer.setSelection(new StructuredSelection(metadataList.get(0)), true);
			metadataViewer.getControl().setEnabled(false);
		}	
	
		rdText = new RowData();
		rdText.width = 100;
		
		addText(composite,firstLine);
		addLabel(composite);
		addText(composite,false);
		addImageLabel(composite, true);
		new Label(composite, SWT.CANCEL);
	}
	
	private void addText(Composite composite, Boolean enabled) {
		final Text text = new Text(composite, SWT.SINGLE | SWT.BORDER);
		text.setLayoutData(rdText);
		text.setEnabled(enabled);
		if (!enabled) 
			text.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		else 
			text.setBackground(null);
		textList.add(text);
		
		text.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR) {
					IViewPart searchView = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.findView("info.textgrid.lab.search.views.SearchView"); //$NON-NLS-1$
					((SearchView)searchView).startAdvancedSearch();
					return;
				}	
				Text nextText = textList.get((textList.indexOf(text))+1);
				if (!nextText.isEnabled()) {
					// enable next text control and add further control
					nextText.setEnabled(true);
					nextText.setBackground(null);
					Boolean visible = deleteImage.isVisible();
					deleteImage.dispose();
					addLabel(nextText.getParent());
					addText(nextText.getParent(), false);
					addImageLabel(nextText.getParent(), visible);
					 
					MetadataMainAreaLine nextLine = metaDataMainArea.getNextLine(MetadataMainAreaLine.this); 
					if (nextLine != null) {
						if (!nextLine.getComboViewer().getControl().isEnabled()) {
							// enable controls of NextLine and add further line
							nextLine.getComboViewer().getControl().setEnabled(true);
							ArrayList<Text> nextLineTextArray = nextLine.getTextList(); 
							nextLineTextArray.get(0).setEnabled(true);
							nextLineTextArray.get(0).setBackground(null);
						}
					} else {
						// add new line
						MetadataMainAreaLine newLine = metaDataMainArea.addLine();
						newLine.getComboViewer().getControl().setEnabled(true);
						ArrayList<Text> newLineTextArray = newLine.getTextList(); 
						newLineTextArray.get(0).setEnabled(true);
						newLineTextArray.get(0).setBackground(null);
					}
					refreshMainComposite();
				}
					
			}
		});
	}
	
	private void addLabel(Composite composite) {
		new Label(composite, SWT.LEFT).setText(Messages.MetadataMainAreaLine_OrLabel);
	}
	
	private void addImageLabel(Composite composite, Boolean visible) {
		deleteImage = new Label(composite, SWT.LEFT);
		deleteImage.setImage(Activator.getDefault().getImageRegistry().get(Activator.ROUND_CLOSE_IMAGE_ID));
		deleteImage.setVisible(visible);
		deleteImage.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				metaDataMainArea.removeLine(MetadataMainAreaLine.this);
			}

			@Override
			public void mouseUp(MouseEvent e) {
			}
			
		});
	}
	
	public ComboViewer getComboViewer() {
		return metadataViewer;
	}
	
	public ArrayList<Text> getTextList() {
		return textList;
	}
	
	public Composite getMainComposite() {
		return mainComposite;
	}
	
	private void refreshMainComposite() {
		mainComposite.getParent().layout();
		mainComposite.getParent().getParent().layout();
		mainComposite.getParent().getParent().getParent().layout();
		scrolledComposite.setMinSize(scrolledComposite.getContent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}
	
	public Label getDeleteImage() {
		return this.deleteImage;
	}
	
	public static void initDynamicLists() {
		dynamicMetadataList.clear();
		dynamicMetadataMap.clear();
		for (String element : standardMetadataList) {
			dynamicMetadataList.add(element);
			dynamicMetadataMap.put(element, standardMetadataMap.get(element));
		}
	}
	
	public void initMetadataViewer() {
		metadataViewer.setInput(dynamicMetadataList);
		//solved TG-1633
		if (!dynamicMetadataList.isEmpty())
			metadataViewer.getCombo().select(dynamicMetadataList.size() - 1);
	}
 	
	public static void addMetadataItem(String name, String type, String project) {
		String key = name + " [" + project + "]";
		dynamicMetadataList.add(key);
		dynamicMetadataMap.put(key, name);
	}
}
