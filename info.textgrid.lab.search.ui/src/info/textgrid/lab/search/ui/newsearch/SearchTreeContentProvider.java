package info.textgrid.lab.search.ui.newsearch;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class SearchTreeContentProvider implements ITreeContentProvider{

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return (Object[]) inputElement;
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof MajorSearchLanguages)
			return (Object[]) ((MajorSearchLanguages) parentElement).getChildren();
		else if (parentElement instanceof SearchContentType)
			return (Object[]) ((SearchContentType) parentElement).getChildren();
		else if (parentElement instanceof SearchTextType)
			return (Object[]) ((SearchTextType) parentElement).getChildren();
		return null;
	}

	@Override
	public Object getParent(Object element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof MajorSearchLanguages) {
			if (((MajorSearchLanguages) element).getChildren().length > 0)
				return true;
		} else if (element instanceof SearchContentType) {
			if (((SearchContentType) element).getChildren().length > 0)
				return true;
		} else if (element instanceof SearchTextType) {
			if (((SearchTextType) element).getChildren().length > 0)
				return true;
		}	
		return false;
	}

}
