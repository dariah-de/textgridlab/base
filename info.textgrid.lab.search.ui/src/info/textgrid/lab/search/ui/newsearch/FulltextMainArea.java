package info.textgrid.lab.search.ui.newsearch;

import java.util.ArrayList;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Label;


/**
 * Class to control the lines of the main fulltext area
 * in the search view. The different lines are in an 
 * and-relationship with each other.
 * @author Frank Queens
 *
 */
public class FulltextMainArea {
	
	private Composite comMainArea;
	private ScrolledComposite scrolledComposite;
	private ArrayList<FulltextMainAreaLine>  fulltextMainAreaLines = new ArrayList<FulltextMainAreaLine>();
	
	public FulltextMainArea(Composite comMainAra, ScrolledComposite scrolledComposite) {
		this.comMainArea = comMainAra;
		this.scrolledComposite = scrolledComposite;
		fulltextMainAreaLines.add(addControlLine(true));
		fulltextMainAreaLines.add(addControlLine(false));
		setRemoveIconsVisible(false);
	}

	private FulltextMainAreaLine addControlLine(Boolean firstLine) {
		Composite composite = new Composite(comMainArea, SWT.NULL);
		return new FulltextMainAreaLine(this, composite, scrolledComposite, firstLine);
	}
	
	public FulltextMainAreaLine addLine() {
		if (fulltextMainAreaLines.size() == 2)
			setRemoveIconsVisible(true);
		FulltextMainAreaLine newLine = addControlLine(false);
		fulltextMainAreaLines.add(newLine);
		return newLine;
	}
	
	public void removeLine(FulltextMainAreaLine line) {
		// two lines should exist
		if (fulltextMainAreaLines.size() <= 2) 
			return;
		
		fulltextMainAreaLines.remove(line);
		line.getMainComposite().dispose();
		comMainArea.layout();
		scrolledComposite.setMinSize(scrolledComposite.getContent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		if (fulltextMainAreaLines.size() == 2)
			setRemoveIconsVisible(false);
	}
 	
	public FulltextMainAreaLine getNextLine(FulltextMainAreaLine line) {
		int index = fulltextMainAreaLines.indexOf(line);
		if (index != -1) {
			try {
				return fulltextMainAreaLines.get(index+1);
			} catch (IndexOutOfBoundsException  e) {
				return null;
			}
		}
		return null;
	}
	
	
	private void setRemoveIconsVisible(Boolean state) {
		for (FulltextMainAreaLine line : fulltextMainAreaLines) {
			line.getDeleteImage().setVisible(state);
		}
	}
	
	
}
