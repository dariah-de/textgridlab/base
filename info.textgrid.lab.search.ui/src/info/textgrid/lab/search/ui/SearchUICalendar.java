package info.textgrid.lab.search.ui;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

/**
 * Creates a small calendar dialog with DateTime object to select
 * a valid date.
 * 
 * @author Frank Queens
 *
 */

public class SearchUICalendar {
	
	Shell dialog;
	DateTime calendar;
	Button ok;
	
	public SearchUICalendar(final Text textDate) {
		dialog = new Shell (PlatformUI.getWorkbench().
				getActiveWorkbenchWindow().getShell(), SWT.DIALOG_TRIM);
		dialog.setLayout (new GridLayout(1, false));
		calendar = new DateTime (dialog, SWT.CALENDAR | SWT.BORDER);
		initCalendar(textDate.getText());
		ok = new Button(dialog, SWT.PUSH);
		ok.setText("OK");
		
		ok.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseDown(MouseEvent e) {
				int iMonth;
				String sMonth, sDay;
				iMonth = (calendar.getMonth() + 1);
				if (iMonth < 10)
					sMonth = "0" + String.valueOf(iMonth);
				else 
					sMonth = String.valueOf(iMonth);;
				
				if ((calendar.getDay()) < 10)
					sDay = "0" + calendar.getDay();
				else 
					sDay = String.valueOf(calendar.getDay());
				
				textDate.setText(calendar.getYear() + "-" + sMonth  + "-" + sDay);
				dialog.close ();	
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		Point point = PlatformUI.getWorkbench().getDisplay().getCursorLocation();
		dialog.setLocation(point.x, point.y + 10);
		dialog.pack ();
		dialog.open();
	}
	
	private void initCalendar(String str_date) {
        DateFormat formatter ;
        Date date;
        GregorianCalendar gCalendar;
        try {
        	formatter = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$
	        date = (Date)formatter.parse(str_date); 
	        gCalendar = new GregorianCalendar();
	        gCalendar.setTime(date);
	        calendar.setDate(gCalendar.get(GregorianCalendar.YEAR), 
	        				 gCalendar.get(GregorianCalendar.MONTH),
	        				 gCalendar.get(GregorianCalendar.DAY_OF_MONTH));
	    } catch (ParseException e) {
	    	IStatus status = new Status(IStatus.WARNING, Activator.PLUGIN_ID, 
	    		    Messages.SearchUICalendar_ErrorMessage1, e);
	    		  Activator.getDefault().getLog().log(status);
	    }    
	}
}
