package info.textgrid.lab.search.ui;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.search.ui"; //$NON-NLS-1$
	
	public static final String ARROW_GREY_RIGHT_IMAGE_ID = "arrowgreyright"; //$NON-NLS-1$
	public static final String ARROW_GREY_DOWN_IMAGE_ID = "arrowgreydown"; //$NON-NLS-1$
	public static final String ARROW_BLACK_RIGHT_IMAGE_ID = "arrowblackright"; //$NON-NLS-1$
	public static final String ARROW_BLACK_DOWN_IMAGE_ID = "arrowblackdown"; //$NON-NLS-1$
	public static final String CALENDAR_IMAGE_ID = "calendar"; //$NON-NLS-1$
	public static final String ROUND_CLOSE_IMAGE_ID = "roundclose"; //$NON-NLS-1$
	public static final String DECORATOR_PUBLIC_ID = "public"; //$NON-NLS-1$
	public static final String PROJECT_IMAGE = "project"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/** Returns true if we want to use the lazy search results feature */
	public boolean useLazySearch() {
		return info.textgrid.lab.search.Activator.isDebugging(info.textgrid.lab.search.Activator.DEBUG_LAZY);
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	protected void initializeImageRegistry(ImageRegistry registry) {
		registerImage(registry, ARROW_GREY_RIGHT_IMAGE_ID, "icons/arrow_state_grey_right.png"); //$NON-NLS-1$
		registerImage(registry, ARROW_GREY_DOWN_IMAGE_ID, "icons/arrow_state_grey_down.png"); //$NON-NLS-1$
		registerImage(registry, ARROW_BLACK_RIGHT_IMAGE_ID, "icons/arrow_state_black_right.png"); //$NON-NLS-1$
		registerImage(registry, ARROW_BLACK_DOWN_IMAGE_ID, "icons/arrow_state_black_down.png"); //$NON-NLS-1$
		registerImage(registry, CALENDAR_IMAGE_ID, "icons/calendar.png"); //$NON-NLS-1$
		registerImage(registry, ROUND_CLOSE_IMAGE_ID, "icons/round_close.png"); //$NON-NLS-1$
		registerImage(registry, DECORATOR_PUBLIC_ID, "icons/public.gif"); //$NON-NLS-1$
		registerImage(registry, PROJECT_IMAGE, "icons/prj_obj.gif"); //$NON-NLS-1$
    }
	
	/**
	 * Registers the image in the image registry.
	 * 
	 * @param registry
	 *            the image registry to use
	 * @param key
	 *            the key by which the image will be adressable
	 * @param fullPath
	 *            the full path to the image, relative to the plugin directory.
	 */
	private void registerImage(ImageRegistry registry, String key, String fullPath) {
		URL imageURL = FileLocator.find(getBundle(), new Path(fullPath), null);
		registry.put(key, ImageDescriptor.createFromURL(imageURL));
	}
}
