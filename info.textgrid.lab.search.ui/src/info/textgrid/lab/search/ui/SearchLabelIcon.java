package info.textgrid.lab.search.ui;

import info.textgrid.lab.search.ui.dialogs.RegardMetadataDialog;
import info.textgrid.lab.search.ui.views.SearchView;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.PlatformUI;


/**
 * The class provides a component to control the open and hide
 * of dedicated controls. The composite which contains the 
 * subordinated controls can be set by the <i> setControl </i>
 * function.
 *
 * @author Frank Queens
 *
 */
public class SearchLabelIcon {
	
	private Label iconLabel;
	private Label textLabel;
	private Label modificationLabel;
	private boolean isModified;
	private Composite container;
	private Composite control;
	private Boolean open = false;
	private Cursor cursorHand = new Cursor(PlatformUI.getWorkbench().getDisplay(), SWT.CURSOR_HAND);
	private Button checkButton = null;
	
	
	public SearchLabelIcon(Composite composite, int style, String text,
							final Composite mainComposite, boolean checkable, int specials) {
		container = new Composite(composite, style);
		GridLayout gl = new GridLayout(4, false);
		gl.horizontalSpacing = 0;
		container.setLayout(gl);
		GridData gd = new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false);
		container.setLayoutData(gd);
		iconLabel = new Label(container, SWT.LEFT);
		iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_BLACK_RIGHT_IMAGE_ID));
		
		if (checkable) 
			checkButton = new Button(container, SWT.CHECK);
		
		textLabel = new Label(container, SWT.LEFT);
		textLabel.setText("  " + text);
		
		switch (specials) {
			case 1 : createProjectSelectButton();
		}
		
		iconLabel.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				if (isOpen()) {
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_GREY_RIGHT_IMAGE_ID));
					if (getControl() != null) {
						((GridData) getControl().getLayoutData()).exclude = true;
						getControl().setVisible(false);
					}	
					setOpen(false);
				} else {
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_GREY_DOWN_IMAGE_ID));
					if (getControl() != null) {
						((GridData) getControl().getLayoutData()).exclude = false;
						getControl().setVisible(true);
					}	
					setOpen(true);
				}
				mainComposite.layout();
//				((ScrolledComposite)mainComposite.getParent().getParent()).setMinSize(mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				((ScrolledComposite)mainComposite.getParent().getParent()).setMinHeight(mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y+10);
			}

			@Override
			public void mouseUp(MouseEvent e) {
			}
			
		});
		
		textLabel.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				if (isOpen()) {
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_GREY_RIGHT_IMAGE_ID));
					if (getControl() != null) {
						((GridData) getControl().getLayoutData()).exclude = true;
						getControl().setVisible(false);
					}	
					setOpen(false);
				} else {
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_GREY_DOWN_IMAGE_ID));
					if (getControl() != null) {
						((GridData) getControl().getLayoutData()).exclude = false;
						getControl().setVisible(true);
					}	
					setOpen(true);
				}	
				mainComposite.layout();
//				((ScrolledComposite)mainComposite.getParent().getParent()).setMinSize(mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				((ScrolledComposite)mainComposite.getParent().getParent()).setMinHeight(mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y+10);
			}

			@Override
			public void mouseUp(MouseEvent e) {
			}
			
		});
		
		
		iconLabel.addListener(SWT.MouseEnter, new Listener(){
			@Override
			public void handleEvent(Event event) {
				if (open) {
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_GREY_DOWN_IMAGE_ID));
					
				} else {
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_GREY_RIGHT_IMAGE_ID));
				}	
				textLabel.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GRAY));
				textLabel.setCursor(cursorHand);
			}
		});
		
		iconLabel.addListener(SWT.MouseExit, new Listener(){
			@Override
			public void handleEvent(Event event) {
				if (open) {
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_BLACK_DOWN_IMAGE_ID));
				} else { 
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_BLACK_RIGHT_IMAGE_ID));
				}
				textLabel.setForeground(null);
				textLabel.setCursor(null);
			}
		});
		
		textLabel.addListener(SWT.MouseEnter, new Listener(){
			@Override
			public void handleEvent(Event event) {
				if (open) {
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_GREY_DOWN_IMAGE_ID));
					
				} else {
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_GREY_RIGHT_IMAGE_ID));
				}	
				iconLabel.setCursor(cursorHand);
				textLabel.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GRAY));
				textLabel.setCursor(cursorHand);
			}
		});
		
		textLabel.addListener(SWT.MouseExit, new Listener(){
			@Override
			public void handleEvent(Event event) {
				if (open) {
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_BLACK_DOWN_IMAGE_ID));
				} else { 
					iconLabel.setImage(Activator.getDefault().getImageRegistry().get(Activator.ARROW_BLACK_RIGHT_IMAGE_ID));
				}
				iconLabel.setCursor(null);
				textLabel.setForeground(null);
				textLabel.setCursor(null);
			}
		});
		
		modificationLabel = new Label(container, SWT.LEFT);	
		GridData gdml = new GridData();
		gdml.widthHint = 10;
		modificationLabel.setLayoutData(gdml);
		modificationLabel.setText(" ");
	}
	
	private void setOpen(Boolean value) {
		this.open = value;
	}
	
	private Boolean isOpen() {
		return open;
	}
	
	public void setControl(Composite composite) {
		control = composite;
		if (control.getLayoutData() != null) {
			((GridData)control.getLayoutData()).exclude = true;
		}	
	}
	
	public Composite getControl() {
		return this.control;
	}
	
	public Button getCheckButton() {
		return this.checkButton;
	}
	
	public void open() {
		iconLabel.notifyListeners(SWT.MouseDown, new Event());
	}
	
	public void setModification(boolean isModified) {
		this.isModified = isModified;
		if (isModified) 
			modificationLabel.setText("*");
		else
			modificationLabel.setText("");
	}
	
	public boolean isModified() {
		return this.isModified;
	}

	private void createProjectSelectButton() {
		final Label label = new Label(container, SWT.NONE);
		label.setImage(Activator.getDefault().getImageRegistry().get(Activator.PROJECT_IMAGE));
		GridData gdLabel = new GridData();
		gdLabel.horizontalIndent = 5;
		label.setLayoutData(gdLabel);
		label.setToolTipText("Regard project specific Metadata");
		
		label.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseDown(MouseEvent e) {
				RegardMetadataDialog dialog = RegardMetadataDialog.openFilterDialog(
													SearchView.getInstance().getRegardedMetadataProjects());
				if (dialog.getReturnCode() == 0) 
					SearchView.getInstance().setProjectSpecificSearchTags(dialog.getRegardedProjects());
			}
			
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		label.addListener(SWT.MouseEnter, new Listener(){
			@Override
			public void handleEvent(Event event) {
				label.setCursor(cursorHand);
			}
		});
		
		label.addListener(SWT.MouseExit, new Listener(){
			@Override
			public void handleEvent(Event event) {
				label.setCursor(null);
			}
		});
	}

}

