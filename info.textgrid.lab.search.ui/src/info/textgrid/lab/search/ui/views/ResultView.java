package info.textgrid.lab.search.ui.views;

import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.SelectionDragAdapter;
import info.textgrid.lab.search.FullTextEntry;
import info.textgrid.lab.search.ItemEntry;
import info.textgrid.lab.search.LazySearchResultProvider;
import info.textgrid.lab.search.NoMatchEntry;
import info.textgrid.lab.search.ResultViewInitItem;
import info.textgrid.lab.search.ui.Activator;

import org.eclipse.core.commands.common.CommandException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.DecoratingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.TextStyle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.PendingUpdateAdapter;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Result View. Holds a TreeViewer with Search results that gets filled
 * deferred.
 * 
 * @author martin
 * 
 */
public class ResultView extends ViewPart {
	private TreeViewer viewer;
	private Link statusLabel;
	private Composite bottomBar;

	static class SearchResultsLabelProvider extends BaseLabelProvider implements
			DelegatingStyledCellLabelProvider.IStyledLabelProvider {

		private static final Styler MATCH_STYLER = new Styler() {

			@Override
			public void applyStyles(TextStyle textStyle) {
				textStyle.font = JFaceResources.getFontRegistry().getBold(
						JFaceResources.DEFAULT_FONT);
//				textStyle.foreground = Display.getDefault().getSystemColor(
//						SWT.COLOR_DARK_GRAY);
			}
		};
		
		private static final Styler NOMATCH_STYLER = new Styler() {

			@Override
			public void applyStyles(TextStyle textStyle) {
				textStyle.font = JFaceResources.getFontRegistry().getItalic(
						JFaceResources.DEFAULT_FONT);
				textStyle.foreground = Display.getDefault().getSystemColor(
						SWT.COLOR_DARK_GRAY);
			}
		};

		public Image getImage(Object element) {
			if (element instanceof ItemEntry) {
				TextGridObject textGridObject = AdapterUtils.getAdapter(
						element, TextGridObject.class);
				if (textGridObject != null) {
					try {
						return textGridObject.getContentType(false).getImage(
								true);
					} catch (CoreException e) {
						StatusManager.getManager().handle(e,
								Activator.PLUGIN_ID);
					}
				}
			}
			return null;
		}
		
		public String getText(Object element) {
			return getStyledText(element).toString();
		}

		public StyledString getStyledText(Object element) {

			if (element instanceof PendingUpdateAdapter)
				return new StyledString(Messages.ResultView_IM_Searching);

			if (element instanceof ItemEntry)
				return getEntryLabel((ItemEntry)element);
			
			if (element instanceof FullTextEntry)
				return getFulltextLabel((FullTextEntry)element);

			if (element instanceof NoMatchEntry)
				return getNoMatchLabel((NoMatchEntry) element);

			return new StyledString(element.toString());
		}

		private StyledString getEntryLabel(ItemEntry item) {
			TextGridObject tgo = AdapterUtils.getAdapter(
					item, TextGridObject.class);
			StyledString styledString = new StyledString();
			try {
				String path = item.getPath();
				styledString.append(tgo.getTitle(), MATCH_STYLER).append("; ");
				if (!path.equals(""))
					styledString.append(item.getPath(), StyledString.DECORATIONS_STYLER).append("; ");
				if (!tgo.getMetadataForReading().getGeneric().getGenerated().getProject().getValue().equals(""))
					styledString.append(tgo.getMetadataForReading().getGeneric().getGenerated().getProject().getValue(),StyledString.QUALIFIER_STYLER).append("; ");
				styledString.append(tgo.getMetadataForReading().getGeneric().getGenerated().getDataContributor()).append("; ");
				styledString.append("(Revision " + tgo.getRevisionNumber() + ")", StyledString.DECORATIONS_STYLER ); //$NON-NLS-1$
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}

			return styledString;
		}
		
		private StyledString getFulltextLabel(FullTextEntry item) {
			StyledString styledString = new StyledString();
			styledString.append("   ").append(item.getLeft(), StyledString.COUNTER_STYLER).append(" ");
			styledString.append(item.getMatch(), MATCH_STYLER).append(" ");
			styledString.append(item.getRight(), StyledString.COUNTER_STYLER);
			return styledString;
		}
		
		private StyledString getNoMatchLabel(NoMatchEntry noMatch) {
			StyledString styledString = new StyledString();
			styledString.append(noMatch.toString(),NOMATCH_STYLER);
			return styledString;
		}
	}
	
	static class SearchResultsLabelDecorator implements ILabelDecorator {

		@Override
		public void addListener(ILabelProviderListener listener) {
		}

		@Override
		public void dispose() {
		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
		}

		@Override
		public Image decorateImage(Image image, Object element) {
			if (!(element instanceof ItemEntry)) 
				return null;

			TGObjectReference tgoRef = ((ItemEntry)element).getTgoRef();
			try {
				if (tgoRef.getTgo().isPublic()) {
					ImageDescriptor overlay = ImageDescriptor.createFromImage(Activator.getDefault().getImageRegistry().get(Activator.DECORATOR_PUBLIC_ID));
					Image decoratedImage = new DecorationOverlayIcon(image, overlay, IDecoration.BOTTOM_RIGHT)
											.createImage();
					return decoratedImage;
				}
			} catch (CoreException e) {
				return null;
			}	
			return null;
		}

		@Override
		public String decorateText(String text, Object element) {
			return null;
		}
	}
	

	/**
	 * Create the TreeViewer together with some Buttons and a status field at
	 * the bottom.
	 */
	@Override
	public void createPartControl(Composite parent) {

		Composite mainComposite = new Composite(parent, SWT.FILL);
		mainComposite.setLayout(new GridLayout(1, false));
		GridData cGD = new GridData();
		cGD.horizontalAlignment = GridData.FILL;
		cGD.grabExcessVerticalSpace = true;
		cGD.grabExcessHorizontalSpace = true;
		cGD.verticalAlignment = GridData.FILL;
		mainComposite.setLayoutData(cGD);

		// use FULL_SELECTION because of a bug in Ownerdraw
		// see https://bugs.eclipse.org/bugs/show_bug.cgi?id=168807
		viewer = new TreeViewer(mainComposite, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION | SWT.VIRTUAL);
		getSite().setSelectionProvider(viewer);
		viewer.setUseHashlookup(true);
		viewer.setContentProvider(new LazySearchResultProvider());
		viewer.setLabelProvider(new DecoratingStyledCellLabelProvider(new SearchResultsLabelProvider(),
																		new SearchResultsLabelDecorator(),
																		null));

		hookDoubleClickAction();
		viewer.addDragSupport(DND.DROP_DEFAULT | DND.DROP_COPY | DND.DROP_MOVE,
				new Transfer[] { LocalSelectionTransfer.getTransfer() }, new SelectionDragAdapter(
				viewer));
//		viewer.setSorter(null);// new NameSorter());

		makeActions();
		hookContextMenu();
		contributeToActionBars();
		viewer.getTree().setLinesVisible(true);

		GridData vGD = new GridData();
		vGD.horizontalAlignment = GridData.FILL;
		vGD.grabExcessVerticalSpace = true;
		vGD.grabExcessHorizontalSpace = true;
		vGD.verticalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(vGD);

		bottomBar = new Composite(mainComposite, SWT.FILL);
		bottomBar.setLayout(new GridLayout(4, false));
		GridDataFactory.fillDefaults().applyTo(bottomBar);

		statusLabel = new Link(bottomBar, SWT.LEFT);
		statusLabel.setText(Messages.ResultView_IM_SearchNotStarted);
		initResultView();
		getSite().setSelectionProvider(viewer);
		
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "info.textgrid.lab.search.ui.ResultView"); //$NON-NLS-1$
	}

	public TreeViewer getViewer() {
		return viewer;
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {

	}

	private void fillLocalToolBar(IToolBarManager manager) {
	}

	private void makeActions() {
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
//				System.out.println("SIZE:" + viewer.getControl().getSize());
				IHandlerService handlerService = (IHandlerService) getSite()
						.getService(IHandlerService.class);
				try {
					handlerService.executeCommand(
							"info.textgrid.lab.ui.core.commands.Open", null); //$NON-NLS-1$
				} catch (CommandException e) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.ResultView_EM_OpenCommand
									+ event, e);
					Activator.getDefault().getLog().log(status);
				}
				// doubleClickAction.run();
			}
		});
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	protected void setStatusLabel(String text) {
		statusLabel.setText(text);
		bottomBar.pack();
	}
	
	private void initResultView() {
		viewer.setInput(new ResultViewInitItem());
	}
	
}