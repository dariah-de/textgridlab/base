package info.textgrid.lab.search.ui.views;

import info.textgrid.lab.authn.AuthBrowser;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.authn.AuthBrowser.ISIDChangedListener;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.newsearch.SearchRequest.EndPoint;
import info.textgrid.lab.newsearch.SearchRequest.TargetModus;
import info.textgrid.lab.search.ui.Activator;
import info.textgrid.lab.search.ui.SearchLabelIcon;
import info.textgrid.lab.search.ui.SearchUICalendar;
import info.textgrid.lab.search.ui.newsearch.BaselineEncodingMainArea;
import info.textgrid.lab.search.ui.newsearch.FulltextMainArea;
import info.textgrid.lab.search.ui.newsearch.MajorSearchLanguages;
import info.textgrid.lab.search.ui.newsearch.MetadataMainArea;
import info.textgrid.lab.search.ui.newsearch.SearchContentType;
import info.textgrid.lab.search.ui.newsearch.SearchTextType;
import info.textgrid.lab.search.ui.newsearch.SearchTreeContentProvider;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.dialogs.ContainerCheckedTreeViewer;
import org.eclipse.ui.part.ViewPart;

public class SearchView extends ViewPart {

	private static SearchView searchView;
	private Composite mainComposite;
	private TabFolder folder;
	private Text simpleSearchText;
	private Text advancedSearchText;
	private Button simpleSearchButton;
	private Button rbSearchFulltext;
	private Button rbSearchMetadata;
	private Button rbSearchBoth;
	private Button cbMetadata;
	private Button cbFulltext;
	private Button cbBaselineEnc;
	// private Button rbFulltextAnd;
	// private Button rbFulltextOr;
	// private Button rbBaselineEncAnd;
	// private Button rbBaselineEncOr;
	private Button advancedSearchButton;
	private Button rbWordDistanceDocument;
	private Button rbWordDistanceWords;
	private Button rbEndpointStandard;
	private Button rbEndpointPublic;

	private SearchLabelIcon sliMetadata;
	private SearchLabelIcon sliFullText;
	private SearchLabelIcon sliBaseline;

	private SearchLabelIcon sliTextTypes;
	private SearchLabelIcon sliContentTypes;
	private SearchLabelIcon sliLanguages;
	private SearchLabelIcon sliDate;
	private SearchLabelIcon sliWordDistances;

	private Label simpleSearchExample;

	private Button cbDateBetween;
	private Text textFromDate;
	private Text textToDate;
	private Label ilFromDate;
	private Label ilToDate;

	private CheckboxTreeViewer textTypeTree;
	private CheckboxTreeViewer contentTypeTree;
	private ContainerCheckedTreeViewer languageTree;

	private ScrolledComposite scrolledComposite;
	private Composite compAdvancedSearch;
	private Composite compAdvancedSearch1;
	// private Composite compAdvancedSearch2;

	private MetadataMainArea metaDataMainArea;
	private FulltextMainArea fulltextMainArea;
	private BaselineEncodingMainArea baselineEncodingDataMainArea;

	private Font italicFont;
	private Font italicSmallFont;

	private ResultView resultView;

	private SearchTextType searchTextType;
	private SearchContentType searchContentType;
	private MajorSearchLanguages mainLanguages;

	private SearchRequest searchRequest;
	private Listener hitCountListener = null;
	private Listener partSearchReadyListener = null;

	private Spinner wordDistanceSpinner;

	private ArrayList<TextGridProject> regardedMetadataProjects = new ArrayList<TextGridProject>();

	String[] header = { Messages.SearchView_SimpleSearch,
			Messages.SearchView_AdvancedSearch, Messages.SearchView_XPath,
			Messages.SearchView_Browse };

	protected static final String ERROR_INVALID_DATE_FORMAT = "Invalid date format. The format should be yyyy-mm-dd or just yyyy.";
	protected static final String ERROR_INVALID_ORDER = "The chronological date order is incorrect.";
	protected static final String ERROR_EMPTY_SEARCH = "There is not enough information to start a search.";

	protected static final String HELP_LINK_SEARCH = "/info.textgrid.lab.help/html/labSearch/toc.html"; //$NON-NLS-1$

	private ISIDChangedListener sidChangedListener;
	private Group epComposite;

	@Override
	public void createPartControl(Composite parent) {
		searchView = this;
		mainComposite = new Composite(parent, SWT.BORDER);
		mainComposite.setLayout(new GridLayout(1, false));
		createEndpointArea(mainComposite);
		folder = new TabFolder(mainComposite, SWT.BORDER);
		folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		for (int i = 0; i <= (header.length - 3); i++) {
			TabItem item = new TabItem(folder, SWT.READ_ONLY);
			item.setText(header[i]);
		}

		createSimpleSearchControl();
		createAdvancedSearchControl();
		folder.setSelection(folder.getItem(0));
		simpleSearchText.forceFocus();
		sliMetadata.open();
		resizeScrolledComposite();

		PlatformUI.getWorkbench().getHelpSystem()
				.setHelp(parent, "info.textgrid.lab.search.ui.SearchView"); //$NON-NLS-1$

		//TG-2067
		if (RBACSession.getInstance().getSID(false).equals("")) {
			System.out.println(11111);
			rbEndpointStandard.setEnabled(false);
			rbEndpointPublic.setSelection(true);
			rbEndpointStandard.setSelection(false);
		}

		sidChangedListener = new ISIDChangedListener() {
			@Override
			public void sidChanged(String newSID, String newEPPN) {
				if (!newEPPN.equalsIgnoreCase("")) {
					rbEndpointStandard.setEnabled(true);
					rbEndpointPublic.setSelection(false);
					rbEndpointStandard.setSelection(true);
				} else {
					rbEndpointStandard.setEnabled(false);
					rbEndpointPublic.setSelection(true);
					rbEndpointStandard.setSelection(false);
				}
				rbEndpointStandard.update();
				rbEndpointStandard.redraw();
			}
		};
		AuthBrowser.addSIDChangedListener(sidChangedListener);
	}

	public final static SearchView getInstance() {
		return searchView;
	}

	public void resizeScrolledComposite() {
		// mainComposite.getParent().layout();
		// mainComposite.getParent().getParent().layout();
		// mainComposite.getParent().getParent().getParent().layout();
		scrolledComposite.setMinSize(scrolledComposite.getContent()
				.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	/**
	 * Generates the TabFolder <i>Simple Search</i> and the concerning
	 * components.
	 */
	private void createSimpleSearchControl() {
		Composite composite = new Composite(folder, SWT.FILL);
		folder.getItem(0).setControl(composite);
		composite.setLayout(new GridLayout(3, false));
		GridData gd1 = new GridData(GridData.FILL, GridData.FILL, true, false);
		composite.setLayoutData(gd1);

		simpleSearchText = new Text(composite, SWT.SINGLE | SWT.FILL
				| SWT.BORDER | SWT.SEARCH | SWT.ICON_SEARCH | SWT.ICON_CANCEL
				| SWT.CANCEL);
		simpleSearchText.setText("");
		GridData gdSearchText = new GridData(GridData.FILL, GridData.CENTER,
				true, false);
		gdSearchText.horizontalSpan = 3;
		simpleSearchText.setLayoutData(gdSearchText);
		// simpleSearchText.setMessage("Example: Pudel AND Goethe");

		simpleSearchText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				if (event.character == SWT.CR) {
					startSimpleSearch();
				}
			}
		});

		simpleSearchText.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.detail == SWT.ICON_SEARCH)
					startSimpleSearch();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		rbSearchFulltext = new Button(composite, SWT.RADIO);
		rbSearchFulltext.setText(Messages.SearchView_SearchFulltext);
		rbSearchMetadata = new Button(composite, SWT.RADIO);
		rbSearchMetadata.setText(Messages.SearchView_SearchMetadata);
		rbSearchBoth = new Button(composite, SWT.RADIO);
		rbSearchBoth.setText(Messages.SearchView_SearchBoth);
		rbSearchBoth.setSelection(true);

		simpleSearchExample = new Label(composite, SWT.NONE);
		simpleSearchExample.setText(Messages.SearchView_ExampleString);
		FontData fontData = simpleSearchExample.getFont().getFontData()[0];
		italicFont = new Font(Display.getDefault(), new FontData(
				fontData.getName(), fontData.getHeight(), SWT.ITALIC));
		italicSmallFont = new Font(Display.getDefault(), new FontData(
				fontData.getName(), fontData.getHeight() - 2, SWT.ITALIC));
		simpleSearchExample.setFont(italicFont);

		simpleSearchExample.setForeground(Display.getDefault().getSystemColor(
				SWT.COLOR_DARK_GRAY));
		GridData gdSearchExample = new GridData(GridData.FILL, GridData.CENTER,
				true, false);
		gdSearchExample.horizontalSpan = 3;
		gdSearchExample.horizontalIndent = 10;
		simpleSearchExample.setLayoutData(gdSearchExample);

		simpleSearchButton = new Button(composite, SWT.PUSH);
		simpleSearchButton.setText(Messages.SearchView_SearchButtonLabel);
		simpleSearchButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				startSimpleSearch();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		GridData gdSSB = new GridData(GridData.END, GridData.END, false, false);
		gdSSB.horizontalSpan = 3;
		simpleSearchButton.setLayoutData(gdSSB);

		Link helplink = new Link(composite, SWT.NULL);
		helplink.setText(Messages.SearchView_LinkLabel);
		PlatformUI.getWorkbench().getHelpSystem()
				.setHelp(helplink, "info.textgrid.lab.search.ui.SearchHints"); //$NON-NLS-1$
		helplink.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				widgetDefaultSelected(e);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				ICommandService commandService = (ICommandService) PlatformUI
						.getWorkbench().getActiveWorkbenchWindow()
						.getService(ICommandService.class);
				try {
					commandService
							.getCommand("org.eclipse.ui.help.dynamicHelp").executeWithChecks(new ExecutionEvent()); //$NON-NLS-1$
				} catch (ExecutionException e1) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.SearchView_EM_OpenHelp1, e1);
					Activator.getDefault().getLog().log(status);
				} catch (NotDefinedException e1) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.SearchView_EM_OpenHelp1, e1);
					Activator.getDefault().getLog().log(status);
				} catch (NotEnabledException e1) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.SearchView_EM_OpenHelp1, e1);
					Activator.getDefault().getLog().log(status);
				} catch (NotHandledException e1) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.SearchView_EM_OpenHelp1, e1);
					Activator.getDefault().getLog().log(status);
				}
			}
		});
		GridData gdSHL = new GridData(GridData.END, GridData.END, false, false);
		gdSHL.horizontalSpan = 3;
		helplink.setLayoutData(gdSHL);

	}

	/**
	 * Generates the TabFolder <i>Advanced Search</i> and the concerning
	 * components.
	 */
	private void createAdvancedSearchControl() {
		scrolledComposite = new ScrolledComposite(folder, SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FILL);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		folder.getItem(1).setControl(scrolledComposite);
		scrolledComposite.setLayout(new FillLayout());
		scrolledComposite.getVerticalBar().setIncrement(10);
		scrolledComposite.getHorizontalBar().setIncrement(10);

		compAdvancedSearch = new Composite(scrolledComposite, SWT.FILL);
		scrolledComposite.setContent(compAdvancedSearch);
		GridLayout gl = new GridLayout(2, false);
		compAdvancedSearch.setLayout(gl);

		compAdvancedSearch1 = new Composite(compAdvancedSearch, SWT.FILL);
		GridLayout gl1 = new GridLayout(1, false);
		compAdvancedSearch1.setLayout(gl1);
		GridData gd1 = new GridData(GridData.FILL, GridData.FILL, true, true);
		compAdvancedSearch1.setLayoutData(gd1);
		GridData gdRadioButtonComposite = new GridData(GridData.BEGINNING,
				GridData.BEGINNING, false, false);
		gdRadioButtonComposite.horizontalSpan = 2;
		gdRadioButtonComposite.verticalIndent = 15;

		// Meatdata
		createMetadataArea();

		Label seperator1 = new Label(compAdvancedSearch1, SWT.SEPARATOR
				| SWT.HORIZONTAL);
		GridData gdSeperator = new GridData(GridData.FILL, GridData.BEGINNING,
				true, false);
		seperator1.setLayoutData(gdSeperator);

		// first set of and/or radiobuttons
		// Composite rbComposite1 = new Composite(compAdvancedSearch1,
		// SWT.NONE);
		// rbComposite1.setLayout(new FillLayout());
		// rbComposite1.setLayoutData(gdRadioButtonComposite);
		// rbFulltextAnd = new Button(rbComposite1, SWT.RADIO);
		// rbFulltextAnd.setText("AND");
		// rbFulltextAnd.setSelection(true);
		// rbFulltextOr = new Button(rbComposite1, SWT.RADIO);
		// rbFulltextOr.setText("OR");
		// rbFulltextAnd.setEnabled(false);
		// rbFulltextOr.setEnabled(false);

		// Fulltext
		createFulltextArea(compAdvancedSearch1);

		Label seperator2 = new Label(compAdvancedSearch1, SWT.SEPARATOR
				| SWT.HORIZONTAL | SWT.SHADOW_OUT);
		seperator2.setLayoutData(gdSeperator);

		// second set of and/or radiobuttons
		// Composite rbComposite2 = new Composite(compAdvancedSearch1,
		// SWT.NONE);
		// rbComposite2.setLayout(new FillLayout());
		// rbComposite2.setLayoutData(gdRadioButtonComposite);
		// rbBaselineEncAnd = new Button(rbComposite2, SWT.RADIO);
		// rbBaselineEncAnd.setText("AND");
		// rbBaselineEncAnd.setSelection(true);
		// rbBaselineEncOr = new Button(rbComposite2, SWT.RADIO);
		// rbBaselineEncOr.setText("OR");
		// rbBaselineEncAnd.setEnabled(false);
		// rbBaselineEncOr.setEnabled(false);

		// Baseline Encodung
		//createBaselineEncArea(compAdvancedSearch1);

		// compAdvancedSearch2 = new Composite(compAdvancedSearch, SWT.FILL);
		// GridLayout gl2 = new GridLayout(1, false);
		// compAdvancedSearch2.setLayout(gl2);
		// GridData gd2 = new GridData(GridData.BEGINNING, GridData.BEGINNING,
		// false, true);
		// compAdvancedSearch2.setLayoutData(gd2);

//		Label seperator3 = new Label(compAdvancedSearch1, SWT.SEPARATOR
//				| SWT.HORIZONTAL | SWT.SHADOW_OUT);
//		seperator3.setLayoutData(gdSeperator);

		advancedSearchButton = new Button(compAdvancedSearch1, SWT.PUSH);
		advancedSearchButton.setText(Messages.SearchView_SearchButtonLabel);
		advancedSearchButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				startAdvancedSearch();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		GridData gdASB = new GridData(GridData.END, GridData.END, false, false);
		gdASB.horizontalSpan = 2;
		advancedSearchButton.setLayoutData(gdASB);

		Link helplink = new Link(compAdvancedSearch1, SWT.NULL);
		helplink.setText(Messages.SearchView_LinkLabel);
		PlatformUI.getWorkbench().getHelpSystem()
				.setHelp(helplink, "info.textgrid.lab.search.ui.SearchHints"); //$NON-NLS-1$
		helplink.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				widgetDefaultSelected(e);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				ICommandService commandService = (ICommandService) PlatformUI
						.getWorkbench().getActiveWorkbenchWindow()
						.getService(ICommandService.class);
				try {
					commandService
							.getCommand("org.eclipse.ui.help.dynamicHelp").executeWithChecks(new ExecutionEvent()); //$NON-NLS-1$
				} catch (ExecutionException e1) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.SearchView_EM_OpenHelp1, e1);
					Activator.getDefault().getLog().log(status);
				} catch (NotDefinedException e1) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.SearchView_EM_OpenHelp1, e1);
					Activator.getDefault().getLog().log(status);
				} catch (NotEnabledException e1) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.SearchView_EM_OpenHelp1, e1);
					Activator.getDefault().getLog().log(status);
				} catch (NotHandledException e1) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							Messages.SearchView_EM_OpenHelp1, e1);
					Activator.getDefault().getLog().log(status);
				}
			}
		});
		GridData gdAHL = new GridData(GridData.END, GridData.END, false, false);
		gdAHL.horizontalSpan = 2;
		helplink.setLayoutData(gdAHL);

		scrolledComposite.setMinSize(compAdvancedSearch1.computeSize(
				SWT.DEFAULT, SWT.DEFAULT));
	}

	/**
	 * Generates the ExpandBar <i>Metadata</i> for the advanced search and the
	 * concerning components.
	 */
	private void createMetadataArea() {
		// Metadata
		sliMetadata = new SearchLabelIcon(compAdvancedSearch1, SWT.NONE,
				Messages.SearchView_Metadata, compAdvancedSearch1, true, 1);
		cbMetadata = sliMetadata.getCheckButton();
		cbMetadata.setSelection(true);

		cbMetadata.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// if (cbMetadata.getSelection() && cbFulltext.getSelection()) {
				// rbFulltextAnd.setEnabled(true);
				// rbFulltextOr.setEnabled(true);
				// } else {
				// rbFulltextAnd.setEnabled(false);
				// rbFulltextOr.setEnabled(false);
				// }
				//
				// if (cbMetadata.getSelection() &&
				// cbBaselineEnc.getSelection()) {
				// rbBaselineEncAnd.setEnabled(true);
				// rbBaselineEncOr.setEnabled(true);
				// } else {
				// rbBaselineEncAnd.setEnabled(false);
				// rbBaselineEncOr.setEnabled(false);
				// }
				if (cbBaselineEnc.getSelection())
					cbBaselineEnc.setSelection(false);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Group mdComposite = new Group(compAdvancedSearch1, SWT.SHADOW_IN);
		GridLayout glMain = new GridLayout(1, false);
		mdComposite.setLayout(glMain);
		mdComposite.setVisible(false);
		GridData gd = new GridData(GridData.FILL, GridData.FILL, true, false);
		gd.horizontalSpan = 2;
		gd.horizontalIndent = 30;
		mdComposite.setLayoutData(gd);
		sliMetadata.setControl(mdComposite);

		// MAIN AREA
		Composite compMainArea = new Composite(mdComposite, SWT.FILL);
		GridLayout gl = new GridLayout(1, false);
		compMainArea.setLayout(gl);
		GridData gdMain = new GridData(GridData.FILL, GridData.FILL, true,
				false);
		compMainArea.setLayoutData(gdMain);
		metaDataMainArea = new MetadataMainArea(compMainArea, scrolledComposite);

		// TEXT TYPES
		createTextTypes(mdComposite);

		// CONTENT TYPES
		createContentTypeGroup(mdComposite);

		// LANGUAGES
		createLanguageGroup(mdComposite);

		// DATE
		createDateGroup(mdComposite);

	}

	/**
	 * Generates the ExpandBar <i>Fulltext</i> for the advanced search and the
	 * concerning components.
	 */
	private void createFulltextArea(final Composite compLevel1) {
		sliFullText = new SearchLabelIcon(compAdvancedSearch1, SWT.NONE,
				Messages.SearchView_Fulltext, compAdvancedSearch1, true, 0);
		cbFulltext = sliFullText.getCheckButton();

		cbFulltext.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// if (cbMetadata.getSelection() && cbFulltext.getSelection()) {
				// rbFulltextAnd.setEnabled(true);
				// rbFulltextOr.setEnabled(true);
				// } else {
				// rbFulltextAnd.setEnabled(false);
				// rbFulltextOr.setEnabled(false);
				// }
				//
				// if ((cbMetadata.getSelection() &&
				// cbBaselineEnc.getSelection()) ||
				// (cbFulltext.getSelection() && cbBaselineEnc.getSelection()))
				// {
				// rbBaselineEncAnd.setEnabled(true);
				// rbBaselineEncOr.setEnabled(true);
				// } else {
				// rbBaselineEncAnd.setEnabled(false);
				// rbBaselineEncOr.setEnabled(false);
				// }
				if (cbBaselineEnc.getSelection())
					cbBaselineEnc.setSelection(false);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Group fulltextComposite = new Group(compAdvancedSearch1, SWT.SHADOW_IN);
		GridLayout glMain = new GridLayout(1, false);
		fulltextComposite.setLayout(glMain);
		fulltextComposite.setVisible(false);
		GridData gd = new GridData(GridData.FILL, GridData.FILL, true, false);
		gd.horizontalSpan = 2;
		gd.horizontalIndent = 30;
		fulltextComposite.setLayoutData(gd);
		sliFullText.setControl(fulltextComposite);

		advancedSearchText = new Text(fulltextComposite, SWT.SINGLE | SWT.FILL
				| SWT.BORDER | SWT.SEARCH | SWT.CANCEL);
		advancedSearchText.setText("");
		GridData gdSearchText = new GridData(GridData.FILL, GridData.CENTER,
				true, false);
		advancedSearchText.setLayoutData(gdSearchText);

		advancedSearchText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				if (event.character == SWT.CR) {
					startAdvancedSearch();
				}
			}
		});

		Composite wordDistance = new Composite(fulltextComposite, SWT.NONE);
		GridLayout wd = new GridLayout(2, false);
		wordDistance.setLayout(wd);
		wordDistance.setLayoutData(new GridData(GridData.FILL, GridData.CENTER,
				false, false));
		Composite wordDistance1 = new Composite(wordDistance, SWT.NONE);
		wordDistance1.setLayout(new GridLayout());
		wordDistance1.setLayoutData(new GridData(GridData.BEGINNING,
				GridData.BEGINNING, false, false));
		Composite wordDistance11 = new Composite(wordDistance1, SWT.NONE);
		wordDistance11.setLayout(new GridLayout(1, false));
		Label wdLabel = new Label(wordDistance11, SWT.NONE);
		wdLabel.setText(Messages.SearchView_WordDistances);
		wdLabel.setLayoutData(gdSearchText);

		Composite wordDistance2 = new Composite(wordDistance, SWT.NONE);
		wordDistance2.setLayout(new GridLayout(1, false));
		Composite wordDistance21 = new Composite(wordDistance2, SWT.NONE);
		wordDistance21.setLayout(new GridLayout(1, false));
		rbWordDistanceDocument = new Button(wordDistance21, SWT.RADIO);
		rbWordDistanceDocument.setText(Messages.SearchView_InOneDocument);
		rbWordDistanceDocument.setSelection(true);

		Composite wordDistance22 = new Composite(wordDistance2, SWT.NONE);
		wordDistance22.setLayout(new GridLayout(3, false));
		rbWordDistanceWords = new Button(wordDistance22, SWT.RADIO);
		rbWordDistanceWords.setText(Messages.SearchView_maximal);
		wordDistanceSpinner = new Spinner(wordDistance22, SWT.NONE);
		wordDistanceSpinner.setValues(40, 1, 1000, 0, 1, 100);
		new Label(wordDistance22, SWT.NONE).setText(Messages.SearchView_words);

		rbWordDistanceDocument.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				rbWordDistanceWords.setSelection(false);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		rbWordDistanceWords.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				rbWordDistanceDocument.setSelection(false);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		// WORD DISTANCES
		// createWordDistances(fulltextComposite);
	}

	/**
	 * Generates the ExpandBar <i>Baseline Encoding</i> for the advanced search
	 * and the concerning components.
	 */
	private void createBaselineEncArea(final Composite compLevel1) {
		sliBaseline = new SearchLabelIcon(compAdvancedSearch1, SWT.NONE,
				"BASELINE ENC", compAdvancedSearch1, true, 0);
		cbBaselineEnc = sliBaseline.getCheckButton();

		cbBaselineEnc.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// if ((cbMetadata.getSelection() &&
				// cbBaselineEnc.getSelection()) ||
				// (cbFulltext.getSelection() && cbBaselineEnc.getSelection()))
				// {
				// rbBaselineEncAnd.setEnabled(true);
				// rbBaselineEncOr.setEnabled(true);
				// } else {
				// rbBaselineEncAnd.setEnabled(false);
				// rbBaselineEncOr.setEnabled(false);
				// }
				if (cbMetadata.getSelection())
					cbMetadata.setSelection(false);
				if (cbFulltext.getSelection())
					cbFulltext.setSelection(false);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Group baselineComposite = new Group(compAdvancedSearch1, SWT.SHADOW_IN);
		GridLayout glMain = new GridLayout(1, false);
		baselineComposite.setLayout(glMain);
		baselineComposite.setVisible(false);
		GridData gd = new GridData(GridData.FILL, GridData.FILL, true, false);
		gd.horizontalSpan = 2;
		gd.horizontalIndent = 30;
		baselineComposite.setLayoutData(gd);
		sliBaseline.setControl(baselineComposite);

		// MAIN AREA
		Composite compMainArea = new Composite(baselineComposite, SWT.FILL);
		GridLayout gl = new GridLayout(1, false);
		compMainArea.setLayout(gl);
		GridData gdMain = new GridData(GridData.FILL, GridData.FILL, true,
				false);
		compMainArea.setLayoutData(gdMain);
		baselineEncodingDataMainArea = new BaselineEncodingMainArea(
				compMainArea, scrolledComposite);
	}

	@Override
	public void setFocus() {
		if (mainComposite != null)
			mainComposite.setFocus();
	}

	private void createTextTypes(final Composite mdComposite) {
		searchTextType = new SearchTextType("root", "", true);
		sliTextTypes = new SearchLabelIcon(mdComposite, SWT.NONE,
				Messages.SearchView_TextTypesWithMarker, compAdvancedSearch1,
				false, 0);
		Composite textTypeComposite = new Composite(mdComposite, SWT.NONE);
		GridLayout gl = new GridLayout(1, false);
		gl.marginWidth = 20;
		textTypeComposite.setLayout(gl);
		textTypeComposite.setVisible(false);
		final GridData gd = new GridData(GridData.FILL, GridData.FILL, true,
				true);
		textTypeComposite.setLayoutData(gd);
		sliTextTypes.setControl(textTypeComposite);
		textTypeTree = new ContainerCheckedTreeViewer(textTypeComposite);
		textTypeTree.getControl().setBackground(
				textTypeComposite.getBackground());
		textTypeTree.setSorter(new ViewerSorter());
		textTypeTree.setContentProvider(new SearchTreeContentProvider());
		textTypeTree.setLabelProvider(new LabelProvider());
		textTypeTree.setInput(searchTextType.getChildren());
		textTypeTree.getControl().setLayoutData(gd);
		for (Object tt : searchTextType.getChildren()) {
			textTypeTree.setSubtreeChecked(tt, true);
			textTypeTree.setExpandedState(tt, true);
		}

		textTypeTree.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				if (textTypeTree.getCheckedElements().length != searchTextType
						.getChildCount())
					sliTextTypes.setModification(true);
				else
					sliTextTypes.setModification(false);
			}
		});
	}

	private void createContentTypeGroup(final Composite mdComposite) {
		searchContentType = SearchContentType.getInstance();
		sliContentTypes = new SearchLabelIcon(mdComposite, SWT.LEFT,
				Messages.SearchView_ContentTypesWithMarker,
				compAdvancedSearch1, false, 0);
		Composite contentComposite = new Composite(mdComposite, SWT.NONE);
		GridLayout gl = new GridLayout(1, false);
		gl.marginWidth = 20;
		contentComposite.setLayout(gl);
		contentComposite.setVisible(false);
		final GridData gd = new GridData(GridData.FILL, GridData.FILL, true,
				true);
		contentComposite.setLayoutData(gd);
		sliContentTypes.setControl(contentComposite);
		contentTypeTree = new ContainerCheckedTreeViewer(contentComposite);
		contentTypeTree.getControl().setBackground(
				contentComposite.getBackground());
		contentTypeTree.setSorter(new ViewerSorter());
		contentTypeTree.setContentProvider(new SearchTreeContentProvider());
		contentTypeTree.setLabelProvider(new LabelProvider());
		contentTypeTree.setInput(searchContentType.getChildren());
		contentTypeTree.getControl().setLayoutData(gd);
		for (SearchContentType ct : searchContentType.getChildren()) {
			contentTypeTree.setSubtreeChecked(ct, true);
			contentTypeTree.setExpandedState(ct, true);
		}

		contentTypeTree.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				if (contentTypeTree.getCheckedElements().length != searchContentType
						.getChildCount())
					sliContentTypes.setModification(true);
				else
					sliContentTypes.setModification(false);
			}
		});
	}

	private void createLanguageGroup(Composite mdComposite) {
		mainLanguages = new MajorSearchLanguages("root", "", true);
		sliLanguages = new SearchLabelIcon(mdComposite, SWT.NONE,
				Messages.SearchView_LanguagesWithMarker, compAdvancedSearch1,
				false, 0);
		Composite languageComposite = new Composite(mdComposite, SWT.NONE);
		GridLayout gl = new GridLayout(1, false);
		gl.marginWidth = 20;
		languageComposite.setLayout(gl);
		languageComposite.setVisible(false);
		GridData gd = new GridData(GridData.FILL, GridData.FILL, true, true);
		languageComposite.setLayoutData(gd);
		sliLanguages.setControl(languageComposite);
		languageTree = new ContainerCheckedTreeViewer(languageComposite);
		languageTree.getControl().setBackground(
				languageComposite.getBackground());
		languageTree.setSorter(new ViewerSorter());
		languageTree.setContentProvider(new SearchTreeContentProvider());
		languageTree.setLabelProvider(new LabelProvider());
		languageTree.setInput(mainLanguages.getChildren());
		languageTree.getControl().setLayoutData(gd);
		for (MajorSearchLanguages ml : mainLanguages.getChildren()) {
			languageTree.setSubtreeChecked(ml, true);
			languageTree.setExpandedState(ml, true);
		}
		languageTree.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				if (languageTree.getCheckedElements().length != mainLanguages
						.getChildCount())
					sliLanguages.setModification(true);
				else
					sliLanguages.setModification(false);
			}
		});
	}

	private void createDateGroup(Composite mdComposite) {
		sliDate = new SearchLabelIcon(mdComposite, SWT.NONE,
				Messages.SearchView_DateWithMarker, compAdvancedSearch1, false,
				0);
		Composite dateComposite = new Composite(mdComposite, SWT.NONE);
		GridLayout glDate = new GridLayout(4, false);
		glDate.marginLeft = 20;
		dateComposite.setLayout(glDate);
		GridData gd = new GridData(GridData.BEGINNING, GridData.CENTER, false,
				false);
		dateComposite.setLayoutData(gd);
		dateComposite.setVisible(false);
		sliDate.setControl(dateComposite);

		cbDateBetween = new Button(dateComposite, SWT.CHECK);
		cbDateBetween.setText(Messages.SearchView_Between);
		cbDateBetween.setSelection(true);

		Composite cpFromDate = new Composite(dateComposite, SWT.NONE);
		GridLayout glFromDate = new GridLayout(2, false);
		glFromDate.horizontalSpacing = 0;
		cpFromDate.setLayout(glFromDate);
		textFromDate = new Text(cpFromDate, SWT.SINGLE | SWT.BORDER);
		textFromDate.setTextLimit(10);
		GridData gdText = new GridData(SWT.RIGHT, SWT.RIGHT, true, true);
		gdText.widthHint = 80;
		textFromDate.setLayoutData(gdText);
		textFromDate.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR) {
					startAdvancedSearch();
				}
			}
		});

		ilFromDate = new Label(cpFromDate, SWT.LEFT);
		ilFromDate.setImage(Activator.getDefault().getImageRegistry()
				.get(Activator.CALENDAR_IMAGE_ID));

		new Label(dateComposite, SWT.NONE).setText(Messages.SearchView_And);

		Composite cpToDate = new Composite(dateComposite, SWT.NONE);
		cpToDate.setLayout(glFromDate);
		textToDate = new Text(cpToDate, SWT.BORDER);
		textToDate.setTextLimit(10);
		textToDate.setLayoutData(gdText);
		textToDate.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR) {
					startAdvancedSearch();
				}
			}
		});

		ilToDate = new Label(cpToDate, SWT.NONE);
		ilToDate.setImage(Activator.getDefault().getImageRegistry()
				.get(Activator.CALENDAR_IMAGE_ID));

		Label lbExample1 = new Label(dateComposite, SWT.NONE);
		lbExample1.setText(Messages.SearchView_DateExampleFrom);
		lbExample1.setFont(italicSmallFont);
		lbExample1.setForeground(Display.getDefault().getSystemColor(
				SWT.COLOR_DARK_GRAY));
		GridData gdDateExample = new GridData(GridData.END, GridData.CENTER,
				true, false);
		gdDateExample.horizontalSpan = 2;
		lbExample1.setLayoutData(gdDateExample);

		Label lbExample2 = new Label(dateComposite, SWT.NONE);
		lbExample2.setText(Messages.SearchView_DateExampleTo);
		lbExample2.setFont(italicSmallFont);
		lbExample2.setForeground(Display.getDefault().getSystemColor(
				SWT.COLOR_DARK_GRAY));
		lbExample2.setLayoutData(gdDateExample);

		ilFromDate.addMouseListener(new MouseListener() {
			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				new SearchUICalendar(textFromDate);
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}
		});

		ilToDate.addMouseListener(new MouseListener() {
			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				new SearchUICalendar(textToDate);
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}
		});
	}

	private void createWordDistances(final Composite fulltextComposite) {
		sliWordDistances = new SearchLabelIcon(fulltextComposite, SWT.NONE,
				Messages.SearchView_WordDistancesWithMarker,
				compAdvancedSearch1, false, 0);
		Composite distancesComposite = new Composite(fulltextComposite,
				SWT.NONE);
		GridLayout gl = new GridLayout(1, false);
		gl.marginWidth = 20;
		distancesComposite.setLayout(gl);
		distancesComposite.setVisible(false);
		final GridData gd = new GridData(GridData.FILL, GridData.FILL, true,
				true);
		distancesComposite.setLayoutData(gd);
		sliWordDistances.setControl(distancesComposite);
		fulltextMainArea = new FulltextMainArea(distancesComposite,
				scrolledComposite);
	}

	public void startSimpleSearch() {
		if (simpleSearchText.getText().length() == 0) {
			MessageBox mb = new MessageBox(getSite().getWorkbenchWindow()
					.getShell(), SWT.OK | SWT.ICON_WARNING);
			mb.setMessage(ERROR_EMPTY_SEARCH);
			if (mb.open() == SWT.OK)
				simpleSearchText.setFocus();
			return;
		}
		initResultView();

		if (rbEndpointPublic.getSelection())
			searchRequest = new SearchRequest(EndPoint.PUBLIC);
		else if (rbEndpointStandard.getSelection())
			searchRequest = new SearchRequest(EndPoint.STANDARD);

		setHitCountListener();
		setPartSearchReadyListener();
		searchRequest.setResolvePath(true);

		if (rbSearchFulltext.getSelection()) {
			searchRequest.setTarget(TargetModus.STRUCTURE);
			// searchRequest.setQueryText("\"" + simpleSearchText.getText() +
			// "\"");
		} else if (rbSearchMetadata.getSelection()) {
			searchRequest.setTarget(TargetModus.METADATA);
			// searchRequest.setQueryMetadata("\"" + simpleSearchText.getText()
			// + "\"");
		} else {
			searchRequest.setTarget(TargetModus.BOTH);
			// searchRequest.setQueryText("\"" + simpleSearchText.getText() +
			// "\"");
		}

		searchRequest.setQueryText(simpleSearchText.getText());
		// searchRequest.setQueryText("\"" + "dies" + "\"" + " AND " + "\"" +
		// "ist" + "\"");
		resultView.getViewer().setInput(searchRequest);
		// System.out.println("\"" + "dies" + "\"" + " AND " + "\"" + "ist" +
		// "\"");
		// System.out.println("SUCHSTRING VOLLTEXT: " +
		// simpleSearchText.getText());
	}

	public void startAdvancedSearch() {
		String searchString = "";

		if (rbEndpointPublic.getSelection())
			searchRequest = new SearchRequest(EndPoint.PUBLIC);
		else if (rbEndpointStandard.getSelection())
			searchRequest = new SearchRequest(EndPoint.STANDARD);

		if (!sliMetadata.getCheckButton().getSelection()
				&& !sliFullText.getCheckButton().getSelection()
				&& !sliBaseline.getCheckButton().getSelection()) {
			MessageBox mb = new MessageBox(getSite().getWorkbenchWindow()
					.getShell(), SWT.OK | SWT.ICON_ERROR);
			mb.setMessage(Messages.SearchView_IM_SelectSection);
			mb.setText(Messages.SearchView_AdvancedSearch);
			mb.open();
			simpleSearchText.setFocus();
			return;
		}

		// TODO
		// case for baseline encoding???
		// and how to combine it with the other modus
		if (sliMetadata.getCheckButton().getSelection()
				&& !sliFullText.getCheckButton().getSelection()
				/*&& !sliBaseline.getCheckButton().getSelection()*/)
			searchRequest.setTarget(TargetModus.METADATA);
		else if (sliMetadata.getCheckButton().getSelection()
				&& sliFullText.getCheckButton().getSelection()
				/*&& !sliBaseline.getCheckButton().getSelection()*/)
			searchRequest.setTarget(TargetModus.BOTH);

		if (sliMetadata.getCheckButton().getSelection())
			searchString = getMetaSearchString();
		if (sliFullText.getCheckButton().getSelection()) {
			searchString = getFulltextSearchString(searchString);
		}
//		if (sliBaseline.getCheckButton().getSelection())
//			searchString = baselineEncodingDataMainArea
//					.getBaselineEncodingMainSearchString();

		if (searchString.equals("")) {
			MessageBox mb = new MessageBox(getSite().getWorkbenchWindow()
					.getShell(), SWT.OK | SWT.ICON_WARNING);
			mb.setMessage(ERROR_EMPTY_SEARCH);
			if (mb.open() == SWT.OK)
				simpleSearchText.setFocus();
			return;
		}

		initResultView();
		setHitCountListener();
		setPartSearchReadyListener();
		searchRequest.setResolvePath(true);
//		if (sliBaseline.getCheckButton().getSelection())
//			searchRequest.setQueryBaseline(searchString);
//		else
			searchRequest.setQueryMetadata(searchString);
		resultView.getViewer().setInput(searchRequest);
		System.out.println("SUCHSTRING META: " + searchString);
	}

	public void initResultView() {
		IViewPart resView = null;
		try {
			resView = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage()
					.showView("info.textgrid.lab.search.views.ResultView"); //$NON-NLS-1$
		} catch (PartInitException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					Messages.SearchView_EM_OpenView, e);
			Activator.getDefault().getLog().log(status);
		}
		if (resView instanceof ResultView) {
			resultView = (ResultView) resView;
			resultView.setStatusLabel(Messages.SearchView_Searching);
		}

		// resultView.fitColumnWidth();
	}

	public void setHitCountListener() {
		if (hitCountListener != null) {
			searchRequest.removeHitCountListener(hitCountListener);
			hitCountListener = null;
		}

		if (hitCountListener == null) {
			hitCountListener = new Listener() {
				@Override
				public void handleEvent(Event event) {
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							resultView.setStatusLabel(String
									.valueOf(searchRequest.getHitCount())
									+ Messages.SearchView_ItemsFound);
						}
					});
				}
			};
			searchRequest.addHitCountListener(hitCountListener);
		}
	}

	public void setPartSearchReadyListener() {
		if (partSearchReadyListener != null) {
			searchRequest.removeHitCountListener(partSearchReadyListener);
			partSearchReadyListener = null;
		}
	}

	public String getMetaSearchString() {
		String strResult = "";
		String strMeta = "";
		boolean isFirst = true;

		strResult = metaDataMainArea.getMetadataMainSearchString();

		// Text Types
		if (textTypeTree.getCheckedElements().length != searchTextType
				.getChildCount()) {
			for (Object item : textTypeTree.getCheckedElements()) {
				if (!item.toString().equals(Messages.SearchView_TextTypes)) {
					if (isFirst) {
						strMeta = strMeta
								+ "( genre:" + ((SearchTextType) item).getTextType(); //$NON-NLS-1$
						isFirst = false;
					} else
						strMeta = strMeta
								+ " OR genre:" + ((SearchTextType) item).getTextType(); //$NON-NLS-1$
				}
			}
			if(!isFirst) {
				strMeta += " )";
			}
		}

		if (!strMeta.equals("")) {
			if (!strResult.equals(""))
				strResult = strResult + " AND " + strMeta;
			else
				strResult = strMeta;
		}

		// Content Types
		isFirst = true;
		strMeta = "";
		if (contentTypeTree.getCheckedElements().length != searchContentType
				.getChildCount()) {
			for (Object item : contentTypeTree.getCheckedElements()) {
				if (!item.toString().equals(Messages.SearchView_ContentTypes)) {
					if (isFirst) {
						strMeta = strMeta
								+ "( format:\"" + ((SearchContentType) item).getFormat().toLowerCase() + "\""; //$NON-NLS-1$
						isFirst = false;
					} else
						strMeta = strMeta
								+ " OR format:\"" + ((SearchContentType) item).getFormat().toLowerCase() + "\""; //$NON-NLS-1$
				}
			}
			if(!isFirst) {
				strMeta += " )";
			}
		}

		if (!strMeta.equals("")) {
			if (!strResult.equals(""))
				strResult = strResult + " AND " + strMeta;
			else
				strResult = strMeta;
		}

		// Languages
		isFirst = true;
		strMeta = "";
		if (languageTree.getCheckedElements().length != mainLanguages
				.getChildCount()) {
			for (Object item : languageTree.getCheckedElements()) {
				if (!item.toString().equals(Messages.SearchView_Languages)) {
					if (isFirst) {
						strMeta = strMeta
								+ "( language:" + ((MajorSearchLanguages) item).getCode().toLowerCase(); //$NON-NLS-1$
						isFirst = false;
					} else
						strMeta = strMeta
								+ " OR language:" + ((MajorSearchLanguages) item).getCode().toLowerCase(); //$NON-NLS-1$
				}
			}
			if(!isFirst) {
				strMeta += " )";
			}
		}

		if (!strMeta.equals("")) {
			if (!strResult.equals(""))
				strResult = strResult + " AND " + strMeta;
			else
				strResult = strMeta;
		}

		// Date
		// TODO: date-field does not exist in index, possibly dateOfCreation? 
		strMeta = "";
		if (cbDateBetween.getSelection()
				&& (!textFromDate.getText().trim().equals("") || !textToDate
						.getText().trim().equals(""))) {
			if (plausiAdvancedMetadataDate()) {
				if (!textFromDate.getText().trim().equals(""))
					strMeta = "created:[" + textFromDate.getText() ; //$NON-NLS-1$

				if (!textToDate.getText().trim().equals("")) {
					if (strMeta.equals(""))
						strMeta = "created:" + textToDate.getText(); //$NON-NLS-1$
					else
						strMeta = strMeta + " TO " + textToDate.getText() + "]"; //$NON-NLS-1$
				}
			} else {
				return "";
			}

		}

		if (!strMeta.equals("")) {
			if (!strResult.equals(""))
				strResult = strResult + " AND " + strMeta; //$NON-NLS-1$
			else
				strResult = strMeta;
		}

		return strResult;
	}

	public String getFulltextSearchString(String searchString) {
		if (!advancedSearchText.getText().trim().equals("")) {
			if (!searchString.trim().equals(""))
				searchString = searchString + " AND "; //$NON-NLS-1$
			
			String ftString = advancedSearchText.getText().trim();
			if (rbWordDistanceWords.getSelection()) {
				System.out.println("dist-query");
				//searchRequest.setWordDistance(wordDistanceSpinner
				//		.getSelection());
				ftString = "\"" + ftString + "\"~" 
						+ wordDistanceSpinner.getSelection();
			}
			
			searchString = searchString + ftString;
		}
		return searchString;
	}

	private boolean plausiAdvancedMetadataDate() {
		boolean result = true;
		DateFormat standardDateFormat = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$
		DateFormat yearDateFormat = new SimpleDateFormat("yyyy"); //$NON-NLS-1$
		Date dateFrom;
		Date dateTo;

		// invalid date format
		try {
			dateFrom = yearDateFormat.parse(textFromDate.getText());
		} catch (ParseException e) {
			try {
				dateFrom = standardDateFormat.parse(textFromDate.getText());
			} catch (ParseException e1) {
				MessageBox mb = new MessageBox(getSite().getWorkbenchWindow()
						.getShell(), SWT.OK | SWT.ICON_ERROR);
				mb.setMessage(ERROR_INVALID_DATE_FORMAT);
				mb.setText(Messages.SearchView_AdvancedSearch);
				mb.open();
				textFromDate.setFocus();
				textToDate.selectAll();
				return false;
			}
		}

		try {
			dateTo = yearDateFormat.parse(textToDate.getText());
		} catch (ParseException e) {
			try {
				dateTo = standardDateFormat.parse(textToDate.getText());
			} catch (ParseException e1) {
				MessageBox mb = new MessageBox(getSite().getWorkbenchWindow()
						.getShell(), SWT.OK | SWT.ICON_ERROR);
				mb.setMessage(ERROR_INVALID_DATE_FORMAT);
				mb.setText(Messages.SearchView_AdvancedSearch);
				mb.open();
				textToDate.setFocus();
				textToDate.selectAll();
				return false;
			}
		}

		// to date less than from date
		if (dateTo.before(dateFrom)) {
			MessageBox mb = new MessageBox(getSite().getWorkbenchWindow()
					.getShell(), SWT.OK | SWT.ICON_ERROR);
			mb.setMessage(ERROR_INVALID_ORDER);
			mb.setText(Messages.SearchView_AdvancedSearch);
			mb.open();
			textFromDate.setFocus();
			textFromDate.selectAll();
			return false;
		}
		return result;
	}

	private void createEndpointArea(Composite parent) {
		String myEPPN = RBACSession.getInstance().getEPPN();
		epComposite = new Group(parent, SWT.NONE);
		GridLayout gl = new GridLayout(3, false);
		gl.marginWidth = 20;
		epComposite.setLayout(gl);
		epComposite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
				false));
		new Label(epComposite, SWT.NONE).setText(Messages.SearchView_SearchIn);
		if (myEPPN.equals("")) {
			rbEndpointStandard = new Button(epComposite, SWT.RADIO);
			rbEndpointStandard.setText(Messages.SearchView_MyProjects);
			rbEndpointStandard.setSelection(false);
			// rbEndpointPublic.setSelection(true);
			rbEndpointStandard.setEnabled(false);
		} else {
			rbEndpointStandard = new Button(epComposite, SWT.RADIO);
			rbEndpointStandard.setText(Messages.SearchView_MyProjects);
			rbEndpointStandard.setSelection(true);
			// rbEndpointPublic.setSelection(false);
			rbEndpointStandard.setEnabled(true);
		}
		rbEndpointPublic = new Button(epComposite, SWT.RADIO);
		rbEndpointPublic.setText(Messages.SearchView_PublicInRep);
		parent.redraw();
	}

	public int getTabFolderSelection() {
		return folder.getSelectionIndex();
	}

	public void setProjectSpecificSearchTags(
			ArrayList<TextGridProject> projectList) {
		metaDataMainArea.setProjectSpecificSearchTags(projectList);
		regardedMetadataProjects = projectList;
	}

	public ArrayList<TextGridProject> getRegardedMetadataProjects() {
		return regardedMetadataProjects;
	}
}