package info.textgrid.lab.search.ui.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.search.ui.views.messages"; //$NON-NLS-1$
	public static String ResultView_EM_OpenCommand;
	public static String ResultView_IM_Searching;
	public static String ResultView_IM_SearchNotStarted;
	public static String ResultView_Size;
	public static String SearchHelpView_EM_SearchHints1;
	public static String SearchHelpView_EM_SearchHints2;
	public static String SearchView_AdvancedSearch;
	public static String SearchView_And;
	public static String SearchView_BaselineEnc;
	public static String SearchView_Between;
	public static String SearchView_Browse;
	public static String SearchView_ContentTypes;
	public static String SearchView_ContentTypesWithMarker;
	public static String SearchView_DateExampleFrom;
	public static String SearchView_DateExampleTo;
	public static String SearchView_DateWithMarker;
	public static String SearchView_EM_OpenHelp1;
	public static String SearchView_EM_OpenView;
	public static String SearchView_ExampleString;
	public static String SearchView_Fulltext;
	public static String SearchView_IM_SelectSection;
	public static String SearchView_InOneDocument;
	public static String SearchView_ItemsFound;
	public static String SearchView_Languages;
	public static String SearchView_LanguagesWithMarker;
	public static String SearchView_LinkLabel;
	public static String SearchView_maximal;
	public static String SearchView_Metadata;
	public static String SearchView_MyProjects;
	public static String SearchView_PublicInRep;
	public static String SearchView_SearchBoth;
	public static String SearchView_SearchButtonLabel;
	public static String SearchView_SearchFulltext;
	public static String SearchView_SearchIn;
	public static String SearchView_Searching;
	public static String SearchView_SearchMetadata;
	public static String SearchView_SimpleSearch;
	public static String SearchView_TextTypes;
	public static String SearchView_TextTypesWithMarker;
	public static String SearchView_WordDistances;
	public static String SearchView_WordDistancesWithMarker;
	public static String SearchView_words;
	public static String SearchView_XPath;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
