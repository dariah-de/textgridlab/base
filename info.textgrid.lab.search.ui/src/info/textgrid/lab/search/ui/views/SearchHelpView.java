package info.textgrid.lab.search.ui.views;

import info.textgrid.lab.core.browserfix.TextGridLabBrowser;
import info.textgrid.lab.search.ui.Activator;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

public class SearchHelpView extends ViewPart {
	private Browser helpbrowser;
	
	@Override
	public void createPartControl(Composite parent) {
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		GridDataFactory groupsFactory = GridDataFactory.createFrom(gridData);

		Composite help1 = new Composite(parent, SWT.NONE);
		help1.setLayout(new GridLayout(2,false));

		groupsFactory.applyTo(help1);

		try {
    		helpbrowser = TextGridLabBrowser.createBrowser(help1);
    		groupsFactory.applyTo(helpbrowser);
    		
    		URL url = FileLocator.find(Activator.getDefault().getBundle(),
    				new Path("resources/SearchHints.html"), null); //$NON-NLS-1$
    		helpbrowser.setUrl(FileLocator.toFileURL(url).toString());
    	} catch (SWTError e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    Messages.SearchHelpView_EM_SearchHints1, e);
			Activator.getDefault().getLog().log(status);	
		} catch (IOException io) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    Messages.SearchHelpView_EM_SearchHints2, io);
			Activator.getDefault().getLog().log(status);				
		}
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
