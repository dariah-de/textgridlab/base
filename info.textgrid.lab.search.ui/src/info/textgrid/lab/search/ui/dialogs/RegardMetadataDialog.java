package info.textgrid.lab.search.ui.dialogs;

import java.util.ArrayList;

import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.search.DeferredTreeContentProvider;
import info.textgrid.lab.ui.core.utils.CheckboxProjectNavigator;

import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.PlatformUI;

public class RegardMetadataDialog extends TrayDialog{
	
	class RegardMetadataContentProvider extends DeferredTreeContentProvider {
		@Override 
		public boolean hasChildren(Object element) throws IllegalStateException {
			return false;
		}
		
		@Override
		public void inputChanged(org.eclipse.jface.viewers.Viewer viewer, Object oldInput, Object newInput) {
			super.inputChanged(viewer, oldInput, newInput);
			getManager().addUpdateCompleteListener(getCompletionJobListener());
		};
		
		protected IJobChangeListener getCompletionJobListener() {
			return new JobChangeAdapter() {

				public void done(IJobChangeEvent event) {
					if (event.getResult().isOK()){
						dialog.setProjectsChecked();
					}
				}
			};
		}	
	}
	
	private static RegardMetadataDialog dialog;
	private CheckboxProjectNavigator projectTree;
	private ArrayList<TextGridProject> regardedProjects = new ArrayList<TextGridProject>();
	private ArrayList<TextGridProject> projects;
	private int returnCode;
	
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Select Projects");  
	}
	
	protected RegardMetadataDialog(Shell shell) {
		super(shell);
	}
	
	public static RegardMetadataDialog openFilterDialog(ArrayList<TextGridProject> projects) {
		RegardMetadataDialog d = new RegardMetadataDialog(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell());
		d.setShellStyle(SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MAX | SWT.APPLICATION_MODAL);
		d.projects = projects;
		dialog = d;
		d.open();
		return d;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Group projectGroup = new Group(parent, SWT.BORDER);
		projectGroup.setText("Which project specific metadata should be regarded in the search GUI?");
		GridLayout gl = new GridLayout(1, false);
		if (parent.getLayoutData() instanceof GridData) {
			GridData gdParent = (GridData) parent.getLayoutData();
			gdParent.verticalIndent = 15;
		}
		projectGroup.setLayout(gl);
		final GridData gd = new GridData(GridData.FILL, GridData.FILL, true, true);
		gd.heightHint = 300;
		gd.widthHint = 500;
		projectGroup.setLayoutData(gd);
		projectTree = new CheckboxProjectNavigator(projectGroup);
		projectTree.setContentProvider(new RegardMetadataContentProvider());
		projectTree.getControl().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		return parent;
	}
	
	@Override
	protected void okPressed() {
		for (Object obj : projectTree.getCheckedElements()) {
			if (obj instanceof TextGridProject) 
				regardedProjects.add((TextGridProject) obj);
		}
		close();
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 3;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = SWT.CENTER;

		parent.setLayoutData(gridData);

		// select all button
		Button selectAllButton = createButton(parent, 2, "Select All", false);
		selectAllButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				for (TreeItem item : projectTree.getTree().getItems()) {
					item.setChecked(true);
				}
			}
		});
		
		// deselect all button
		Button deselectAllButton = createButton(parent, 3, "Deselect All", false);
		deselectAllButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				for (TreeItem item : projectTree.getTree().getItems()) {
					item.setChecked(false);
				}
			}
		});
	}
	
	public void setProjectsChecked() {
		for (TextGridProject project : projects) {
			projectTree.setChecked(project, true);
		}
	}
	
	public ArrayList<TextGridProject> getRegardedProjects() {
		return regardedProjects;
	}
}

