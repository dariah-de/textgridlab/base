package info.textgrid.lab.search.ui.handler;

import info.textgrid.lab.search.ui.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class SearchHintHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {

		try {
			PlatformUI
			.getWorkbench()
			.getActiveWorkbenchWindow()
			.getActivePage()
			.showView(
					"info.textgrid.lab.search.views.SearchHintView"); //$NON-NLS-1$
		} catch (PartInitException e) {
			IStatus status = new Status(IStatus.ERROR,
					Activator.PLUGIN_ID, Messages.SearchHintHandler_EM_OpenSearchHelp,
					e);
			Activator.getDefault().getLog().log(status);
		}

		return null;
	}
}