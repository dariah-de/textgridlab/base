package info.textgrid.lab.search.ui.handler;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.search.ui.handler.messages"; //$NON-NLS-1$
	public static String SearchHintHandler_EM_OpenSearchHelp;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
