package info.textgrid.lab.search.ui.handler;
import info.textgrid.lab.search.ui.views.SearchView;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PlatformUI;


public class StartSearchHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {


			IViewPart searchView = null;
			searchView = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage().findView("info.textgrid.lab.search.views.SearchView"); //$NON-NLS-1$
			if (searchView != null && searchView instanceof SearchView) {
				int index = ((SearchView) searchView).getTabFolderSelection();
				if (index == 0) 
					((SearchView) searchView).startSimpleSearch();
				else if (index == 1)
					((SearchView) searchView).startAdvancedSearch();
			}	
		return null;
	}
}

