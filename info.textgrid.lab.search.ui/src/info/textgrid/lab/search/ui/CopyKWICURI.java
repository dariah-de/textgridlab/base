package info.textgrid.lab.search.ui;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.AdapterUtils.AdapterNotFoundException;
import info.textgrid.lab.search.KWICEntry;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;


/**
 * A specialized Copy-URI handler that will copy the fragment information for
 * KWIC entries.
 * 
 * @author tv
 * 
 */
public class CopyKWICURI extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		if (currentSelection instanceof IStructuredSelection) {
			KWICEntry[] kwicEntries = AdapterUtils.getAdapters(((IStructuredSelection) currentSelection).toArray(),
					KWICEntry.class, false);

			try {

				List<URI> uris = Lists.newLinkedList();

				for (KWICEntry kwicEntry : kwicEntries) {
					URI objectUri;
					objectUri = AdapterUtils.getAdapterChecked(kwicEntry, TextGridObject.class).getURI();
					String path = kwicEntry.getPath();
					URI uri = new URI(objectUri.getScheme(), objectUri.getSchemeSpecificPart(), "xpath(" + path + ")");
					uris.add(uri);
				}
				String uriString = Joiner.on("\n").join(uris);
				
				if (uriString.length() > 0) {
					Clipboard clipboard = new Clipboard(HandlerUtil.getActiveShell(event)
							.getDisplay());
					clipboard.setContents(new Object[] { uriString.toString() },
							new Transfer[] { TextTransfer.getInstance() });
				}
			} catch (AdapterNotFoundException e) { // shouldn't happen
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			} catch (URISyntaxException e) { // shouldn't happen
				StatusManager.getManager().handle(
						new Status(IStatus.ERROR, Activator.PLUGIN_ID, Messages.CopyKWICURI_ErrorMessage1, e));
			}
		}

		return null;
	}

}
