package info.textgrid.lab.search.ui;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.search.ui.messages"; //$NON-NLS-1$
	public static String CopyKWICURI_ErrorMessage1;
	public static String SearchUICalendar_ErrorMessage1;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
