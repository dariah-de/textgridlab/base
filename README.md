# base

[info.textgrid.lab.feature.complete](info.textgrid.lab.feature.complete)
  ~ The feature for all of TextGridLab except for the P2 specific
    stuff. **Start here:** Also contains product and target platform.
[info.textgrid.lab.feature.complete.site](info.textgrid.lab.feature.complete.site)
  ~ The site project for building the TextGridLab using buckminster
[info.textgrid.lab.feature.supplements](info.textgrid.lab.feature.supplements)
  ~ Eclipse plug-ins required by the feature which are not
    contained in an Eclipse feature we require
[info.textgrid.lab.feature.update.p2](info.textgrid.lab.feature.update.p2)
  ~ Feature bundling required P2 stuff
[info.textgrid.lab.update.config](info.textgrid.lab.update.config)
  ~ Plugin with the TextGridLab specific P2 configuration
[README.html](README.html)
  ~ See [](README.html)


## Change Version

To change the version use the [Tycho versions plugin](https://www.eclipse.org/tycho/sitedocs/tycho-release/tycho-versions-plugin/set-version-mojo.html)

	mvn tycho-versions:set-version -DnewVersion=4.0.0-SNAPSHOT
