#!/bin/sh

# fix tglab webkit browser on wayland
export GDK_BACKEND=x11

# find absolute path to lab, even if links are involved
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

$SCRIPTPATH/textgridlab

