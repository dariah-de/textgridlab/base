package info.textgrid.lab.feedback.ui.bugreport;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.feedback.ui.bugreport.messages"; //$NON-NLS-1$
	public static String BugInfoDialog_AllFiles;
	public static String BugInfoDialog_CannotOpenLogView;
	public static String BugInfoDialog_CopyLog;
	public static String BugInfoDialog_CopyLogTo;
	public static String BugInfoDialog_ErrorLogView;
	public static String BugInfoDialog_LogFiles;
	public static String BugInfoDialog_SaveLog;
	public static String BugInfoDialog_Title;
	public static String BugInfoDialog_WriteError;
	public static String BugInfoDialogArea_bottomMessage;
	public static String BugInfoDialogArea_CouldNotReadLog;
	public static String BugInfoDialogArea_introMessage;
	public static String BugInfoDialogArea_LogHides;
	public static String BugInfoDialogArea_logLink;
	public static String BugInfoDialogArea_LogTooltip;
	public static String BugInfoDialogArea_ReadingLogFile;
	public static String BugInfoDialogArea_step1;
	public static String BugInfoDialogArea_step2;
	public static String BugInfoDialogArea_step3;
	public static String BugInfoDialogArea_step4;
	public static String BugInfoDialogArea_step5;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
