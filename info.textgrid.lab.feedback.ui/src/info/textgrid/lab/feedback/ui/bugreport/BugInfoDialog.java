/**
 * 
 */
package info.textgrid.lab.feedback.ui.bugreport;

import info.textgrid.lab.feedback.ui.Activator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

/**
 * @author tv
 * 
 */
public class BugInfoDialog extends TrayDialog {

	private static final int COPY_ID = -1;
	private static final int OPEN_VIEW_ID = -2;
	private static final int SAVE_LOG_ID = -3;
	private BugInfoDialogArea dialogArea;

	protected BugInfoDialog(IShellProvider parentShell) {
		super(parentShell);
		initialize();
	}

	public BugInfoDialog(Shell parentShell) {
		super(parentShell);
		initialize();
	}

	protected void initialize() {
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, COPY_ID, Messages.BugInfoDialog_CopyLog, false);
		createButton(parent, OPEN_VIEW_ID, Messages.BugInfoDialog_ErrorLogView,
				false);
		createButton(parent, SAVE_LOG_ID, Messages.BugInfoDialog_SaveLog, false);
		createButton(parent, IDialogConstants.CLOSE_ID,
				IDialogConstants.CLOSE_LABEL, true);
	}

	public void saveLogAs() {
		FileDialog dialog = new FileDialog(getShell(), SWT.SAVE);
		dialog.setFileName("textgridlab.log"); //$NON-NLS-1$
		dialog.setText(Messages.BugInfoDialog_CopyLogTo);
		dialog.setFilterExtensions(new String[] { "*.log", "*" }); //$NON-NLS-1$ //$NON-NLS-2$
		dialog.setFilterNames(new String[] {Messages.BugInfoDialog_LogFiles, Messages.BugInfoDialog_AllFiles});
		String target = dialog.open();
		if (target != null) {
			IPath log = Platform.getLogFileLocation();
			FileChannel inChannel = null;
			FileChannel outChannel = null;
			try {
				inChannel = new FileInputStream(log.toFile()).getChannel();
				outChannel = new FileOutputStream(target).getChannel();
				inChannel.transferTo(0, inChannel.size(), outChannel);
			} catch (FileNotFoundException e) {
				Activator.getDefault().handleProblem(IStatus.ERROR,
						NLS.bind(Messages.BugInfoDialogArea_LogHides, log), e);
			} catch (IOException e) {
				Activator.getDefault().handleProblem(IStatus.ERROR,
						NLS.bind(Messages.BugInfoDialog_WriteError, target), e);
			} finally {
				try {
					if (inChannel != null)
						inChannel.close();
					if (outChannel != null)
						outChannel.close();
				} catch (IOException e) {
					Activator.getDefault().handleProblem(IStatus.ERROR, e);
				}
			}

		}

	}

	@Override
	protected Control createDialogArea(Composite parent) {
		dialogArea = new BugInfoDialogArea(parent, SWT.NONE, this);
		dialogArea.updateLogText();
		if (getShell() != null)
			getShell().setText(Messages.BugInfoDialog_Title);
		return dialogArea;
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.CLOSE_ID)
			this.close();
		else if (buttonId == OPEN_VIEW_ID)
			showLogView();
		else if (buttonId == SAVE_LOG_ID)
			saveLogAs();
		else if (buttonId == COPY_ID)
			dialogArea.copy();
	}

	public void showLogView() {
		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage().showView(
							"org.eclipse.pde.runtime.LogView"); //$NON-NLS-1$
		} catch (PartInitException e) {
			getButton(OPEN_VIEW_ID).setEnabled(false);
			Activator.getDefault().handleProblem(IStatus.WARNING,
					Messages.BugInfoDialog_CannotOpenLogView, e);
		}
	}

}
