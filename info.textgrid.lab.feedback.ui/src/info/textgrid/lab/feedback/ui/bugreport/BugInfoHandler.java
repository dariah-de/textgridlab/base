package info.textgrid.lab.feedback.ui.bugreport;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.handlers.HandlerUtil;

public class BugInfoHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		BugInfoDialog bugInfoDialog = new BugInfoDialog(HandlerUtil.getActiveShell(event));
		bugInfoDialog.open();
		
		return null;
	}

}
