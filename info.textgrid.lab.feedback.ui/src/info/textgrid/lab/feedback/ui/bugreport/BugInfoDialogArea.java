/**
 * 
 */
package info.textgrid.lab.feedback.ui.bugreport;

import info.textgrid.lab.feedback.ui.Activator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.CharBuffer;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

/**
 * The dialog area (without the buttons) of our {@link BugInfoDialog}.
 * 
 * @author tv
 * 
 */
public class BugInfoDialogArea extends Composite {

	/**
	 * Used for the links in this dialog. The link elements should contain a
	 * hyperlink in <code>&lt;a&gt;</code> tags, with an optional
	 * <code>href</code> attribute. This adaptor handles the links as follows:
	 * 
	 * <dl>
	 * <dt>#save</dt>
	 * <dd>Opens the save as dialog to copy the log to hard disk.</dd>
	 * <dt>#view</dt>
	 * <dd>Opens the Error Log View.</dd>
	 * <dt>anything else</dt>
	 * <dd>is interpreted as an {@link URL} and opened in a browser window.</dd>
	 * </dl>
	 * 
	 * @author tv
	 */
	protected final class LinkSelectionAdapter extends SelectionAdapter {

		protected void openBrowser(String url) {
			try {
				PlatformUI.getWorkbench().getBrowserSupport().createBrowser(
						"info.textgrid.lab.feedback.ui.bugreport") //$NON-NLS-1$
						.openURL(new URL(url));
			} catch (PartInitException e1) {
				Activator.getDefault().handleProblem(IStatus.WARNING, e1);
			} catch (MalformedURLException e1) {
				Activator.getDefault().handleProblem(IStatus.ERROR, e1);
			}
		}

		public void widgetSelected(SelectionEvent event) {
			if (event.text.equals("#save")) //$NON-NLS-1$
				dialog.saveLogAs();
			else if (event.text.equals("#view")) //$NON-NLS-1$
				dialog.showLogView();
			else
				openBrowser(event.text);
		}
	}

	private Link logLink = null;
	private Text logArea = null;
	private BugInfoDialog dialog;
	private Label introMessage;
	private Link step1;
	private Label step2;
	private SelectionListener linkSelectionAdaptor = new LinkSelectionAdapter();
	private Label step3;
	private Link step4;
	private Label step5;
	private Link options;

	public BugInfoDialogArea(Composite parent, int style, BugInfoDialog dialog) {
		super(parent, style);
		this.dialog = dialog;
		initialize();
	}

	private void initialize() {
		// This is used for all the labels and links.
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = GridData.CENTER;
		GridDataFactory labelsFactory = GridDataFactory.createFrom(gridData);

		introMessage = new Label(this, SWT.WRAP);
		introMessage
				.setText(Messages.BugInfoDialogArea_introMessage);
		labelsFactory.applyTo(introMessage);

		step1 = new Link(this, SWT.NONE);
		step1
				.setText(Messages.BugInfoDialogArea_step1);
		labelsFactory.applyTo(step1);
		step1.addSelectionListener(linkSelectionAdaptor);

		step2 = new Label(this, SWT.WRAP);
		step2
				.setText(Messages.BugInfoDialogArea_step2);
		labelsFactory.applyTo(step2);

		step3 = new Label(this, SWT.WRAP);
		step3.setText(Messages.BugInfoDialogArea_step3);
		labelsFactory.applyTo(step3);

		step4 = new Link(this, SWT.WRAP);
		step4
				.setText(Messages.BugInfoDialogArea_step4);
		labelsFactory.applyTo(step4);
		step4.addSelectionListener(linkSelectionAdaptor);

		step5 = new Label(this, SWT.WRAP);
		step5.setText(Messages.BugInfoDialogArea_step5);
		labelsFactory.applyTo(step5);

		GridData logAreaGridData = new GridData();
		logAreaGridData.horizontalAlignment = GridData.FILL;
		logAreaGridData.grabExcessVerticalSpace = true;
		logAreaGridData.grabExcessHorizontalSpace = true;
		logAreaGridData.heightHint = 300;
		logAreaGridData.widthHint = 500;
		logAreaGridData.verticalAlignment = GridData.FILL;
		
		logArea = new Text(this, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER | SWT.READ_ONLY);
		logArea.setEditable(false);
		logArea.setToolTipText(Messages.BugInfoDialogArea_LogTooltip);
		logArea.setLayoutData(logAreaGridData);

		logLink = new Link(this, SWT.NONE);
		GridData logLinkGridData = new GridData();
		logLinkGridData.horizontalAlignment = SWT.FILL;
		logLinkGridData.grabExcessHorizontalSpace = true;
		logLink.addSelectionListener(linkSelectionAdaptor);

		options = new Link(this, SWT.WRAP);
		options
				.setText(Messages.BugInfoDialogArea_bottomMessage);
		labelsFactory.applyTo(options);
		options.addSelectionListener(linkSelectionAdaptor);

		setSize(new Point(434, 547));
		setLayout(new GridLayout());
	}

	protected void saveAs() {
		dialog.saveLogAs(); // FIXME ugly
	}

	public void setLogText(String logText) {
		logArea.setText(logText);
	}

	public String getLogText() {
		return logArea.getText();
	}

	/*
	 * Sets the editor area to the contents of the log file.
	 */
	public void updateLogText() {
		final File logFile = Platform.getLogFileLocation().toFile();
		logLink.setText(NLS.bind(Messages.BugInfoDialogArea_logLink, logFile
				.toString()));
		logLink.setToolTipText(logFile.toString());
		new Job(Messages.BugInfoDialogArea_ReadingLogFile) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {

				try {
					FileReader fileReader = new FileReader(logFile);
					try {
						final CharBuffer buffer = CharBuffer.allocate(logFile
								.length() <= Integer.MAX_VALUE ? (int) logFile
								.length() : Integer.MAX_VALUE);
						fileReader.read(buffer);
						buffer.rewind();
						new UIJob(Messages.BugInfoDialogArea_ReadingLogFile) {
							public IStatus runInUIThread(
									IProgressMonitor monitor) {
								setLogText(buffer.toString());
								return Status.OK_STATUS;
							};
						}.schedule();

					} finally {
						if (fileReader != null)
							fileReader.close();
					}
				} catch (FileNotFoundException e) {
					return Activator.getDefault().handleProblem(
							IStatus.ERROR,
							NLS.bind(Messages.BugInfoDialogArea_LogHides,
									logFile.getAbsolutePath()), e);
				} catch (IOException e) {
					return Activator.getDefault().handleProblem(
							IStatus.ERROR,
							NLS.bind(
									Messages.BugInfoDialogArea_CouldNotReadLog,
									logFile.getAbsolutePath()), e);
				}
				return Status.OK_STATUS;
			}
		}.schedule();
	}

	public void copy() {
		Point selection = logArea.getSelection();
		if (selection.x == selection.y) // empty selection
			logArea.selectAll();
		logArea.copy();
		logArea.setSelection(selection);
	}

} // @jve:decl-index=0:visual-constraint="10,10"
