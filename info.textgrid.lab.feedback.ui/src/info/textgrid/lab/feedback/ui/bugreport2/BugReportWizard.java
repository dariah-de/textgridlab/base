package info.textgrid.lab.feedback.ui.bugreport2;

import info.textgrid.lab.feedback.ui.Activator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.internal.ConfigurationInfo;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.collect.Lists;
import com.google.common.io.Files;

@SuppressWarnings("restriction")
public class BugReportWizard extends Wizard {

	private static final String BUGHANDLER_URL = "https://textgridlab.org/bugreport/bughandler.py"; //$NON-NLS-1$
	private BasicReportPage basicPage;
	private BugDataPage dataPage;
	private volatile String systemSummary;
	private String systemLog;
	private WizardDialog dialog;

	public BugReportWizard() {
		setWindowTitle(Messages.BugReportWizard_WindowTitle);
	}

	@Override
	public void addPages() {
		basicPage = new BasicReportPage();
		dataPage = new BugDataPage();
		addPage(basicPage);
		addPage(dataPage);
	}

	@Override
	public boolean performFinish() {
		final String subject = basicPage.subjectText.getText();
		final String description = basicPage.descriptionText.getText();
		final String name = basicPage.name.getText();
		final String mail = basicPage.mail.getText();
		final String eppn = basicPage.eppn.getText();

		final File config;
		final File log;
		try {
			config = dataPage.getTempConfFile();
			log = dataPage.getTempLogFile();

			dialog.run(true, true, new IRunnableWithProgress() {

				@Override
				public void run(final IProgressMonitor monitor) throws InvocationTargetException,
						InterruptedException {
					final SubMonitor progress = SubMonitor.convert(monitor, Messages.BugReportWizard_PostingProgress, 30);
					if (progress.isCanceled())
						return;

					final List<Part> parts = Lists.newArrayList();

					parts.add(new StringPart("subject", subject, "UTF-8")); //$NON-NLS-1$ //$NON-NLS-2$
					parts.add(new StringPart("description", description, "UTF-8")); //$NON-NLS-1$ //$NON-NLS-2$
					parts.add(new StringPart("name", name, "UTF-8")); //$NON-NLS-1$ //$NON-NLS-2$
					parts.add(new StringPart("mail", mail, "UTF-8")); //$NON-NLS-1$ //$NON-NLS-2$
					parts.add(new StringPart("eppn", eppn, "UTF-8")); //$NON-NLS-1$ //$NON-NLS-2$

					progress.worked(5);
					if (progress.isCanceled())
						return;

					try {
						if (config != null)
							parts.add(new FilePart("config", config, //$NON-NLS-1$
									"text/plain", "UTF-8")); //$NON-NLS-1$ //$NON-NLS-2$
						if (log != null)
							parts.add(new FilePart("log", log, "text/plain", //$NON-NLS-1$ //$NON-NLS-2$
									"UTF-8")); //$NON-NLS-1$

						for (final File attachment : dataPage.getFiles()) {
							parts.add(new FilePart("attach", attachment)); //$NON-NLS-1$
						}

					} catch (final FileNotFoundException e) {
						throw new InvocationTargetException(e);
					}

					progress.worked(5);
					if (progress.isCanceled())
						return;

					final PostMethod post = new PostMethod(BUGHANDLER_URL);
					post.setRequestEntity(new MultipartRequestEntity(parts.toArray(new Part[0]), post.getParams()));
					final HttpClient client = new HttpClient();

					progress.worked(5);
					if (progress.isCanceled())
						return;
					try {
						client.executeMethod(post);
						System.out.println(post.getResponseBodyAsString());
						progress.worked(15);
					} catch (final IOException e) {
						throw new InvocationTargetException(e);
					}

				}
			});

		} catch (final IOException e) {
			StatusManager.getManager().handle( new Status(IStatus.ERROR, Activator.PLUGIN_ID, Messages.BugReportWizard_TempFileError + e.getLocalizedMessage(), e));
		} catch (final InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	public void show(final Shell shell) {
		dialog = new WizardDialog(shell == null? getShell() : shell, this) {

			@Override
			protected void createButtonsForButtonBar(Composite parent) {
				Button saveLogButton = createButton(parent, 50, Messages.BugReportWizard_SaveLog, false);
				saveLogButton.addSelectionListener(new SelectionAdapter() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						dataPage.saveGeneratedStuff();
					}
					
				});
				saveLogButton.setAlignment(SWT.LEAD);
				saveLogButton.setToolTipText(Messages.BugReportWizard_SaveLogTooltip);
				super.createButtonsForButtonBar(parent);
			}
			
		};
		dialog.setBlockOnOpen(false);
		dialog.create();
		dialog.open();

		final Job job = new Job(Messages.BugReportWizard_CollectingData) {


			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				final IPath logFileLocation = Platform.getLogFileLocation();
				try {
					systemLog = Files.toString(logFileLocation.toFile(), Charset.defaultCharset());
				} catch (final IOException e) {
					systemLog = e.toString();
				}
				systemSummary = ConfigurationInfo.getSystemSummary();
				return Status.OK_STATUS;
			}
		};
		job.addJobChangeListener(new JobChangeAdapter() {

			@Override
			public void done(final IJobChangeEvent event) {
				if (event.getResult().isOK()) {
				final UIJob uiJob = new UIJob("") { //$NON-NLS-1$

					@Override
					public IStatus runInUIThread(final IProgressMonitor monitor) {
						if (systemSummary != null && dataPage != null && !dataPage.getControl().isDisposed()) {
							dataPage.setSummary(systemSummary, systemLog);
						}
						return Status.OK_STATUS;
					}
				};
				uiJob.setSystem(true);
				uiJob.schedule();
				}
			}

		});
		job.schedule();
	}


}
