package info.textgrid.lab.feedback.ui.bugreport2;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.handlers.HandlerUtil;

public class ShowBugWizard extends AbstractHandler implements IHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final BugReportWizard wizard = new BugReportWizard();
		wizard.show(HandlerUtil.getActiveShell(event));
		return null;
	}

}
