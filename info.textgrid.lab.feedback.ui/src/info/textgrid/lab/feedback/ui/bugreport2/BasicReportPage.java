package info.textgrid.lab.feedback.ui.bugreport2;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.namespaces.middleware.tgauth.UserDetail;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class BasicReportPage extends WizardPage {
	protected Text descriptionText;
	protected Text name;
	protected Text mail;
	protected Text eppn;
	protected Text subjectText;

	/**
	 * Create the wizard.
	 */
	public BasicReportPage() {
		super(Messages.BasicReportPage_PageTitle);
		setTitle(Messages.BasicReportPage_this_title);
		setDescription(Messages.BasicReportPage_this_description);
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	@Override
	public void createControl(final Composite parent) {
		final Composite container = new Composite(parent, SWT.NULL);

		setControl(container);
		container.setLayout(new GridLayout(1, false));

		final Label subjectLabel = new Label(container, SWT.NONE);
		subjectLabel.setText(Messages.BasicReportPage_subjectLabel_text);

		subjectText = new Text(container, SWT.BORDER);
		subjectText.setMessage(Messages.BasicReportPage_subjectText_message);
		subjectText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		final Label descriptionLabel = new Label(container, SWT.NONE);
		descriptionLabel.setText(Messages.BasicReportPage_descriptionLabel_text);

		descriptionText = new Text(container, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		descriptionText.setText(Messages.BasicReportPage_descriptionText_text);
		descriptionText.setMessage(Messages.BasicReportPage_descriptionText_message);
		final GridData gd_descriptionText = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_descriptionText.heightHint = 200;
		descriptionText.setLayoutData(gd_descriptionText);

		final Group grpSender = new Group(container, SWT.NONE);
		grpSender.setText(Messages.BasicReportPage_grpSender_text);
		grpSender.setLayout(new GridLayout(2, true));
		final GridData gd_grpSender = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_grpSender.widthHint = 400;
		grpSender.setLayoutData(gd_grpSender);

		name = new Text(grpSender, SWT.BORDER);
		name.setMessage(Messages.BasicReportPage_name_message);
		name.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		mail = new Text(grpSender, SWT.BORDER);
		mail.setMessage(Messages.BasicReportPage_mail_message);
		mail.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(grpSender, SWT.NONE);

		eppn = new Text(grpSender, SWT.BORDER);
		eppn.setMessage(Messages.BasicReportPage_eppn_message);
		eppn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		final Label lblNewLabel_2 = new Label(grpSender, SWT.WRAP);
		lblNewLabel_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		lblNewLabel_2.setText(Messages.BasicReportPage_lblNewLabel_2_text);

		final RBACSession rbac = RBACSession.getInstance();
		final String currentEPPN = rbac.getEPPN();
		if (!"".equals(currentEPPN)) { //$NON-NLS-1$
			final UserDetail userDetail = rbac.getDetails(currentEPPN);
			eppn.setText(currentEPPN);
			if (userDetail.getName() != null)
				name.setText(userDetail.getName());
			if (userDetail.getMail() != null)
				mail.setText(userDetail.getMail());
		}

	}
}
