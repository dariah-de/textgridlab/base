package info.textgrid.lab.feedback.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.feedback.ui"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Handles the given exception in a default way.
	 * 
	 * @param severity	one of the {@link IStatus} severity constants.
	 * @param exception	the exception to cope with
	 * 
	 */
	public void handleProblem(int severity, Throwable exception) {
		handleProblem(severity, null, exception);
	}

	/**
	 * Handles the given exception in a default way.
	 * 
	 * @param severity	one of the {@link IStatus} severity constants.
	 * @param message	the (localized) error message, may be null if an exception is given
	 * @param exception	the exception that caused the error.		
	 */
	public IStatus handleProblem(int severity, String message, Throwable exception) {
		
		if (message == null || "".equals(message)) { //$NON-NLS-1$
			if (exception != null) {
				message = exception.getLocalizedMessage();
				if (message == null || "".equals(message)) //$NON-NLS-1$
					message = exception.getClass().getName();
			}  else {
				message = Messages.Activator_UnknownError;
			}
		}
		Status status = new Status(severity, PLUGIN_ID, message, exception);
		StatusManager.getManager().handle(status);
		return status;
	}

}
