package info.textgrid.lab.update.config;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.equinox.p2.ui.Policy;
import org.eclipse.ui.statushandlers.StatusManager;

public class P2PolicyConfigurer {
	private Policy policy;

	public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
		policy.setRestartPolicy(Policy.RESTART_POLICY_PROMPT);
		StatusManager.getManager().handle(
				new Status(IStatus.INFO, "info.textgrid.lab.update.config",
						"Configured the p2 UI policy."));
	}

}
