package info.textgrid.lab.projectadmin.dialogs;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.projectadmin.dialogs.messages"; //$NON-NLS-1$
	public static String CreateProjectDialog_13;
	public static String CreateProjectDialog_14;
	public static String CreateProjectDialog_15;
	public static String CreateProjectDialog_16;
	public static String CreateProjectDialog_17;
	public static String CreateProjectDialog_18;
	public static String CreateProjectDialog_19;
	public static String CreateProjectDialog_GeneralHint;
	public static String CreateProjectDialog_NewProject;
	public static String CreateProjectDialog_NewProjectText;
	public static String CreateProjectDialog_OpenUM;
	public static String CreateProjectDialog_ProjectDescriptionLabel;
	public static String CreateProjectDialog_ProjectNameLabel;
	public static String CreateProjectDialog_StatusCreating;
	public static String CreateProjectDialog_StatusHint;
	public static String CreateProjectDialog_StatusHintMessage;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
