package info.textgrid.lab.projectadmin.dialogs;

import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.navigator.DeferredTreeContentProvider;
import info.textgrid.lab.navigator.DeferredTreeContentProvider.IDeferredTreeListener;
import info.textgrid.lab.navigator.NaviView;

import java.rmi.RemoteException;
import java.text.MessageFormat;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

/**
 * Creation of a textgrid Project. Modal Dialogue. Accesses RBAC through
 * TGProject.
 * 
 * @author martin
 * 
 */
public class CreateProjectDialog extends TrayDialog {
	Group g;
	Text newProjectName;
	Text newProjectDescription;
	TextGridProject newtgp;
	Label statusLabel;
	Button closeButton;
	Button createButton;
	final Shell parent;
	Button centerUM;
	IDeferredTreeListener naviListener;

	public CreateProjectDialog(Shell shell) {
		super(shell);
		// TODO Auto-generated constructor stub
		parent = shell;
		initialize();
	}

	public CreateProjectDialog(IShellProvider parentShell) {
		super(parentShell);
		// TODO Auto-generated constructor stub
		parent = null;
		initialize();
	}

	protected void initialize() {
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		parent.setLayout(new GridLayout(3, false)); // Extrawurst for centerUM
		centerUM = new Button(parent, SWT.CHECK);
		centerUM.setText(Messages.CreateProjectDialog_OpenUM);
		centerUM.setSelection(true);
		closeButton = createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		createButton = createButton(parent, -123, "Create", true); //$NON-NLS-1$
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.CANCEL_ID)
			this.close();
		else if (buttonId == -123) {
			createButton.setEnabled(false);
			boolean created = evaluateAndCreate();
			if (created) {
				this.close();
			} else
				createButton.setEnabled(true);
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		parent.getShell().setText(Messages.CreateProjectDialog_NewProject);
		GridData parentGD = new GridData();
		parentGD.horizontalAlignment = GridData.FILL;
		parentGD.grabExcessVerticalSpace = true;
		parentGD.grabExcessHorizontalSpace = true;
		parentGD.heightHint = 300;
		parentGD.widthHint = 400;
		parentGD.verticalAlignment = GridData.FILL;

		g = new Group(parent, SWT.NONE);
		g.setLayout(new GridLayout(1, false));
		g.setText(Messages.CreateProjectDialog_NewProjectText);

		Label hint = new Label(g, SWT.WRAP);
		hint.setText(MessageFormat
				.format(Messages.CreateProjectDialog_GeneralHint,
						TextGridProject
								.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER)));

		Composite c = new Composite(g, SWT.FILL);
		GridData cGD = new GridData();
		cGD.horizontalAlignment = GridData.FILL;
		cGD.grabExcessVerticalSpace = true;
		cGD.grabExcessHorizontalSpace = true;
		cGD.heightHint = 100;
		cGD.widthHint = 400;
		cGD.verticalAlignment = GridData.FILL;
		c.setLayout(new GridLayout(2, false));
		c.setLayoutData(cGD);

		Label nameLabel = new Label(c, SWT.LEFT);
		nameLabel.setText(Messages.CreateProjectDialog_ProjectNameLabel);
		newProjectName = new Text(c, SWT.SINGLE | SWT.LEFT);
		GridData gd1 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		newProjectName.setLayoutData(gd1);

		Label descLabel = new Label(c, SWT.LEFT);
		descLabel.setText(Messages.CreateProjectDialog_ProjectDescriptionLabel);
		newProjectDescription = new Text(c, SWT.SINGLE | SWT.LEFT);
		GridData gd2 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		newProjectDescription.setLayoutData(gd2);

		Label stLabel = new Label(c, SWT.LEFT);
		stLabel.setText(""); //$NON-NLS-1$
		statusLabel = new Label(c, SWT.WRAP);

		GridData stGD = new GridData();
		stGD.horizontalAlignment = GridData.FILL;
		stGD.grabExcessVerticalSpace = true;
		stGD.grabExcessHorizontalSpace = true;
		stGD.heightHint = 80;
		stGD.widthHint = 300;
		stGD.verticalAlignment = GridData.FILL;

		statusLabel.setText(Messages.CreateProjectDialog_StatusHint);
		statusLabel
				.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true));

		setNaviViewListener();

		return g;
	}

	private boolean evaluateAndCreate() {
		if (newProjectName.getText().length() > 0
				&& newProjectDescription.getText().length() > 0) {

			informUser(Messages.CreateProjectDialog_StatusCreating);

			startJob(newProjectName.getText(), newProjectDescription.getText(),
					centerUM.getSelection());
			return true;
		} else {
			informUser(Messages.CreateProjectDialog_StatusHintMessage);
			return false;
		}
	}

	public void informUser(String message) {
		statusLabel.setText(message);
		statusLabel.pack();
	}

	private void startJob(final String name, final String description,
			final boolean centerUM) {
		Job currentJob = new Job(Messages.CreateProjectDialog_13) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(Messages.CreateProjectDialog_14,
						IProgressMonitor.UNKNOWN);
				String errorMessage = Messages.CreateProjectDialog_15;
				try {

					newtgp = TextGridProject
							.createNewProject(name, description);

					if (centerUM) {
						parent.getDisplay().asyncExec(new Runnable() {
							public void run() {
								showUM();
							}
						});
					}
					return Status.OK_STATUS;

				} catch (RemoteException e) {
					OnlineStatus.netAccessFailed(MessageFormat.format(
							Messages.CreateProjectDialog_16, name), e);
					errorMessage = e.getMessage();
				}

				return Status.OK_STATUS;
			}
		};
		currentJob.setPriority(Job.LONG);
		currentJob.schedule();

	}

	public void showUM() {
		try {
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchPage wbPage = wb.getActiveWorkbenchWindow()
					.getActivePage();
			if (wbPage == null)
				wb.getActiveWorkbenchWindow().openPage(
						Messages.CreateProjectDialog_17, null);
			PlatformUI.getWorkbench().showPerspective(
					Messages.CreateProjectDialog_18,
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
		} catch (WorkbenchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void centerNavigatorOnNewProject(TextGridProject newtgp) {
		IViewPart naviView = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.findView(Messages.CreateProjectDialog_19);
		if (naviView != null && naviView instanceof NaviView && newtgp != null) {
			NaviView nVinstance = (NaviView) naviView;
			ISelection sel = new StructuredSelection(newtgp);
			nVinstance.setSelection(sel, true);
			DeferredTreeContentProvider.removeListener(naviListener);
		}
	}

	private void setNaviViewListener() {
		DeferredTreeContentProvider
				.addListener(naviListener = new IDeferredTreeListener() {
					public void deferredTreeDone() {
						centerNavigatorOnNewProject(newtgp);
					}
				});
	}

}
