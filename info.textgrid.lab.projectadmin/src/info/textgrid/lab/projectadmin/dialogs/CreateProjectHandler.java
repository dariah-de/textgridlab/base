package info.textgrid.lab.projectadmin.dialogs;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.handlers.HandlerUtil;

public class CreateProjectHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		CreateProjectDialog cpDialog = new CreateProjectDialog(HandlerUtil.getActiveShell(event));
		cpDialog.open();
		
		return null;
	}

}
