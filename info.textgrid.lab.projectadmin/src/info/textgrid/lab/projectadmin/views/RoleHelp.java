package info.textgrid.lab.projectadmin.views;

import info.textgrid.lab.core.browserfix.TextGridLabBrowser;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.projectadmin.Activator;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.part.ViewPart;

public class RoleHelp extends ViewPart {
	private Browser helpbrowser;
	private static final String LINESEPARATOR = System.getProperty("line.separator"); //$NON-NLS-1$
	// this does not work on windows, see https://bugs.eclipse.org/bugs/show_bug.cgi?id=97077
	
	@Override
	public void createPartControl(Composite parent) {
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		GridDataFactory groupsFactory = GridDataFactory.createFrom(gridData);

		Composite help1 = new Composite(parent, SWT.NONE);
		help1.setLayout(new GridLayout(2,false));

		groupsFactory.applyTo(help1);

		try {
    		helpbrowser = TextGridLabBrowser.createBrowser(help1);
    		groupsFactory.applyTo(helpbrowser);
    		
    		URL url = FileLocator.find(Activator.getDefault().getBundle(),
    				new Path(Messages.RoleHelp_rolehelpURL), null);
    		helpbrowser.setUrl(FileLocator.toFileURL(url).toString());
    	} catch (SWTError e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    "Could not display Role Help in Browser", e); //$NON-NLS-1$
			Activator.getDefault().getLog().log(status);	
		} catch (IOException io) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    "Rolehelp: could not display, somebody tampered with plugin", io); //$NON-NLS-1$
			Activator.getDefault().getLog().log(status);				
		}
		
		Group help1a = new Group (help1, SWT.NONE);
		help1a.setLayout(new GridLayout(1,false));
		help1a.setText(Messages.RoleHelp_StandardText);
		GridData gd3a = new GridData(SWT.CENTER, SWT.TOP, false, true);
		gd3a.heightHint = 500;
		help1a.setLayoutData(gd3a);
		
		Table t1a = new Table(help1a,SWT.NONE );
		t1a.setLinesVisible (true);
		t1a.setHeaderVisible (true);
		GridData gd3b = new GridData(SWT.CENTER, SWT.TOP, true, true);
		gd3b.heightHint = 500;
		t1a.setLayoutData(gd3b);

		
		TableColumn c1a1 = new TableColumn (t1a, SWT.NONE);
		c1a1.setText (Messages.RoleHelp_Role);
		TableColumn c1a2 = new TableColumn (t1a, SWT.NONE);
		c1a2.setText (Messages.RoleHelp_Resources);
		TableColumn c1a3 = new TableColumn (t1a, SWT.NONE);
		c1a3.setText (Messages.RoleHelp_Projects);

		
		
		TableItem itemPL = new TableItem (t1a, SWT.WRAP);
		itemPL.setText (0, TextGridProject.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER));
		itemPL.setText (1, Messages.RoleHelp_delegate+LINESEPARATOR+Messages.RoleHelp_publish);
		itemPL.setText (2, Messages.RoleHelp_delegate);
		TableItem itemAd = new TableItem (t1a, SWT.NONE);
		itemAd.setText (0, TextGridProject.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_ADMINISTRATOR));
		itemAd.setText (1, Messages.RoleHelp_delete);
		itemAd.setText (2, ""); //$NON-NLS-1$
		TableItem itemBa= new TableItem (t1a, SWT.WRAP);
		itemBa.setText (0, TextGridProject.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_EDITOR));
		itemBa.setText (1, Messages.RoleHelp_read +LINESEPARATOR+Messages.RoleHelp_write);
		itemBa.setText (2, Messages.RoleHelp_create);
		TableItem itemBo = new TableItem (t1a, SWT.NONE);
		itemBo.setText (0, TextGridProject.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_WATCHER));
		itemBo.setText (1, Messages.RoleHelp_read);
		itemBo.setText (2, ""); //$NON-NLS-1$
	
		for (int i=0; i<3; i++) {
			t1a.getColumn (i).pack ();
		}	
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
