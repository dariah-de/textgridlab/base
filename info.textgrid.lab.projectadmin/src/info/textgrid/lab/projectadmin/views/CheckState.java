package info.textgrid.lab.projectadmin.views;

import java.io.IOException;

import info.textgrid.lab.projectadmin.Activator;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

public enum CheckState {
	ONROLE("checked.png", true),  //$NON-NLS-1$
	OFFROLE("unchecked.png", false),  //$NON-NLS-1$
	DISABLEDONROLE("disabled-checked.png", true),  //$NON-NLS-1$
	DISABLEDOFFROLE("disabled-unchecked.png", false),  //$NON-NLS-1$
	MODIFIEDONROLE("modified-checked.png", true),  //$NON-NLS-1$
	MODIFIEDOFFROLE("modified-unchecked.png", false); //$NON-NLS-1$

	private Image icon;
	private boolean checked;

	CheckState(String iconFileName, boolean checked) {
		this.checked = checked;
		try {
			icon = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path("icons/" //$NON-NLS-1$
							+ iconFileName), null))).createImage();

		} catch (IOException e1) {
			Activator.handleProblem(IStatus.ERROR, e1, "Could not find icons"); //$NON-NLS-1$
		}

	}

	public Image icon() {
		return icon;
	}

	public boolean isChecked() {
		return checked;
	}

}
