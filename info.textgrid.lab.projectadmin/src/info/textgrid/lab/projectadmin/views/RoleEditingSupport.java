package info.textgrid.lab.projectadmin.views;

import java.text.MessageFormat;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.UserRole2;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.PlatformUI;

public class RoleEditingSupport extends EditingSupport {

	private CellEditor editor;
	private String role;

	public RoleEditingSupport(ColumnViewer viewer, int column) {
		// column is not absolute, but relative to the role colums, i.e.
		// Project Manager starts with 0
		super(viewer);
		editor = new CheckboxCellEditor(null, SWT.CHECK);
		role = UserManagement2.currentProject.getAvailableRoles()[column];
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return editor;
	}

	@Override
	protected boolean canEdit(Object element) {
		return UserManagement2.canEdit(element, this.role);
	}

	@Override
	public Object getValue(Object element) {
		// would have loved to return a CheckState here, but Runtime Assertions
		// forbid it
		return UserManagement2.getCheckStateValue(element, this.role)
				.isChecked();
	}

	@Override
	protected void setValue(Object element, Object value) {
		UserManagement2.toggleCheckState(element, this.role);
		getViewer().update(element, null);

		// check whether user is about to remove own Manager role, and give warning.
		if (element instanceof UserRole2) {
			UserRole2 user = (UserRole2) element;
			if (user.getePPN().equals(RBACSession.getInstance().getEPPN())
					&& role.equals(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER)) {
				Boolean futureStillLeader = (Boolean) value;
				if (!futureStillLeader) {
					MessageBox mb = new MessageBox(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(), SWT.OK
							| SWT.ICON_WARNING);
					mb.setText(MessageFormat.format(Messages.RoleEditingSupport_DeleteLeaderMessageTitle,  TextGridProject
							.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER)));
					mb.setMessage(MessageFormat.format(Messages.RoleEditingSupport_DeleteLeaderMessage, TextGridProject
							.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER)));
					mb.open();

				}
			}
		}
	}
}
