package info.textgrid.lab.projectadmin.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.projectadmin.views.messages"; //$NON-NLS-1$
	public static String RoleEditingSupport_DeleteLeaderMessage;
	public static String RoleEditingSupport_DeleteLeaderMessageTitle;
	public static String RoleHelp_create;
	public static String RoleHelp_delegate;
	public static String RoleHelp_delete;
	public static String RoleHelp_Projects;
	public static String RoleHelp_publish;
	public static String RoleHelp_read;
	public static String RoleHelp_Resources;
	public static String RoleHelp_Role;
	public static String RoleHelp_rolehelpURL;
	public static String RoleHelp_StandardText;
	public static String RoleHelp_write;
	public static String UserManagement2_ApplyButtonText;
	public static String UserManagement2_ApplyButtonToolTip;
	public static String UserManagement2_ApplyJobStatus;
	public static String UserManagement2_ApplyJobStatusShort;
	public static String UserManagement2_CannotDeleteLastLeaderMessageDescription;
	public static String UserManagement2_CannotDeleteLastLeaderMessageHeading;
	public static String UserManagement2_CheckedToolTip;
	public static String UserManagement2_CheckToGiveRoleToolTip;
	public static String UserManagement2_HelpLink;
	public static String UserManagement2_ID_ToolTip;
	public static String UserManagement2_NameToolTip;
	public static String UserManagement2_PleaseSelectProject;
	public static String UserManagement2_ProjectUpdateRolesJobStatus;
	public static String UserManagement2_ProjectUpdateUsersJobStatus;
	public static String UserManagement2_RevertButtonText;
	public static String UserManagement2_RevertButtonToolTip;
	public static String UserManagement2_searchBoxMessage;
	public static String UserManagement2_searchBoxToolTip;
	public static String UserManagement2_searchButtonText;
	public static String UserManagement2_SearchJobStatus;
	public static String UserManagement2_ShowContactsButtonText;
	public static String UserManagement2_ShowContactsToolTip;
	public static String UserManagement2_ShowSearchResultsButton;
	public static String UserManagement2_ShowSearchResultsToolTip;
	public static String UserManagement2_UserID;
	public static String UserManagement2_Username;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
