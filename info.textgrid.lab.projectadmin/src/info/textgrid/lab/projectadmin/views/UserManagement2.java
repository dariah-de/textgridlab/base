package info.textgrid.lab.projectadmin.views;

import info.textgrid.lab.authn.AuthBrowser;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.UserRole2;
import info.textgrid.lab.projectadmin.Activator;
import info.textgrid.namespaces.middleware.tgauth.Friend;
import info.textgrid.namespaces.middleware.tgauth.GetIDsRequest;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.UserDetail;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

public class UserManagement2 extends ViewPart {
	public static final String ID = "info.textgrid.lab.projectadmin.views.UserManagement"; //$NON-NLS-1$

	static final int IsInProjectColumnNumber = 0;
	static final String IsInProjectColumnText = ""; //$NON-NLS-1$
	static final int UserNameColumnNumber = 1;
	static final String UserNameColumnText = Messages.UserManagement2_Username;
	static final int UserIDColumnNumber = 2;
	static final String UserIDColumnText = Messages.UserManagement2_UserID;
	static final int FirstRoleColumnNumber = 3;

	public static TextGridProject currentProject;
	private final ISelectionListener projectSelectedListener;

	private Composite mainComposite;
	private Text viewHeading;
	private Button wantFriends;
	private Button wantSearchResults;
	private Text searchText;

	private boolean showFriends;
	private boolean showSearchResults;

	private Job currentJob = null;
	private Job searchJob = null;
	private Job applyJob = null;

	private ArrayList<Object> userRecords = new ArrayList<Object>();
	public ArrayList<Friend> friends = null;
	public ArrayList<UserDetail> searchResults = new ArrayList<UserDetail>();

	private ArrayList<TableViewerColumn> roleColumns = new ArrayList<TableViewerColumn>();

	private static HashMap<String, RoleModification> roleMods = new HashMap<String, RoleModification>();
	private boolean rolesHaveChanged = false;

	TableViewer viewer;

	public UserManagement2() {
		projectSelectedListener = new ISelectionListener() {
			public void selectionChanged(IWorkbenchPart sourcepart,
					ISelection selection) {
				if (sourcepart != UserManagement2.this
						&& selection instanceof IStructuredSelection) {
					final IStructuredSelection iss = (IStructuredSelection) selection;
					// take first selected element
					if (!iss.isEmpty()) {
						Object firstelem = iss.toArray()[0];
						if (firstelem instanceof TextGridProject && isVisible()) {
							currentProject = (TextGridProject) firstelem;
							reloadFromRBAC(currentProject);
						} else {
							reloadFromRBAC(null);
						}
					} else {
						reloadFromRBAC(null);
					}
				}
			}
		};
	}

	public boolean isVisible() {
		try {
			if (PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage().isPartVisible(this)) {
				return true;
			}
		} catch (Exception e) {
			return true; // to be on the safe side...
		}
		return false;
	}

	private class IsInProjectLabelProvider extends ColumnLabelProvider {
		public String getText(Object obj) {
			return null;
		}

		public Image getImage(Object obj) {
			if (obj instanceof UserRole2) {
				return InProjectState.INPROJECT.icon();
			} else if (obj instanceof Friend) {
				return InProjectState.CONTACT.icon();
			} else {
				return InProjectState.SEARCHED.icon();
			}
		}
	}

	private class UserNameLabelProvider extends ColumnLabelProvider {
		public String getText(Object obj) {
			if (obj instanceof UserRole2) {
				UserRole2 ur = (UserRole2) obj;
				String a = RBACSession.getInstance().getFullName(ur.getePPN());
				// System.out.println("Showing: " + a);
				return a;
			} else if (obj instanceof Friend) {
				Friend f = (Friend) obj;
				return RBACSession.getInstance().getDetails(f.getUsername())
						.getName();
			} else if (obj instanceof UserDetail) {
				UserDetail ud = (UserDetail) obj;
				return ud.getName();
			} else
				return "no known class"; //$NON-NLS-1$
		}
	}

	private class EPPNLabelProvider extends ColumnLabelProvider {
		public String getText(Object obj) {
			if (obj instanceof UserRole2) {
				UserRole2 ur = (UserRole2) obj;
				return ur.getePPN();
			} else if (obj instanceof Friend) {
				Friend f = (Friend) obj;
				return f.getUsername();
			} else if (obj instanceof UserDetail) {
				UserDetail ud = (UserDetail) obj;
				return ud.getEPPN();
			} else
				return "no known class"; //$NON-NLS-1$
		}
	}

	private class RoleCheckLabelProvider extends ColumnLabelProvider {
		private String role = null;

		private RoleCheckLabelProvider(String role) {
			super();
			this.role = role;
		}

		public String getText(Object obj) {
			// if (obj instanceof UserRole2) {
			// UserRole2 ur = (UserRole2) obj;
			// if (ur.getRoles().contains(role)) {
			// return "yes";
			// } else {
			// return "no";
			// }
			// } else
			// return "no";
			return null;
		}

		@Override
		public Image getImage(Object obj) {
			return getCheckStateValue(obj, role).icon();
		}
	}

	@Override
	public void createPartControl(Composite parent) {

		mainComposite = new Composite(parent, SWT.FILL);
		// GridData cGD = new GridData();
		// cGD.horizontalAlignment = GridData.FILL;
		// cGD.grabExcessVerticalSpace = true;
		// cGD.grabExcessHorizontalSpace = true;
		// cGD.heightHint = 100;
		// cGD.widthHint = 400;
		// cGD.verticalAlignment = GridData.FILL;
		mainComposite.setLayout(new GridLayout(1, false));
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		mainComposite.setLayoutData(gdc);

		viewHeading = new Text(mainComposite, SWT.BOLD | SWT.READ_ONLY);
		GridData gd3b = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd3b.widthHint = 2000;
		viewHeading.setLayoutData(gd3b);
		viewHeading.setText(Messages.UserManagement2_PleaseSelectProject);
		viewHeading.setBackground(mainComposite.getBackground());

		viewer = new TableViewer(mainComposite, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION | SWT.VIRTUAL);
		GridData gd3v = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd3v.widthHint = 2000;

		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);

		TableViewerColumn tcChecked = new TableViewerColumn(viewer, SWT.NONE);
		tcChecked.getColumn().setAlignment(SWT.LEFT);
		tcChecked.getColumn().setText(IsInProjectColumnText);
		tcChecked.getColumn().setToolTipText(
				Messages.UserManagement2_CheckedToolTip);
		tcChecked.getColumn().setWidth(20);
		tcChecked.setLabelProvider(new IsInProjectLabelProvider());
		// tcChecked.setEditingSupport(new WBLinkEditingSupport(viewer, 0));

		TableViewerColumn tcName = new TableViewerColumn(viewer, SWT.NONE);
		tcName.getColumn().setAlignment(SWT.LEFT);
		tcName.getColumn().setText(UserNameColumnText);
		tcName.getColumn().setToolTipText(Messages.UserManagement2_NameToolTip);
		tcName.getColumn().setWidth(150);
		tcName.setLabelProvider(new UserNameLabelProvider());
		// tcName.setEditingSupport(new WBLinkEditingSupport(viewer, 0));

		TableViewerColumn tcID = new TableViewerColumn(viewer, SWT.NONE);
		tcID.getColumn().setAlignment(SWT.LEFT);
		tcID.getColumn().setText(UserIDColumnText);
		tcID.getColumn().setToolTipText(Messages.UserManagement2_ID_ToolTip);
		tcID.getColumn().setWidth(150);
		tcID.setLabelProvider(new EPPNLabelProvider());
		// tcName.setEditingSupport(new WBLinkEditingSupport(viewer, 0));

		GridData vGD = new GridData();
		vGD.horizontalAlignment = GridData.FILL;
		vGD.grabExcessVerticalSpace = true;
		vGD.grabExcessHorizontalSpace = true;
		// vGD.heightHint = 100;
		// vGD.widthHint = 400;
		vGD.verticalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(vGD);

		// this works only with buggy SetModel viewer.setContentProvider(new
		// DeferredContentProvider(URCOMP));
		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setComparator(new AlphabeticalUserRecordsViewerSorter());
		// viewer.setLabelProvider(lB);
		getSite().getPage().addSelectionListener(projectSelectedListener);

		// viewer.setInput(userRecords);

		Composite bottom2 = new Composite(mainComposite, SWT.FILL);
		bottom2.setLayout(new GridLayout(2, false));
		GridData gd2 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		bottom2.setLayoutData(gd2);

		searchText = new Text(bottom2, SWT.SINGLE | SWT.LEFT | SWT.BORDER
				| SWT.SEARCH | SWT.CANCEL);
		searchText.setText(""); //$NON-NLS-1$
		GridData gd6 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		gd6.widthHint = 2000;
		searchText.setLayoutData(gd6);
		searchText.setToolTipText(Messages.UserManagement2_searchBoxToolTip);
		searchText.setMessage(Messages.UserManagement2_searchBoxMessage);

		searchText.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent event) {
				if (event.character == SWT.CR) {
					startSearch();
				}
			}
		});

		Button searchButton = new Button(bottom2, SWT.PUSH);
		searchButton.setText(Messages.UserManagement2_searchButtonText);
		searchButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				startSearch();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		Composite bottom = new Composite(mainComposite, SWT.FILL);
		bottom.setLayout(new GridLayout(5, false));
		GridData gd1 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		bottom.setLayoutData(gd1);

		wantFriends = new Button(bottom, SWT.CHECK);
		wantFriends.setText(Messages.UserManagement2_ShowContactsButtonText);
		wantFriends.setImage(InProjectState.CONTACT.icon());
		wantFriends
				.setToolTipText(Messages.UserManagement2_ShowContactsToolTip);
		wantFriends.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				if (wantFriends.getSelection()) {
					showFriends = true;
					addFriendsToViewer(true);
				} else {
					showFriends = false;
					hideFriendsFromViewer(true);
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		wantSearchResults = new Button(bottom, SWT.CHECK);
		GridData gd3a = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd3a.widthHint = 2000;
		wantSearchResults.setLayoutData(gd3a);
		wantSearchResults
				.setText(Messages.UserManagement2_ShowSearchResultsButton);
		wantSearchResults.setImage(InProjectState.SEARCHED.icon());
		wantSearchResults
				.setToolTipText(Messages.UserManagement2_ShowSearchResultsToolTip);
		wantSearchResults.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				if (wantSearchResults.getSelection()) {
					showSearchResults = true;
					addSearchResultsToViewer(true);
				} else {
					showSearchResults = false;
					hideSearchResultsFromViewer(true);
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		Link helplink = new Link(bottom, SWT.NULL);
		helplink.setText(Messages.UserManagement2_HelpLink);
		helplink.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				widgetDefaultSelected(e);
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				try {
					PlatformUI
							.getWorkbench()
							.getActiveWorkbenchWindow()
							.getActivePage()
							.showView(
									"info.textgrid.lab.projectadmin.views.RoleHelp"); //$NON-NLS-1$
				} catch (PartInitException e1) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID, "Could not open Role Help!", //$NON-NLS-1$
							e1);
					Activator.getDefault().getLog().log(status);
				}
			}
		});

		Button revertButton = new Button(bottom, SWT.PUSH);
		revertButton.setText(Messages.UserManagement2_RevertButtonText);
		revertButton
				.setToolTipText(Messages.UserManagement2_RevertButtonToolTip);
		revertButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				roleMods.clear();
				viewer.refresh();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		Button applyButton = new Button(bottom, SWT.PUSH);
		applyButton.setText(Messages.UserManagement2_ApplyButtonText);
		applyButton.setToolTipText(Messages.UserManagement2_ApplyButtonToolTip);
		applyButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				boolean allowed = checkThatOneManagerRemains();
				if (!allowed) {
					MessageBox mb = new MessageBox(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(), SWT.OK
							| SWT.ICON_ERROR);
					mb.setText(MessageFormat
							.format(Messages.UserManagement2_CannotDeleteLastLeaderMessageHeading,
									TextGridProject
											.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER)));
					mb.setMessage(MessageFormat
							.format(Messages.UserManagement2_CannotDeleteLastLeaderMessageDescription,
									TextGridProject
											.findLabelForRBACRole(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER)));
					mb.open();
					return;
				} else {
					writeModsToRBAC(); // this one clears the roleMods and
										// reloads the viewer
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		PlatformUI
				.getWorkbench()
				.getHelpSystem()
				.setHelp(parent,
						"info.textgrid.lab.projectadmin.UserManagementView"); //$NON-NLS-1$

		// System.out.println("createpart");
	}

	private boolean checkThatOneManagerRemains() {
		int currentManagers = 0;
		for (Object o : userRecords) {
			if (o instanceof UserRole2) {
				UserRole2 projectmember = (UserRole2) o;
				if (projectmember.getRoles().contains(
						TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER)) {
					currentManagers++;
				}
			}
		}
		int managersForDeletion = 0;
		for (String ePPN : roleMods.keySet()) {
			for (Iterator<String> it = roleMods.get(ePPN).toBeRemoved
					.iterator(); it.hasNext();) {
				String delRole = it.next();
				if (delRole
						.equals(TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER)) {
					managersForDeletion++;
				}
			}
		}
		return currentManagers > managersForDeletion;
	}

	private void writeModsToRBAC() {
		if (applyJob != null) {
			applyJob.cancel();
		}
		// find out how many mods will be made
		final int expectedMods = countMods();

		applyJob = new Job(Messages.UserManagement2_ApplyJobStatus) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(Messages.UserManagement2_ApplyJobStatusShort,
						expectedMods + 1);
				rolesHaveChanged = true;

				try {
					// first delete, then add, as add has a side effect of
					// not returning if there are many sessions by that user
					// which all have to be extended by the role. Delete
					// does not have this effect as we concluded not to
					// steal other's roles, only to add new ones.
					for (String ePPN : roleMods.keySet()) {
						for (Iterator<String> it = roleMods.get(ePPN).toBeRemoved
								.iterator(); it.hasNext();) {
							String delRole = it.next();
							currentProject.deleteRole(ePPN, delRole);
							it.remove();
							monitor.worked(1);
							if (monitor.isCanceled())
								return Status.CANCEL_STATUS;
						}
					}
					for (String ePPN : roleMods.keySet()) {
						for (Iterator<String> it = roleMods.get(ePPN).toBeAdded
								.iterator(); it.hasNext();) {
							String addRole = it.next();
							currentProject.addRoleMember(ePPN, addRole);
							monitor.worked(1);
							if (monitor.isCanceled())
								return Status.CANCEL_STATUS;
						}
					}
					for (String ePPN : roleMods.keySet()) {
						for (Iterator<String> it = roleMods.get(ePPN).toBeAdded
								.iterator(); it.hasNext();) {
							String activateRole = it.next();
							currentProject.activateRoleInSession(ePPN,
									activateRole);
							it.remove();
							monitor.worked(1);
							if (monitor.isCanceled())
								return Status.CANCEL_STATUS;
						}
					}
					// If add does not return, all deletions had been made
					// and all additions as well, RBAC then just hangs in
					// the add-session-update-loop, so the Job will fail but the
					// roles are changed.
				} catch (Exception e) {
					IStatus status = new Status(IStatus.INFO,
							Activator.PLUGIN_ID, "Could not add/delete roles!", //$NON-NLS-1$
							e);
					Activator.getDefault().getLog().log(status);

					IStatus info = new Status(
							IStatus.INFO,
							Activator.PLUGIN_ID,
							"The following roles may not have been added/removed successfully in all sessions of the user, please check in the list."); //$NON-NLS-1$
					Activator.getDefault().getLog().log(info);

					for (String ePPN : roleMods.keySet()) {
						for (String addRole : roleMods.get(ePPN).toBeAdded) {
							info = new Status(IStatus.INFO,
									Activator.PLUGIN_ID, "Could not add " //$NON-NLS-1$
											+ addRole + " to " + ePPN); //$NON-NLS-1$
							Activator.getDefault().getLog().log(info);
						}
						for (String delRole : roleMods.get(ePPN).toBeRemoved) {
							info = new Status(IStatus.INFO,
									Activator.PLUGIN_ID, "Could not remove " //$NON-NLS-1$
											+ delRole + " from " + ePPN); //$NON-NLS-1$
							Activator.getDefault().getLog().log(info);
						}
					}
				}

				monitor.worked(1);
				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;

				viewer.getTable().getDisplay().asyncExec(new Runnable() {
					public void run() {
						// refresh Navigator to reflect my own changed settings
						String me = RBACSession.getInstance().getEPPN()
								.toLowerCase();
						if (roleMods.containsKey(me)
								&& (roleMods.get(me).toBeAdded.size() > 0 || roleMods
										.get(me).toBeRemoved.size() > 0))
							AuthBrowser.sessionChanged();
						// ...and reload the viewer, clearing the roleMods
						reloadFromRBAC(currentProject);
					}
				});

				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
				return Status.OK_STATUS;
			}
		};
		applyJob.setPriority(Job.LONG);
		applyJob.schedule();

	}

	public void searchForUsers(final String querystring) {
		if (searchJob != null) {
			searchJob.cancel();
		}
		searchJob = new Job(Messages.UserManagement2_SearchJobStatus) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(Messages.UserManagement2_SearchJobStatus, 5);

				try {
					PortTgextra stub = TextGridProject.ensureStubIsLoaded();

					GetIDsRequest gidr = new GetIDsRequest();
					gidr.setAuth(RBACSession.getInstance().getSID(false));

					// as we want to OR the query results, have to ask for mail,
					// name, org separately
					gidr.setMail(""); //$NON-NLS-1$
					gidr.setName(querystring);
					gidr.setOrganisation(""); //$NON-NLS-1$
					if (monitor.isCanceled()) {
						return Status.CANCEL_STATUS;
					}
					List<UserDetail> udNames = stub.getIDs(gidr)
							.getUserdetails();
					if (udNames == null)
						udNames = new ArrayList<UserDetail>();
					monitor.worked(1);

					gidr.setMail(querystring);
					gidr.setName(""); //$NON-NLS-1$
					gidr.setOrganisation(""); //$NON-NLS-1$
					if (monitor.isCanceled()) {
						return Status.CANCEL_STATUS;
					}
					List<UserDetail> udMails = stub.getIDs(gidr)
							.getUserdetails();
					if (udMails == null)
						udMails = new ArrayList<UserDetail>();
					monitor.worked(1);

					gidr.setMail(""); //$NON-NLS-1$
					gidr.setName(""); //$NON-NLS-1$
					gidr.setOrganisation(querystring);
					if (monitor.isCanceled()) {
						return Status.CANCEL_STATUS;
					}
					List<UserDetail> udOrgs = stub.getIDs(gidr)
							.getUserdetails();
					if (udOrgs == null)
						udOrgs = new ArrayList<UserDetail>();
					monitor.worked(1);

					// now merge the three results
					if (monitor.isCanceled()) {
						return Status.CANCEL_STATUS;
					}
					ArrayList<UserDetail> tmpSearchResult = new ArrayList<UserDetail>(
							udNames.size());
					for (UserDetail name : udNames) {
						tmpSearchResult.add(name);
					}
					for (UserDetail mail : udMails) {
						boolean found = false;
						for (UserDetail tmp : tmpSearchResult) {
							if (mail.getEPPN().equals(tmp.getEPPN())) {
								found = true;
								break;
							}
						}
						if (!found)
							tmpSearchResult.add(mail);
					}
					monitor.worked(1);

					if (monitor.isCanceled()) {
						return Status.CANCEL_STATUS;
					}
					for (UserDetail org : udOrgs) {
						boolean found = false;
						for (UserDetail tmp : tmpSearchResult) {
							if (org.getEPPN().equals(tmp.getEPPN())) {
								found = true;
								break;
							}
						}
						if (!found)
							tmpSearchResult.add(org);
					}
					monitor.worked(1);

					searchResults = tmpSearchResult;

				} catch (Exception e) {
					IStatus status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID,
							"Could not execute search job successfully!", e); //$NON-NLS-1$
					Activator.getDefault().getLog().log(status);

				}

				if (monitor.isCanceled() || viewer.getTable().isDisposed()) {
					return Status.CANCEL_STATUS;
				}

				viewer.getTable().getDisplay().asyncExec(new Runnable() {
					public void run() {
						addSearchResultsToViewer(true);
					}
				});

				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
				return Status.OK_STATUS;
			}
		};
		searchJob.setPriority(Job.LONG);
		searchJob.schedule();
	}

	private void addRoleColumns(TableViewer viewer) {
		String[] roles = currentProject.getAvailableRoles();
		for (int i = 0; i < roles.length; i++) {
			String role = roles[i];
			TableViewerColumn tcR = new TableViewerColumn(viewer, SWT.NONE);
			tcR.getColumn().setAlignment(SWT.LEFT);
			tcR.getColumn().setText(TextGridProject.findLabelForRBACRole(role));
			tcR.getColumn().setToolTipText(
					MessageFormat.format(
							Messages.UserManagement2_CheckToGiveRoleToolTip,
							TextGridProject.findLabelForRBACRole(role)));
			tcR.setEditingSupport(new RoleEditingSupport(viewer, i));
			tcR.setLabelProvider(new RoleCheckLabelProvider(role));
			tcR.getColumn().pack();
			roleColumns.add(tcR);
		}
	}

	private int countMods() {
		int count = 0;
		for (String ePPN : roleMods.keySet()) {
			count += roleMods.get(ePPN).toBeAdded.size() * 2 // because for add,
																// we make
																// addmember AND
																// addactiverole
					+ roleMods.get(ePPN).toBeRemoved.size(); // for remove, we
																// only drop and
																// leave the
																// active role
																// there
		}
		return count;
	}

	private void addFriendsToViewer(boolean refresh) {
		// only ONCE query for friends and not whenever the user scrolls over
		// the project list
		// do not forget to repeat it whenever role changes are applied
		if (friends == null)
			friends = RBACSession.getInstance().getFriends(true);

		ArrayList<Friend> remainingFriends = new ArrayList<Friend>();
		remainingFriends.addAll(friends);
		for (Object o : userRecords) {

			if (o instanceof Friend) { // this contact got checked for
				// inclusion in the project, we will
				// not add it twice
				remainingFriends.remove(o);
			} else if (o instanceof UserRole2) {
				UserRole2 projectmember = (UserRole2) o;
				for (Friend f : friends) {
					if (projectmember.getePPN().equalsIgnoreCase(
							f.getUsername())) {
						remainingFriends.remove(f);
						break;
					}
				}
			} else if (o instanceof UserDetail) {
				UserDetail projectmember = (UserDetail) o;
				for (Friend f : friends) {
					if (projectmember.getEPPN().equalsIgnoreCase(
							f.getUsername())) {
						remainingFriends.remove(f);
						break;
					}
				}
			}
		}
		userRecords.addAll(remainingFriends);
		if (refresh) {
			viewer.setInput(userRecords);
		}
	}

	private void hideFriendsFromViewer(boolean refresh) {
		for (Friend cand : friends) {
			if (roleMods.get(cand.getUsername()) != null
					&& roleMods.get(cand.getUsername()).toBeAdded.size() > 0) {
			} else {
				userRecords.remove(cand);
			}
		}
		if (refresh) {
			viewer.setInput(userRecords);
		}
	}

	private void addSearchResultsToViewer(boolean refresh) {
		ArrayList<UserDetail> remainingSearchResults = new ArrayList<UserDetail>();
		remainingSearchResults.addAll(searchResults);
		for (Object o : userRecords) {
			if (o instanceof UserDetail) { // this search result got checked for
											// inclusion in the project, we will
											// not add it twice
				remainingSearchResults.remove(o);
			} else if (o instanceof UserRole2) {
				UserRole2 projectmember = (UserRole2) o;
				for (UserDetail ud : searchResults) {
					if (projectmember.getePPN().equalsIgnoreCase(ud.getEPPN())) {
						remainingSearchResults.remove(ud);
						break;
					}
				}
			} else if (o instanceof Friend) {
				Friend projectmember = (Friend) o;
				for (UserDetail ud : searchResults) {
					if (projectmember.getUsername().equalsIgnoreCase(
							ud.getEPPN())) {
						remainingSearchResults.remove(ud);
						break;
					}
				}
			}
		}
		userRecords.addAll(remainingSearchResults);
		if (refresh) {
			viewer.setInput(userRecords);
			viewer.setSelection(
					new StructuredSelection(remainingSearchResults), true);
		}
	}

	private void hideSearchResultsFromViewer(boolean refresh) {
		for (UserDetail cand : searchResults) {
			if (roleMods.get(cand.getEPPN()) != null
					&& roleMods.get(cand.getEPPN()).toBeAdded.size() > 0) {
			} else {
				userRecords.remove(cand);
			}
		}
		if (refresh) {
			viewer.setInput(userRecords);
		}
	}

	@Override
	public void setFocus() {
		if (mainComposite != null)
			mainComposite.setFocus();
	}

	public void dispose() {
		if (projectSelectedListener != null) {
			getSite().getPage()
					.removeSelectionListener(projectSelectedListener);
		}
	}

	public static boolean canEdit(Object element, String thisrole) {
		if (currentProject.iAmLeader())
			return true;
		else {
			if (element instanceof UserRole2) {
				UserRole2 user = (UserRole2) element;
				if (user.getePPN().equalsIgnoreCase(
						RBACSession.getInstance().getEPPN())
						&& (user.getRoles().contains(thisrole)))
					// user may remove (not add) an existing own role, see
					// TG-698
					return true;
				else
					return false;

			} else
				return false;
		}

	}

	public static CheckState getCheckStateValue(Object element, String thisrole) {
		if (element instanceof UserRole2) {
			// project members can have 6 possible states depending upon whether
			// a) user is Project Manager
			// b) a role is already set in RBAC
			// c) whether a checkbox had been edited before
			UserRole2 user = (UserRole2) element;
			if (user.getRoles().contains(thisrole)) {
				if (!canEdit(element, thisrole)) {
					return CheckState.DISABLEDONROLE;
				} else {

					if (UserManagement2.roleMods.get(user.getePPN()) != null
							&& UserManagement2.roleMods.get(user.getePPN()).toBeRemoved
									.contains(thisrole))
						return CheckState.MODIFIEDOFFROLE;
					else
						return CheckState.ONROLE;
				}
			} else {
				if (!canEdit(element, thisrole)) {
					return CheckState.DISABLEDOFFROLE;
				} else {

					if (UserManagement2.roleMods.get(user.getePPN()) != null
							&& UserManagement2.roleMods.get(user.getePPN()).toBeAdded
									.contains(thisrole))
						return CheckState.MODIFIEDONROLE;
					else
						return CheckState.OFFROLE;
				}
			}
		} else { // Friends or SearchResults have only three possible states
			if (!canEdit(element, thisrole)) {
				return CheckState.DISABLEDOFFROLE;
			} else {
				String ePPN;
				if (element instanceof Friend) {
					ePPN = ((Friend) element).getUsername();
				} else if (element instanceof UserDetail) {
					ePPN = ((UserDetail) element).getEPPN();
				} else {
					return null;
				}
				if (UserManagement2.roleMods.get(ePPN) != null
						&& UserManagement2.roleMods.get(ePPN).toBeAdded
								.contains(thisrole)) {
					return CheckState.MODIFIEDONROLE;
				} else {
					return CheckState.OFFROLE;
				}
			}
		}

	}

	public static void toggleCheckState(Object element, String thisrole) {
		String ePPN = ""; //$NON-NLS-1$
		if (element instanceof UserRole2) {
			ePPN = ((UserRole2) element).getePPN();
		} else if (element instanceof UserDetail) {
			ePPN = ((UserDetail) element).getEPPN();
		} else if (element instanceof Friend) {
			ePPN = ((Friend) element).getUsername();
		}

		// now we only switch, access check done by canEdit, value check by
		// getValue
		RoleModification presentRM = roleMods.get(ePPN);
		if (presentRM == null) {
			presentRM = new RoleModification();
		}
		if (getCheckStateValue(element, thisrole) == CheckState.OFFROLE) {
			presentRM.toBeAdded.add(thisrole);
			roleMods.put(ePPN, presentRM);
			// System.out.println("Adding " + thisrole + " to " + ePPN);
		} else if (getCheckStateValue(element, thisrole) == CheckState.ONROLE) {
			presentRM.toBeRemoved.add(thisrole);
			roleMods.put(ePPN, presentRM);
			// System.out.println("Removing " + thisrole + " from " + ePPN);
		} else if (getCheckStateValue(element, thisrole) == CheckState.MODIFIEDOFFROLE) {
			presentRM.toBeRemoved.remove(thisrole);
			roleMods.put(ePPN, presentRM);
			// System.out.println("Giving back " + thisrole + " to " + ePPN);
		} else if (getCheckStateValue(element, thisrole) == CheckState.MODIFIEDONROLE) {
			presentRM.toBeAdded.remove(thisrole);
			roleMods.put(ePPN, presentRM);
			// System.out.println("No, better don't give " + thisrole + " to " +
			// ePPN);
		}

	}

	/**
	 * called in a the UI Thread..., will spawn a Job and update the viewer in
	 * the UI thread once it's loaded
	 * 
	 * @param tgp
	 */
	private void reloadFromRBAC(final TextGridProject tgp) {
		if (tgp == null) {
			viewHeading.setText(Messages.UserManagement2_PleaseSelectProject);
			userRecords.clear();
			roleMods.clear();
			if (currentJob != null) {
				currentJob.cancel();
			}
			viewer.refresh();
			viewer.setInput(userRecords);
			return;
		}

		viewHeading
				.setText(tgp.getName().replaceAll("&", "&&") + " (" + tgp.getId() + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

		if (currentJob != null) {
			currentJob.cancel();
		}
		currentJob = new Job(
				Messages.UserManagement2_ProjectUpdateUsersJobStatus) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(
						Messages.UserManagement2_ProjectUpdateRolesJobStatus,
						IProgressMonitor.UNKNOWN);
				userRecords.clear();
				roleMods.clear();

				if (rolesHaveChanged) {
					friends = RBACSession.getInstance().getFriends(true);
					monitor.worked(1);
					if (monitor.isCanceled())
						return Status.CANCEL_STATUS;
					rolesHaveChanged = false;
				}

				ArrayList<UserRole2> ur = tgp.getUserRoles2FromRBAC(monitor);
				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
				if (ur != null && ur.size() > 0) {
					userRecords.addAll(ur);
				}
				monitor.worked(1);
				if (monitor.isCanceled() || viewer.getTable().isDisposed()) {
					return Status.CANCEL_STATUS;
				}
				viewer.getTable().getDisplay().asyncExec(new Runnable() {
					public void run() {
						for (TableViewerColumn c : roleColumns) {
							c.getColumn().dispose();
						}
						viewer.refresh();
						roleColumns.clear();
						addRoleColumns(viewer);
						if (showFriends) {
							addFriendsToViewer(false);
						}
						if (showSearchResults) {
							addSearchResultsToViewer(false);
						}
						viewer.refresh();
						viewer.setInput(userRecords);
					}
				});

				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
				return Status.OK_STATUS;
			}
		};
		currentJob.setPriority(Job.LONG);
		currentJob.schedule();
	}

	private void startSearch() {
		if (searchText.getText().length() > 0) {
			hideSearchResultsFromViewer(true);
			searchResults.clear();
			searchForUsers(searchText.getText());
			wantSearchResults.setSelection(true);
			showSearchResults = true;
		} // TODO... else?
	}

	/**
	 * The current project in the user mangement is set to the given project and
	 * the table viewer content is refreshed.
	 * 
	 * @param project
	 *            the new project
	 */
	public void setProject(TextGridProject project) {
		if ((project != null) && (project != currentProject)) {
			currentProject = project;
			reloadFromRBAC(currentProject);
		}
	}

}
