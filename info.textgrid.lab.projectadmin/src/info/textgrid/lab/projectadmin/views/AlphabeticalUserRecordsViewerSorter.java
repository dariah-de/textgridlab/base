package info.textgrid.lab.projectadmin.views;

import info.textgrid.lab.core.model.UserRole2;
import info.textgrid.namespaces.middleware.tgauth.Friend;
import info.textgrid.namespaces.middleware.tgauth.UserDetail;

import java.util.Comparator;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;


/**
 * A Comparator for UserRoles, Friends and Userdetails.
 * 
 * @author martin
 * 
 */
public class AlphabeticalUserRecordsViewerSorter extends ViewerComparator implements
		Comparator<Object> {

	public int compare(Viewer viewer, Object o1, Object o2) {
		return this.compare(o1, o2);
	}

	/**
	 * Sorts Users in Project first, then Friends, then Search results. Within
	 * the categories by ePPN alphabetically, the Friends by score, then ePPN alphabetically
	 */
	public int compare(Object o1, Object o2) {
		if (o1.getClass() == o2.getClass()) {
			if (o1 instanceof UserRole2) {
				return ((UserRole2) o1).getePPN().compareToIgnoreCase(
						((UserRole2) o2).getePPN());
			} else if (o1 instanceof Friend) {
				int compared = ((Friend) o2).getScore().compareTo(
						((Friend) o1).getScore());
				if (compared == 0) {
					return ((Friend) o1).getUsername().compareToIgnoreCase(
							((Friend) o2).getUsername());
				} else
					return compared;
			} else if (o1 instanceof UserDetail) {
				return ((UserDetail) o1).getEPPN().compareToIgnoreCase(
						((UserDetail) o2).getEPPN());
			} else {
				return 0;
			}
		} else if (o1 instanceof UserRole2 || o2 instanceof UserDetail) {
			return -1;
		} else {
			return 1;
		}
	}
}
