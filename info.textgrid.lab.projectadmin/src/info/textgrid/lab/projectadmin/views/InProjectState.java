package info.textgrid.lab.projectadmin.views;

import java.io.IOException;

import info.textgrid.lab.projectadmin.Activator;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

public enum InProjectState {
	INPROJECT("114-Projekt-Nutzer.gif"),  //$NON-NLS-1$
	CONTACT("116-Kontakt-Nutzer.gif"),  //$NON-NLS-1$
	SEARCHED("115-Suche-Nutzer.gif"); //$NON-NLS-1$
	private Image icon;

	InProjectState(String iconFileName) {
		try {
			icon = ImageDescriptor.createFromURL(
					FileLocator.toFileURL(FileLocator.find(Activator
							.getDefault().getBundle(), new Path("icons/" //$NON-NLS-1$
							+ iconFileName), null))).createImage();
		} catch (IOException e1) {
			Activator.handleProblem(IStatus.ERROR, e1, "Could not find icons"); //$NON-NLS-1$
		}

	}

	public Image icon() {
		return icon;
	}

}
