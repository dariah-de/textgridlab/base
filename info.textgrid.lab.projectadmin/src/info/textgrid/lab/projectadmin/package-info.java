/**
 * The user interface for the project and user administration.
 * 
 * <h4>Subpackages</h4>
 * <ul>
 * <li>{@link info.textgrid.lab.projectadmin.dialogs}</li>
 * <li>{@link info.textgrid.lab.projectadmin.views}</li>
 * </ul>
 */
package info.textgrid.lab.projectadmin;

