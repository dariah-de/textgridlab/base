package info.textgrid.lab.navigator;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.IChildListChangedListener;
import info.textgrid.lab.core.model.IChildListParent;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridProject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.progress.DeferredTreeContentManager;
import org.eclipse.ui.progress.UIJob;

/**
 *
 * 
 * This ContentProvider uses org.eclipse.ui.progress.DeferredTreeContentManager
 * for deferredness
 *
 */
public class DeferredTreeContentProvider implements ITreeContentProvider,
		IChildListChangedListener {
	
	public interface IDeferredTreeListener {
		public void deferredTreeDone();
	}

	private static final Object[] NO_CHILDREN = new Object[0];
	private DeferredTreeContentManager manager;
	private Viewer viewer;
	private IChildListParent input;
	private static ListenerList doneListeners = new ListenerList();

	public Object[] getChildren(Object parentElement) {
		Object[] managerResults = null;

		if (manager != null)
			managerResults = manager.getChildren(parentElement);
		
		if (managerResults != null)
			return managerResults;

		IWorkbenchAdapter workbenchAdapter = getAdapter(parentElement, IWorkbenchAdapter.class);
		if (workbenchAdapter != null)
			return workbenchAdapter.getChildren(parentElement);

		return NO_CHILDREN;

	}

	public Object getParent(Object element) {
		IWorkbenchAdapter adapter = getAdapter(element, IWorkbenchAdapter.class);
		if (adapter != null)
			return adapter.getParent(element);
		else
			return null;
	}

	public boolean hasChildren(Object element) {
		return manager.mayHaveChildren(element);
	}

	public Object[] getElements(Object inputElement) {
		return this.getChildren(inputElement);
	}

	public void dispose() {
		if (input != null)
			this.input.removeChildListChangedListener(this);
	}

	public void cancel(Object input) {
		if (manager != null) {
			manager.cancel(input);
		}
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		this.viewer = viewer;
		if (this.input != newInput && newInput instanceof IChildListParent) {
			if (this.input != null ) {
				this.input.removeChildListChangedListener(this);
			}
			this.input = (IChildListParent) newInput; 
			this.input.addChildListChangedListener(this);
		}
//		System.out.println("input changed. instance " + this.toString()
//				+ " manager == null : " + (manager == null));

		if (viewer instanceof AbstractTreeViewer && this.manager == null) {
			manager = new DeferredTreeContentManager((AbstractTreeViewer) viewer);
		}
		manager.addUpdateCompleteListener(getCompletionJobListener());
	}

	public void childListChanged(IChildListParent parent) {
		if (viewer != null) {
			viewer.setInput(parent);
		}
	}
	
	protected IJobChangeListener getCompletionJobListener() {
		return new JobChangeAdapter() {

			public void done(IJobChangeEvent event) {
				if (event.getResult().isOK()){
					notifyListeners();
					NaviExpandedElements exElements = NaviExpandedElements.getInstance();
					TreePath[] actualExTreePaths = exElements.getExpandedTreePaths();
					
					if ((actualExTreePaths != null) && (actualExTreePaths.length > 0)) {
						for (TreePath element : actualExTreePaths) {
							
							for (int i = 0; i <= (element.getSegmentCount()-1); i++) {
								Object obj = element.getSegment(i);
								if (obj instanceof TGObjectReference) {
										if (!((AbstractTreeViewer)viewer).getExpandedState(obj))
											((AbstractTreeViewer)viewer).setExpandedState(obj, true);
								} else if (obj instanceof TextGridProject) {
									if (!exElements.isDemandedProject((TextGridProject)obj)) {
										exElements.addDemandedProject((TextGridProject)obj);
										((AbstractTreeViewer)viewer).setExpandedState(obj, true);
									}	
								}
							}	
						}
					}			
				}
			}
		};
	}
	
	/**
	 * Adds a done listener.
	 * 
	 * @param listener
	 */
	public static void addListener(final IDeferredTreeListener listener) {
		doneListeners.add(listener);
	}

	/**
	 * Removes a listener.
	 * 
	 * @param listener
	 */
	public static void removeListener(final IDeferredTreeListener listener) {
		doneListeners.remove(listener);
	}

	/**
	 * Notifies the registered listeners.
	 */
	public static void notifyListeners() {

		final UIJob job = new UIJob("Notifying DeferredTreeContent listeners") {

			@Override
			public IStatus runInUIThread(final IProgressMonitor monitor) {
				for (final Object listener : doneListeners.getListeners())
					((IDeferredTreeListener) listener).deferredTreeDone();
				return Status.OK_STATUS;
			}
		};
		job.setSystem(true);
		job.schedule();
	}
}
