package info.textgrid.lab.navigator;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.IChildListChangedListener;
import info.textgrid.lab.core.model.IChildListParent;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.search.TextGridRepositoryItem;

import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.progress.DeferredTreeContentManager;

/**
 * Copy of DeferredTreeContentProvider with special modifications for the 
 * navigator content TextGridRepositoryItem.
 *
 */
public class TextGridRepositoryContentProvider implements ITreeContentProvider, IChildListChangedListener {
	
	public interface IDeferredTreeListener {
		public void deferredTreeDone();
	}

	private static final Object[] NO_CHILDREN = new Object[0];
	private DeferredTreeContentManager manager;
	private Viewer viewer;
	private IChildListParent input;
	private TextGridRepositoryItem repositroryItem = new TextGridRepositoryItem();

	public Object[] getChildren(Object parentElement) {
		Object[] managerResults = null;
		
		if (parentElement instanceof TextGridProjectRoot) {
			Object[] returnValue = {repositroryItem};
			return returnValue;
		}

		if (manager != null)
			managerResults = manager.getChildren(parentElement);
		
		if (managerResults != null)
			return managerResults;

		IWorkbenchAdapter workbenchAdapter = getAdapter(parentElement, IWorkbenchAdapter.class);
		if (workbenchAdapter != null)
			return workbenchAdapter.getChildren(parentElement);

		return NO_CHILDREN;

	}

	public Object getParent(Object element) {
		IWorkbenchAdapter adapter = getAdapter(element, IWorkbenchAdapter.class);
		if (adapter != null)
			return adapter.getParent(element);
		else
			return null;
	}

	public boolean hasChildren(Object element) {
		if (element instanceof TextGridRepositoryItem)
			return true;
		else 
			return manager.mayHaveChildren(element);
	}

	public Object[] getElements(Object inputElement) {
		return this.getChildren(inputElement);
	}

	public void dispose() {
		if (input != null)
			this.input.removeChildListChangedListener(this);
	}

	public void cancel(Object input) {
		if (manager != null) {
			manager.cancel(input);
		}
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		this.viewer = viewer;

		if (viewer instanceof AbstractTreeViewer && this.manager == null) {
			manager = new DeferredTreeContentManager((AbstractTreeViewer) viewer);
		}
	}

	public void childListChanged(IChildListParent parent) {
		if (viewer != null) {
			viewer.setInput(parent);
		}
	}
}

