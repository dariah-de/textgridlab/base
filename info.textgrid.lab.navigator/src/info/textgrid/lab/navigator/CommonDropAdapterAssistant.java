package info.textgrid.lab.navigator;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.aggregations.ui.treeWriter.AggregationWriter;
import info.textgrid.lab.core.model.AggregationReader;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.AdapterUtils.AdapterNotFoundException;
import info.textgrid.lab.ui.core.DropSetValues;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgsearch.client.SearchClient;
import info.textgrid.namespaces.middleware.tgsearch.EntryType;
import info.textgrid.namespaces.middleware.tgsearch.PathGroupType;
import info.textgrid.namespaces.middleware.tgsearch.PathResponse;
import info.textgrid.namespaces.middleware.tgsearch.PathType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.stream.XMLStreamException;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IParameter;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.Parameterization;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.navigator.CommonDropAdapter;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;

public class CommonDropAdapterAssistant extends
		org.eclipse.ui.navigator.CommonDropAdapterAssistant {

	private final class PostSaveNavigatorUpdater extends JobChangeAdapter {
		private final List<TGObjectReference> movedTGOs;
		private final Object aTarget;

		private PostSaveNavigatorUpdater(List<TGObjectReference> movedTGOs, Object aTarget) {
			this.movedTGOs = movedTGOs;
			this.aTarget = aTarget;
		}

		public void done(IJobChangeEvent event) {
		    if (event.getResult().isOK()) {
		        UIJob uiJob = new UIJob(Messages.CommonDropAdapterAssistant_IM_UpdateNavigatorGUI) {
		    	    public IStatus runInUIThread(IProgressMonitor monitor) {
		    	    	for (TGObjectReference tgoRef : movedTGOs) {
		    	    		try {
								if (((TGObjectReference)aTarget).getTgo().getProjectInstance() == 
									tgoRef.getTgo().getProjectInstance()) {
									NaviView.getViewer().remove(tgoRef);
								}
							} catch (CoreException e) {
								Activator.handleError(e, Messages.CommonDropAdapterAssistant_EM_CouldNotUpdateNavigatorGUI);
							}
		    	    		NaviView.getViewer().add(aTarget,tgoRef);
		    	    	}
		    	        return Status.OK_STATUS;
		    	    }
		    	};
		    	// the remove operation updates the affected aggregation automatically,
		    	// so this delay is to make sure, that crud has already finished its operations.
		    	uiJob.schedule(500);
		    }	
		}
	}

	public CommonDropAdapterAssistant() {
	}

	@Override
	public IStatus handleDrop(CommonDropAdapter aDropAdapter,
			DropTargetEvent aDropTargetEvent, final Object aTarget) {
		if (aDropTargetEvent.data != null) {
			if (aDropTargetEvent.data instanceof TreeSelection) {
				try {
					LocalSelectionTransfer localTransfer = LocalSelectionTransfer
							.getTransfer();
					if (aTarget instanceof TextGridProject) {
						final IStatus status = dropToProject((TextGridProject) aTarget, localTransfer);
						if (!status.isOK())
							return status;
					} else if (aTarget instanceof TGObjectReference)
						dropToAggregation(aTarget, localTransfer);
				} catch (CoreException e) {
					StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
					return Status.CANCEL_STATUS;
				} catch (OfflineException e) {
					StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getLocalizedMessage(), e), StatusManager.SHOW);
				}
			} else {
				dropFromNonLab(aDropTargetEvent, aTarget);
			}
			// whole selection copied
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}

	private void dropFromNonLab(DropTargetEvent aDropTargetEvent, final Object aTarget) {
		//drag & drop operation from outside to the lab
		IHandlerService handlerService = (IHandlerService) PlatformUI
		.getWorkbench().getService(IHandlerService.class);
		
		ICommandService commandService = (ICommandService) PlatformUI.getWorkbench()
		.getActiveWorkbenchWindow().getService(ICommandService.class);
		
		Command command = commandService.getCommand("info.textgrid.lab.core.importexport.import"); //$NON-NLS-1$
		
		IParameter parameter;
		try {
			parameter = command.getParameter("info.textgrid.lab.core.importexport.import.usedroppedset"); //$NON-NLS-1$
			Parameterization parm = new Parameterization(parameter, "true");
			ParameterizedCommand parmCommand = new ParameterizedCommand(
					command, new Parameterization[] { parm });
			DropSetValues.setValues((String[])aDropTargetEvent.data);
			if (aTarget instanceof TextGridProject) 
				DropSetValues.setProject((TextGridProject)aTarget);
			handlerService.executeCommand(
					parmCommand,
					null);
		} catch (NotDefinedException e) {
			Activator.handleError(e, Messages.CommonDropAdapterAssistant_EM_CouldNotOpenImportPerspective, aTarget);
		} catch (ExecutionException e) {
			Activator.handleError(e, Messages.CommonDropAdapterAssistant_EM_CouldNotOpenImportPerspective, aTarget);
		} catch (NotEnabledException e) {
			Activator.handleError(e, Messages.CommonDropAdapterAssistant_EM_CouldNotOpenImportPerspective, aTarget);
		} catch (NotHandledException e) {
			Activator.handleError(e, Messages.CommonDropAdapterAssistant_EM_CouldNotOpenImportPerspective, aTarget);
		}
	}

	private IStatus dropToAggregation(final Object aTarget, LocalSelectionTransfer localTransfer) throws CoreException, OfflineException {
		{
			// drop on an aggregation
			//###################################
			//TG-1902
			//###################################
			final TextGridObject target = ((TGObjectReference) aTarget).getTgo();
			target.refreshWorkspaceIfNeccessary();

			final HashMap<TextGridObject, List<String>> aggregationMap = new HashMap<TextGridObject, List<String>>();
		
			final List<URI> targetsCurrentContent = AggregationReader.read(target, false);
			final List<TGObjectReference> movedTGOs = new ArrayList<TGObjectReference>();
			if (localTransfer.getSelection() instanceof TreeSelection) {
				TreeSelection droppedSelection = (TreeSelection) localTransfer.getSelection();
				@SuppressWarnings("rawtypes")
				Iterator droppedItemsIterator = droppedSelection.iterator();
				while (droppedItemsIterator.hasNext()) {
					Object droppedItem = droppedItemsIterator.next();
					if (droppedItem instanceof TGObjectReference) {
						
						//Feature #9156
						//##################################
						final URI droppedURI = ((TGObjectReference) droppedItem).getTgo().getPreparedURI();
						final ImmutableSet<URI> targetAncestors = getAncestors(target.getURI());
						//is it a circle?									
						if (targetAncestors.contains(toLatestURI(droppedURI))) {
							reportAggregationCycle(); 
							return Status.CANCEL_STATUS;
						} 

                        if (!confirmMove())
                        	return Status.CANCEL_STATUS;
						}
						//##################################
						
						
						targetsCurrentContent.add(URI.create(((TGObjectReference) droppedItem).getTgo().getLatestURI()));
						movedTGOs.add((TGObjectReference) droppedItem);
						if (((TGObjectReference)droppedItem).isPartOfAggregation()) {
							TextGridObject aggregation = ((TGObjectReference)droppedItem).getAggregation().getTgo(); 
							List<String> list = aggregationMap.get(aggregation);
							if (list == null)
								list = new LinkedList<String>();
							list.add(((TGObjectReference)droppedItem).getRefUri());
							aggregationMap.put(aggregation, list);
						}
					}
				}
				
				for (TGObjectReference ref : movedTGOs) {
					ref.setAggregation((TGObjectReference)aTarget);
				}
				
				Job saveJob = new Job(NLS.bind(Messages.CommonDropAdapterAssistant_IM_SavingAggregation, target)) {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						// save target aggregation
					SubMonitor progress = SubMonitor.convert(monitor,
							NLS.bind(Messages.CommonDropAdapterAssistant_IM_SavingAggregaion, target),
							100 + 100*aggregationMap.entrySet().size());
						try {
							IFile file = AdapterUtils.getAdapterChecked(target, IFile.class);
							InputStream aggregationContent = AggregationWriter.getAggregationStream(targetsCurrentContent);
							file.setContents(aggregationContent, true, false, progress.newChild(targetsCurrentContent.size()));
							
							// save changed source aggregations
							Iterator<Entry<TextGridObject, List<String>>> iterator = aggregationMap.entrySet().iterator();
							while (iterator.hasNext()) {
								Entry<TextGridObject, List<String>> e = iterator.next();
								TextGridObject sourceAggregation = e.getKey();
								List<String> val = e.getValue();
								List<URI> sourceURIs = new LinkedList<URI>();
								try {
									sourceAggregation.refreshWorkspaceIfNeccessary();
									List<URI> aggregatesSource = AggregationReader.read(sourceAggregation, false);
									for (URI uri : aggregatesSource) {
										if (!val.remove(uri.toString()))
											sourceURIs.add(uri);
									}
									try {
										final IFile sourceFile = AdapterUtils.getAdapterChecked(sourceAggregation, IFile.class);
										sourceFile.setContents(AggregationWriter.getAggregationStream(sourceURIs), 
												true, false, progress.newChild(100));
									} catch (XMLStreamException e2) {
										Activator.handleError(e2, Messages.CommonDropAdapterAssistant_EM_CouldNotWriteAggregation, sourceAggregation);
									} catch (AdapterNotFoundException e2) {
										StatusManager.getManager().handle(new Status(IStatus.WARNING, Activator.PLUGIN_ID, "source equals target -> noop"));
									} catch (CoreException e2) {
										Activator.handleError(e2, Messages.CommonDropAdapterAssistant_EM_CouldNotWriteAggregation, sourceAggregation);
									}
								} catch (CoreException e1) {
									StatusManager.getManager().handle(e1, Activator.PLUGIN_ID);
								}
							}
						} catch (XMLStreamException e) {
							Activator.handleError(e, Messages.CommonDropAdapterAssistant_EM_CouldNotWriteAggregation, target);
						} catch (AdapterNotFoundException e) {
							StatusManager.getManager().handle(new Status(IStatus.WARNING, Activator.PLUGIN_ID, "source equals target -> noop"));
						} catch (CoreException e) {
							Activator.handleError(e, Messages.CommonDropAdapterAssistant_EM_CouldNotWriteAggregation, target);
						}
						
						aggregationMap.clear();
						return Status.OK_STATUS;
					}
				};
				
				saveJob.addJobChangeListener(new PostSaveNavigatorUpdater(movedTGOs, aTarget));
				
				saveJob.setUser(true);
				saveJob.schedule();
			}
		return Status.OK_STATUS;
		}

	/**
	 * Returns whether the user really wants to move that file (potentially showing a dialog)
	 */
	private boolean confirmMove() {
		boolean confirmMove = info.textgrid.lab.conf.ConfPlugin.getDefault().getPreferenceStore().getBoolean("movewarning");
		if (confirmMove) {
			MessageDialog messageDialog = new MessageDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						Messages.MOVE_ShowReport, null, Messages.MOVE_ReportMessage, MessageDialog.QUESTION,
							new String[] { "OK",
											Messages.CopyService_Cancel }, 1);

			return messageDialog.open() == 0;
		} else
			return true;
	}

	/**
	 * Report to the user that operation would introduce a cycle.
	 */
	private void reportAggregationCycle() {
		MessageBox dialog = 
				  new MessageBox(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow()
							.getShell(), SWT.ERROR | SWT.OK);
				dialog.setText(Messages.MOVE_ShowReport);
				dialog.setMessage(Messages.MOVE_Circle_ReportMessage);

				//# open dialog and await user selection
				dialog.open();
	}

	/**
	 * The stuff has been dropped onto a project -> copy it there
	 */
	private IStatus dropToProject(final TextGridProject aTarget, LocalSelectionTransfer localTransfer) throws CoreException {
		// drop on a project
		ArrayList<String> tgSelectionUris = new ArrayList<String>();
		String targetName = ((TextGridProject) aTarget).getId();
		if (localTransfer.getSelection() instanceof TreeSelection) {
			TreeSelection treeSelection = (TreeSelection) localTransfer
					.getSelection();
			@SuppressWarnings("rawtypes")
			Iterator iterator = treeSelection.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				TextGridObject tgo1 = AdapterUtils.getAdapter(next, TextGridObject.class);
				if (tgo1 != null) {
					if (tgo1.getProject().equals(targetName)) {
						// copying within original project
						StatusManager
								.getManager()
								.handle(new Status(IStatus.WARNING,
										Activator.PLUGIN_ID,
										"source equals target -> noop"));
						return Status.CANCEL_STATUS;
					}
					tgSelectionUris.add(tgo1.getURI().toString());
				}
			}
			CopyService.getInstance().copy(tgSelectionUris, targetName);
		}
		return Status.OK_STATUS;
	}

	
	private URI toLatestURI(URI uri) {
		return URI.create(uri.toString().split("\\.")[0]);
	}
	
	/**
	 * Returns the _latest_ URIs all aggregations that (directly or transitively) aggregate the given (target)-object
	 * @throws OfflineException when we can't reach TG-search
	 */
	private ImmutableSet<URI> getAncestors(URI uri) throws OfflineException {
		final Builder<URI> ancestors = ImmutableSet.builder();
		final SearchClient searchClient = new SearchClient(ConfClient.getInstance().getValue(ConfservClientConstants.TG_SEARCH));
		final PathResponse pr = searchClient.getPath(uri.toString());
		ancestors.add(toLatestURI(uri));
		for (final PathGroupType group : pr.getPathGroup())
			for (final PathType path : group.getPath())
				for (final EntryType entry : path.getEntry())
					ancestors.add(toLatestURI(URI.create(entry.getTextgridUri())));

		//System.out.println("--->>>>"+uris.toString());
		return ancestors.build();
	}

	@Override
	public IStatus validateDrop(Object target, int operation,
			TransferData transferType) {
		return Status.OK_STATUS;
	}
	
	@Override
	public boolean isSupportedType(TransferData aTransferType) {
		return true;
	}

}
