package info.textgrid.lab.navigator;

import info.textgrid.lab.ui.core.utils.TextGridObjectTransfer;
import info.textgrid.lab.ui.core.utils.TextGridUriHtmlTransfer;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;

public class CommonDragAdapterAssistant extends
		org.eclipse.ui.navigator.CommonDragAdapterAssistant {

	public CommonDragAdapterAssistant() {
	}

	@Override
	public Transfer[] getSupportedTransferTypes() {
		return new Transfer[] {
				TextGridObjectTransfer.getTransfer(),
				TextGridUriHtmlTransfer.getTransfer()
		};
	}

	@Override
	public boolean setDragData(DragSourceEvent anEvent,
			IStructuredSelection aSelection) {
		Object obj = aSelection.getFirstElement();
		anEvent.data = obj;
		return true;
	}

}
