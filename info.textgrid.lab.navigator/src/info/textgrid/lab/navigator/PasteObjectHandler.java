package info.textgrid.lab.navigator;

import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.ui.core.utils.TextGridObjectTransfer;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

public class PasteObjectHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		Object copyJob = null;
		String projectTargetID = "";
		try {
			// #13614
			ISelection selection = HandlerUtil.getCurrentSelection(event);
			if (selection instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selection).getFirstElement();
				if (firstElement != null && firstElement instanceof TextGridProject) {
					projectTargetID = ((TextGridProject)firstElement).getId();
				}
			}

			TextGridObjectTransfer tgTransfer = TextGridObjectTransfer
					.getTransfer();
			ISelection tgSelection = tgTransfer.getCopiedSelection();
//			Object applicationContext = event.getApplicationContext();
			
			if (tgSelection != null /*&& applicationContext instanceof EvaluationContext*/) {				
//				EvaluationContext ec = (EvaluationContext) applicationContext;
//				Object defaultVariable = ec.getDefaultVariable();
//				List da = (List)defaultVariable;
//				Object firstElement = da.get(0);
//				if (firstElement != null && firstElement instanceof TextGridProject) {
					CopyService.getInstance().copy(tgTransfer.getCopiedSelectionAsStringList(),
							projectTargetID);
//				}

			}
		} catch (Exception e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					Messages.PasteObjectHandler_EM_NotAbleToCopyObjects, e);
			Activator.getDefault().getLog().log(status);
		}
		return copyJob;
	}

}