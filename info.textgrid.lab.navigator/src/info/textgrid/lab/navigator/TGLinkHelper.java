package info.textgrid.lab.navigator;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.navigator.ILinkHelper;

public class TGLinkHelper implements ILinkHelper {

	@Override
	public IStructuredSelection findSelection(IEditorInput anInput) {
		TextGridObject tgo = (TextGridObject) anInput.getAdapter(TextGridObject.class);
		if (tgo != null) {
			return new StructuredSelection(NaviView.getTGObjectReference(tgo));
		}
		return null;
	}

	@Override
	public void activateEditor(IWorkbenchPage aPage,
			IStructuredSelection aSelection) {
		if (aSelection == null || aSelection.isEmpty())
			return;
		if (aSelection.getFirstElement() instanceof TGObjectReference) {
			TGObjectReference tgoRef = (TGObjectReference) aSelection.getFirstElement();
			IEditorInput input = null;
			try {
				if (tgoRef.getTgo().getContentTypeID().contains("aggregation")) //$NON-NLS-1$
					input = (IEditorInput) getAdapter(
						((TGObjectReference)aSelection.getFirstElement()),
						IEditorInput.class);
				else 
					input = (IEditorInput) getAdapter(
							((TGObjectReference)aSelection.getFirstElement()).getTgo(),
							IEditorInput.class);
			} catch (CoreException e) {
				Activator.handleError(e,
						 Messages.TGLinkHelper_EM_CouldNotRetieveContentTypeOf, tgoRef);
			}
			IEditorPart editor = null;
			if ((editor = aPage.findEditor(input)) != null)
				aPage.bringToTop(editor);
		}
	}

}
