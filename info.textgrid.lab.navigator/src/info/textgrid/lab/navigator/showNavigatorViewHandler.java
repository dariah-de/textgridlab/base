package info.textgrid.lab.navigator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.intro.IIntroManager;

public class showNavigatorViewHandler extends AbstractHandler implements IHandler{

	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		
	
		// TODO Auto-generated method stub
		try {
			IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			if (activePage == null) 
				MessageDialog
				.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), Messages.showNavigatorViewHandler_HeaderNoOpenPerspective,
						Messages.showNavigatorViewHandler_IM_NoOpenPerspective);
			else 
				activePage.showView("info.textgrid.lab.navigator.view"); //$NON-NLS-1$
			
			// close Welcome-Screen
			IIntroManager manager = PlatformUI.getWorkbench().getIntroManager();
			//manager.setIntroStandby(manager.getIntro(), true);
			manager.closeIntro(manager.getIntro());
		} catch (Exception e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    Messages.showNavigatorViewHandler_EM_CouldNotOpenView, e);
			Activator.getDefault().getLog().log(status);
		}
		return null;
	}

}
