package info.textgrid.lab.navigator;

import static info.textgrid.lab.core.model.Activator.PLUGIN_ID;
import info.textgrid.lab.authn.AuthBrowser;
import info.textgrid.lab.authn.AuthBrowser.ISIDChangedListener;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.OnlineStatus.IStatusChangeListener;
import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.ITextGridProjectListener;
import info.textgrid.lab.core.model.ProjectDoesNotExistException;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TGObjectReference.ITGObjectReferenceListener;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.navigator.filters.ContentFilter;
import info.textgrid.lab.navigator.filters.ProjectFilter;

import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.common.CommandException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.handlers.RadioState;
import org.eclipse.ui.navigator.CommonNavigator;
import org.eclipse.ui.navigator.CommonViewer;

public class NaviView extends CommonNavigator {

	private Action action1;
	private IMemento memento;

	private static CommonViewer viewer;

	private ITextGridObjectListener objectListener;
	private ITextGridProjectListener projectListener;
	private ITGObjectReferenceListener tgObjectReferenceListener;
	private ISIDChangedListener sidChangedListener;

	public static enum NavigatorSortModi {
		MODIFICATION_DATE, TITLE, TYPE, NOSORTING
	};

	public static enum NavigatorSortSequence {
		ASCENDING, DESCENDING
	};

	public static NavigatorSortModi sortModus;
	public static NavigatorSortSequence sortSequence = NavigatorSortSequence.ASCENDING;

	private static Map<TextGridObject, TGObjectReference> objectReferenceRegistry = new ConcurrentHashMap<TextGridObject, TGObjectReference>();

	/**
	 * This method is used to reload all TGProjects and TGObjects in the
	 * Navigator
	 */
	public static void refreshNavigator() {

		NaviExpandedElements exElements = NaviExpandedElements.getInstance();
		exElements.setExpandedTreePaths(viewer.getExpandedTreePaths());
		objectReferenceRegistry.clear();
		TextGridProjectRoot.getInstance().reset();
	}

	@Override
	public void init(IViewSite site, IMemento memento) throws PartInitException {
		super.init(site, memento);
		this.memento = memento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.navigator.CommonNavigator#createCommonViewer(org.eclipse
	 * .swt.widgets.Composite)
	 * 
	 * SWT.VIRTUAL flag is needed for deferredness
	 */
	@Override
	protected CommonViewer createCommonViewer(Composite aParent) {
		CommonViewer aViewer = new CommonViewer(getViewSite().getId(), aParent,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.VIRTUAL);
		initListeners(aViewer);
		aViewer.getNavigatorContentService().restoreState(memento);
		aViewer.addTreeListener(new ITreeViewerListener() {
			public void treeCollapsed(TreeExpansionEvent event) {
				NaviExpandedElements.getInstance().clear();
			}

			public void treeExpanded(TreeExpansionEvent event) {
				NaviExpandedElements.getInstance().clear();
			}
		});
		return aViewer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.navigator.CommonNavigator#getInitialInput()
	 * 
	 * A Listener is added to the AuthBrowser which calls refreshNavigator()
	 * when the Authentication-PArameter has changed. Another Listener is added
	 * to TextGridObject. This reacts to DELETED, CREATED, CREATED_TEMPORARY and
	 * METADATA_CHANGED events by adding, removing or updating the corresponding
	 * TextGridObject. TextGridProjectRoot is returned as initial input for the
	 * Navigator.
	 */
	@Override
	protected IAdaptable getInitialInput() {
		viewer = this.getCommonViewer();

		// set persistent sort modus
		ICommandService cmdService = (ICommandService) getSite().getService(
				ICommandService.class);
		String modusValue = (String) cmdService
				.getCommand("info.textgrid.lab.navigator.sortprojectcontent"). //$NON-NLS-1$
				getState(RadioState.STATE_ID).getValue();

		try {
			sortModus = NavigatorSortModi.valueOf(modusValue);
		} catch (Exception e) {
			sortModus = NavigatorSortModi.TITLE;
		}

		// set persistent sort sequence
		String sequenceValue = (String) cmdService
				.getCommand("info.textgrid.lab.navigator.sequenceorder"). //$NON-NLS-1$
				getState(RadioState.STATE_ID).getValue();

		try {
			sortSequence = NavigatorSortSequence.valueOf(sequenceValue);
		} catch (Exception e) {
			sortSequence = NavigatorSortSequence.ASCENDING;
		}

		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();

		viewer.getTree().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.F5) {
					refreshNavigator();
				}
				// TG-1993
				else if ((e.stateMask == SWT.CTRL & e.keyCode == 99)
						|| (e.stateMask == SWT.COMMAND & e.keyCode == 99)) { //TG-2046
					// copy
					try {
						IHandlerService hS = (IHandlerService) getSite()
								.getService(IHandlerService.class);
						hS.executeCommand(
								"info.textgrid.lab.navigator.copytgobject",
								null);
					} catch (Exception ex) {
						// System.err.println("info.textgrid.lab.navigator.copytgobject-Command"+
						// ex.getMessage());
						Activator
								.handleError(ex,
										"copy-Exception with (Ctrl/Cmd + c)",
										(Object[]) null);
					}
				} else if ((e.stateMask == SWT.CTRL & e.keyCode == 118)
						|| (e.stateMask == SWT.COMMAND & e.keyCode == 118)) {  //TG-2046
					// paste
					try {
						IHandlerService hS = (IHandlerService) getSite()
								.getService(IHandlerService.class);
						hS.executeCommand(
								"info.textgrid.lab.navigator.pastetgobject",
								null);
					} catch (Exception ex) {
						// System.err.println("info.textgrid.lab.navigator.pastetgobject-Command"+
						// ex.getMessage());
						Activator.handleError(ex,
								"paste-Exception with (Ctrl/Cmd + v)",
								(Object[]) null);
					}
				}
			}
		});

		objectListener = new ITextGridObjectListener() {

			public void textGridObjectChanged(
					info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event event,
					TextGridObject object) {
				TGObjectReference tgoRef = objectReferenceRegistry.get(object);
				if (tgoRef == null && event != Event.CREATED)
					return;

				switch (event) {
				case DELETED:
					viewer.remove(tgoRef);
					objectReferenceRegistry.remove(object);
					break;

				// case CREATED_TEMPORARY: try {
				// if (object.isComplete()) {
				// viewer.add(object.getProjectInstance(), object);
				// }
				// // System.out.println("cr_temp called on: " +
				// object.getNameCandidate() + "name: " + object.getTitle() +
				// ".");
				// } catch (CoreException e) {
				// Activator.handleError(e,
				// "Could not add the object {0} to the Navigator.", object);
				// }
				// break;

				case CREATED:
					try {
						tgoRef = new TGObjectReference(object.getLatestURI(),
								object);
						TGObjectReference
								.notifyListeners(
										ITGObjectReferenceListener.Event.NAVIGATOR_OBJECT_CREATED,
										tgoRef);
						if (!object.getContentTypeID().equals(
								"text/tg.projectfile+xml")) { //$NON-NLS-1$
							if ((!object.getRevisionNumber().equals("") && !object
									.getRevisionNumber().equals("0"))
									|| object.getContentTypeID().contains(
											"aggregation")) {
								try {
									viewer.refresh(TextGridProject
											.getProjectInstance(object
													.getProject()));
								} catch (RemoteException e) {
									Activator
											.handleProblem(IStatus.ERROR, e,
													"TextGridProject.getProjectInstance");
								} catch (ProjectDoesNotExistException e) {
									throw new CoreException(
											new Status(
													IStatus.ERROR,
													PLUGIN_ID,
													MessageFormat
															.format(Messages.NaviView_EM_ProjectDoesNotExist,
																	this,
																	object.getProject())));
								}
							} else {
								viewer.add(object.getProjectInstance(), tgoRef);
							}
							// System.out.println("cr called on: " +
							// object.getNameCandidate());
						}
					} catch (CoreException e) {
						Activator.handleError(e,
								Messages.NaviView_EM_CouldNotOpenNavigator,
								object);
					}
					break;

				case METADATA_SAVED:
				case METADATA_CHANGED:
					viewer.update(tgoRef, null);
					break;
				}

			}

		};

		TextGridObject.addListener(objectListener);

		sidChangedListener = new ISIDChangedListener() {
			public void sidChanged(String newSID, String newEPPN) {
				refreshNavigator();
			}
		};
		AuthBrowser.addSIDChangedListener(sidChangedListener);

		projectListener = new ITextGridProjectListener() {
			@Override
			public void textGridProjectChanged(
					ITextGridProjectListener.Event event,
					TextGridProject project) {
				if (event.equals(Event.CONTENT_CHANGED)) {
					refreshNavigator();
				} else if (event.equals(Event.NO_CHILDREN_CHECK)) {
					viewer.setHasChildren(project, false);
				}
			}
		};
		TextGridProject.addListener(projectListener);

		tgObjectReferenceListener = new ITGObjectReferenceListener() {
			@Override
			public void tgObjectReferenceChanged(
					info.textgrid.lab.core.model.TGObjectReference.ITGObjectReferenceListener.Event event,
					TGObjectReference object) {
				if (event.equals(Event.NAVIGATOR_OBJECT_CREATED))
					objectReferenceRegistry.put(object.getTgo(), object);

			}
		};

		TGObjectReference.addListener(tgObjectReferenceListener);

		// set existing filters
		setFilters();

		return TextGridProjectRoot.getInstance();
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {

				NaviView.this.fillContextMenu(manager);

			}
		});
		Menu menu = menuMgr.createContextMenu(this.getCommonViewer()
				.getControl());
		this.getCommonViewer().getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, this.getCommonViewer());

	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(action1);
		manager.add(new Separator());
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(action1);
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(action1);
		manager.add(new Separator());
	}

	/**
	 * A "refresh"-button is added to the navigator. An IStatusChangeListener is
	 * registered at OnlineStatus which disables the "refresh"-button when
	 * offline and enables it when online.
	 */
	private void makeActions() {
		action1 = new Action() {
			@Override
			public void run() {

				refreshNavigator();

			}
		};

		IStatusChangeListener listener = new IStatusChangeListener() {

			public void statusChanged(boolean newStatus) {
				action1.setEnabled(newStatus);

			}

		};
		OnlineStatus.addOnlineStatusChangeListener(listener);
		action1.setImageDescriptor(ImageDescriptor.createFromFile(
				this.getClass(), "/resources/refresh.gif")); //$NON-NLS-1$
		action1.setText(Messages.NaviView_Refresh);
		action1.setToolTipText(Messages.NaviView_Refresh);
		// System.out.println("isonline: " + OnlineStatus.isOnline());
		if (!OnlineStatus
				.isOnlineWithNotification(Messages.NaviView_IM_NeedToBeOnline)) {
			action1.setEnabled(false);
			// System.out.println("not online!!");
		}

	}

	/**
	 * Adds an IDoubleClickListener to this viewer. It tries to open a
	 * TextGridObject on which a double-click is performed by executing the
	 * "info.textgrid.lab.ui.core.commands.Open"-Command.
	 */
	private void hookDoubleClickAction() {
		this.getCommonViewer().addDoubleClickListener(
				new IDoubleClickListener() {
					public void doubleClick(DoubleClickEvent event) {

						ISelection selection = viewer.getSelection();
						Object obj = ((IStructuredSelection) selection)
								.getFirstElement();

						TextGridObject tgo = AdapterUtils.getAdapter(obj,
								TextGridObject.class);
						if (tgo != null) {
							String contentType;
							try {
								contentType = tgo.getContentTypeID();
							} catch (CoreException e1) {
								contentType = "";
							}
							if (!contentType.contains("aggregation")) { //$NON-NLS-1$
								IHandlerService handlerService = (IHandlerService) getSite()
										.getService(IHandlerService.class);
								try {
									handlerService
											.executeCommand(
													"info.textgrid.lab.ui.core.commands.Open", //$NON-NLS-1$
													null);
								} catch (CommandException e) {
									Activator
											.handleError(
													e,
													Messages.NaviView_EM_CouldNotOpenFor,
													event);
								}
							}

						}
					}
				});

	}

	@Override
	public void dispose() {

		AuthBrowser.removeSIDChangedListener(sidChangedListener);
		TextGridObject.removeListener(objectListener);
		TextGridProject.removeListener(projectListener);

		super.dispose();
	}

	public ISelection getSelection() {
		return this.getCommonViewer().getSelection();
	}

	public void setSelection(ISelection sel, boolean reveal) {
		this.getCommonViewer().setSelection(sel, reveal);
	}

	public NavigatorSortModi getSortModus() {
		return sortModus;
	}

	public void setSortModus(NavigatorSortModi sortModus) {
		this.sortModus = sortModus;
	}

	public static boolean isExpanded() {
		return (viewer.getExpandedElements().length != 0);
	}

	public static void collapseAll() {
		viewer.collapseAll();
	}

	public static TGObjectReference getTGObjectReference(TextGridObject tgo) {
		return objectReferenceRegistry.get(tgo);
	}

	public static CommonViewer getViewer() {
		return viewer;
	}

	/**
	 * Set filters for the navigator tree.
	 */
	private void setFilters() {
		String content = info.textgrid.lab.navigator.Activator
				.getPreference(info.textgrid.lab.navigator.Activator.FILTER_PROJECT);
		if (!content.equals(""))
			viewer.addFilter(new ProjectFilter(content));

		content = info.textgrid.lab.navigator.Activator
				.getPreference(info.textgrid.lab.navigator.Activator.FILTER_CONTENT_TYPE);
		if (!content.equals(""))
			viewer.addFilter(new ContentFilter(content));
	}

}
