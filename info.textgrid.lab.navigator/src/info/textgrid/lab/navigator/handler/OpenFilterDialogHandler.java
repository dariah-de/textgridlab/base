package info.textgrid.lab.navigator.handler;

import info.textgrid.lab.navigator.Activator;
import info.textgrid.lab.navigator.filters.FilterDialog;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;

public class OpenFilterDialogHandler extends AbstractHandler implements IHandler, IElementUpdater{

	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		
		FilterDialog.openFilterDialog();
		return null;
	}

	@Override
	public void updateElement(UIElement element, @SuppressWarnings("rawtypes") Map parameters) {
		if (!Activator.getPreference(Activator.FILTER_PROJECT).equals("")) {
			element.setIcon(Activator.getDefault().getImageRegistry().getDescriptor(Activator.IMG_FILTERED));
			return;
		}
		if (!Activator.getPreference(Activator.FILTER_CONTENT_TYPE).equals("")) {
			element.setIcon(Activator.getDefault().getImageRegistry().getDescriptor(Activator.IMG_FILTERED));
			return;
		}
		element.setIcon(Activator.getDefault().getImageRegistry().getDescriptor(Activator.IMG_FILTER));
	}

}
