package info.textgrid.lab.navigator.handler;

import info.textgrid.lab.navigator.NaviView;
import info.textgrid.lab.navigator.NaviView.NavigatorSortSequence;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.RadioState;

public class SequenceOrderHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		if(HandlerUtil.matchesRadioState(event))
	        return null; // updated state - do nothing
	    
		String currentState = event.getParameter(RadioState.PARAMETER_ID);
		NavigatorSortSequence sequence = NavigatorSortSequence.valueOf(currentState);
		
		// set navigator sequence and refresh it
		NaviView.sortSequence = sequence;
		if (NaviView.isExpanded())
			NaviView.refreshNavigator();

	    // update the current state
	    HandlerUtil.updateRadioState(event.getCommand(), currentState);

	    return null;
	}

}