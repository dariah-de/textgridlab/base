package info.textgrid.lab.navigator.handler;

import info.textgrid.lab.navigator.NaviView;
import info.textgrid.lab.navigator.NaviView.NavigatorSortModi;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.RadioState;

public class SortNavigatorContentHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		if(HandlerUtil.matchesRadioState(event))
	        return null; // updated state - do nothing
	    
		String currentState = event.getParameter(RadioState.PARAMETER_ID);
		NavigatorSortModi modus = NavigatorSortModi.valueOf(currentState);
		
		// set navigator modus and refresh it
		NaviView.sortModus = modus;
		if (NaviView.isExpanded())
			NaviView.refreshNavigator();

	    // update the current state
	    HandlerUtil.updateRadioState(event.getCommand(), currentState);

	    return null;
	}

}
