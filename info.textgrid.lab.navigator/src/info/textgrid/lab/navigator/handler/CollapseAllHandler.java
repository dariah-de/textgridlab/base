package info.textgrid.lab.navigator.handler;

import info.textgrid.lab.navigator.NaviView;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;

public class CollapseAllHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		// collapse command must be called twice to fix TG-1330
		NaviView.collapseAll();
		NaviView.collapseAll();
		return null;
	}

}
