package info.textgrid.lab.navigator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {
	
	// Images
	public static final String IMG_FILTER = "filter"; //$NON-NLS-1$
	public static final String IMG_FILTERED = "filtered"; //$NON-NLS-1$

	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		super.initializeImageRegistry(reg);
		reg.put(IMG_FILTER, imageDescriptorFromPlugin(PLUGIN_ID, "resources/filter.gif")); //$NON-NLS-1$
		reg.put(IMG_FILTERED, imageDescriptorFromPlugin(PLUGIN_ID, "resources/filtered.gif")); //$NON-NLS-1$
	}

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.navigator";
	
	//The identifiers for the preferences
	public static final String FILTER_PROJECT = "filterProject";
	public static final String FILTER_CONTENT_TYPE = "filterContentType";

	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}
	
	/**
	 * Cares for the given problem. The <var>message</var> and the
	 * <var>args</var> are passed to {@link NLS#bind(String, Object[])} and the
	 * complete argument list to {@link #handleProblem(int, String, Throwable)}.
	 * 
	 * @param severity
	 *            One of the severity constants defined in {@link IStatus}.
	 *            Currently, IStatus.ERROR will pop up an error dialogue.
	 * @param cause
	 *            The exception that caused the problem (or null)
	 * @param message
	 *            The message, use {0}, {1} ... for arguments
	 * @param args
	 *            Exactly as many arguments as referenced in the message
	 * @return the {@link IStatus} object created while handling the error
	 */	
	public static IStatus handleProblem(int severity, Throwable cause,
			String message, Object... args) {
		return Activator.getDefault().handleProblem(severity,
				NLS.bind(message, args), cause);
	}
	
	public IStatus handleProblem(int severity, String message, Throwable cause) {
		if (message == null || message.equals("")) {
			if (cause != null)
				message = cause.getLocalizedMessage();
		}
		IStatus status = new Status(severity, PLUGIN_ID, message, cause);
		StatusManager.getManager().handle(
				status,
				status.getSeverity() == IStatus.ERROR ? StatusManager.SHOW
						| StatusManager.LOG : StatusManager.LOG);
		return status;
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	public static void handleError(Throwable e, String message, Object... args) {
		StatusManager.getManager().handle(
				new Status(IStatus.ERROR, PLUGIN_ID, NLS.bind(message, args)));
	}
	
	/**
	 * Delivers the stored value of the requested preference.
	 * @param preferenceID
	 * 				ID of requested preference
	 * @return value of preference
	 */
	public static String getPreference(String preferenceID) {
		return getDefault().getPreferenceStore().getString(preferenceID);
	}
	
	/**
	 * Set the value of the given preference, and triggers 
	 * the property change event.
	 * @param preferenceID
	 * 				ID of the preference
	 * @param value
	 * 				value of the preference
	 */
	public static void setPreference(String preferenceID, String value) {
		String oldValue = getDefault().getPreferenceStore().getString(preferenceID);
		getDefault().getPreferenceStore().putValue(preferenceID, value);
		getDefault().getPreferenceStore().firePropertyChangeEvent(preferenceID, oldValue, value);
	}

}
