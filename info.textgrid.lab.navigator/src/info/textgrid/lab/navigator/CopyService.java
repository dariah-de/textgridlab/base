package info.textgrid.lab.navigator;

import info.textgrid.lab.authn.AuthBrowser;
import info.textgrid.lab.authn.AuthBrowser.ISIDChangedListener;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishStatus;
import info.textgrid.middleware.tgpublish.client.PublishClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

/**
 * Service to copy TextGrid objects. The service
 * uses TG-publish's copy function.
 * 
 * @author Frank Queens for TextGrid
 */
public class CopyService {
	
	private final class CopyJob extends Job {
		private final List<String> objects;
		private final String projectID;

		private CopyJob(String name, List<String> objects, String projectID) {
			super(name);
			this.objects = objects;
			this.projectID = projectID;
		}

		@Override
		public IStatus run(IProgressMonitor monitor) {
			String UUID;
			
			SubMonitor progress = SubMonitor.convert(monitor,
					NLS.bind(Messages.CopyService_IM_CopyObjects,
							count),
							100);

			Response response = publishClient.copy(objects, projectID);
			InputStream is = (InputStream)response.getEntity();
			
			try {
				UUID = convertStreamToString(is);
				int lastProgress = 0;
				while (true) {
					PublishResponse publishResponse = publishClient.getStatus(UUID);
					PublishStatus publishStatus = publishResponse.getPublishStatus();
					int currentProgress = publishStatus.progress;
					if (currentProgress == 100) {
						publishObjects = publishResponse.getPublishObjects();
						break;
					} else if (currentProgress != lastProgress) {
						progress.worked(currentProgress - lastProgress);
						lastProgress = currentProgress;
					}
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						Activator.handleError(e, e.getMessage(), "CopyService");
					}
					
					if (progress.isCanceled()) {
						publishClient.abort(UUID);
						publishObjects = publishResponse.getPublishObjects();
						userAbort = true;
						break;
					}	
				}	
				
			} catch (IOException e) {
				Activator.handleError(e, e.getMessage(), "CopyService");
				return Status.CANCEL_STATUS;
			}
			
			UIJob refreshNavi = new UIJob(Messages.CopyTGObjectByWorkflow_RefreshNavigator) {
				@SuppressWarnings("static-access")
				@Override
				public IStatus runInUIThread(IProgressMonitor arg0) {
					NaviView navi = (NaviView) PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getActivePage()
							.findView("info.textgrid.lab.navigator.view"); //$NON-NLS-1$
					navi.refreshNavigator();
					return new Status(IStatus.OK, Activator.PLUGIN_ID,
							Messages.CopyTGObjectByWorkflow_RefreshNavigator);
				}
			};
			refreshNavi.schedule();
			
			if (showCopyReport || userAbort) {
				UIJob copyReport = new UIJob("Copy Report Dialog") {
					@Override
					public IStatus runInUIThread(IProgressMonitor arg0) {
						CopyReportDialog reportDialog = new CopyReportDialog(
								PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
						reportDialog.setPublishObjects(publishObjects, allCopyURIs, userAbort);
						reportDialog.open();
						return Status.OK_STATUS;
					}
				};
				copyReport.schedule();
			}
			
			if (userAbort)
				return Status.CANCEL_STATUS;
			else
				return Status.OK_STATUS;
		}
		
		public String convertStreamToString(InputStream is) throws IOException {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();

			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			br.close();
			return sb.toString();
		}
	}
	
	private final class GetURIsJob extends Job {
		private List<String> objects; 
		
		public GetURIsJob(String name, List<String> objects) {
			super(name);
			this.objects = objects;
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			monitor.setTaskName(Messages.CopyService_IM_CanTageAMoment);
			gatherObjectURIs(objects);
			count = getObjectCounter();
			return Status.OK_STATUS;
		}
		
	}
	
	private static CopyService instance;
	private static PublishClient publishClient;
	private Job copyJob = null;
	private ISIDChangedListener sidChangedListener;
	private boolean showCopyReport = true;
	private boolean userAbort = false;
	private List<PublishObject> publishObjects;
	private HashMap<String,HashMap<String,Integer>> allCopyURIs;
	private int count;

	private CopyService() {
		String SID = RBACSession.getInstance().getSID(false);
		try {
			publishClient = new PublishClient(ConfClient.getInstance().
					getValue(ConfservClientConstants.TGPUBLISH), SID);
			sidChangedListener = new ISIDChangedListener() {
				public void sidChanged(String newSID, String newEPPN) {
					instance = null;
				}
			};
			AuthBrowser.addSIDChangedListener(sidChangedListener);
		} catch (OfflineException e) {
			Activator.handleError(e, e.getMessage(), "CopyService");
		}
	}
	
	
	/**
	 * Provides the CopyService instance.
	 * @return
	 * 			CopyService instance
	 */
	public synchronized static CopyService getInstance() 
    {
    	if (instance == null) {
        	instance = new CopyService();
        }
        return instance;
    }
	
	/**
	 * Copies the given list of TextGrid objects. First the user
	 * has to confirm the copy process in a dialog. Afterwards a
	 * {@link #CopyJob} is scheduled.
	 * @param objects
	 * 			uri list of object, which should be copied
	 * @param projectID
	 * 			target project id
	 */
	public void copy(final List<String> objects, final String projectID) {
		userAbort = false;
		GetURIsJob getURIsJob = new GetURIsJob(Messages.CopyService_IM_FindNumberOfObjects, objects);
		
		getURIsJob.addJobChangeListener(new JobChangeAdapter() {
			public void done(IJobChangeEvent event) {
		        if (event.getResult().isOK()) {
		        	
		        	UIJob copydialog = new UIJob("Copy Service") {
						
						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							String message;
				        	if (count == 1)
				    			message = NLS.bind(Messages.CopyService_QE_DoYouReallyWantToCopy, objects.get(0));
				    		else
				    			message = NLS.bind(Messages.CopyService_QU_DoYouReallyWantToCopyNumber,
				    					count);

				    		if (objects.size() < 1) {
				    			MessageDialog
				    					.openError(
				    							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				    							Messages.CopyService_UI_NothingToCopy,
				    							Messages.CopyService_UI_NoSelection);
				    			return Status.CANCEL_STATUS;
				    		} else {
				    			final MessageDialogWithToggle dialog = new MessageDialogWithToggle (
				    					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				    					Messages.CopyService_UI_CopyObjects, 
				    					null, 
				    					message,
				    					MessageDialog.QUESTION, 
				    					new String[] { Messages.CopyService_Cancel, "OK" }, 
				    					1,
				    					Messages.CopyService_ShowReport,
				    					showCopyReport);
				    			
				    			int doCopy = dialog.open();
				    			showCopyReport = dialog.getToggleState();
				    			
				    			//Bug #11405
				    			if ( doCopy != Window.OK )
				    				return Status.CANCEL_STATUS; // Cancel
				    			
//				    			if (doCopy == 1 || doCopy == 256)
//				    				return Status.CANCEL_STATUS; // Cancel
				    		}
				    		
				    		copyJob = new CopyJob(Messages.CopyService_UI_Copying, objects, projectID);
				    		copyJob.setUser(true);
				    		copyJob.schedule();
							return Status.OK_STATUS;
						}
					};
					copydialog.setUser(true);
					copydialog.schedule();
		        }
			}    
		});
		
		getURIsJob.setUser(true);
		getURIsJob.schedule();
	}
	
	/**
	 * Retrieves all objects by TG-Search. If a given
	 * objects is an aggregation all the nested objects 
	 * are regarded.
	 * @param objects
	 */
	private void gatherObjectURIs(List<String> objects) {
		allCopyURIs = new HashMap<String,HashMap<String,Integer>>();
		HashMap<String,Integer> titleCount;
		SearchRequest searchRequestPublic = null;
		SearchRequest searchRequestNonPublic = null;
		
		for(String object : objects) {
			try {
				TextGridObject tgo = TextGridObject.getInstance(new URI(object), true);
				String title = tgo.getTitle();
				int count;
				
				if (allCopyURIs.containsKey(object)) {
					titleCount = allCopyURIs.get(object);
					count = titleCount.get(title);
					count++;
					titleCount.put(title, count);
				} else {
					titleCount = new HashMap<String,Integer>();
					titleCount.put(title, 1);
					allCopyURIs.put(object, titleCount);
				}

				if (tgo.getContentTypeID().contains("aggregation")) {
					if (tgo.isPublic()) {
						if (searchRequestPublic == null) {
							searchRequestPublic = new SearchRequest(SearchRequest.EndPoint.PUBLIC);
						}
						allCopyURIs = searchRequestPublic.gatherAggregationURIs(tgo.getURI().toString(), allCopyURIs);
					} else {
						if (searchRequestNonPublic == null) {
							searchRequestNonPublic = new SearchRequest();
						}
						allCopyURIs = searchRequestNonPublic.gatherAggregationURIs(tgo.getURI().toString(), allCopyURIs);
					}
				}
			} catch (CrudServiceException e) {
				Activator.handleError(e, e.getMessage(), object);
			} catch (URISyntaxException e) {
				Activator.handleError(e, e.getMessage(), object);
			} catch (CoreException e) {
				Activator.handleError(e, e.getMessage(), object);
			}
		}
	}
	
	/**
	 * Retrieves the count of uris contained in 
	 * the map aggregationURIs.
	 * @return
	 * 		count of uris
	 */
	private int getObjectCounter() {
		HashMap<String,Integer> titleCount;
		int counter =0;
		for(Map.Entry e : allCopyURIs.entrySet()){
			titleCount = (HashMap<String,Integer>)e.getValue();
			for(Map.Entry t : titleCount.entrySet()) {
				counter = counter + (Integer)t.getValue();
			}		
		}
		return counter;
	}
}