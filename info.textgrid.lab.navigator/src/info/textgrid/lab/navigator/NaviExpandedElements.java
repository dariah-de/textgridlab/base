package info.textgrid.lab.navigator;

import info.textgrid.lab.core.model.TextGridProject;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.TreePath;

public class NaviExpandedElements {
	private static NaviExpandedElements instance = null;
	private TreePath[] expandedElements = null;
	private List<TextGridProject> demadedProjects = new ArrayList<TextGridProject>();
	 
	   public static NaviExpandedElements getInstance()
	   {
	     if (instance == null) {
	       instance = new NaviExpandedElements();
	     }
	     return instance;
	   }

	public TreePath[] getExpandedTreePaths() {
		TreePath[] exElements = expandedElements;
		return exElements;
	}

	public void setExpandedTreePaths(TreePath[] expandedElements) {
		this.expandedElements = expandedElements;
		clearDemandedProjects();
	}
	
	public void clear() {
		expandedElements = null;
	}
	
	public void addDemandedProject(TextGridProject project) {
		this.demadedProjects.add(project);
	}
	 
	public boolean isDemandedProject(TextGridProject project) {
		return this.demadedProjects.contains(project);
	}
	
	private void clearDemandedProjects() {
		this.demadedProjects.clear();
	}

}
