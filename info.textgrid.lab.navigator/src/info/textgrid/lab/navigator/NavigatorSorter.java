package info.textgrid.lab.navigator;

import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.navigator.NaviView.NavigatorSortModi;
import info.textgrid.lab.navigator.NaviView.NavigatorSortSequence;

import java.text.Collator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.Viewer;

/* 
 * This class reorders the TG-Objects of a TG-Project depending on 
 * the adjusted sort mode.
 * 
 * @author Frank Queens for TextGrid
*/
public class NavigatorSorter extends org.eclipse.jface.viewers.ViewerSorter {
	Collator col = Collator.getInstance();
	
	
    public int compare(Viewer viewer, Object e1, Object e2) {
    	NavigatorSortModi sortModus = NaviView.sortModus;
    	NavigatorSortSequence sortSequence = NaviView.sortSequence;
        int level1 = 0;
        String string1, string2;
        
        TextGridObject e1Tgo = AdapterUtils.getAdapter(e1, TextGridObject.class);
        TextGridObject e2Tgo = AdapterUtils.getAdapter(e2, TextGridObject.class);
        
        try {
    		switch (sortModus) {		
	    		case MODIFICATION_DATE : {
	    			if (e1Tgo != null && e2Tgo != null) {
		    			if (e1Tgo.getLastModified().before(e2Tgo.getLastModified())) {
		    				if (sortSequence.equals(NavigatorSortSequence.ASCENDING))
		    					level1 = -1;
		    				else 
		    					level1 = 1;
		    			} else if (e1Tgo.getLastModified().equals(e2Tgo.getLastModified()))
		    				level1 = 0;
		    			else {
		    				if (sortSequence.equals(NavigatorSortSequence.ASCENDING))
		    					level1 = 1;
		    				else 
		    					level1 = -1;
		    			}	
	    			} else {
	    				return 0;
	    			}
	    			break;
	    		}
	    		case TITLE : {
	    			if (e1Tgo != null)
	    				string1 = e1Tgo.getTitle();
	    			else 
	    				string1 = e1.toString();
	    			
	    			if (e2Tgo != null)
	    				string2 = e2Tgo.getTitle();
	    			else 
	    				string2 = e2.toString();
	    	
	    			level1 = col.compare(string1, string2);
					if (sortSequence.equals(NavigatorSortSequence.DESCENDING))
						level1 = level1 * -1;
					break;
	    		}
	    		case TYPE : {
	    			if (e1Tgo != null)
	    				string1 = e1Tgo.getContentType(false).toString();
	    			else 
	    				string1 = e1.toString();
	    			
	    			if (e2Tgo != null)
	    				string2 = e2Tgo.getContentType(false).toString();
	    			else 
	    				string2 = e2.toString();
	    			
	    			level1 = col.compare(string1, string2);
					if (sortSequence.equals(NavigatorSortSequence.DESCENDING))
						level1 = level1 * -1;
					break;
	    		}
	    		default : return 0;
    		}
		    		
			if ((level1 != 0) || (sortModus == NavigatorSortModi.TITLE)) {
				return level1;
			} else {
				if (e1Tgo != null)
    				string1 = e1Tgo.getTitle();
    			else 
    				string1 = e1.toString();
    			
    			if (e2Tgo != null)
    				string2 = e2Tgo.getTitle();
    			else 
    				string2 = e2.toString();
    	
    			int level2 = col.compare(string1, string2);
				
				if (sortSequence.equals(NavigatorSortSequence.DESCENDING))
					level2 = level2 * -1;
				return level2;
			}
		} catch (CoreException e) {
			Activator.handleProblem(IStatus.ERROR, e,
									Messages.NavigatorSorter_EM_NoTGO);
			return 0;
		}
    }
}
