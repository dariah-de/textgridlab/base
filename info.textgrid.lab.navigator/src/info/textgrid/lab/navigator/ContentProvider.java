package info.textgrid.lab.navigator;

import info.textgrid.lab.core.model.IChildListChangedListener;
import info.textgrid.lab.core.model.IChildListParent;

import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.progress.DeferredTreeContentManager;

/**
 * This ContentProvider uses org.eclipse.ui.progress.DeferredTreeContentManager
 * for deferredness
 * 
 * @deprecated we don't need separate DeferredTreeContentProviders for Navigator
 *             and Search, especially since there are two of this in the
 *             navigator plugin
 */
public class ContentProvider implements ITreeContentProvider,
		IChildListChangedListener {

	private DeferredTreeContentManager manager;
	private Viewer viewer;
	private IChildListParent input;

	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
	 * 
	 * 
	 * returns Object[] org.eclipse.ui.progress.DeferredTreeContentManager.getChildren(Object parent)
	 * or an empty array if there is no such manager.
	 */
	public Object[] getChildren(Object parentElement) {
		
//		if(parentElement instanceof TextGridProject){
//			IDeferredWorkbenchAdapter adapter =(IDeferredWorkbenchAdapter) ((TextGridProject)parentElement).getAdapter(IDeferredWorkbenchAdapter.class);
//			if(adapter != null){
//				
//				return manager == null ? new Object[0] : manager
//						.getChildren(adapter); 
//			}else{
//			
//			return new Object[0];
//			}
//		}

		return manager == null ? new Object[0] : manager
				.getChildren(parentElement);

	}

	public Object getParent(Object element) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean hasChildren(Object element) {
//		if(element instanceof TextGridProject){
//			
//			return true;
//		}
		return manager.mayHaveChildren(element);

	}

	public Object[] getElements(Object inputElement) {
		return this.getChildren(inputElement);
	}

	public void dispose() {
		this.input.removeChildListChangedListener(this);
	}

	public void cancel(Object input) {
		if (manager != null) {
			manager.cancel(input);
		}
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		this.viewer = viewer;
		if (this.input != newInput && newInput instanceof IChildListParent) {
			if (this.input != null ) {
				this.input.removeChildListChangedListener(this);
			}
			this.input = (IChildListParent) newInput; 
			this.input.addChildListChangedListener(this);
		}
//		System.out.println("input changed. instance " + this.toString()
//				+ " manager == null : " + (manager == null));

		if (viewer instanceof AbstractTreeViewer && this.manager == null) {

			manager = new DeferredTreeContentManager(this,
					(AbstractTreeViewer) viewer);
		}
		manager.addUpdateCompleteListener(getCompletionJobListener());
	}

	public void childListChanged(IChildListParent parent) {
		if (viewer != null) {
			viewer.setInput(parent);
		}
	}
	
	protected IJobChangeListener getCompletionJobListener() {
		return new JobChangeAdapter() {

			public void done(IJobChangeEvent event) {
				if (event.getResult().isOK());
					//System.out.println("Baum aufgebaut");
			}

		};
	}


}
