package info.textgrid.lab.navigator;

import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.util.ModelUtil.InUIThreadException;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishError;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishWarning;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class CopyReportDialog extends TitleAreaDialog{
	
	private Text text;
	private List<PublishObject> publishObjects;
	private HashMap<String,HashMap<String,Integer>> allCopyURIs;
	private boolean userAbort;

	public CopyReportDialog(Shell parentShell) {
		super(parentShell);
	}
	
	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle("Copy Report"); //$NON-NLS-1$
		setMessage(Messages.CopyReportDialog_IM_ProcessInformation, IMessageProvider.INFORMATION);
		Composite area = (Composite) super.createDialogArea(parent);
		
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		area.setLayout(new GridLayout(1, false));

		text = new Text (area, SWT.H_SCROLL | SWT.V_SCROLL | SWT.READ_ONLY | SWT.MULTI);
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		fillReport();
		text.setSelection(0, 0);
		return area;
	}	
	
	private void fillReport() {
		String content = "";
		if (userAbort) 
			content = Messages.CopyReportDialog_IM_UserAbort;
		int counter = 1;
		HashMap<String, Integer> titleCount;
		if (publishObjects != null) {
			for (final PublishObject object : publishObjects) {
				String line = "";
				String errorLine = "";
				String warningLine = "";
				String title = "";
				titleCount = allCopyURIs.get(object.uri);
				//TG-1932
				if (titleCount == null) {
					try {
						//TODO
						TGObjectReference tgObj = new TGObjectReference(new URI(object.uri));
						if (tgObj.getTgo().getContentTypeID().equals("text/tg.work+xml")) {
							title = tgObj.getTgo().getNameCandidate();
						}
					} catch (URISyntaxException e) {
						Activator.handleError(e, e.getMessage(), "");
					} catch (CoreException e) {
						Activator.handleError(e, e.getMessage(), "");
					}
				} else {
					for (Map.Entry t : titleCount.entrySet()) {
						title = (String) t.getKey();
					}
				}

				line = counter + ". \t" + title + " (" + object.uri + ");"
						+ " Status:" + object.status.name() + "\n";

				//Bug #10951
				// Errors
				if (object.getErrors().size() > 0) {
					errorLine = "\tErrors:";
					for (PublishError publishError : object.getErrors()) {
						errorLine = errorLine + "\n\t\n"
								+ publishError.getMessage();
					}
				}

				// Warnings
				if (object.getWarnings().size() > 0) {
					warningLine = "\tWarnings:";
					for (PublishWarning publishWarning : object.getWarnings()) {
						warningLine = warningLine + "\n\t\n"
								+ publishWarning.getMessage();
					}
				}

				counter++;
				content = content + line + errorLine + warningLine;
			}

		}
		text.setText(content);
	}
	
	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(800, 400); 
	}

	public void setPublishObjects(List<PublishObject> publishObjects, 
								  HashMap<String,HashMap<String,Integer>> allCopyURIs,
								  boolean userAbort) {
		this.publishObjects = publishObjects;
		this.allCopyURIs = allCopyURIs;
		this.userAbort = userAbort;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
	}
	
}
