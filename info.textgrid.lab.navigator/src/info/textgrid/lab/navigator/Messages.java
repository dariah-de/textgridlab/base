package info.textgrid.lab.navigator;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.navigator.messages"; //$NON-NLS-1$
	public static String CommonDropAdapterAssistant_EM_CouldNotCreateURI;
	public static String CommonDropAdapterAssistant_EM_CouldNotOpenImportPerspective;
	public static String CommonDropAdapterAssistant_EM_CouldNotUpdateNavigatorGUI;
	public static String CommonDropAdapterAssistant_EM_CouldNotWriteAggregation;
	public static String CommonDropAdapterAssistant_IM_SavingAggregaion;
	public static String CommonDropAdapterAssistant_IM_SavingAggregation;
	public static String CommonDropAdapterAssistant_IM_UpdateNavigatorGUI;
	public static String CopyReportDialog_IM_ProcessInformation;
	public static String CopyReportDialog_IM_UserAbort;
	public static String CopyService_Cancel;
	public static String CopyService_Copy;
	public static String CopyService_IM_CanTageAMoment;
	public static String CopyService_IM_CopyObjects;
	public static String CopyService_IM_FindNumberOfObjects;
	public static String CopyService_QE_DoYouReallyWantToCopy;
	public static String CopyService_QU_DoYouReallyWantToCopyNumber;
	public static String CopyService_ShowReport;
	public static String CopyService_UI_Copying;
	public static String CopyService_UI_CopyObjects;
	public static String CopyService_UI_NoSelection;
	public static String CopyService_UI_NothingToCopy;
	public static String CopyTGObjectByWorkflow_EM_ConfServer;
	public static String CopyTGObjectByWorkflow_EM_CouldNotCopyObjects;
	public static String CopyTGObjectByWorkflow_EM_NoURI;
	public static String CopyTGObjectByWorkflow_EM_WorkflowCouldNotStart;
	public static String CopyTGObjectByWorkflow_IM_Copieing;
	public static String CopyTGObjectByWorkflow_IM_NothingToCopyHeader;
	public static String CopyTGObjectByWorkflow_IM_NothingToCopyMessage;
	public static String CopyTGObjectByWorkflow_IM_WorkflowCouldNotStart;
	public static String CopyTGObjectByWorkflow_IM_WorkflowPreparation;
	public static String CopyTGObjectByWorkflow_ObjectNumber;
	public static String CopyTGObjectByWorkflow_RefreshNavigator;
	public static String CopyTGObjectHandler_EM_NotAbleToCopyObjects;
	public static String NavigatorSorter_EM_NoTGO;
	public static String NaviView_EM_CouldNotOpenFor;
	public static String NaviView_EM_CouldNotOpenNavigator;
	public static String NaviView_EM_ProjectDoesNotExist;
	public static String NaviView_IM_NeedToBeOnline;
	public static String NaviView_Refresh;
	public static String PasteObjectHandler_EM_NotAbleToCopyObjects;
	public static String showNavigatorViewHandler_EM_CouldNotOpenView;
	public static String showNavigatorViewHandler_HeaderNoOpenPerspective;
	public static String showNavigatorViewHandler_IM_NoOpenPerspective;
	public static String TGLinkHelper_EM_CouldNotRetieveContentTypeOf;
	public static String MOVE_ShowReport;
	public static String MOVE_ReportMessage;
	public static String MOVE_Circle_ReportMessage;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
