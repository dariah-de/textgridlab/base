package info.textgrid.lab.navigator.filters;

import info.textgrid.lab.navigator.Activator;
import info.textgrid.lab.navigator.NaviExpandedElements;
import info.textgrid.lab.navigator.NaviView;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.dialogs.ContainerCheckedTreeViewer;


public class FilterDialog extends TrayDialog{
	
	public static final String FILTER_PROJECT_ID = "info.textgrid.lab.navigator.filters.projectfilter"; //$NON-NLS-1$
	public static final String FILTER_CONTENT_ID = "info.textgrid.lab.navigator.filters.contentfilter"; //$NON-NLS-1$
	
	ContainerCheckedTreeViewer projectFilterTree;
	ContainerCheckedTreeViewer contentFilterTree;
	
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(Messages.FilterDialog_NavigatorFilters);  
	}
	
	protected FilterDialog(Shell shell) {
		super(shell);
	}
	
	public static void openFilterDialog() {
		FilterDialog d = new FilterDialog(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell());
		d.open();
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Group projectGroup = new Group(parent, SWT.BORDER);
		projectGroup.setText(Messages.FilterDialog_ProjectFilter);
		GridLayout gl = new GridLayout(1, false);
		projectGroup.setLayout(gl);
		final GridData gd = new GridData(GridData.FILL, GridData.FILL, true, true);
		projectGroup.setLayoutData(gd);
		projectFilterTree = new ContainerCheckedTreeViewer(projectGroup);
		projectFilterTree.setSorter(new ViewerSorter());
		projectFilterTree.setContentProvider(new ProjectFilterConentProvider());
		projectFilterTree.setLabelProvider(new LabelProvider());
		projectFilterTree.setInput(ProjectFilterData.getInstance());
		projectFilterTree.getControl().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		projectFilterTree.expandAll();
		
		Group contentGroup = new Group(parent, SWT.BORDER);
		contentGroup.setText(Messages.FilterDialog_ContentFilter);
		contentGroup.setLayout(gl);
		final GridData gdContent = new GridData(GridData.FILL, GridData.FILL, true, true);
		contentGroup.setLayoutData(gdContent);
		contentFilterTree = new ContainerCheckedTreeViewer(contentGroup	);
		contentFilterTree.setSorter(new ViewerSorter());
		contentFilterTree.setContentProvider(new ContentTypeFilterContentProvider());
		contentFilterTree.setLabelProvider(new LabelProvider());
		contentFilterTree.setInput(ContentTypeFilterData.getInstance());
		contentFilterTree.getControl().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		contentFilterTree.expandAll();
		
		//initialize project filter
		String projectFilter = Activator.getPreference(Activator.FILTER_PROJECT);
		if (projectFilter.equals("")) {
			projectFilterTree.setChecked(ProjectFilterData.getInstance().getChildren()[0], true);
		} else {
			for (String itemRole : projectFilter.split(" ")) {
				ProjectFilterData item = ProjectFilterData.getItem(itemRole);
				if (item != null)
					projectFilterTree.setChecked(item, true);
			}
		}	
		
		//initialize content type filter
		String contentTypeFilter = Activator.getPreference(Activator.FILTER_CONTENT_TYPE);
		if (contentTypeFilter.equals("")) {
			contentFilterTree.setChecked(ContentTypeFilterData.getInstance().getChildren()[0], true);
		} else {
			for (String itemFormat : contentTypeFilter.split(" ")) {
				ContentTypeFilterData item = ContentTypeFilterData.getItem(itemFormat);
				if (item != null)
					contentFilterTree.setChecked(item, true);
			}
		}	
		
		return parent;
	}
	
	@Override
	protected void okPressed() {
		TreePath[] treePath = NaviExpandedElements.getInstance().getExpandedTreePaths();
		
		// set project filter
		String pFilter = getProjectFilterString();
		
		if (pFilter.equals("")) {
			MessageDialog
			.openError(
					PlatformUI.getWorkbench().getDisplay().getActiveShell(),
					Messages.FilterDialog_NoValidInput,
					Messages.FilterDialog_IM_NoRoleSelected);
			return;
		}
		
		if (projectFilterTree.getChecked(ProjectFilterData.getInstance().getChildren()[0])) { 
			if (pFilter.equals("*ALL")) {
				setFilter(FILTER_PROJECT_ID, false);
				Activator.setPreference(Activator.FILTER_PROJECT, "");
			} else {
				setFilter(FILTER_PROJECT_ID, true);
				Activator.setPreference(Activator.FILTER_PROJECT, pFilter);
			}
		} else {	
			setFilter(FILTER_PROJECT_ID, false);
			Activator.setPreference(Activator.FILTER_PROJECT, "");
		}	
		
		// set content Type 
		String cFilter = getContentTypeFilterString();
		
		if (cFilter.equals("")) {
			MessageDialog
			.openError(
					PlatformUI.getWorkbench().getDisplay().getActiveShell(),
					Messages.FilterDialog_NoValidInput,
					Messages.FilterDialog_IM_NoContentTypeSelected);
			return;
		}
		
		if (contentFilterTree.getChecked(ContentTypeFilterData.getInstance().getChildren()[0])) { 
			if (cFilter.equals("*ALL")) {
				setFilter(FILTER_CONTENT_ID, false);
				Activator.setPreference(Activator.FILTER_CONTENT_TYPE, "");
			} else {
				setFilter(FILTER_CONTENT_ID, true);
				Activator.setPreference(Activator.FILTER_CONTENT_TYPE, cFilter);
			}
		} else {	
			setFilter(FILTER_CONTENT_ID, false);
			Activator.setPreference(Activator.FILTER_CONTENT_TYPE, "");
		}	
		
		close();
		NaviExpandedElements.getInstance().setExpandedTreePaths(treePath);
		NaviView.refreshNavigator();
		
		ICommandService commandService = (ICommandService) PlatformUI.getWorkbench().getService(
			    ICommandService.class);
		commandService.refreshElements("info.textgrid.lab.navigator.openfilterdialog", null); //$NON-NLS-1$
	}

	private void setFilter(String filterID, boolean active) {
		//is filter already active? 
		if (active) {
			for (ViewerFilter filter : NaviView.getViewer().getFilters()) {
				if (filter.toString().toLowerCase().contains(filterID))
					return;
			}
		}
		
		if (active) {
			if (filterID.equals(FILTER_CONTENT_ID))
				NaviView.getViewer().addFilter(new ContentFilter(null));
			else if (filterID.equals(FILTER_PROJECT_ID))
				NaviView.getViewer().addFilter(new ProjectFilter(null));
		} else {
			for (ViewerFilter filter : NaviView.getViewer().getFilters()) {
				if (filter.toString().toLowerCase().contains(filterID))
					NaviView.getViewer().removeFilter(filter);
			}
		}	
	}
	
	private String getContentTypeFilterString() {
		String type = "";
		if (contentFilterTree.getCheckedElements().length != ContentTypeFilterData.getChildCount()) {
			for (Object item : contentFilterTree.getCheckedElements()) {
				if (item instanceof ContentTypeFilterData) {
					if (!item.toString().equals(Messages.FilterDialog_ContentTypes)) {
						type = type + ((ContentTypeFilterData)item).getFormat() + " ";
					}	
				}
			}
		} else
			return "*ALL";
		return type.trim();
	}
	
	private String getProjectFilterString() {
		String type = "";
		if (projectFilterTree.getCheckedElements().length != ProjectFilterData.getChildCount()) {
			for (Object item : projectFilterTree.getCheckedElements()) {
				if (item instanceof ProjectFilterData) {
					if (!item.toString().equals(Messages.FilterDialog_ProjectRoles)) {
						type = type + ((ProjectFilterData)item).getRole() + " ";
					}	
				}
			}
		} else
			return "*ALL";	
		return type.trim();
	}

}
