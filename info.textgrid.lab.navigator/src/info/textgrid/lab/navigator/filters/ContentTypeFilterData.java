package info.textgrid.lab.navigator.filters;

import info.textgrid.lab.core.model.TGContentType;

import java.util.ArrayList;

public class ContentTypeFilterData {
	private String name;
	private String format;
	private ArrayList<ContentTypeFilterData> children = new ArrayList<ContentTypeFilterData>();
	private static ContentTypeFilterData instance;
	
	private ContentTypeFilterData(String name, String format, boolean isRoot, boolean init) {
		this.name = name;
		this.format = format;
		if (isRoot) {
			add(new ContentTypeFilterData(Messages.ContentTypeFilterData_ContentTypes, "", false, true));
		} else if (init) {
			for (TGContentType ct : TGContentType.getContentTypes(false)) {
				add(new ContentTypeFilterData(ct.getDescription(), ct.getId(), false, false)); 
			}
		}
	}
	
	public synchronized static ContentTypeFilterData getInstance() {
        if (instance == null) {
            instance = new ContentTypeFilterData("Root", "", true, true);
        }
        return instance;
    }
	
	public String toString() {
		String format = "";
		if (!this.format.equals("")) {
			format = " (" + this.format + ")";
		}
		return this.name + format;
	}
	
	public String getFormat() {
		return this.format;
	}
	
	public void add(ContentTypeFilterData child) {
		children.add(child);
	}
	
	public static int getChildCount() {
		return instance.getChildren()[0].getChildren().length +1;
	}
	
	public ContentTypeFilterData[] getChildren() {
		return children.toArray(new ContentTypeFilterData[0]);
	}
	
	public static ContentTypeFilterData getItem(String format) {
		for (ContentTypeFilterData item : instance.getChildren()[0].getChildren()) {
			if (item.format.equals(format))
				return item;
		}
		return null;
	}
}

