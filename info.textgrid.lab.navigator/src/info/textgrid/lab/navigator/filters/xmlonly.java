package info.textgrid.lab.navigator.filters;

import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TextGridObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

public class xmlonly extends ViewerFilter {

	public xmlonly() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {

		if(element instanceof TextGridObject){
			try {
				if (((TextGridObject)element).getContentTypeID().contains("aggregation"))
					return true;
			} catch (CoreException e1) {
				Activator.handleError(e1, Messages.xmlonly_EM_CouldNotGetContentType, element);
			}
			String uri = ((TextGridObject)element).getURI().toString();
			String[] segs = uri.split( "\\:" );
			String format = "";
			if(segs.length>4){
				try {
				  format = URLDecoder.decode(segs[4], "UTF-8");
				} catch (UnsupportedEncodingException e) {
					Activator.handleError(e, "Encoding Exception on object {0}.", element);
				}
				if(format.contains("xml") || format.contains("XML")){
					return true;
				}
		}
	  return false;
	}
		return true;}}


