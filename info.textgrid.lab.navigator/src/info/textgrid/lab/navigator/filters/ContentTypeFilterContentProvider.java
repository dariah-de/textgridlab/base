package info.textgrid.lab.navigator.filters;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class ContentTypeFilterContentProvider implements ITreeContentProvider {

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return ((ContentTypeFilterData) inputElement).getChildren();
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		return ((ContentTypeFilterData) parentElement).getChildren();
	}

	@Override
	public Object getParent(Object element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (((ContentTypeFilterData) element).getChildren().length == 0)
			return false;
		else 
			return true;
	}

}
