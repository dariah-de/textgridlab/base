package info.textgrid.lab.navigator.filters;


import info.textgrid.lab.core.model.TextGridProject;

import java.util.ArrayList;

public class ProjectFilterData {
	private String name;
	private String role;
	private ArrayList<ProjectFilterData> children = new ArrayList<ProjectFilterData>();
	private static ProjectFilterData instance = new ProjectFilterData("Root", null);
	
	private ProjectFilterData(String name, String role) {
		this.name = name;
		this.role = role;
	}
	
	public static ProjectFilterData getInstance() {
		if (!instance.isParent())
			init();
        return instance;
	}
	
	private static void init() {
//		ProjectFilterData p1 = new ProjectFilterData("All Projects with Public Content");
		ProjectFilterData p2 = new ProjectFilterData(Messages.ProjectFilterData_ProjectRoles, null);
//		instance.children.add(p1);
		instance.children.add(p2);
		ProjectFilterData c1 = new ProjectFilterData(Messages.ProjectFilterData_AuthorityToDeleteRole, TextGridProject.TG_STANDARD_ROLE_ADMINISTRATOR);
		ProjectFilterData c2 = new ProjectFilterData(Messages.ProjectFilterData_EditorRole, TextGridProject.TG_STANDARD_ROLE_EDITOR);
		ProjectFilterData c3 = new ProjectFilterData(Messages.ProjectFilterData_ObserverRole, TextGridProject.TG_STANDARD_ROLE_WATCHER);
		ProjectFilterData c4 = new ProjectFilterData(Messages.ProjectFilterData_ProjectManagerRole, TextGridProject.TG_STANDARD_ROLE_PROJECTLEADER);
		p2.children.add(c1);
		p2.children.add(c2);
		p2.children.add(c3);
		p2.children.add(c4);
	}
	
	public String toString() {
		return this.name;
	}
	
	public ProjectFilterData[] getChildren() {
		return children.toArray(new ProjectFilterData[0]);
	}
	
	public boolean isParent() {
		return !children.isEmpty();
	}
	
	public String getRole() {
		return this.role;
	}
	
	public static ProjectFilterData getItem(String role) {
		for (ProjectFilterData item : instance.getChildren()[0].getChildren()) {
			if (item.role.equals(role))
				return item;
		}
		return null;
	}
	
	public static int getChildCount() {
		return instance.getChildren()[0].getChildren().length +1;
	}
}
