package info.textgrid.lab.navigator.filters;

import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TGObjectReference;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

public class ContentFilter extends ViewerFilter {
	
	ArrayList<String> ctArrayList = new ArrayList<String>();
	
	public ContentFilter(String filter) {
		if (filter != null && !filter.equals("")) {
			ctArrayList.clear();
			ctArrayList.addAll(Arrays.asList(filter.split(" ")));
		}
		
		info.textgrid.lab.navigator.Activator.getDefault().getPreferenceStore()
				.addPropertyChangeListener(new IPropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent event) {
						if (event
								.getProperty()
								.equals(info.textgrid.lab.navigator.Activator.FILTER_CONTENT_TYPE)) {
							ctArrayList.clear();
							ctArrayList.addAll(Arrays
									.asList(info.textgrid.lab.navigator.Activator
											.getPreference(
													info.textgrid.lab.navigator.Activator.FILTER_CONTENT_TYPE)
											.split(" ")));
						}
					}
				});
	}
	
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {

		if(element instanceof TGObjectReference){
			try {
				String contentType = ((TGObjectReference)element).getTgo().getContentTypeID().toString();
				if (ctArrayList.contains(contentType))
					return true;
				else 
					return false;
			} catch (CoreException e1) {
				Activator.handleError(e1, Messages.ContentFilter_EM_CouldNotGetContentType, element);
			}
		}	
		return true;
	}
}