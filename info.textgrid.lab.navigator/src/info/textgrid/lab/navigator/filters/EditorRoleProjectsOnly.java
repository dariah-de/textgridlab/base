package info.textgrid.lab.navigator.filters;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.UserRole2;

import java.util.ArrayList;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;


public class EditorRoleProjectsOnly extends ViewerFilter {

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {

		if(element instanceof TextGridProject){
			
			String myEPPN = RBACSession.getInstance().getEPPN().toUpperCase();
			ArrayList<UserRole2> userRoles = ((TextGridProject)element).getUserRoles2Offline();
			if (userRoles == null)
				userRoles = ((TextGridProject)element).getUserRoles2FromRBAC(null);
			for (UserRole2 role : userRoles) {
				if (role.getePPN().toUpperCase().equals(myEPPN) &&
					role.getRoles().contains(TextGridProject.TG_STANDARD_ROLE_EDITOR))
					return true;
				
			}
			return false;
		}	
		return true;
	}
}

