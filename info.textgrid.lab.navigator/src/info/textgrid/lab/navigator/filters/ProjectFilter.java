package info.textgrid.lab.navigator.filters;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.UserRole2;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;


public class ProjectFilter extends ViewerFilter {
	
	ArrayList<String> pArrayList = new ArrayList<String>();

	public ProjectFilter(String filter) {
		if (filter != null && !filter.equals("")) {
			pArrayList.clear();
			pArrayList.addAll(Arrays.asList(filter.split(" ")));
		}	
		
		info.textgrid.lab.navigator.Activator.getDefault().getPreferenceStore()
				.addPropertyChangeListener(new IPropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent event) {
						if (event
								.getProperty()
								.equals(info.textgrid.lab.navigator.Activator.FILTER_PROJECT)) {
							pArrayList.clear();
							pArrayList.addAll(Arrays
									.asList(info.textgrid.lab.navigator.Activator
											.getPreference(
													info.textgrid.lab.navigator.Activator.FILTER_PROJECT)
											.split(" ")));
						}
					}
				});
	}
	
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {

		if(element instanceof TextGridProject){
			
			String myEPPN = RBACSession.getInstance().getEPPN().toUpperCase();
			ArrayList<UserRole2> userRoles = ((TextGridProject)element).getUserRoles2Offline();
			if (userRoles == null)
				userRoles = ((TextGridProject)element).getUserRoles2FromRBAC(null);
			for (UserRole2 role : userRoles) {
				if (role.getePPN().toUpperCase().equals(myEPPN)) {
					for (String prole : pArrayList) {
						if (role.getRoles().contains(prole))
							return true;
					}
				}	
			}
			return false;
		}	
		return true;
	}
}
