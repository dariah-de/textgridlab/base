package info.textgrid.lab.navigator.filters;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.navigator.filters.messages"; //$NON-NLS-1$
	public static String ContentFilter_EM_CouldNotGetContentType;
	public static String ContentTypeFilterData_ContentTypes;
	public static String FilterDialog_ContentFilter;
	public static String FilterDialog_ContentTypes;
	public static String FilterDialog_IM_NoContentTypeSelected;
	public static String FilterDialog_IM_NoRoleSelected;
	public static String FilterDialog_NavigatorFilters;
	public static String FilterDialog_NoValidInput;
	public static String FilterDialog_ProjectFilter;
	public static String FilterDialog_ProjectRoles;
	public static String ImageOnly_EM_CouldNotGetContentType;
	public static String ProjectFilterData_AuthorityToDeleteRole;
	public static String ProjectFilterData_EditorRole;
	public static String ProjectFilterData_ObserverRole;
	public static String ProjectFilterData_ProjectManagerRole;
	public static String ProjectFilterData_ProjectRoles;
	public static String xmlonly_EM_CouldNotGetContentType;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
