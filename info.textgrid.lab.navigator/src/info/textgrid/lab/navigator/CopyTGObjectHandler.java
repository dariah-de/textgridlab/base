package info.textgrid.lab.navigator;

import info.textgrid.lab.ui.core.utils.TextGridObjectTransfer;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

public class CopyTGObjectHandler extends AbstractHandler implements IHandler{

		public Object execute(ExecutionEvent arg0) throws ExecutionException {
			try {
				ISelection sel = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getSelection();
				if (!sel.isEmpty()) {
					IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					IViewPart iviewPart = activePage.findView("info.textgrid.lab.navigator.view"); //$NON-NLS-1$
					if (iviewPart == null) {
						iviewPart = activePage.showView("info.textgrid.lab.navigator.view"); //$NON-NLS-1$
					}
					TextGridObjectTransfer.getTransfer().setCopiedSelection(sel);
				}
			} catch (Exception e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					    Messages.CopyTGObjectHandler_EM_NotAbleToCopyObjects, e);
				Activator.getDefault().getLog().log(status);
			}
			return null;
		}

	}