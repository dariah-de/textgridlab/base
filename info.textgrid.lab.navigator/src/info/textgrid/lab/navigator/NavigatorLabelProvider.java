package info.textgrid.lab.navigator;

import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.ui.core.utils.TGODefaultLabelProvider;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.navigator.IDescriptionProvider;

public class NavigatorLabelProvider extends TGODefaultLabelProvider implements IDescriptionProvider{

	@Override
	public String getDescription(Object anElement) {
		if (anElement instanceof TextGridProject) {
			return ((TextGridProject)anElement).getName() + " / " + ((TextGridProject)anElement).getId();
		} else if (anElement instanceof TGObjectReference) {
			TextGridObject tgo = ((TGObjectReference)anElement).getTgo();
			try {
				return tgo.getTitle() + " / " + tgo.getURI().toString();
			} catch (CoreException e) {
				return null;
			}
		} 
		return null;
	}

}
