This plugin contains the source code required for the TextGridLab application
(i.e. those sources that setup the main application window). Additionally, it
contains product and target definitions.

== Product definitions ==

The product definition files contain the information required to build the
deployable TextGridLab ZIP files. Most important is the list of required
plug-ins on the Dependencies page: If you want your plug-in to be included,
add it to this list.

Additionally, you can configure launching arguments on the Launching page.
Note that some arguments defined here are required to work around bugs, cf.
our Jira.

Finally, the Splash and Branding pages define, well, the splash screen and
branding information (icon, title). These are synchronized to the plugin.xml.

=== Available Products ===

* textgridlab.product
    
    Contains the default TextGridLab build.

* textgridlab-everything.product
    
    A build with all plugins, including experimental stuff.

* textgridlab-ecf.product

    A build with the Eclipse Communications Framework


== Target definitions ==

The target definition files can be used to setup a target platform to compile
TextGridLab against. Open the target definition and click on the 'Set as
Target Platform' link in the top right. You can re-configure the target
definition used internally in your Eclipse build via Window -> Preferences ->
Plug-in Development -> Target Platforms.

=== Available Target Platforms ===

* TextGridLab-Galileo.target

    A target platform to compile TextGridLab, using the Galileo update site to
    resolve the required features (for textgridlab.product)

* TextGridLab-ECF-3.1.target

    Contains everything required for textgridlab-ecf.product (for
    textgridlab.product and textgridlab-ecf.product)

=== Modifying Target Platforms ===

If you wish to modify one of the target platform definitions, please make sure
that the corresponding products work:

* Save the target platform and let eclipse resolve it
* Set it as current target platform, wait for the build to complete
* make sure there are no compile errors
* open the product, validate it, make sure there are no errors
* export versions for all supported target OS.

