package info.textgrid.lab.core.application;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.internal.ProductProperties;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.core.application"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@SuppressWarnings("restriction")
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		StatusManager.getManager().handle(
				new Status(IStatus.INFO, PLUGIN_ID, ProductProperties
						.getAboutText(Platform.getProduct())));
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	public static IStatus handleProblem(int severity, Throwable cause,
			String message, Object... arguments) {
		if ((message == null || "".equals(message)) && cause != null) { //$NON-NLS-1$
			message = cause.getLocalizedMessage();
			if (message == null)
				message = cause.getClass().getSimpleName();
		}
		
		message = NLS.bind(message, arguments);
		
		IStatus status;
		
		if (cause != null && cause instanceof CoreException) {
			status = new MultiStatus(PLUGIN_ID, severity,
					new IStatus[] { ((CoreException) cause).getStatus() },
					message, cause);
		} else {
			status = new Status(severity, PLUGIN_ID, message, cause);
		}
		
		StatusManager.getManager().handle(status);

		return status;
	}

}
