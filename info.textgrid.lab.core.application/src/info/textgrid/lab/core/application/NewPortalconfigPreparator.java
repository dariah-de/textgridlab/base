package info.textgrid.lab.core.application;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.NewObjectWizard;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;

/**
 * Sets up a Portalconfig skeleton and calls the default handler then.
 */
public class NewPortalconfigPreparator implements INewObjectPreparator {

	private ITextGridWizard wizard;
	
	public static final String TEMPLATE_LOCATION = "resources/templates/portalconfig.tmpl.xml";

	public void initializeObject(TextGridObject textGridObject) {
	}

	public boolean performFinish(TextGridObject textGridObject) {
		try {
	    InputStream inputStream = FileLocator.openStream(Activator.getDefault().getBundle(),
						 new Path(TEMPLATE_LOCATION),false);

		  String xml = new BufferedReader(
      new InputStreamReader(inputStream, StandardCharsets.UTF_8))
        .lines()
        .collect(Collectors.joining("\n"));
			textGridObject.setInitialContent(xml.getBytes(StandardCharsets.UTF_8));

		} catch (IllegalStateException e) {
			Activator
					.handleProblem(
							IStatus.WARNING,
							e,
							Messages.NewXSDPreparator_AlreadyPersistant,
							textGridObject);
		} catch (UnsupportedEncodingException e) {
			Activator
					.handleProblem(
							IStatus.ERROR,
							e,
							"UTF-8 is insupported!? What a strange setup is this? It will be hard to run the lab here ..."); //$NON-NLS-1$
		}catch (IOException e) {
			Activator
					.handleProblem(
							IStatus.ERROR,
							e,
							"Error reading template " + TEMPLATE_LOCATION); //$NON-NLS-1$
		}

		if (wizard instanceof NewObjectWizard)
			return ((NewObjectWizard) wizard).defaultPerformFinish();
		else
			return false;
	}

	public void setWizard(ITextGridWizard wizard) {
		this.wizard = wizard;
	}

}
