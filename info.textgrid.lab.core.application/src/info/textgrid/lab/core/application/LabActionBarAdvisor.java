package info.textgrid.lab.core.application;

import org.eclipse.core.runtime.IExtension;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.ide.IDEActionFactory;
import org.eclipse.ui.internal.WorkbenchPlugin;
import org.eclipse.ui.internal.registry.ActionSetRegistry;
import org.eclipse.ui.internal.registry.IActionSetDescriptor;

import info.textgrid.lab.conf.OnlineStatus;

/**
 * The Action bar advisor is responsible for creating toolbar items and menus
 * which are common for the IDE. Many of this stuff is copied from the IDE's
 * implementation.
 *
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv)
 * @see ActionBarAdvisor
 * @see IDEActionFactory
 */
@SuppressWarnings("restriction")
public class LabActionBarAdvisor extends ActionBarAdvisor {

	private IWorkbenchAction newWizardAction;
	private IWorkbenchAction newWizardDropDownAction;
	private IWorkbenchAction importResourcesAction;
	private IWorkbenchAction exportResourcesAction;
	private IWorkbenchAction saveAction;
	private IWorkbenchAction saveAsAction;
	private IWorkbenchAction saveAllAction;
	private IWorkbenchAction newWindowAction;
	private IWorkbenchWindow window;
	private IWorkbenchAction newEditorAction;
	private IWorkbenchAction undoAction;
	private IWorkbenchAction redoAction;
	private IWorkbenchAction cutAction;
	private IWorkbenchAction copyAction;
	private IWorkbenchAction pasteAction;
	private IWorkbenchAction printAction;
	private IWorkbenchAction selectAllAction;
	private IWorkbenchAction findAction;
	private IWorkbenchAction closeAction;
	private IWorkbenchAction closeAllAction;
	private IWorkbenchAction closeOthersAction;
	private IWorkbenchAction closeAllSavedAction;
	private IWorkbenchAction helpContentsAction;
	private IWorkbenchAction helpSearchAction;
	private IWorkbenchAction dynamicHelpAction;
	private IWorkbenchAction aboutAction;
	private IWorkbenchAction openPreferencesAction;
	private IWorkbenchAction addBookmarkAction;
	private IWorkbenchAction addTaskAction;
	private IWorkbenchAction deleteAction;
	private IWorkbenchAction showViewMenuAction;
	private IWorkbenchAction showPartPaneMenuAction;
	private IWorkbenchAction nextEditorAction;
	private IWorkbenchAction prevEditorAction;
	private IWorkbenchAction nextPartAction;
	private IWorkbenchAction prevPartAction;
	private IWorkbenchAction nextPerspectiveAction;
	private IWorkbenchAction prevPerspectiveAction;
	private IWorkbenchAction activateEditorAction;
	private IWorkbenchAction maximizePartAction;
	private IWorkbenchAction minimizePartAction;
	private IWorkbenchAction switchToEditorAction;
	private IWorkbenchAction workbookEditorsAction;
	private IWorkbenchAction hideShowEditorAction;
	private IWorkbenchAction savePerspectiveAction;
	private IWorkbenchAction editActionSetAction;
	private IWorkbenchAction lockToolBarAction;
	private IWorkbenchAction resetPerspectiveAction;
	private IWorkbenchAction closePerspAction;
	private IWorkbenchAction closeAllPerspsAction;
	private IWorkbenchAction forwardHistoryAction;
	private IWorkbenchAction backwardHistoryAction;
	private IWorkbenchAction revertAction;
	private IWorkbenchAction refreshAction;
	private IWorkbenchAction propertiesAction;
	private IWorkbenchAction quitAction;
	private IWorkbenchAction moveAction;
	private IWorkbenchAction renameAction;
	private IWorkbenchAction goIntoAction;
	private IWorkbenchAction backAction;
	private IWorkbenchAction forwardAction;
	private IWorkbenchAction upAction;
	private IWorkbenchAction nextAction;
	private IWorkbenchAction previousAction;
	private IWorkbenchAction openProjectAction;
	private IWorkbenchAction closeProjectAction;
	private IWorkbenchAction openWorkspaceAction;
	private IWorkbenchAction projectPropertyDialogAction;
	private IWorkbenchAction introAction;

	public LabActionBarAdvisor(IActionBarConfigurer configurer) {
		super(configurer);

		OnlineStatus.setStatusLineManager(configurer.getStatusLineManager());


		window = configurer.getWindowConfigurer().getWindow();

		////EXPERIMENTAL: remove "Convert Line Delimiters To..." Menu Entry and other annoying stuff
	      ActionSetRegistry reg = WorkbenchPlugin.getDefault().getActionSetRegistry();
	      IActionSetDescriptor[] actionSets = reg.getActionSets();
	      String[] badIds = new String[] {
	    		  "org.eclipse.ui.edit.text.actionSet.navigation", // Last edit location //$NON-NLS-1$
	    		  "org.eclipse.ui.edit.text.actionSet.convertLineDelimitersTo", //$NON-NLS-1$
	      };
	      for (int i = 0; i <actionSets.length; i++) {
	    	  for (int j=0; j < badIds.length; j++) {
	    		  if (!actionSets[i].getId().equals(badIds[j]))
	    			  continue;
	              	IExtension ext = actionSets[i].getConfigurationElement()
	                  .getDeclaringExtension();
	              	reg.removeExtension(ext, new Object[] { actionSets[i] });
	    	  }
	      }


	}


	/**
	 * Creates actions (and contribution items) for the menu bar, toolbar and
	 * status line.
	 *
	 * Blatantly copied from WorkbenchActionBuilder.
	 */
	@Override
	protected void makeActions(final IWorkbenchWindow window) {

		newWizardAction = ActionFactory.NEW.create(window);
		register(newWizardAction);

		newWizardDropDownAction = IDEActionFactory.NEW_WIZARD_DROP_DOWN
				.create(window);
		register(newWizardDropDownAction);

		importResourcesAction = ActionFactory.IMPORT.create(window);
		register(importResourcesAction);

		exportResourcesAction = ActionFactory.EXPORT.create(window);
		register(exportResourcesAction);

		saveAction = ActionFactory.SAVE.create(window);
		register(saveAction);

		saveAsAction = ActionFactory.SAVE_AS.create(window);
		register(saveAsAction);

		saveAllAction = ActionFactory.SAVE_ALL.create(window);
		register(saveAllAction);

		newWindowAction = ActionFactory.OPEN_NEW_WINDOW.create(getWindow());
		register(newWindowAction);

		newEditorAction = ActionFactory.NEW_EDITOR.create(window);
		register(newEditorAction);

		undoAction = ActionFactory.UNDO.create(window);
		register(undoAction);

		redoAction = ActionFactory.REDO.create(window);
		register(redoAction);

		cutAction = ActionFactory.CUT.create(window);
		register(cutAction);

		copyAction = ActionFactory.COPY.create(window);
		register(copyAction);

		pasteAction = ActionFactory.PASTE.create(window);
		register(pasteAction);

		printAction = ActionFactory.PRINT.create(window);
		register(printAction);

		selectAllAction = ActionFactory.SELECT_ALL.create(window);
		register(selectAllAction);

		findAction = ActionFactory.FIND.create(window);
		register(findAction);

		closeAction = ActionFactory.CLOSE.create(window);
		register(closeAction);

		closeAllAction = ActionFactory.CLOSE_ALL.create(window);
		register(closeAllAction);

		closeOthersAction = ActionFactory.CLOSE_OTHERS.create(window);
		register(closeOthersAction);

		closeAllSavedAction = ActionFactory.CLOSE_ALL_SAVED.create(window);
		register(closeAllSavedAction);

		helpContentsAction = ActionFactory.HELP_CONTENTS.create(window);
		register(helpContentsAction);

		helpSearchAction = ActionFactory.HELP_SEARCH.create(window);
		register(helpSearchAction);

		dynamicHelpAction = ActionFactory.DYNAMIC_HELP.create(window);
		register(dynamicHelpAction);

		aboutAction = ActionFactory.ABOUT.create(window);
		register(aboutAction);

		openPreferencesAction = ActionFactory.PREFERENCES.create(window);
		openPreferencesAction.setImageDescriptor(ImageDescriptor.createFromFile(
				this.getClass(), "/resources/101-zeige-Grundeinstellungen3.gif")); //$NON-NLS-1$
		register(openPreferencesAction);

		addBookmarkAction = IDEActionFactory.BOOKMARK.create(window);
		register(addBookmarkAction);

		addTaskAction = IDEActionFactory.ADD_TASK.create(window);
		register(addTaskAction);

		deleteAction = ActionFactory.DELETE.create(window);
		register(deleteAction);

		// Actions for invisible accelerators
		showViewMenuAction = ActionFactory.SHOW_VIEW_MENU.create(window);
		register(showViewMenuAction);

		showPartPaneMenuAction = ActionFactory.SHOW_PART_PANE_MENU
				.create(window);
		register(showPartPaneMenuAction);

		nextEditorAction = ActionFactory.NEXT_EDITOR.create(window);
		register(nextEditorAction);
		prevEditorAction = ActionFactory.PREVIOUS_EDITOR.create(window);
		register(prevEditorAction);
		ActionFactory.linkCycleActionPair(nextEditorAction, prevEditorAction);

		nextPartAction = ActionFactory.NEXT_PART.create(window);
		register(nextPartAction);
		prevPartAction = ActionFactory.PREVIOUS_PART.create(window);
		register(prevPartAction);
		ActionFactory.linkCycleActionPair(nextPartAction, prevPartAction);

		nextPerspectiveAction = ActionFactory.NEXT_PERSPECTIVE.create(window);
		register(nextPerspectiveAction);
		prevPerspectiveAction = ActionFactory.PREVIOUS_PERSPECTIVE
				.create(window);
		register(prevPerspectiveAction);
		ActionFactory.linkCycleActionPair(nextPerspectiveAction,
				prevPerspectiveAction);

		activateEditorAction = ActionFactory.ACTIVATE_EDITOR.create(window);
		register(activateEditorAction);

		maximizePartAction = ActionFactory.MAXIMIZE.create(window);
		register(maximizePartAction);

		minimizePartAction = ActionFactory.MINIMIZE.create(window);
		register(minimizePartAction);

		switchToEditorAction = ActionFactory.SHOW_OPEN_EDITORS.create(window);
		register(switchToEditorAction);

		workbookEditorsAction = ActionFactory.SHOW_WORKBOOK_EDITORS
				.create(window);
		register(workbookEditorsAction);

		hideShowEditorAction = ActionFactory.SHOW_EDITOR.create(window);
		register(hideShowEditorAction);
		savePerspectiveAction = ActionFactory.SAVE_PERSPECTIVE.create(window);
		register(savePerspectiveAction);
		editActionSetAction = ActionFactory.EDIT_ACTION_SETS.create(window);
		register(editActionSetAction);
		lockToolBarAction = ActionFactory.LOCK_TOOL_BAR.create(window);
		register(lockToolBarAction);
		resetPerspectiveAction = ActionFactory.RESET_PERSPECTIVE.create(window);
		register(resetPerspectiveAction);
		closePerspAction = ActionFactory.CLOSE_PERSPECTIVE.create(window);
		register(closePerspAction);
		closeAllPerspsAction = ActionFactory.CLOSE_ALL_PERSPECTIVES
				.create(window);
		register(closeAllPerspsAction);

		forwardHistoryAction = ActionFactory.FORWARD_HISTORY.create(window);
		register(forwardHistoryAction);

		backwardHistoryAction = ActionFactory.BACKWARD_HISTORY.create(window);
		register(backwardHistoryAction);

		revertAction = ActionFactory.REVERT.create(window);
		register(revertAction);

		refreshAction = ActionFactory.REFRESH.create(window);
		register(refreshAction);

		propertiesAction = ActionFactory.PROPERTIES.create(window);
		register(propertiesAction);

		quitAction = ActionFactory.QUIT.create(window);
		register(quitAction);

		moveAction = ActionFactory.MOVE.create(window);
		register(moveAction);

		renameAction = ActionFactory.RENAME.create(window);
		register(renameAction);

		goIntoAction = ActionFactory.GO_INTO.create(window);
		register(goIntoAction);

		backAction = ActionFactory.BACK.create(window);
		register(backAction);

		forwardAction = ActionFactory.FORWARD.create(window);
		register(forwardAction);

		upAction = ActionFactory.UP.create(window);
		register(upAction);

		nextAction = ActionFactory.NEXT.create(window);
		register(nextAction);

		previousAction = ActionFactory.PREVIOUS.create(window);
		register(previousAction);

		openProjectAction = IDEActionFactory.OPEN_PROJECT.create(window);
		register(openProjectAction);

		closeProjectAction = IDEActionFactory.CLOSE_PROJECT.create(window);
		register(closeProjectAction);

		openWorkspaceAction = IDEActionFactory.OPEN_WORKSPACE.create(window);
		register(openWorkspaceAction);

		projectPropertyDialogAction = IDEActionFactory.OPEN_PROJECT_PROPERTIES
				.create(window);
		register(projectPropertyDialogAction);

		if (window.getWorkbench().getIntroManager().hasIntro()) {
			introAction = ActionFactory.INTRO.create(window);
			register(introAction);
		}

		ContributionItemFactory.PIN_EDITOR.create(window);
	}



	/**
	 * Creates and returns the File menu.
	 */
	private MenuManager createFileMenu() {
		MenuManager menu = new MenuManager(Messages.LabActionBarAdvisor_File,
				IWorkbenchActionConstants.M_FILE);
		Separator openEditors = new Separator("openeditors"); //$NON-NLS-1$
		openEditors.setVisible(false);
		menu.add(openEditors);
		menu.add(ContributionItemFactory.REOPEN_EDITORS.create(getWindow()));
		Separator openEditorsEnd = new Separator("openeditorsend"); //$NON-NLS-1$
		openEditorsEnd.setVisible(true);
		menu.add(openEditorsEnd);
		menu.add(new GroupMarker(IWorkbenchActionConstants.MRU));
		return menu;
	}

	/**
	 * Creates and returns the Edit menu.
	 */
	private MenuManager createEditMenu() {
		MenuManager menu = new MenuManager(Messages.LabActionBarAdvisor_Edit,
				IWorkbenchActionConstants.M_EDIT);
		menu.add(new GroupMarker(IWorkbenchActionConstants.EDIT_START));
 		menu.add(new GroupMarker(IWorkbenchActionConstants.FIND_EXT));
		return menu;
	}

	/**
	 * Creates and returns the Window menu.
	 */
	private MenuManager createWindowMenu() {
		MenuManager menu = new MenuManager(Messages.LabActionBarAdvisor_Window,
				IWorkbenchActionConstants.M_WINDOW);

		Separator beforePreferences = new Separator("windowbeforepreferences"); //$NON-NLS-1$
		beforePreferences.setVisible(false);
		menu.add(beforePreferences);
		ActionContributionItem openPreferencesItem = new ActionContributionItem(
				openPreferencesAction);
		openPreferencesItem.setVisible((!"carbon".equals(SWT.getPlatform()) && !"cocoa".equals(SWT.getPlatform()))); //$NON-NLS-1$ //$NON-NLS-2$
		menu.add(openPreferencesItem);
		menu.add(new Separator("windowafterpreferences")); //$NON-NLS-1$
		return menu;
	}

	/**
	 * Creates and returns the Help menu.
	 */
	private MenuManager createHelpMenu() {
		MenuManager menu = new MenuManager(Messages.LabActionBarAdvisor_Help,
				IWorkbenchActionConstants.M_HELP);

		addSeparatorOrGroupMarker(menu, "group.updates"); //$NON-NLS-1$
		addSeparatorOrGroupMarker(menu, IWorkbenchActionConstants.MB_ADDITIONS);
		menu.add(new Separator("helpbeforeabout")); //$NON-NLS-1$
		ActionContributionItem aboutItem = new ActionContributionItem(
				aboutAction);
		aboutItem.setVisible((!"carbon".equals(SWT.getPlatform()) && !"cocoa".equals(SWT.getPlatform()))); //$NON-NLS-1$ //$NON-NLS-2$
		menu.add(aboutItem);
		menu.add(new Separator("helpafterabout")); //$NON-NLS-1$
		return menu;
	}

	/**
	 * Adds a <code>GroupMarker</code> or <code>Separator</code> to a menu. The
	 * test for whether a separator should be added is done by checking for the
	 * existence of a preference matching the string useSeparator.MENUID.GROUPID
	 * that is set to <code>true</code>.
	 *
	 * @param menu
	 *            the menu to add to
	 * @param groupId
	 *            the group id for the added separator or group marker
	 */
	private void addSeparatorOrGroupMarker(MenuManager menu, String groupId) {
		boolean addExtraSeparators = true;
		if (addExtraSeparators) {
			menu.add(new Separator(groupId));
		} else {
			menu.add(new GroupMarker(groupId));
		}
	}

	/**
	 * Fills the menu bar with the workbench actions.
	 */
	@Override
	protected void fillMenuBar(IMenuManager menuBar) {
		menuBar.add(createFileMenu());
		menuBar.add(createEditMenu());
		menuBar.add(createWindowMenu());
		menuBar.add(createHelpMenu());
	}

	/**
	 * @return the window
	 */
	IWorkbenchWindow getWindow() {
		return window;
	}

	/**
	 * Fills the coolbar with the workbench actions.
	 */
	@Override
	protected void fillCoolBar(ICoolBarManager coolBar) {
		IToolBarManager welcomebar = new ToolBarManager(coolBar.getStyle());
		coolBar.add(new ToolBarContributionItem(welcomebar, "welcomebar")); //$NON-NLS-1$
		IToolBarManager commandbar = new ToolBarManager(coolBar.getStyle());
		coolBar.add(new ToolBarContributionItem(commandbar, "commandbar")); //$NON-NLS-1$
		IToolBarManager perspectivesbar = new ToolBarManager(coolBar.getStyle());
		coolBar.add(new ToolBarContributionItem(perspectivesbar, "perspectivesbar")); //$NON-NLS-1$
		IToolBarManager toolsbar = new ToolBarManager(coolBar.getStyle());
		coolBar.add(new ToolBarContributionItem(toolsbar, "toolsbar")); //$NON-NLS-1$
		coolBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
	}

}
