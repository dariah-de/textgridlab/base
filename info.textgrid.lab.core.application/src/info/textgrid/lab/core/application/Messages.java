package info.textgrid.lab.core.application;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.core.application.messages"; //$NON-NLS-1$
	public static String Application_Error_Message_Info;
	public static String Application_NeedJava6;
	public static String Application_No_Rights_For_Workspace_Directory;
	public static String Application_Troobleshooting_Info;
	public static String LabActionBarAdvisor_Edit;
	public static String LabActionBarAdvisor_File;
	public static String LabActionBarAdvisor_Help;
	public static String LabActionBarAdvisor_Navigation;
	public static String LabActionBarAdvisor_ShowDictionaryEntries;
	public static String LabActionBarAdvisor_ShowMDView;
	public static String LabActionBarAdvisor_ShowView;
	public static String LabActionBarAdvisor_Window;
	public static String LabWorkbenchAdvisor_ProxyMismatch;
	public static String NewXSDPreparator_AlreadyPersistant;
	public static String OpenPerspectiveAction_OpenSearch;
	public static String OpenPerspectiveAction_OpenXMLEditor;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
