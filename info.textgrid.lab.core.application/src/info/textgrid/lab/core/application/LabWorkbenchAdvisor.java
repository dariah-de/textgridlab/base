/**
 * 
 */
package info.textgrid.lab.core.application;

import info.textgrid.lab.conf.ConfPlugin;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.internal.ide.application.IDEWorkbenchAdvisor;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.texteditor.spelling.SpellingService;

/**
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv)
 * @see WorkbenchAdvisor
 * @see PLUGIN_ROOT/org.eclipse.platform.doc.isv/guide/rcp_advisor.htm
 */
@SuppressWarnings("restriction")
public class LabWorkbenchAdvisor extends IDEWorkbenchAdvisor {
	
	
	@Override
	public void initialize(IWorkbenchConfigurer configurer) {
		super.initialize(configurer);
		configurer.setSaveAndRestore(false);
		IPreferenceStore editorPreferences = EditorsUI.getPreferenceStore();
		// PrefUtil.getAPIPreferenceStore();
		editorPreferences.setValue(SpellingService.PREFERENCE_SPELLING_ENABLED, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.application.WorkbenchAdvisor#getInitialWindowPerspectiveId()
	 */
	@Override
	public String getInitialWindowPerspectiveId() {
		return "info.textgrid.lab.welcome.RecherchePerspective"; // TODO factor //$NON-NLS-1$
																	// these
		// constants out (->
		// ui.core)
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.application.WorkbenchAdvisor#createWorkbenchWindowAdvisor(org.eclipse.ui.application.IWorkbenchWindowConfigurer)
	 */
	@Override
	public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(
			IWorkbenchWindowConfigurer configurer) {
		return new LabWorkbenchWindowAdvisor(configurer);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.application.WorkbenchAdvisor#postStartup()
	 */
	@Override
	public void postStartup() {
		super.postStartup();
		
		// checks if the system proxy is differnt from the lab proxy
		// TODO: currently works only on windows systems
		//		 the property http.proxyHost seams not to be available
		//		 on mac os x or ubuntu
		String proxyHost = System.getProperty("http.proxyHost"); //$NON-NLS-1$
		
		if (proxyHost != null) {
			if (!ConfPlugin.getDefault().getPreferenceStore().getBoolean(ConfPlugin.PROXY_CONNECTION) ||
				!ConfPlugin.getDefault().getPreferenceStore().getString(ConfPlugin.PROXY_CONNECTION_HTTP).equals(proxyHost) ||
				(ConfPlugin.getDefault().getPreferenceStore().getInt(ConfPlugin.PROXY_CONNECTION_PORT) !=  Integer.parseInt(System.getProperty("http.proxyPort")))) { //$NON-NLS-1$
				MessageBox mb = new MessageBox(PlatformUI.getWorkbench().getDisplay().getActiveShell(), SWT.YES | SWT.NO | SWT.ICON_WARNING);
				mb.setMessage(NLS.bind(Messages.LabWorkbenchAdvisor_ProxyMismatch, 
						proxyHost, System.getProperty("http.proxyPort"))); //$NON-NLS-1$
				if (mb.open() == SWT.YES) {
					PreferenceDialog dialog = PreferencesUtil.createPreferenceDialogOn(PlatformUI.getWorkbench().getDisplay().getActiveShell(),
																					   "info.textgrid.lab.conf.ConfservPrefPage",  //$NON-NLS-1$
																					   null, null);
				    dialog.open();
				}
			}
		}
	}

	@Override
	public boolean preShutdown() {
		IWorkbench workbench = PlatformUI.getWorkbench();
		for (IWorkbenchWindow window : workbench.getWorkbenchWindows()) {
			for (IWorkbenchPage page : window.getPages()) {
				for (IEditorReference reference : page.getEditorReferences()) {
					IEditorPart editor = reference.getEditor(false);
					if (editor != null) {
						page.closeEditor(editor, true);
					}
				}
				for(IPerspectiveDescriptor perspective : page.getOpenPerspectives()) {
					StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, MessageFormat.format("Closing the {0} perspective", perspective.getLabel()))); //$NON-NLS-1$
					page.closePerspective(perspective, true, true);
				}
			}
		}
		
		return super.preShutdown();
	}

}
