package info.textgrid.lab.core.application;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;


//This class is intended to create an action for opening pre-defined perspectives with a 
//certain description or any view with a specified description. The action be called
//from menu definitions. 

public class OpenPerspectiveAction extends Action {
	
	//continue accordingly for new perspectives
	public static final int XMLEDITOR = 0;
	public static final int RECHERCHE = 1;
	
	private String perspective_id;
	private boolean open_view = false;
	private String view_id;
	
	public OpenPerspectiveAction(int operation) {
		switch (operation) {
			case XMLEDITOR:
				perspective_id="net.sf.vex.editor.DocumentPerspective"; //$NON-NLS-1$
				this.setText(Messages.OpenPerspectiveAction_OpenXMLEditor);
				break;
			case RECHERCHE:
				perspective_id="info.textgrid.lab.welcome.RecherchePerspective"; //$NON-NLS-1$
				this.setText(Messages.OpenPerspectiveAction_OpenSearch);
				break;
		}
	}
	
	
	//in case that we rather want to open a certain view whose ID we already know
	public OpenPerspectiveAction(String view_id, String descr) {
		this.open_view = true;
		this.view_id = view_id;
		this.setText(descr);
	}
	
	
	public void run() {
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		
		if (window != null) {

			try {
				
				if (open_view == false) {
					//open the perspective in the current workbench window
					workbench.showPerspective(this.perspective_id, window);

				} else if (open_view == true) {
					IViewPart view = window.getActivePage().
					showView(this.view_id);
				}
					
			} catch (Exception e) {e.printStackTrace();}
			
			}
		}
}
                                                 