package info.textgrid.lab.core.application;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.internal.util.PrefUtil;
import org.eclipse.ui.intro.IIntroManager;
import org.eclipse.ui.progress.UIJob;

/**
 * The Workbench Window Advisor is responsible for configuring the common
 * elements of the TextGridLab's window like menus and toolbars.
 *
 * Many of this stuff is copied from the IDE's implementation.
 *
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv)
 * @see WorkbenchWindowAdvisor
 * @see org.eclipse.ui.internal.ide.application.IDEWorkbenchWindowAdvisor
 */
public class LabWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	private IWorkbenchWindowConfigurer workbenchWindowConfigurer;

	public LabWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		super(configurer);
		this.workbenchWindowConfigurer = configurer;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#getWindowConfigurer()
	 */
	@Override
	protected IWorkbenchWindowConfigurer getWindowConfigurer() {
		return workbenchWindowConfigurer;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.ui.application.WorkbenchAdvisor#preWindowOpen
	 */
	@Override
	public void preWindowOpen() {
		IWorkbenchWindowConfigurer configurer = getWindowConfigurer();

		// show the shortcut bar and progress indicator, which are hidden by
		// default
		configurer.setShowPerspectiveBar(true);
		configurer.setShowFastViewBars(true);
		configurer.setShowProgressIndicator(true);


		/* FIXME drag and drop support for the editor area
		// add the drag and drop support for the editor area
		configurer.addEditorAreaTransfer(EditorInputTransfer.getInstance());
		configurer.addEditorAreaTransfer(ResourceTransfer.getInstance());
		configurer.addEditorAreaTransfer(FileTransfer.getInstance());
		configurer.addEditorAreaTransfer(MarkerTransfer.getInstance());
		configurer.configureEditorAreaDropListener(new EditorAreaDropAdapter(
				configurer.getWindow())); */
		// hookTitleUpdateListeners(configurer);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#createActionBarAdvisor(org.eclipse.ui.application.IActionBarConfigurer)
	 */
	@Override
	public ActionBarAdvisor createActionBarAdvisor(
			IActionBarConfigurer configurer) {
		return new LabActionBarAdvisor(configurer);
	}

	//This overridden brute-force implementation of openIntro
	//is responsible for always opening the Welcome Screen on system start

	@Override
	public void openIntro () {
 // TODO: Refactor this into an IIntroManager.openIntro(IWorkbenchWindow) call

        // introOpened flag needs to be global
        IWorkbenchConfigurer wbConfig = getWindowConfigurer().getWorkbenchConfigurer();
        final String key = "introOpened"; //$NON-NLS-1$
        Boolean introOpened = (Boolean) wbConfig.getData(key);
//        if (introOpened != null && introOpened.booleanValue()) {
//			return;
//		}

        wbConfig.setData(key, Boolean.TRUE);

        boolean showIntro = PrefUtil.getAPIPreferenceStore().getBoolean(
                IWorkbenchPreferenceConstants.SHOW_INTRO);

        IIntroManager introManager = wbConfig.getWorkbench().getIntroManager();

        boolean hasIntro = introManager.hasIntro();
        boolean isNewIntroContentAvailable = introManager.isNewContentAvailable();



		//if (hasIntro && (showIntro || isNewIntroContentAvailable)) {
        if (hasIntro) {
        	introManager
                    .showIntro(getWindowConfigurer().getWindow(), false);

            PrefUtil.getAPIPreferenceStore().setValue(
                    IWorkbenchPreferenceConstants.SHOW_INTRO, false);
            PrefUtil.saveAPIPrefs();
        }
	}

	@Override
	public void postWindowOpen() {
		super.postWindowOpen();
		// the userstorage menu is not removable completeley with extension points in plugin.xml, so here we code it :-/
		// try to avoid concurrent-mod-exception using a job

		UIJob job = new UIJob("remove userstorage button") {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				IWorkbenchWindowConfigurer workbenchWindowConfigurer = getWindowConfigurer();
				IActionBarConfigurer actionBarConfigurer = workbenchWindowConfigurer.getActionBarConfigurer();
				IMenuManager menuManager = actionBarConfigurer.getMenuManager();
				IMenuManager helpMenuManager = menuManager.findMenuUsingPath("help");

				IContributionItem ustorageEntry = null;
				for(IContributionItem item : helpMenuManager.getItems()) {
					if ("org.eclipse.userstorage.accounts".equals(item.getId())) {
						ustorageEntry = item;
						break;
					}
				}
				if(ustorageEntry != null) {
					helpMenuManager.remove(ustorageEntry);
				}
				helpMenuManager.update(true);
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	  }
}
