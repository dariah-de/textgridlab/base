package info.textgrid.lab.core.application;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.NewObjectWizard;

import java.io.UnsupportedEncodingException;

import org.eclipse.core.runtime.IStatus;

/**
 * Sets up a new W3C XML Schema skeleton and calls the default handler then.
 */
public class NewXSDPreparator implements INewObjectPreparator {
	
	public static final String NEW_SCHEMA_CONTENTS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" //$NON-NLS-1$
			+ "<schema xmlns=\"http://www.w3.org/2001/XMLSchema\"\n" //$NON-NLS-1$
			+ "        targetNamespace=\"http://www.example.org/NewXMLSchema\"\n" //$NON-NLS-1$
			+ "        xmlns:tns=\"http://www.example.org/NewXMLSchema\"\n" //$NON-NLS-1$
			+ "        elementFormDefault=\"qualified\">\n" + "</schema>"; //$NON-NLS-1$ //$NON-NLS-2$

	private ITextGridWizard wizard;

	public void initializeObject(TextGridObject textGridObject) {
	}

	public boolean performFinish(TextGridObject textGridObject) {
		try {
			textGridObject.setInitialContent(NEW_SCHEMA_CONTENTS
					.getBytes("UTF-8")); //$NON-NLS-1$
		} catch (IllegalStateException e) {
			Activator
					.handleProblem(
							IStatus.WARNING,
							e,
							Messages.NewXSDPreparator_AlreadyPersistant,
							textGridObject);
		} catch (UnsupportedEncodingException e) {
			Activator
					.handleProblem(
							IStatus.ERROR,
							e,
							"UTF-8 is insupported!? What a strange setup is this? It will be hard to run the lab here ..."); //$NON-NLS-1$
		}
		
		if (wizard instanceof NewObjectWizard)
			return ((NewObjectWizard) wizard).defaultPerformFinish();
		else
			return false;
	}

	public void setWizard(ITextGridWizard wizard) {
		this.wizard = wizard;
	}

}
