/**
 * 
 */
package info.textgrid.lab.core.application;

import java.io.File;
import java.net.URL;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/**
 * The application controls the TextGridLab's lifecycle.
 * 
 * @see IApplication
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv)
 * 
 */
public class Application implements IApplication {

	private static final String TEXT_GRID_LAB_WORKSPACE = "TextGridLab.workspace";
	private boolean msgLogged = false;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.
	 * IApplicationContext)
	 */
	public Object start(IApplicationContext context) throws Exception {
		Display display = PlatformUI.createDisplay();

		// Java Version Check
		String[] javaVersion = System
				.getProperty("java.specification.version").split("\\."); //$NON-NLS-1$ //$NON-NLS-2$
		if (javaVersion[0].equals("1")) { //$NON-NLS-1$
			try {
				int subVersion = Integer.parseInt(javaVersion[1]);
				if (subVersion < 6) {
					Shell shell = new Shell(display);
					MessageBox mb = new MessageBox(shell, SWT.OK
							| SWT.ICON_WARNING);
					mb.setMessage(NLS.bind(Messages.Application_NeedJava6,
							System.getProperty("java.specification.version"))); //$NON-NLS-1$
					mb.open();
					System.exit(0);
				}
			} catch (NumberFormatException e) {
			}
		}

		//TG-1936
		Location instanceLoc = Platform.getInstanceLocation();
		boolean standardPath;
//		System.out.println("Loc readOnly: " + instanceLoc.isReadOnly());
//		System.out.println("Loc isSet: " + instanceLoc.isSet());
		if (instanceLoc.isReadOnly() && !instanceLoc.isSet()) {
			standardPath = false; // means path to workspace for a
									// "ReadOnly"-environment
			try {
				String path = getWorkspacePathSuggestion(standardPath);
				instanceLoc.release();
				instanceLoc.set(new URL("file", null, path), false);
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		} else if (!instanceLoc.isSet()) {
			standardPath = true; // means standard path to workspace
			try {
				String path = getWorkspacePathSuggestion(standardPath);
				instanceLoc.release();
				instanceLoc.set(new URL("file", null, path), false);
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		
		if (!msgLogged)
			logWorkspacePath(new StringBuffer(instanceLoc.getURL().getPath()));

		try {
			int code = PlatformUI.createAndRunWorkbench(display,
					new LabWorkbenchAdvisor());
			return code == PlatformUI.RETURN_RESTART ? EXIT_RESTART : EXIT_OK;
		} finally {
			if (display != null)
				display.dispose();
		}
	}

	// suggests a path
	private String getWorkspacePathSuggestion(boolean standardPath) {
		StringBuffer buf = new StringBuffer();
		String uHome = "";
		if (standardPath) {
			// Standard path to the workspace
			// TODO is this the right location????
			uHome = Platform.getInstanceLocation().toString();
		} else {
			uHome = System.getProperty("user.home");
			if (uHome == null) {
				// set location to c:\temp
				uHome = "c:" + File.separator + "temp";
			}
		}

		buf.append(uHome);
		buf.append(File.separator);
		buf.append(TEXT_GRID_LAB_WORKSPACE);

		if (!msgLogged)
			logWorkspacePath(buf);
		
		return buf.toString();
	}

	private void logWorkspacePath(StringBuffer buf) {
		msgLogged = true;
		String msg = "path to workspace location: ";
		Activator
				.getDefault()
				.getLog()
				.log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, msg
						+ buf.toString()));
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		// TODO Auto-generated method stub

	}

}
