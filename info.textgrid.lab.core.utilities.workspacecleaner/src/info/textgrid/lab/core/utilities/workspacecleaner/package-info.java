/**
 * This bundle is a hack that cleans the Lab's workspace on startup to resolve
 * TG-196.
 * 
 * To use, you must
 * <ol>
 * <li>Include this plugin with the product</li>
 * <li>Configure this plugin with a very low start level so it is started before
 * the resources plugin</li>
 * <li>Configure this plugin as an auto-start plugin.</li>
 * </ol>
 * The latter two steps can be done by the following option in the product
 * definition:
 * 
 * <pre>
 * {@literal
 *    <configurations>
 *       	<plugin id="info.textgrid.lab.core.utilities.workspacecleaner" autoStart="true" startLevel="0" />
 *       	<!-- further start level config etc. -->
 *       </configurations>
 * }
 * </pre>
 * 
 * See
 * {@link info.textgrid.lab.core.utilities.workspacecleaner.WorkspaceCleanerBundle}
 * for details on what this does.
 */
package info.textgrid.lab.core.utilities.workspacecleaner;

