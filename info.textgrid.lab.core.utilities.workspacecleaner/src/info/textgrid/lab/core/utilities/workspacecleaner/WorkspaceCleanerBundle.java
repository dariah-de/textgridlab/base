package info.textgrid.lab.core.utilities.workspacecleaner;

import java.io.File;
import java.io.FileFilter;
import java.text.MessageFormat;

import org.eclipse.core.internal.runtime.InternalPlatform;
import org.eclipse.core.runtime.IPath;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * The bundle activator actually does perform the evil workspace cleaning hack.
 * 
 * On startup, it deletes
 * <ol>
 * <li>all workspace projects that contain only a '.project' file, along with
 * that project file</li>
 * <li>the resource plugin's state area (which contains workspace metadata).</li>
 * </ol>
 * 
 * @author vitt
 */
@SuppressWarnings("restriction")
public class WorkspaceCleanerBundle implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(final BundleContext bundleContext) throws Exception {
		WorkspaceCleanerBundle.context = bundleContext;
		System.out.println("Cleaner bundle started");
		cleanWorkspace();

	}

	private void cleanWorkspace() {
		// Yes, it's evil, but we want to be earlier than the Workspace.
		final IPath workspacePath = InternalPlatform.getDefault().getMetaArea().getInstanceDataLocation();
		System.out.println("The workspace is here: " + workspacePath);
		final File workspace = workspacePath.toFile();
		if (workspace.exists() && workspace.isDirectory()) {
			final File[] projectDirs = workspace.listFiles(new FileFilter() {

				@Override
				public boolean accept(final File pathname) {
					return pathname.isDirectory() && !pathname.getName().startsWith(".");
				}
			});
			for (final File projectDir : projectDirs) {
				File projectFile = null;
				boolean doNotTouch = false;
				final File[] files = projectDir.listFiles();
				for (final File file : files) {
					if (file.getName().equals(".project"))
						projectFile = file;
					else {
						doNotTouch = true;
						System.err.println(MessageFormat.format("{0} blocks deletion of {1}", file, projectDir));
						break;
					}
				}
				if (!doNotTouch) {
					if (projectFile != null)
						projectFile.delete();
					final boolean success = projectDir.delete();
					if (success)
						System.out.println(MessageFormat.format("Successfully deleted {0}", projectDir));
					else
						System.err.println(MessageFormat.format("Could not delete {0}", projectDir));
				}

			}
			// Delete the Resources Plugin directory
			final File resourcesPluginDir = new File(workspace, "/.metadata/.plugins/org.eclipse.core.resources");
			if (resourcesPluginDir.exists() && resourcesPluginDir.isDirectory()) {
				deleteRecursively(resourcesPluginDir);
			}
		}
	}

	private static void deleteRecursively(final File dir) {
		for (final File file : dir.listFiles()) {
			if (file.isDirectory())
				deleteRecursively(file);
			file.delete();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(final BundleContext bundleContext) throws Exception {
		WorkspaceCleanerBundle.context = null;
	}

}
