The Template editor (Wizard) is responsible for creating a user defined
XUL-Document to describe the structure of the Metadata editor.

This wizard can be used to add optional or custom elements 
to the description (XUL) of the Metadata editor.

The Main class to create the wizard is TemplateEditorWizard.java 
in package info.textgrid.lab.templateeditor/wizard.