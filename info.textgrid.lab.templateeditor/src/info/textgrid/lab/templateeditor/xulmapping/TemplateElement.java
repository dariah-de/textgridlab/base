/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.xulmapping;

import info.textgrid.lab.templateeditor.Activator;

import java.io.InputStream;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

/**
 * An abstract class to be used as a base class for the classes in the
 * xulmapping-package.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public abstract class TemplateElement {

	public static String XLAYOUT_DIR = "XLayoutFiles";

	/**
	 * To set this element as checked (active) or not (not active).
	 */
	public void setChecked(boolean checked) {

		preCreate();
		if (checked) {
			create();
		}
	}

	/**
	 * An abstract method to create the element.
	 */
	protected abstract void create();

	/**
	 * An method for the Pre-Creating.
	 */
	protected void preCreate() {
	}

	/**
	 * Adds the Content from the {@link InputStream} to the central XUL-Element.
	 * 
	 * @param in
	 *            the referred {@link InputStream}
	 */
	protected void addToDocument(InputStream in) {

		XULCreator.getInstance().addToDocument(in);
	}

	/**
	 * Returns the {@link InputStream} for the element.
	 * 
	 * @param filename
	 */
	protected InputStream getInputStream(String filename) {

		IPath path = new Path(XLAYOUT_DIR).append(filename);
		URL url = FileLocator.find(Activator.getDefault().getBundle(), path,
				null);

		InputStream in = null;
		try {
			url = FileLocator.resolve(url);
			in = url.openStream();
		} catch (Exception e) {
			Activator.handleError(e);
		}

		return in;
	}
}