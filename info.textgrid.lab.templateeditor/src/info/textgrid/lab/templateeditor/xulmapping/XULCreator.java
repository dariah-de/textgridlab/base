/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.xulmapping;

import info.textgrid.lab.core.model.util.StringToOM;
import info.textgrid.lab.templateeditor.Activator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMElement;

/**
 * This singleton class is responsible for creating the XUL-Document.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class XULCreator {

	private StringWriter writer = null;
	private static XULCreator instance = null;

	/**
	 * The default constructor.
	 */
	private XULCreator() {
		writer = new StringWriter();
	}

	/**
	 * Recreates the existing XUL-Instance.
	 */
	public static void reCreate() {
		instance = null;
		instance = new XULCreator();
	}

	/**
	 * @return the instance of this class
	 */
	public static XULCreator getInstance() {
		if (instance == null)
			instance = new XULCreator();

		return instance;
	}

	/**
	 * Adds the content of the {@link InputStream} to the XUL-Document.
	 * 
	 * @param in
	 *            {@link InputStream}
	 */
	public void addToDocument(InputStream in) {
		try {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));

			String s = null;
			while ((s = reader.readLine()) != null) {
				// writer.write(s+"\n");
				writer.write(s);
			}
			writer.flush();
		} catch (IOException e) {
			Activator.handleError(e);
		}
	}

	/**
	 * Adds a line (String) to the XUL-Document.
	 * 
	 * @param line
	 *            the line to add
	 */
	public void addToDocument(String line) {
		writer.write(line);
		writer.flush();
	}

	/**
	 * Creates the XUL-Document.
	 * 
	 * @return the created XUL-Document as {@link OMElement}
	 */
	public OMElement createXUL() {
		try {
			if (writer.getBuffer().length() > 0)
				return StringToOM.getOMElement(writer.getBuffer().toString());
		} catch (XMLStreamException e) {
			Activator.handleError(e);
		} catch (UnsupportedEncodingException e) {
			Activator.handleError(e);
		} catch (FactoryConfigurationError e) {
			Activator.handleError(e);
		}
		return null;
	}

}