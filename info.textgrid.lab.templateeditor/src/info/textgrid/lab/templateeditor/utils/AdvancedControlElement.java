/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.utils;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class AdvancedControlElement {

	private Control control = null;

	private String id = null;
	private String name = null;
	private String ref = null;

	// refId to be used in the RepeatElements
	private String refId = null;
	private int readOnlyFlag = 0;

	private OMElement element = null;

	private String inputType = "string";
	private boolean required = false;

	private Label label = null;

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public AdvancedControlElement(Control control, String id, String name,
			String ref, int readOnlyFlag) {

		this.control = control;
		this.id = this.refId = id;
		this.name = name;
		this.ref = ref;
		this.readOnlyFlag = readOnlyFlag;

		// XXX if name is empty, then take the id as element-name
		if (this.name == null) {
			this.name = id;
		}
	}

	public String getId() {

		return id;
	}

	void setRefId(String refId) {

		this.refId = refId;
	}

	public String getRefId() {

		return refId;
	}

	public String getName() {

		return name;
	}

	public String getRef() {

		return ref;
	}

	public Control getControl() {

		return control;
	}

	public OMElement getOMElement() {

		return element;
	}

	public int getReadOnlyFlag() {

		return readOnlyFlag;
	}

	void createOMElement(OMFactory omFactory, OMElement parent, OMNamespace ns,
			boolean asAttribute) {

		if (!asAttribute)
			createOMElementControl(omFactory, parent, ns);
		else
			createOMElementControlAsAttribute(omFactory, parent);
	}

	private void createOMElementControl(OMFactory omFactory, OMElement parent,
			OMNamespace ns) {

		String value = "";

		if (control instanceof Text) {
			Text t = (Text) control;
			value = t.getText();
		} else if (control instanceof Combo) {
			Combo c = (Combo) control;
			value = c.getText();
		} else if (control instanceof List) {
			List l = (List) control;
			if (l.getSelectionIndex() > -1)
				value = l.getItem(l.getSelectionIndex());
		} else {
			// XXX no suitable controls
			return;
		}

		element = omFactory.createOMElement(this.name, ns);
		element.setText(value);

		parent.addChild(element);
	}

	void clearContent() {

		if (control instanceof Text) {
			((Text) control).setText("");
		} else if (control instanceof Combo) {
			((Combo) control).select(0);
		} else if (control instanceof List) {
			((List) control).select(0);
		} else {
			// XXX no suitable controls
			return;
		}
	}

	private void createOMElementControlAsAttribute(OMFactory omFactory,
			OMElement parent) {

		String value = "";

		if (control instanceof Text) {
			Text t = (Text) control;
			value = t.getText();
		} else if (control instanceof Combo) {
			Combo c = (Combo) control;
			value = c.getText();
		} else if (control instanceof List) {
			List l = (List) control;
			if (l.getSelectionIndex() > -1)
				value = l.getItem(l.getSelectionIndex());
		}

		parent.addAttribute(omFactory.createOMAttribute(this.name, null, value));
	}

	public void dispose() {

		if (control != null)
			control.dispose();
	}

}
