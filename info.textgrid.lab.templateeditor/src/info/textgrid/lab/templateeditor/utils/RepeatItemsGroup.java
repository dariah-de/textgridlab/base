/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.utils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class RepeatItemsGroup {

	private LinkedList<AdvancedControlElement> advancedControls = new LinkedList<AdvancedControlElement>();
	private Map<String, Control> elements = new HashMap<String, Control>();
	private Button remove_btn = null;

	public void addAdvancedControl(AdvancedControlElement e) {

		advancedControls.addLast(e);
		elements.put(e.getId(), e.getControl());
	}

	public void addRemoveButton(Button b) {

		remove_btn = b;
	}

	public LinkedList<Control> getControls() {

		LinkedList<Control> l = new LinkedList<Control>();

		for (AdvancedControlElement e : advancedControls) {
			l.add(e.getControl());
		}

		return l;
	}

	void clearControls() {

		for (AdvancedControlElement e : advancedControls) {
			e.clearContent();
		}
	}

	public LinkedList<AdvancedControlElement> getAdvancedControls() {

		return advancedControls;
	}

	public Map<String, Control> getItems() {

		return elements;
	}

	public Control getControlById(String id) {

		return elements.get(id);
	}

	String getIdByRefId(String refId) {

		for (AdvancedControlElement e : advancedControls) {
			if (e.getRefId().equals(refId))
				return e.getId();
		}

		return refId;
	}

	private AdvancedControlElement getAdvancedControlElementById(String id) {

		for (AdvancedControlElement e : advancedControls) {
			if (e.getId().equals(id))
				return e;
		}

		return null;
	}

	void createOMElements(OMFactory omFactory, OMElement parent, OMNamespace ns) {

		for (AdvancedControlElement e : advancedControls) {
			if (e.getRef() != null) {
				AdvancedControlElement ac = getAdvancedControlElementById(e
						.getRef());
				if (ac != null)
					e.createOMElement(omFactory, ac.getOMElement(), ns, true);
			} else {
				e.createOMElement(omFactory, parent, ns, false);
			}
		}
	}

	public void dispose() {

		while (!advancedControls.isEmpty()) {
			AdvancedControlElement e = advancedControls.getLast();
			e.dispose();
			advancedControls.removeLast();
		}
		remove_btn.dispose();
		elements.clear();
	}

}
