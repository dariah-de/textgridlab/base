/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.xsdcreator;

import java.util.ArrayList;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

/**
 * This class is responsible for creating the XSD-Schema.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class XSDSchema {

	private ArrayList<XSDElement> elements = null;
	private String tNS = null;

	/**
	 * The constructor.
	 * 
	 * @param tNS
	 *            the target namespace
	 */
	public XSDSchema(String tNS) {

		this.tNS = tNS;
		elements = new ArrayList<XSDElement>();
	}

	/**
	 * Adds the referred {@link XSDElement} to the list of the elements.
	 * 
	 * @param xsdElem
	 *            the referred {@link XSDElement}
	 */
	public void addElement(XSDElement xsdElem) {

		elements.add(xsdElem);
	}

	/**
	 * Creates the Schema and returns it as {@link OMElement}.
	 * 
	 * @return {@link OMElement}
	 */
	public OMElement createSchema() {

		OMFactory omfactory = OMAbstractFactory.getOMFactory();

		OMNamespace ns1 = omfactory.createOMNamespace(
				"http://www.w3.org/2001/XMLSchema", "xs");
		OMNamespace ns2 = omfactory.createOMNamespace(tNS, "tns");

		OMAttribute attr1 = omfactory.createOMAttribute("targetNamespace",
				null, tNS);

		OMElement schema = omfactory.createOMElement("schema", ns1);
		schema.declareNamespace(ns2);
		schema.addAttribute(attr1);

		// *************************
		OMAttribute attr3 = omfactory.createOMAttribute("name", null,
				"customType");
		OMElement compType = omfactory.createOMElement("ComplexType", ns1);
		compType.addAttribute(attr3);
		schema.addChild(compType);

		// ************************
		OMElement sequence = omfactory.createOMElement("sequence", ns1);
		compType.addChild(sequence);

		for (XSDElement e : elements) {
			sequence.addChild(e.createXSDElement(omfactory, ns1));
		}

		return schema;
	}

	/**
	 * Clears the list of the elements.
	 */
	public void clear() {

		elements.clear();
	}
}