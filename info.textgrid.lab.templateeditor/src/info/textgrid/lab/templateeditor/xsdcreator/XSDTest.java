/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.xsdcreator;

import javax.xml.stream.XMLStreamException;

/**
 * A test class for the XSD-Creator.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class XSDTest {

	/**
	 * The main method.
	 * 
	 * @param args
	 * @throws XMLStreamException
	 */
	public static void main(String[] args) throws XMLStreamException {

		XSDSchema s = new XSDSchema("http://www.test.de");
		s.addElement(new XSDElement("paragraph", "integer", "0", "unbounded"));
		s.addElement(new XSDElement("first sentence", "string", "1", "1"));

		System.out.println(s.createSchema());
	}
}