/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.xsdcreator;

import java.util.Arrays;

import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

/**
 * This class is responsible for creating the XSD elements.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class XSDElement {

	private String name;
	private String type;
	private String min;
	private String max;
	public static String[] xsd_types = new String[] { "string", "date",
			"integer", "boolean", "time", "decimal" };

	/**
	 * The constructor.
	 * 
	 * @param name
	 * @param type
	 * @param min
	 * @param max
	 */
	public XSDElement(String name, String type, String min, String max) {

		this.name = name;
		this.type = type;
		this.min = min;
		this.max = max;
		Arrays.sort(xsd_types);
	}

	/**
	 * Creates an {@link OMElement} which represents this XSD-Element.
	 * 
	 * @param omfactory
	 * @param ns
	 *            the used namespace
	 * 
	 * @return the created {@link OMElement}
	 */
	OMElement createXSDElement(OMFactory omfactory, OMNamespace ns) {

		OMAttribute name = omfactory.createOMAttribute("name", null, this.name);

		String pref = "";
		if (Arrays.binarySearch(xsd_types, this.type) > -1) {
			pref = ns.getPrefix() + ":";
		}
		OMAttribute type = omfactory.createOMAttribute("type", null, pref
				+ this.type);
		OMAttribute min = omfactory.createOMAttribute("minOccurs", null,
				this.min);
		OMAttribute max = omfactory.createOMAttribute("maxOccurs", null,
				this.max);

		OMElement e = omfactory.createOMElement("element", ns);
		e.addAttribute(name);
		e.addAttribute(type);
		e.addAttribute(min);
		e.addAttribute(max);

		return e;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getMin() {
		return min;
	}

	public String getMax() {
		return max;
	}

}
