/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.config;

import info.textgrid.lab.templateeditor.Activator;

import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.xpath.AXIOMXPath;
import org.jaxen.JaxenException;

/**
 * This singleton class is responsible for creating the Configuration-Document
 * of the template-Editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class ConfigCreator {

	public static String templateEditorSectionNS = "http://textgrid.info/namespaces/metadata/templateEditor/config/2009-01-31";

	public static QName templateConfQname = new QName(templateEditorSectionNS,
			"templateEditorConfig", "tc");

	private static QName customElementsQname = new QName(
			templateEditorSectionNS, "customElements", "tc");

	private static ConfigCreator instance = null;
	private ArrayList<ConfigCustomElement> cus_elements = null;

	/**
	 * Creates a singleton instance of this class.
	 * 
	 * @return A single instance of this class
	 */
	public static ConfigCreator getInstance() {

		if (instance == null)
			instance = new ConfigCreator();

		return instance;
	}

	/**
	 * The private default constructor.
	 */
	private ConfigCreator() {
		cus_elements = new ArrayList<ConfigCustomElement>();
	}

	/**
	 * Adds an {@link ConfigCustomElement} to the list {@link #cus_elements}
	 * 
	 * @param elem
	 */
	public void addConfigElement(ConfigCustomElement elem) {

		cus_elements.add(elem);
	}

	/**
	 * Creates the Configuration-Document and returns it as {@link OMElement}.
	 * 
	 * @return {@link OMElement}
	 */
	public OMElement createConfig() {

		OMFactory omfactory = OMAbstractFactory.getOMFactory();

		OMElement root = omfactory.createOMElement(templateConfQname);
		OMElement customElems = omfactory.createOMElement(customElementsQname);

		OMNamespace ns = omfactory.createOMNamespace(
				templateConfQname.getNamespaceURI(),
				templateConfQname.getPrefix());

		for (ConfigCustomElement e : this.cus_elements) {
			customElems.addChild(e.createConfigCustomElement(omfactory, ns));
		}
		root.addChild(customElems);

		clear();

		return root;
	}

	/**
	 * Clears all lists.
	 */
	private void clear() {
		if (!cus_elements.isEmpty())
			cus_elements.clear();
	}

	/**
	 * Searches for a specific element in the parent {@link OMElement}.
	 * 
	 * @param elementName
	 * @param root
	 *            the parent {@link OMElement}
	 * 
	 * @return {@link OMElement}
	 * 
	 * @throws JaxenException
	 */
	private static OMElement getElementWithName(String elementName,
			OMElement root) throws JaxenException {

		AXIOMXPath xpath = new AXIOMXPath(templateConfQname.getPrefix() + ":"
				+ elementName);
		xpath.addNamespace(templateConfQname.getPrefix(),
				templateConfQname.getNamespaceURI());
		OMElement elem = (OMElement) xpath.selectSingleNode(root);

		return elem;
	}

	/**
	 * Searches for the {@link ConfigCustomElement}s in a parent
	 * {@link OMElement}
	 * 
	 * @param root
	 *            the parent {@link OMElement}
	 * 
	 * @return a list of the found elements
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<ConfigCustomElement> getConfigCustomElements(
			OMElement root) {

		OMElement cusElems = null;
		ArrayList<ConfigCustomElement> elements = new ArrayList<ConfigCustomElement>();
		try {
			cusElems = getElementWithName(customElementsQname.getLocalPart(),
					root);

			OMElement name = null;
			OMElement type = null;
			OMElement repeatable = null;
			OMElement required = null;

			Iterator iter = cusElems.getChildElements();
			while (iter.hasNext()) {
				OMElement elem = (OMElement) iter.next();
				if (elem != null) {

					name = getElementWithName("name", elem);
					type = getElementWithName("type", elem);
					repeatable = getElementWithName("repeatable", elem);
					required = getElementWithName("required", elem);

					elements.add(new ConfigCustomElement(name.getText(), type
							.getText(), Boolean.parseBoolean(repeatable
							.getText()), (required != null) ? Boolean
							.parseBoolean(required.getText()) : false));
				}
			}
		} catch (JaxenException e) {
			Activator.handleError(e);
		}
		return elements;
	}

}