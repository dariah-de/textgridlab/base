/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.config;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

/**
 * This class is to save the configuration for the selected optional elements.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class ConfigCustomElement {

	private String name = null;
	private String type = null;
	private boolean repeatable = false;
	private boolean required = false;

	/**
	 * The constructor.
	 * 
	 * @param name
	 * @param type
	 * @param repeatable
	 *            determines whether the element is repeatable or not
	 */
	public ConfigCustomElement(String name, String type, boolean repeatable,
			boolean required) {

		this.name = name;
		this.type = type;
		this.repeatable = repeatable;
		this.required = required;
	}

	/**
	 * @return the name of the element
	 */
	public String getName() {

		return name;
	}

	/**
	 * @return the type of the element
	 */
	public String getType() {

		return type;
	}

	/**
	 * @return a boolean value which determines whether the element is
	 *         repeatable or not
	 */
	public boolean isRepeatable() {

		return repeatable;
	}

	/**
	 * @return a boolean value which determines whether the element is required
	 *         or not
	 */
	public boolean isRequired() {

		return required;
	}

	/**
	 * Creates an {@link OMElement} which represents this ConfigCustomElement.
	 * 
	 * @param omfactory
	 * @param ns
	 *            the used namespace
	 * 
	 * @return the created {@link OMElement}
	 */
	OMElement createConfigCustomElement(OMFactory omfactory, OMNamespace ns) {

		OMElement elem = omfactory.createOMElement("element", ns);

		OMElement name = omfactory.createOMElement("name", ns);
		name.setText(this.name);
		elem.addChild(name);

		OMElement type = omfactory.createOMElement("type", ns);
		type.setText(this.type);
		elem.addChild(type);

		OMElement repeatable = omfactory.createOMElement("repeatable", ns);
		repeatable.setText(String.valueOf(this.repeatable));
		elem.addChild(repeatable);

		OMElement required = omfactory.createOMElement("required", ns);
		required.setText(String.valueOf(this.required));
		elem.addChild(required);

		return elem;
	}

}