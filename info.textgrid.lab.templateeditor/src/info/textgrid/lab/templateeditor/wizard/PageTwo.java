/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.wizard;

import info.textgrid.lab.templateeditor.config.ConfigCustomElement;
import info.textgrid.lab.templateeditor.utils.AdvancedControlElement;
import info.textgrid.lab.templateeditor.utils.RepeatItemsGroup;
import info.textgrid.lab.templateeditor.xsdcreator.XSDElement;

import java.util.ArrayList;
import java.util.LinkedList;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

/**
 * This is the class for the third page of the Template editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class PageTwo extends WizardPage implements IWizardPage,
		ITemplateEditorPage {

	private Composite container = null;

	private FormToolkit toolkit = null;
	private ScrolledForm customForm = null;

	private Section customSection = null;

	public ArrayList<Text> groupList = new ArrayList<Text>();

	private LinkedList<RepeatItemsGroup> groups = new LinkedList<RepeatItemsGroup>();
	private Button addButton = null;
	private Composite customSectionClient = null;
	private Wizard wizard = null;

	/**
	 * The default constructor.
	 */
	public PageTwo() {

		super("The custom elements");
		setTitle("The custom elements");

		setMessage("You can create user defined elements to add to your metadata's schema for the selected project.");
	}

	/**
	 * Initializes the current page with the Template-editor wizard.
	 */
	@Override
	public void init(Wizard wizard) {

		this.wizard = wizard;
	}

	/**
	 * A call-back method to create the page.
	 * 
	 * @param parent
	 *            the parent composite
	 */
	@Override
	public void createControl(Composite parent) {

		container = new Composite(parent, SWT.BORDER);
		container.setLayoutData(parent.getLayoutData());
		container.setLayout(parent.getLayout());

		toolkit = new FormToolkit(container.getDisplay());

		customForm = toolkit.createScrolledForm(container);
		customForm.setBackground(container.getBackground());
		customForm.getBody().setLayout(new GridLayout());
		// customForm.setExpandHorizontal(true);

		// +++ section.setLayoutData(gridData) +++
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = org.eclipse.swt.layout.GridData.FILL;

		customSection = toolkit.createSection(customForm.getBody(),
				ExpandableComposite.TITLE_BAR | ExpandableComposite.EXPANDED);

		customSection.setBackground(customForm.getBackground());
		customSection.setText("Custom Elements");
		customSection.setLayoutData(gridData);
		customSection.setLayout(new GridLayout());

		customSection.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				customForm.reflow(true);
			}
		});

		customSectionClient = toolkit.createComposite(customSection, SWT.BORDER
				| SWT.FILL);
		customSectionClient.setBackground(customSection.getBackground());
		customSectionClient.setLayout(new GridLayout(1, true));// TableWrapLayout());
		// customSectionClient.setSize(250, 300);

		customSection.setClient(customSectionClient);

		addButton = toolkit.createButton(customSectionClient,
				"Add new Element", SWT.PUSH);
		addButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER,
				true, false));
		addButton
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						addControlsGroup();
					}
				});

		// Just if wizard instance of TemplateEditorWizardSelected
		if (this.wizard instanceof TemplateEditorWizardSelected) {
			((TemplateEditorWizardSelected) this.wizard).initPageTwo();
		}

		setControl(container);
		setPageComplete(true);
	}

	/**
	 * Check whether the element's name a valid xml element's name.
	 * 
	 * @param name
	 * @return
	 */
	private boolean checkElementName(String name) {
		return name.matches("^(?!([xX][mM][lL]|[_\\d\\W]))[^\\s\\W]+$");
	}

	/**
	 * Creates a group of controls as custom member and adds it to the page.
	 */
	private void addControlsGroup() {

		if (addButton != null)
			addButton.dispose();

		final RepeatItemsGroup group = new RepeatItemsGroup();

		final Label l1 = toolkit.createLabel(customSectionClient,
				"Element Name");
		l1.setBackground(customSectionClient.getBackground());

		AdvancedControlElement adv = new AdvancedControlElement(l1, "", "", "",
				0);
		adv.setInputType("string");
		adv.setRequired(false);

		group.addAdvancedControl(adv);

		final Text element_name = toolkit.createText(customSectionClient, "",
				SWT.BORDER);

		l1.setForeground(l1.getDisplay().getSystemColor(SWT.COLOR_RED));
		setErrorMessage("Some elements have invalid names!");
		setPageComplete(false);

		element_name.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				if (checkElementName(element_name.getText())) {

					l1.setForeground(l1.getDisplay().getSystemColor(
							SWT.COLOR_BLACK));
					setErrorMessage(null);
					setPageComplete(true);

					boolean notCorrect = false;

					// check the other fields and if the given name is in use
					for (RepeatItemsGroup g : groups) {
						if (g == group)
							continue;
						for (AdvancedControlElement c : g.getAdvancedControls()) {
							if (c != null && c.getName().equals("name")) {
								Text t = (Text) c.getControl();
								if (t != null) {
									if (!checkElementName(t.getText())) {
										c.getLabel().setForeground(
												l1.getDisplay().getSystemColor(
														SWT.COLOR_RED));
										setErrorMessage("Some elements have invalid names!");
										setPageComplete(false);
										notCorrect = true;
									} else {
										c.getLabel().setForeground(
												l1.getDisplay().getSystemColor(
														SWT.COLOR_BLACK));
										setErrorMessage(null);
										setPageComplete(true);
									}

									if (!"".equals(element_name.getText())
											&& element_name.getText().equals(
													t.getText())) {
										l1.setForeground(l1.getDisplay()
												.getSystemColor(SWT.COLOR_RED));
										setErrorMessage("The given name is in use!");
										setPageComplete(false);
										notCorrect = true;
									}
								}

								break;
							}
						}

						if (notCorrect)
							break;

					}
					// ---

					if (!notCorrect) {
						l1.setForeground(l1.getDisplay().getSystemColor(
								SWT.COLOR_BLACK));
						setErrorMessage(null);
						setPageComplete(true);
					}
				} else {
					l1.setForeground(l1.getDisplay().getSystemColor(
							SWT.COLOR_RED));
					setErrorMessage("Some elements have invalid names!");
					setPageComplete(false);
				}

			}
		});

		GridData gd = new GridData(GridData.FILL, GridData.CENTER, true, false);
		element_name.setLayoutData(gd);

		AdvancedControlElement advText = new AdvancedControlElement(
				element_name, "", "name", "", 0);
		advText.setLabel(l1);

		group.addAdvancedControl(advText);

		Label l2 = toolkit.createLabel(customSectionClient, "Element Type");
		l2.setBackground(customSectionClient.getBackground());
		group.addAdvancedControl(new AdvancedControlElement(l2, "", "", "", 0));

		Combo element_type = new Combo(customSectionClient, SWT.READ_ONLY);
		// element_type.setLayoutData(gd);

		String[] elems = XSDElement.xsd_types;

		element_type.setItems(elems);

		int index = 0;

		for (int i = 0; i < elems.length; i++) {
			if ((elems[i]).contains("string")) {
				index = i;
				break;
			}
		}

		element_type.select(index);

		group.addAdvancedControl(new AdvancedControlElement(element_type, "",
				"type", "", 0));

		Button repeatable = toolkit.createButton(customSectionClient,
				"Repeatable", SWT.CHECK);
		repeatable.setBackground(customSectionClient.getBackground());
		group.addAdvancedControl(new AdvancedControlElement(repeatable, "",
				"repeatable", "", 0));

		Button required = toolkit.createButton(customSectionClient,
				"Mandatory", SWT.CHECK);
		required.setBackground(customSectionClient.getBackground());
		group.addAdvancedControl(new AdvancedControlElement(required, "",
				"required", "", 0));

		final Button removeButton = toolkit.createButton(customSectionClient,
				"Remove", SWT.PUSH);
		removeButton
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						if (groups.size() > 0) {
							groups.remove(group);
							group.dispose();
							customForm.reflow(true);

							setErrorMessage(null);
							setPageComplete(true);

							for (RepeatItemsGroup g : groups) {
								for (AdvancedControlElement c : g
										.getAdvancedControls()) {
									if (c != null && c.getName().equals("name")) {
										Text t = (Text) c.getControl();
										if (t != null) {
											if (!checkElementName(t.getText())) {
												setErrorMessage("Some elements have invalid names!");
												setPageComplete(false);
											}
										}
									}
								}

							}

						}
					}
				});

		Label sep = new Label(customSectionClient, SWT.SEPARATOR
				| SWT.HORIZONTAL);
		sep.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		group.addAdvancedControl(new AdvancedControlElement(sep,
				"lbl_seperator", null, null, 0));

		group.addRemoveButton(removeButton);
		groups.addLast(group);

		addButton = toolkit.createButton(customSectionClient,
				"Add new Element", SWT.PUSH);
		addButton.setLayoutData(gd);
		addButton
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						addControlsGroup();
					}
				});

		customForm.reflow(true);
	}

	/**
	 * Creates a group of controls as custom member and adds it to the page.
	 * 
	 * @param name
	 *            the name of member
	 * @param type
	 *            the type
	 * @param repeat
	 *            determines whether this member repeatable or not
	 */
	private void addControlsGroup(String name, String type, boolean repeat,
			boolean require) {

		if (addButton != null)
			addButton.dispose();

		final RepeatItemsGroup group = new RepeatItemsGroup();

		final Label l1 = toolkit.createLabel(customSectionClient,
				"Element Name");
		l1.setBackground(customSectionClient.getBackground());

		AdvancedControlElement adv = new AdvancedControlElement(l1, "", "", "",
				0);
		adv.setInputType(type);
		adv.setRequired(require);
		group.addAdvancedControl(adv);

		final Text element_name = toolkit.createText(customSectionClient, name,
				SWT.BORDER);

		if (checkElementName(element_name.getText())) {
			l1.setForeground(l1.getDisplay().getSystemColor(SWT.COLOR_BLACK));
			setErrorMessage(null);
			setPageComplete(true);
		} else {
			l1.setForeground(l1.getDisplay().getSystemColor(SWT.COLOR_RED));
			setErrorMessage("Some elements have invalid names!");
			setPageComplete(false);
		}

		element_name.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				if (checkElementName(element_name.getText())) {

					boolean nameInUse = false;

					// check if the given name is in use
					for (RepeatItemsGroup g : groups) {
						if (g == group)
							continue;
						for (AdvancedControlElement c : g.getAdvancedControls()) {
							if (c != null && c.getName().equals("name")) {
								Text t = (Text) c.getControl();
								if (t != null) {
									if (element_name.getText().equals(
											t.getText())) {
										l1.setForeground(l1.getDisplay()
												.getSystemColor(SWT.COLOR_RED));
										setErrorMessage("The given name is in use!");
										setPageComplete(false);
										nameInUse = true;
									}
								}
							}
						}

					}
					// ---

					if (!nameInUse) {

						l1.setForeground(l1.getDisplay().getSystemColor(
								SWT.COLOR_BLACK));
						setErrorMessage(null);
						setPageComplete(true);
					}
				} else {
					l1.setForeground(l1.getDisplay().getSystemColor(
							SWT.COLOR_RED));
					setErrorMessage("Some elements have invalid names!");
					setPageComplete(false);
				}

			}
		});

		GridData gd = new GridData(GridData.FILL, GridData.CENTER, true, false);
		element_name.setLayoutData(gd);

		AdvancedControlElement advText = new AdvancedControlElement(
				element_name, "", "name", "", 0);
		advText.setLabel(l1);

		group.addAdvancedControl(advText);

		Label l2 = toolkit.createLabel(customSectionClient, "Element Type");
		l2.setBackground(customSectionClient.getBackground());
		group.addAdvancedControl(new AdvancedControlElement(l2, "", "", "", 0));

		ArrayList<String> types = new ArrayList<String>();
		for (String t : XSDElement.xsd_types) {
			types.add(t);
		}

		if (!types.contains(type))
			types.add(type);

		Combo element_type = new Combo(customSectionClient, SWT.READ_ONLY);
		element_type.setItems(types.toArray(new String[types.size()]));
		element_type.select(types.indexOf(type));
		group.addAdvancedControl(new AdvancedControlElement(element_type, "",
				"type", "", 0));

		Button repeatable = toolkit.createButton(customSectionClient,
				"Repeatable", SWT.CHECK);
		repeatable.setBackground(customSectionClient.getBackground());
		repeatable.setSelection(repeat);
		group.addAdvancedControl(new AdvancedControlElement(repeatable, "",
				"repeatable", "", 0));

		Button required = toolkit.createButton(customSectionClient,
				"Mandatory", SWT.CHECK);
		required.setBackground(customSectionClient.getBackground());
		required.setSelection(require);
		group.addAdvancedControl(new AdvancedControlElement(required, "",
				"required", "", 0));

		final Button removeButton = toolkit.createButton(customSectionClient,
				"Remove", SWT.PUSH);
		removeButton
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						if (groups.size() > 0) {
							groups.remove(group);
							group.dispose();
							customForm.reflow(true);

							setErrorMessage(null);
							setPageComplete(true);

							for (RepeatItemsGroup g : groups) {
								for (AdvancedControlElement c : g
										.getAdvancedControls()) {
									if (c != null && c.getName().equals("name")) {
										Text t = (Text) c.getControl();
										if (t != null) {
											if (!checkElementName(t.getText())) {
												setErrorMessage("Some elements have invalid names!");
												setPageComplete(false);
											}
										}
									}
								}

							}
						}
					}
				});

		Label sep = new Label(customSectionClient, SWT.SEPARATOR
				| SWT.HORIZONTAL);
		sep.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		group.addAdvancedControl(new AdvancedControlElement(sep,
				"lbl_seperator", null, null, 0));

		group.addRemoveButton(removeButton);
		groups.addLast(group);

		addButton = toolkit
				.createButton(customSectionClient,
						"                  Add new Element                  ",
						SWT.PUSH);
		addButton.setLayoutData(gd);
		addButton
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						addControlsGroup();
					}
				});

		customForm.reflow(true);
	}

	/**
	 * Sets the already created custom elements in the current page.
	 * 
	 * @param elems
	 *            a list of {@link ConfigCustomElement}
	 */
	void setLastCreatedCustomElements(ArrayList<ConfigCustomElement> elems) {

		while (!groups.isEmpty()) {
			groups.removeLast().dispose();
		}
		customForm.reflow(true);

		if (elems == null || elems.isEmpty())
			return;

		for (ConfigCustomElement e : elems) {
			addControlsGroup(e.getName(), e.getType(), e.isRepeatable(),
					e.isRequired());
		}
	}

	/**
	 * Returns the page container.
	 */
	@Override
	public Control getControl() {

		return container;
	}

	/**
	 * To be called on clicking the next button.
	 */
	@Override
	public void finishPage() {

		if (this.wizard instanceof TemplateEditorWizard)
			((TemplateEditorWizard) wizard).setCustomElements(groups);
		else
			((TemplateEditorWizardSelected) wizard).setCustomElements(groups);
	}

}