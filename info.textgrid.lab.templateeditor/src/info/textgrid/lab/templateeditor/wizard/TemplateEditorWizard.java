/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.wizard;

import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.templateeditor.config.ConfigCustomElement;
import info.textgrid.lab.templateeditor.utils.RepeatItemsGroup;

import java.util.ArrayList;
import java.util.LinkedList;

import org.eclipse.jface.dialogs.IPageChangingListener;
import org.eclipse.jface.dialogs.PageChangingEvent;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

/**
 * This is the class to create the Template editor as a wizard.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class TemplateEditorWizard extends Wizard implements
		IPageChangingListener {

	private PageOne pageOne = null;
	private PageTwo pageTwo = null;
	private WizardDialog dialog = null;
	private WizardUtils wizard_util = null;

	private LinkedList<RepeatItemsGroup> groups = null;

	private TextGridProject project = null;

	/**
	 * The default constructor which does the necessary initializations.
	 */
	public TemplateEditorWizard() {

		super();
		wizard_util = new WizardUtils();
		setWindowTitle("Metadata Template Editor");
		setNeedsProgressMonitor(true);
	}

	/**
	 * Displays the wizard.
	 * 
	 * @param activeShell
	 * @param project
	 */
	public void showDialog(Shell activeShell, TextGridProject project) {

		this.project = project;

		WizardDialog dialog = new WizardDialog(activeShell, this);
		dialog.setBlockOnOpen(false);
		dialog.open();

	}

	/**
	 * A call-back method that gets the selected {@link TextGridProject} from
	 * the first page and sets it in the {@link WizardUtils} instance.
	 * 
	 * The last selected and created optional and custom elements will be set in
	 * the second and third wizard pages.
	 * 
	 * @param project
	 *            the selected project
	 */
	void setSelectedProject(TextGridProject project) {

		wizard_util.setProject(project);
		pageTwo.setLastCreatedCustomElements(wizard_util
				.getLastSavedCustomElements());
	}

	/**
	 * A call-back method that gets the created custom elements from the third
	 * page.
	 * 
	 * @param groups
	 *            a list of the created {@link RepeatItemsGroup}
	 */
	void setCustomElements(LinkedList<RepeatItemsGroup> groups) {

		this.groups = groups;
	}

	/**
	 * Creates the pages and adds them to the wizard.
	 */
	@Override
	public void addPages() {

		super.addPages();

		pageOne = new PageOne(project);
		pageOne.init(this);
		addPage(pageOne);

		pageTwo = new PageTwo();
		pageTwo.init(this);
		addPage(pageTwo);

		IWizardContainer container = getContainer();
		if (container instanceof WizardDialog) {
			dialog = (WizardDialog) container;
			dialog.addPageChangingListener(this);
		}
	}

	/**
	 * This method will be called on clicking the finish button.
	 */
	@Override
	public boolean performFinish() {

		((ITemplateEditorPage) getContainer().getCurrentPage()).finishPage();

		if (groups == null)
			pageTwo.finishPage();

		ArrayList<ConfigCustomElement> lastElems = wizard_util
				.getLastSavedCustomElements();

		if (lastElems != null && !lastElems.isEmpty()) {

			ArrayList<String> notFoundElems = new ArrayList<String>(10), inputElems = new ArrayList<String>(
					10);

			for (RepeatItemsGroup g : groups) {
				for (Control c : g.getControls()) {
					if (c instanceof Text) {
						inputElems.add(((Text) c).getText());
					}
				}
			}

			String s = "";
			for (ConfigCustomElement c : lastElems) {
				s = c.getName();
				if (!inputElems.contains(s)) {
					notFoundElems.add(s);
				}
			}

			if (!notFoundElems.isEmpty()) {
				MessageBox warn = new MessageBox(PlatformUI.getWorkbench()
						.getDisplay().getActiveShell(), SWT.ICON_WARNING
						| SWT.YES | SWT.NO);
				String message = "The following fields has been renamed or removed:\n";

				for (int i = 0; i < notFoundElems.size(); ++i) {
					message += i + 1 + ") " + notFoundElems.get(i) + "\n";
				}

				message += "\nThis may cause data loss if the listed fields were filled with data!\n\n"
						+ "If you wanted to add new elements, please do it by clicking\n"
						+ "the \"Add New Element\" button instead of rewriting the available elements.\n\n"
						+ "Do you want to proceed?";
				warn.setMessage(message);

				if (warn.open() != SWT.YES) {
					return false;
				}
			}
		}

		wizard_util.createAndSaveData(groups);

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IPageChangingListener#handlePageChanging(org
	 * .eclipse.jface.dialogs.PageChangingEvent)
	 */
	@Override
	public void handlePageChanging(PageChangingEvent event) {

		if (event.getTargetPage() == getNextPage((IWizardPage) event
				.getCurrentPage())) {
			((ITemplateEditorPage) event.getCurrentPage()).finishPage();
		}
	}

}