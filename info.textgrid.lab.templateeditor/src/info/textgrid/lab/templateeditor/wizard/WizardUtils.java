/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author  Yahya Al-Hajj (alhajj@fh-worms.de)
 */
package info.textgrid.lab.templateeditor.wizard;

import static org.eclipse.core.runtime.IProgressMonitor.UNKNOWN;
import info.textgrid.lab.core.metadataeditor.MetaDataView;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.ProjectDoesNotExistException;
import info.textgrid.lab.core.model.ProjectFileException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectFile;
import info.textgrid.lab.core.model.util.StringToOM;
import info.textgrid.lab.templateeditor.Activator;
import info.textgrid.lab.templateeditor.config.ConfigCreator;
import info.textgrid.lab.templateeditor.config.ConfigCustomElement;
import info.textgrid.lab.templateeditor.utils.AdvancedControlElement;
import info.textgrid.lab.templateeditor.utils.RepeatItemsGroup;
import info.textgrid.lab.templateeditor.xsdcreator.XSDElement;
import info.textgrid.lab.templateeditor.xsdcreator.XSDSchema;
import info.textgrid.lab.templateeditor.xulmapping.TemplateElement;
import info.textgrid.namespaces.metadata.projectfile._2008_11_27.AppData;
import info.textgrid.namespaces.metadata.projectfile._2008_11_27.TgProjectFile;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * This class offers many tools for the Template editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class WizardUtils {

	private TextGridProject project = null;
	private TextGridProjectFile projectFile = null;

	private ArrayList<ConfigCustomElement> custom_elements = null;

	private XSDSchema custom_schema = null;

	private String custom_xul = "";

	private int index = 1;

	/**
	 * Sets the selected {@link TextGridProject}.
	 * 
	 * @param project
	 *            the selected project
	 */
	void setProject(final TextGridProject project) {

		try {
			this.project = TextGridProject.getProjectInstance(project.getId());
			projectFile = null;
			custom_elements = null;
			custom_schema = null;
			readConfig();
		} catch (RemoteException e) {
			Activator.handleError(e);
		} catch (CrudServiceException e) {
			Activator.handleError(e);
		} catch (ProjectDoesNotExistException e) {
			Activator.handleError(e);
		}
	}

	/**
	 * Reads the configuration from the project file of the selected project.
	 */
	private void readConfig() {

		if (project == null)
			return;

		Job read_job = new Job("Reading the projectfile...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();

				monitor.beginTask("Fetching the projectfile data...", UNKNOWN);

				projectFile = new TextGridProjectFile(project);

				try {
					TgProjectFile projectFileData = projectFile
							.getProjectFileData(false, true, monitor);

					OMElement[] appDataChildren = TextGridProjectFile.extractAppDataChildren(projectFileData);

					OMElement metaSection = null;
					if (appDataChildren != null && appDataChildren.length > 0)
						for (OMElement child : appDataChildren)
							if (child.getQName().equals(
									TextGridProjectFile.metadataSectionQName)) {
								metaSection = child;
								break;
							}

					if (metaSection != null) {
						OMElement config = metaSection
								.getFirstChildWithName(ConfigCreator.templateConfQname);

						if (config != null) {

							System.out.println(config);

							custom_elements = ConfigCreator
									.getConfigCustomElements(config);
						}
					}

					monitor.done();
					return Status.OK_STATUS;

				} catch (Exception e) {
					Activator
							.handleError(e,
									"Couldn't read the configuration from the project-file.");
				}
				return Status.CANCEL_STATUS;
			}
		};

		read_job.schedule();

		try {
			read_job.join();
		} catch (InterruptedException e) {
		}

	}

	/**
	 * Returns a list of the found custom elements in the
	 * {@link TextGridProjectFile}.
	 * 
	 * @return a list of {@link ConfigCustomElement}
	 */
	ArrayList<ConfigCustomElement> getLastSavedCustomElements() {
		return custom_elements;
	}

	/**
	 * Creates the necessary data and saves it in the project file.
	 * 
	 * @param templateObjects
	 * @param checkBoxes
	 * @param groups
	 */
	void createAndSaveData(final LinkedList<RepeatItemsGroup> groups) {

		ConfigCreator conf = ConfigCreator.getInstance();

		if (groups != null)
			createCustom(groups);

		OMElement schema = (custom_schema != null) ? custom_schema
				.createSchema() : null;
		OMElement config = conf.createConfig();

		System.out.println(custom_xul);
		System.out.println(schema);
		System.out.println(config);

		try {
			OMElement xul_om = StringToOM.getOMElement(custom_xul);
			addMetadataPartToProjectFile(xul_om, schema, config);
		} catch (UnsupportedEncodingException e) {
			Activator.handleError(e,
					"Couldn't set the data into the projectfile!");
		} catch (XMLStreamException e) {
			Activator.handleError(e,
					"Couldn't set the data into the projectfile!");
		} catch (FactoryConfigurationError e) {
			Activator.handleError(e,
					"Couldn't set the data into the projectfile!");
		}
	}

	/**
	 * Adds an Metadata part to the project file or modifies it. This Method
	 * will be called from
	 * {@link #createAndSaveData(Map, LinkedList, LinkedList)}
	 * 
	 * @param xul
	 * @param schema
	 * @param config
	 */
	private void addMetadataPartToProjectFile(final OMElement xul,
			final OMElement schema, final OMElement config) {

		if (projectFile == null)
			return;

		Job saveJob = new Job("Saving into the project-file...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					if (monitor == null)
						monitor = new NullProgressMonitor();

					monitor.beginTask("Saving into the project-file...", 100);

					monitor.worked(10);

					TgProjectFile projectFileData = projectFile
							.getProjectFileData(true, true, monitor);
					OMFactory omFactory = OMAbstractFactory.getOMFactory();

					OMElement appDataOM = TextGridProjectFile.extractAppDataXML(projectFileData);

					monitor.worked(40);

					OMElement metaDataOM = omFactory
							.createOMElement(TextGridProjectFile.metadataSectionQName);
					if (xul != null)
						metaDataOM.addChild(xul);
					if (schema != null)
						metaDataOM.addChild(schema);
					if (config != null)
						metaDataOM.addChild(config);

					metaDataOM
							.addAttribute(TextGridProjectFile.descriptionChanged);

					monitor.worked(10);

					final List<Element> newAppData = new LinkedList<Element>();
					Iterator<OMElement> childElements = appDataOM == null ? null
							: appDataOM.getChildElements();
					if (childElements != null)
						while (childElements.hasNext()) {
							final OMElement next = childElements.next();
							if (!metaDataOM.getLocalName().equals(next.getLocalName()))
								newAppData.add(toDOM(next));
						}
					newAppData.add(toDOM(metaDataOM));

					if (projectFileData.getAppData() == null)
						projectFileData.setAppData(new AppData());
					else
						projectFileData.getAppData().getAny().clear();
					projectFileData.getAppData().getAny().addAll(newAppData);

					projectFile.saveProjectFileData(projectFileData, monitor);

					new UIJob("Updating the metadata view...") {

						@Override
						public IStatus runInUIThread(final IProgressMonitor arg0) {
							MetaDataView metadataView = (MetaDataView) PlatformUI
									.getWorkbench().getActiveWorkbenchWindow()
									.getActivePage().findView(MetaDataView.ID);

							if (metadataView != null)
								try {
									metadataView.reloadProjectFile(project);
								} catch (CoreException e) {
									Activator.handleWarning(e);
									return Status.CANCEL_STATUS;
								}

							return Status.OK_STATUS;
						}
					}.schedule();

					monitor.done();
				} catch (ProjectFileException e) {
					Activator
							.handleError(
									e,
									"Couldn't write in the project-file.\nMaybe you don't have the right to edit the project file!");
					return Status.CANCEL_STATUS;
				} catch (CrudServiceException e) {
					Activator
							.handleError(
									e,
									"Couldn't write in the project-file.\nMaybe you don't have the right to edit the project file!");
					return Status.CANCEL_STATUS;
				}

				return Status.OK_STATUS;
			}
		};

		saveJob.setUser(true);
		saveJob.schedule();

	}

	/**
	 * Creates a custom element as a group of custom members.
	 * 
	 * @param groups
	 *            a list of the created {@link RepeatItemsGroup}
	 */
	private void createCustom(final LinkedList<RepeatItemsGroup> groups) {

		custom_schema = new XSDSchema(TextGridObject.CUSTOM_NAMESPACE);
		ConfigCreator conf = ConfigCreator.getInstance();

		createCustomHead();

		for (RepeatItemsGroup g : groups) {

			String name = "";
			String type = "";
			boolean repeatable = false;
			boolean required = false;

			for (AdvancedControlElement c : g.getAdvancedControls())
				if (c.getName().equals("name")
						&& c.getControl() instanceof Text) {
					Text t = (Text) c.getControl();
					if (t != null)
						name = t.getText();
				} else if (c.getName().equals("type")
						&& c.getControl() instanceof Combo) {
					Combo com = (Combo) c.getControl();
					if (com != null)
						type = com.getText();
				} else if (c.getName().equals("repeatable")
						&& c.getControl() instanceof Button) {
					Button b = (Button) c.getControl();
					if (b != null)
						repeatable = b.getSelection();
				} else if (c.getName().equals("required")
						&& c.getControl() instanceof Button) {
					Button b = (Button) c.getControl();
					if (b != null)
						required = b.getSelection();
				}

			createCustomMember(name, type, repeatable, required);
			custom_schema.addElement(new XSDElement(name, type, "1",
					repeatable ? "unbounded" : "1"));
			conf.addConfigElement(new ConfigCustomElement(name, type,
					repeatable, required));
		}

		createCustomEnd();
	}

	/**
	 * Creates the head of the custom element in the xul-description.
	 */
	private void createCustomHead() {
		custom_xul = "<tr xmlns:ct=\"http://logabit.com/xlayout/controls/1.0\" xmlns:tg=\"http://textgrid.de/xlayout/controls\"> <td width=\"100%\"> <tg:custom id=\"cstmSection\"> <table>";
	}

	/**
	 * Creates the end of the custom element in the xul-description.
	 */
	private void createCustomEnd() {
		custom_xul += "</table> </tg:custom> </td> </tr>";
	}

	/**
	 * Creates a member in the custom element.
	 * 
	 * @param name
	 *            name of the custom member
	 * @param repeatable
	 *            determines whether this member have to be repeatable or not
	 */
	private void createCustomMember(final String name, final String type,
			final boolean repeatable, final Boolean required) {

		IPath path = new Path(TemplateElement.XLAYOUT_DIR)
				.append((repeatable ? "CustomTextRepeatable.txt"
						: "CustomText.txt"));
		URL url = FileLocator.find(Activator.getDefault().getBundle(), path,
				null);

		InputStream in = null;
		try {
			url = FileLocator.resolve(url);
			in = url.openStream();
			BufferedReader reader = null;

			reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

			String s = null;

			while ((s = reader.readLine()) != null)
				custom_xul += s
						.replace("$name$", name)
						.replace("$id$", "cstmItem_" + name + "_" + index)
						.replace("$type$", type)
						.replace("$required$", required.toString())
						.replace("$optional$",
								(new Boolean(!required)).toString());

			++index;
		} catch (Exception e) {
			Activator.handleError(e);
			return;
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					Activator.handleWarning(e);
				}
		}
	}

	public static void main(final String[] args) {
		SchemaFactory f = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		f.setErrorHandler(new ErrorHandler() {

			@Override
			public void warning(final SAXParseException exception)
					throws SAXException {
				exception.printStackTrace();
			}

			@Override
			public void fatalError(final SAXParseException exception)
					throws SAXException {
				exception.printStackTrace();

			}

			@Override
			public void error(final SAXParseException exception) throws SAXException {
				exception.printStackTrace();

			}
		});

		try {
			StreamSource ss = new StreamSource(
					new ByteArrayInputStream(
							"<?xml version=\"1.0\" encoding=\"UTF-8\"?><xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"><xs:element name=\"nr\" type=\"xs:integer\"></xs:element></xs:schema>"
									.getBytes("UTF-8")));
			StreamSource ss2 = new StreamSource(new ByteArrayInputStream(
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?><nr>124</nr>"
							.getBytes("UTF-8")));
			Schema s = f.newSchema(ss);

			Validator v = s.newValidator();
			v.validate(ss2);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	/**
     * Converts a given OMElement to a DOM Element. (from Axis2's XMLUtils)
     *
     * @param element
     * @return Returns Element.
     */
    private static Element toDOM(final OMElement element) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
			element.serialize(baos);
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        return factory.newDocumentBuilder().parse(bais).getDocumentElement();
        } catch (XMLStreamException e) {
        	throw new IllegalArgumentException(e);
        } catch (SAXException e) {
        	throw new IllegalStateException("Internal error in data model conversion.", e);
		} catch (IOException e) {
			// Its an internal byte stream ...
        	throw new IllegalStateException("Internal error in data model conversion.", e);
		} catch (ParserConfigurationException e) {
        	throw new IllegalStateException("Internal error in data model conversion.", e);
		}
    }
}
