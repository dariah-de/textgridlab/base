/**
 * Project managers use the template editor to configure the project specific
 * aspects of the metadata editor.
 * 
 * <h4>Subpackages</h4>
 * 
 * <ul>
 * <li>{@link info.textgrid.lab.templateeditor.config}</li>
 * <li>{@link info.textgrid.lab.templateeditor.utils}</li>
 * <li>{@link info.textgrid.lab.templateeditor.wizard}</li>
 * <li>{@link info.textgrid.lab.templateeditor.xsdcreator}</li>
 * <li>{@link info.textgrid.lab.templateeditor.xulmapping}</li>
 * </ul>
 */
package info.textgrid.lab.templateeditor;

