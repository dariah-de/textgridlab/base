package info.textgrid.lab.welcome;

import java.util.Properties;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.common.CommandException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;
import org.eclipse.ui.statushandlers.StatusManager;

public class RunCommandAction implements IIntroAction {

	@Override
	public void run(IIntroSite site, Properties params) {
		ICommandService service = (ICommandService) site
				.getService(ICommandService.class);
		Command command = service.getCommand(params.getProperty("id"));
		try {
			command.executeWithChecks(new ExecutionEvent());
		} catch (CommandException e) {
			StatusManager.getManager().handle(
					new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(
							"Could not execute command {0} ({1})",
							command.getId(), e.getLocalizedMessage()), e), StatusManager.SHOW | StatusManager.LOG);
		}
	}

}
