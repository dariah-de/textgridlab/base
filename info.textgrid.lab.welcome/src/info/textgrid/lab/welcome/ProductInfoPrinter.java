package info.textgrid.lab.welcome;

import java.io.PrintWriter;
import java.util.Map;

import org.eclipse.equinox.p2.core.ProvisionException;
import org.eclipse.equinox.p2.engine.IProfile;
import org.eclipse.equinox.p2.metadata.IInstallableUnit;
import org.eclipse.ui.about.ISystemSummarySection;

import com.google.common.base.Joiner;

public class ProductInfoPrinter implements ISystemSummarySection {

	@Override
	public void write(PrintWriter writer) {
		VersionUtils versionInfo = Activator.getDefault().getVersionInfo();
		writer.println(versionInfo.getProductDesc(true));
		try {
			IProfile profile = versionInfo.getProfile();
			printProperties(writer, profile.getProperties(), "Profile Properties:");
			writer.println("Installed Root Items:");
			for (IInstallableUnit root : versionInfo.getRootIUs())
				printProperties(writer, root.getProperties(), VersionUtils.describeIU(root, true));
		} catch (ProvisionException e) {
			writer.println("No details: " + e.getMessage());
		}
	}

	private void printProperties(PrintWriter writer, Map<String, String> properties, String heading) {
		writer.println(heading);
		writer.println("  " + Joiner.on("\n  ").withKeyValueSeparator(":\t").join(properties));
		writer.println();
	}

}
