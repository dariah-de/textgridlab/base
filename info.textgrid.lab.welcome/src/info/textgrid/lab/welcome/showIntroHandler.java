package info.textgrid.lab.welcome;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

public class showIntroHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchPage wbPage = wb.getActiveWorkbenchWindow().getActivePage();
			if (wbPage == null) {
				wb.getActiveWorkbenchWindow().openPage(
						"info.textgrid.lab.welcome.RecherchePerspective", null); //$NON-NLS-1$
			}
			PlatformUI.getWorkbench().getIntroManager().showIntro(null, false);
		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					Messages.showIntroHandler_EM_CouldNotOpenIntro, e);
			Activator.getDefault().getLog().log(status);
		}
		return null;
	}
}
