package info.textgrid.lab.welcome;


import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.projectadmin.views.UserManagement2;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

public class showAdminPerspectiveHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// notice the project if possible
			TextGridProject tgp = null;
			ISelection selection = HandlerUtil.getCurrentSelection(event);
			if (selection instanceof IStructuredSelection) {
				Object obj = ((IStructuredSelection) selection).getFirstElement();
				if (obj instanceof TextGridProject) {
					tgp = (TextGridProject)obj;
				}
			}
			
			// first reset the perspective...
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchPage wbPage = wb.getActiveWorkbenchWindow().getActivePage();
			if (wbPage == null) 
				wb.getActiveWorkbenchWindow().openPage(
						"info.textgrid.lab.welcome.AdministrationPerspective", null); //$NON-NLS-1$
			wbPage.setPerspective(
					wb.getPerspectiveRegistry().findPerspectiveWithId(
							"info.textgrid.lab.welcome.AdministrationPerspective")); //$NON-NLS-1$
			wb.getActiveWorkbenchWindow().getActivePage().resetPerspective();
			
			// show it when necessary
			PlatformUI.getWorkbench().showPerspective(
					"info.textgrid.lab.welcome.AdministrationPerspective", //$NON-NLS-1$
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
			wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());
			
			// set project
			if (tgp != null) {
				IViewPart view = PlatformUI.getWorkbench().
									getActiveWorkbenchWindow().getActivePage().
									findView("info.textgrid.lab.projectadmin.views.UserManagement"); //$NON-NLS-1$
				((UserManagement2)view).setProject(tgp);
			}
			
		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    Messages.showAdminPerspectiveHandler_CouldNotOpenPerspective, e);
			Activator.getDefault().getLog().log(status);
		}
		return null;
	}
}
