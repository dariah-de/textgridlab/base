package info.textgrid.lab.welcome;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

public class showXMLEditorPerspectiveHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// first reset the perspective...
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchPage wbPage = wb.getActiveWorkbenchWindow().getActivePage();
			if (wbPage == null) 
				wb.getActiveWorkbenchWindow().openPage(
						"info.textgrid.lab.welcome.XMLEditorPerspective", null); //$NON-NLS-1$
			wbPage.setPerspective(
					wb.getPerspectiveRegistry().findPerspectiveWithId(
							"info.textgrid.lab.welcome.XMLEditorPerspective")); //$NON-NLS-1$
			wb.getActiveWorkbenchWindow().getActivePage().resetPerspective();

			// show it when necessary
			PlatformUI.getWorkbench().showPerspective(
					"info.textgrid.lab.welcome.XMLEditorPerspective", //$NON-NLS-1$
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
			wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());
		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    Messages.showXMLEditorPerspectiveHandler_EM_CouldNotOpenPerspecitve, e);
			Activator.getDefault().getLog().log(status);
		}
		return null;
	}
}
