package info.textgrid.lab.welcome;

//import info.textgrid.lab.linkeditor.rcp_linkeditor.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

@Deprecated
public class showLinkeditorPerspectiveHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			IWorkbench wb = PlatformUI.getWorkbench();
			PlatformUI.getWorkbench().showPerspective(
					info.textgrid.lab.welcome.LinkeditorPerspective.ID,
					wb.getActiveWorkbenchWindow());
			wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());
		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    Messages.showLinkeditorPerspectiveHandler_CouldNotOpenPerspective, e);
			Activator.getDefault().getLog().log(status);
		}
		return null;
	}
}
