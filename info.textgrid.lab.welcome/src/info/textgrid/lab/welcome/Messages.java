package info.textgrid.lab.welcome;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.welcome.messages"; //$NON-NLS-1$
	public static String IntroXHTMLContentProvider_ClickHereToChange;
	public static String IntroXHTMLContentProvider_ClickHereToLogin;
	public static String IntroXHTMLContentProvider_CouldNotDetermineRepository;
	public static String IntroXHTMLContentProvider_EM_CouldNotReloadScreen;
	public static String IntroXHTMLContentProvider_IM_DifferentInstance;
	public static String IntroXHTMLContentProvider_IM_VersionOutOfDate;
	public static String IntroXHTMLContentProvider_LoggedInAs;
	public static String IntroXHTMLContentProvider_OpenPreferencePage;
	public static String IntroXHTMLContentProvider_ReloadingWelcomeScreen;
	public static String OpenAuthPopupAction_Authenticating;
	public static String OpenAuthPopupAction_Configuring;
	public static String OpenAuthPopupAction_EM_CouldNotConnectToAuth;
	public static String OpenMailAction_MailBody;
	public static String OpenMailAction_MailSubject;
	public static String OpenMailAction_YourMessage;
	public static String RemoteStatusProvider_CheckDemand;
	public static String RemoteStatusProvider_EM_NoStatus;
	public static String RemoteStatusProvider_LookingForStatusInfo;
	public static String RemoteStatusProvider_StatusDocumentFailure;
	public static String RemoteStatusProvider_WM_FailedToFetchStatus;
	public static String showAdminPerspectiveHandler_CouldNotOpenPerspective;
	public static String showIntroHandler_EM_CouldNotOpenIntro;
	public static String showLinkeditorPerspectiveHandler_CouldNotOpenPerspective;
	public static String showRecherchePerspectiveHandler_EM_CouldNotOpenPerspective;
	public static String showXMLEditorPerspectiveHandler_EM_CouldNotOpenPerspecitve;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
