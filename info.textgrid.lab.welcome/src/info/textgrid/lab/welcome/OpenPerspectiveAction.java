package info.textgrid.lab.welcome;

import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.intro.config.IIntroAction;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.IIntroManager;
import java.util.Properties;

/** This class allows to open perspectives from the 
 * welcome screen by URL.
 * Use the following link in your (X)HTML files 
 * to open VEX DocumentPerspective:
 * 
 * 			http://org.eclipse.ui.intro/runAction? \
 * 			pluginId=info.textgrid.lab.welcome&amp; \
 * 			class=info.textgrid.lab.welcome.OpenPerspectiveAction&amp; \
 * 			perspective=net.sf.vex.editor.DocumentPerspective"> 
 *
 * We specified a new command in org.eclipse.ui.intro.configExtension
 * which reduces this unfriendlich URL to this.
 * 
 * 			http://org.eclipse.ui.intro/openPerspective? \
 * 			perspective=net.sf.vex.editor.DocumentPerspective
 *
 * @see		IIntroAction, org.eclipse.ui.intro.config, 
 * 			org.eclipse.ui.intro.configExtension
 * 
 * @author	Christian Simon <simon@ids-mannheim.de>
 */

public class OpenPerspectiveAction implements IIntroAction {


	public void run(IIntroSite site, Properties params) {
		
		String perspective = params.getProperty("perspective"); //$NON-NLS-1$
		
		IWorkbench workbench = PlatformUI.getWorkbench();
		IIntroManager manager = workbench.getIntroManager();
		
		//open the perspective in the current workbench window
		try {
		
			workbench.showPerspective(perspective, 
				workbench.getActiveWorkbenchWindow());
		
		} catch (Exception e) {e.printStackTrace();}
		
		//close the welcome screen
			//manager.closeIntro(manager.getIntro());
		manager.setIntroStandby(manager.getIntro(), true);

	}
}
