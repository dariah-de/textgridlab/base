package info.textgrid.lab.welcome;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.intro.config.IIntroContentProviderSite;
import org.eclipse.ui.intro.config.IIntroXHTMLContentProvider;
import org.eclipse.ui.progress.UIJob;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.base.Joiner;

/**
 * Inserts an XHTML fragment from a given URL as status notification etc. This
 * works only in XHTML welcome screens.
 * 
 * Clients insert a fragment of the form {@code
 *    <contentProvider 
 *    	id="http://www.textgrid.de/status.xml" 
 *    	class="info.textgrid.lab.welcome.RemoteStatusProvider" 
 *    	pluginId="info.textgrid.lab.welcome"></contentProvider>
 * } into their welcome screen and put a document that contains the status
 * message in the form described below to the URL the id points to.
 * 
 * <h4>Status document format</h4>
 * <p>
 * The status document must be a well-formed XML document that contains an XHTML
 * element with a {@code class} attribute that contains the value {@code status}
 * somewhere. The first element with a <em>status</em> class and all its
 * children will be copied to the welcome screen. The document itself may be a
 * valid XHTML document, but it may also be a simple fragment containing just,
 * e.g., an XHTML {@code p} element.
 * </p>
 * 
 * 
 * <h4>TextGridLab welcome screen config</h4> As defined in main.css, the
 * status.xml should always define the class {@code status} plus another class:
 * <dl>
 * <dt>error</dt>
 * <dd>when the repository is seriously broken</dd>
 * <dt>warning</dt>
 * <dd>when some functionality doesn't work</dd>
 * <dt>ok</dt>
 * <dd>when everything is fine -- this message may be hidden by the Lab</dd>
 * <dt>info</dt> for announcements, e.g. for a downtime. Examples:
 * 
 * <pre>
 * {@literal
 * 	<p xmlns="http://www.w3.org/1999/xhtml" class="status error">
 * 		The repository currently doesn't work. We'll be back by noon.
 *  </p>
 *  }
 * </pre>
 * 
 * or
 * 
 * <pre>
 *  {@literal
 * 	<div xmlns="http://www.w3.org/1999/xhtml" class="status info">
 * 		There will be scheduled downtimes at
 * 		<ul>
 * 			<li>Friday, 09:00-12:00</li>
 * 			<li>Monday, 12:00-14:00</li>
 * 		</ul>
 * </div>
 * }
 * 
 * </pre>
 */
public class RemoteStatusProvider implements IIntroXHTMLContentProvider {

	private static class StatusJob extends Job {

		private volatile Document document;
		private volatile IStatus lastStatus;
		private Element statusElement;
		private final String[] urls;

		public StatusJob(final String url) {
			super(NLS.bind(Messages.RemoteStatusProvider_LookingForStatusInfo, url));
			urls = url.split("\\s+"); //$NON-NLS-1$
		}

		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(Messages.RemoteStatusProvider_LookingForStatusInfo, Joiner.on(", ").join(urls)), urls.length * 10); //$NON-NLS-1$

			IStatus status = Status.CANCEL_STATUS;
			for (String url : urls) {
				status = tryURL(progress.newChild(10), url);
				if (status.isOK() || monitor.isCanceled())
					return status;
			}
			return status;
		}

		private IStatus tryURL(final IProgressMonitor monitor, final String url) {
			SubMonitor progress = SubMonitor.convert(monitor, NLS.bind(Messages.RemoteStatusProvider_LookingForStatusInfo, url), IProgressMonitor.UNKNOWN);
			try {
				final DocumentBuilder builder = getDocumentBuilder();
				if (progress.isCanceled())
					return Status.CANCEL_STATUS;
				
				final URL realURL = new URL(url);
				final URLConnection connection = realURL.openConnection();
				connection.setConnectTimeout(30 * 1000);
				
				document = builder.parse(connection.getInputStream());
				if (progress.isCanceled())
					return lastStatus = Status.CANCEL_STATUS;
				statusElement = document.getElementById("status"); //$NON-NLS-1$
				if (statusElement == null)
					statusElement = findStatusElement(document.getDocumentElement());
				if (statusElement == null)
					return handleError(new IllegalArgumentException(
							Messages.RemoteStatusProvider_EM_NoStatus));
			} catch (final MalformedURLException e) {
				return handleError(e);
			} catch (final IOException e) {
				return handleError(e);
			} catch (final ParserConfigurationException e) {
				return handleError(e);
			} catch (final SAXException e) {
				return handleError(e);
			} finally {
				progress.done();
			}
			if (progress.isCanceled())
				return lastStatus = Status.CANCEL_STATUS;
			else
				return lastStatus = Status.OK_STATUS;
		}

		/** finds the first element with a class attribute containing 'status' */
		private Element findStatusElement(final Element element) {
			if (element.hasAttribute("class") && element.getAttribute("class").contains("status")) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				return element;
			else {
				NodeList childNodes = element.getChildNodes();
				for (int i = 0; i < childNodes.getLength(); i++) {
					Node node = childNodes.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element candidate = findStatusElement((Element) node);
						if (candidate != null)
							return candidate;
					}

				}
			}
			return null;
		}

		/**
		 * prepares a DOM parser that doesn#t follow references to external DTDs
		 */
		private DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setExpandEntityReferences(false);
			factory.setValidating(false);
			factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			factory.setFeature("http://xml.org/sax/features/external-general-entities", false); //$NON-NLS-1$
			factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false); //$NON-NLS-1$
			factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false); //$NON-NLS-1$
			final DocumentBuilder builder = factory.newDocumentBuilder();
			return builder;
		}

		protected IStatus handleError(final Throwable e) {
			final Status status = new Status(IStatus.WARNING, Activator.PLUGIN_ID, MessageFormat.format(
					Messages.RemoteStatusProvider_WM_FailedToFetchStatus, e.getClass().getSimpleName(), e.getLocalizedMessage()), e);
			lastStatus = status;
			return status;
		}

		public synchronized Element getReturnedElement() {
			if (document == null || statusElement == null)
				return null;
			Element result =  (Element) statusElement.cloneNode(true);
			String[] classes = result.getAttribute("class").split("\\s+"); //$NON-NLS-1$ //$NON-NLS-2$
			Pattern LEVEL = Pattern.compile("info|error|warning"); //$NON-NLS-1$
			StringBuilder newCls = new StringBuilder("alert "); //$NON-NLS-1$
			for (String cls : classes) {
				if (LEVEL.matcher(cls).matches())
					newCls.append("alert-").append(cls); //$NON-NLS-1$
				else if (cls.equals("ok")) //$NON-NLS-1$
					newCls.append("alert-hidden"); //$NON-NLS-1$
				else
					newCls.append(cls);
				newCls.append(' ');
			}
			result.setAttribute("class", newCls.toString()); //$NON-NLS-1$
			result.setAttribute("role", "alert"); //$NON-NLS-1$ //$NON-NLS-2$
			return result;
		}

		public synchronized IStatus getLastStatus() {
			return lastStatus;
		}
	}

	private final Map<String, StatusJob> statusJobs = new HashMap<String, StatusJob>();
	private IIntroContentProviderSite site;
	private boolean disposed;

	/**
	 * Returns the status job for the given URL
	 * 
	 * @param url
	 * @return
	 */
	protected StatusJob getJob(final String url) {
		StatusJob job = statusJobs.get(url);
		if (job == null) {
			job = new StatusJob(url);
			statusJobs.put(url, job);
		}
		return job;
	}

	@Override
	public void init(final IIntroContentProviderSite site) {
		this.site = site;
	}

	@Override
	public void createContent(final String id, final PrintWriter out) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createContent(final String id, final Composite parent, final FormToolkit toolkit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		disposed = true;
		for (StatusJob job : statusJobs.values()) {
			job.cancel();
		}
	}

	@Override
	public void createContent(final String id, final Element parent) {
		final StatusJob statusJob = getJob(id);
		final Element element = statusJob.getReturnedElement();
		if (element == null && statusJob.getLastStatus() == null) {
			statusJob.addJobChangeListener(new JobChangeAdapter() {

				@Override
				public void done(final IJobChangeEvent event) {
					if (event.getResult().isOK() && !disposed)
						reflow();
				}
			});
			statusJob.schedule();
			// createElement(parent, "p",
			// "status pending").setTextContent("Checking for repository status ...");
		} else if (statusJob.getLastStatus() != null && statusJob.getLastStatus().isOK()) {
			parent.appendChild(parent.getOwnerDocument().importNode(element, true));
		} else {
			Element p = createElement(parent, "div", "status alert alert-error"); //$NON-NLS-1$ //$NON-NLS-2$
			p.setTextContent(Messages.RemoteStatusProvider_StatusDocumentFailure
					+ statusJob.getLastStatus().getMessage());
			Element statusLink = createElement(p, "a", null); //$NON-NLS-1$
			statusLink.setTextContent(Messages.RemoteStatusProvider_CheckDemand);
			statusLink.setAttribute("href", id); //$NON-NLS-1$
		}

	}

	/**
	 * Creates, appends and returns a simple element with some text content.
	 * 
	 * @param parent
	 *            The element the new element is to be appended to as child
	 *            node.
	 * @param name
	 *            The name of the new element
	 * @param htmlClass
	 *            The value of the HTML {@code class} attribute or null if not
	 *            desired
	 * @return The new element, already appended
	 */
	private Element createElement(final Element parent, final String name, final String htmlClass) {
		final Document document = parent.getOwnerDocument();
		final Element element = document.createElement(name);
		element.setAttribute("class", htmlClass); //$NON-NLS-1$
		parent.appendChild(element);
		return element;
	}

	protected void reflow() {
		if (site != null)
			new UIJob("") { //$NON-NLS-1$

				@Override
				public IStatus runInUIThread(final IProgressMonitor monitor) {
					if (!disposed)
						site.reflow(RemoteStatusProvider.this, true);
					return Status.OK_STATUS;
				}
			}.schedule();
	}

}
