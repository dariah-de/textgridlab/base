package info.textgrid.lab.welcome;

import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.Properties;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.middleware.confclient.ConfservClientConstants;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;
import org.eclipse.ui.progress.UIJob;



public class OpenAuthPopupAction implements IIntroAction {

	private String endpoint = null;
	private String loggingParam = ""; //$NON-NLS-1$
	private String rbacID;
	
	private final class AuthJob extends Job {
		
		private AuthJob(String name) {
			super(name);
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			//copied from info.textgrid.lab.navigator.objects.NavigatorRoot.refresh()

			try {
				endpoint = ConfClient.getInstance().getValue(ConfservClientConstants.TG_AUTH);
			} catch (OfflineException e) {
				OnlineStatus.netAccessFailed(Messages.OpenAuthPopupAction_EM_CouldNotConnectToAuth, e);	
			}

			//pop up the Authentication Dialog and get your ID
//			String rbacID = "ryytp55xmUIvmoXEMv663QWeYoj0t2s6COeX5mYi6nbGpUkxjb1R7pCXqUNqT49umfFs1I9JvI";
			
			return Status.OK_STATUS;
		}
	}
	
	private final class RbacUIJob extends UIJob {
		private RbacUIJob(String name) {
			super(name);
		}

		@Override
		public IStatus runInUIThread(IProgressMonitor monitor) {

			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;

			RBACSession.neverAsk(false);
			rbacID = RBACSession.getInstance().getSID(true);

			return Status.OK_STATUS;
		}
	}

	public void run(IIntroSite site, Properties params) {
						
		new AuthJob(Messages.OpenAuthPopupAction_Configuring).schedule();	
		
		new RbacUIJob(Messages.OpenAuthPopupAction_Authenticating).schedule();
		
		//at this point you'll have to decide for yourself what to do with your ID
		//the navigator would start the AuthnThread
		
		if(endpoint != null){
			System.out.println("Please tell me what to do from this point on!"); //$NON-NLS-1$
			//new AuthnThread(this, loggingParam, rbacID, endpoint).start();
		}
		
	}
}


