package info.textgrid.lab.welcome;

import java.util.Properties;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;

public class OpenMailAction implements IIntroAction {

	private static final String YOUR_MESSAGE = Messages.OpenMailAction_YourMessage;
	private static final String TG_SUPPORT_MAIL_ADDRESS = "textgrid-support@gwdg.de"; //$NON-NLS-1$
	private static final String TG_SUPPORT_MAIL_SUBJECT = Messages.OpenMailAction_MailSubject;
	private static String TG_SUPPORT_MAIL_BODY = Messages.OpenMailAction_MailBody;

	public void run(IIntroSite site, Properties params) {
		try {
			// Just for Linux/Unix OS only (TG-1981)
			if (System.getProperty("os.name").equals("Linux") //$NON-NLS-1$ //$NON-NLS-2$
					|| System.getProperty("os.name").equals("Unix")) { //$NON-NLS-1$ //$NON-NLS-2$
				email2(TG_SUPPORT_MAIL_SUBJECT, TG_SUPPORT_MAIL_BODY);
			} else
				email(TG_SUPPORT_MAIL_SUBJECT, TG_SUPPORT_MAIL_BODY);
		} catch (Exception e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					e.getMessage(), e);
			Activator.getDefault().getLog().log(status);
		}
	}

	/*
	 * This is working for WinOS and MacOS but not for Linux/Unix
	 */
	public void email(String subject, String body) throws Exception {
		String cmd = "open mailto:" + TG_SUPPORT_MAIL_ADDRESS; //$NON-NLS-1$
		cmd += "?subject=" + urlEncode(subject); //$NON-NLS-1$
		cmd += "&body=" + urlEncode(body); //$NON-NLS-1$
		Runtime.getRuntime().exec(cmd);
	}

	/*
	 * This is working for Linux/Unix
	 */
	public void email2(String subject, String body) throws Exception {
		String to = TG_SUPPORT_MAIL_ADDRESS;
		String[] args = new String[] { "xdg-email", to, "--subject", //$NON-NLS-1$ //$NON-NLS-2$
				urlEncode(subject), "--body", YOUR_MESSAGE }; //$NON-NLS-1$
		Process p = Runtime.getRuntime().exec(args);
		p.waitFor();

		if (p.exitValue() != 0) {
			IStatus status = new Status(IStatus.ERROR, String.valueOf(p
					.exitValue()), this.getClass().getName() + "\n" //$NON-NLS-1$
					+ "xdg-email exit code " + "(" //$NON-NLS-1$ //$NON-NLS-2$
					+ String.valueOf(p.exitValue()) + ")" + ": " + "<" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ getErrorCodeMessage(p.exitValue()) + ">"); //$NON-NLS-1$
			Activator.getDefault().getLog().log(status);
		}

		// Program.launch("xdg-open" + cmd);
		// Runtime.getRuntime().exec("thunderbird -compose to=to,subject=subject,body=body");
	}

	private static String urlEncode(String s) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (Character.isLetterOrDigit(ch)) {
				sb.append(ch);
			} else {
				sb.append(String.format("%%%02X", (int) ch)); //$NON-NLS-1$
			}
		}
		return sb.toString();
	}

	/*
	 * xdg-email exit code info
	 * http://portland.freedesktop.org/xdg-utils-1.1.0-rc1/scripts/xdg-email
	 * http://portland.freedesktop.org/xdg-utils-1.0/xdg-email.html
	 */
	private String getErrorCodeMessage(int errCode) {
		String errMessage = ""; //$NON-NLS-1$
		switch (errCode) {
		case 1:
			errMessage = "Error in command line syntax.";
			break;
		case 2:
			errMessage = "One of the files passed on the command line did not exist.";
			break;
		case 3:
			errMessage = "A required tool could not be found.";
			break;
		case 4:
			errMessage = "The action failed.";
			break;
		case 5:
			errMessage = "No permission to read one of the files passed on the command line.";
			break;

		default:
			break;
		}

		return errMessage;
	}
}
