
package info.textgrid.lab.welcome;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;


/**
 * Implements the Document perspective.
 */
public class XMLEditorPerspective implements IPerspectiveFactory {
    
    public void createInitialLayout(IPageLayout layout) {
      	IWorkbench wb = PlatformUI.getWorkbench();
    	wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());
    	
    	defineActions(layout);
    	defineLayout(layout);
    }
    
    /**
     * Defines the initial actions for a page.  
     */
    public void defineActions(IPageLayout layout) {
    	// Add "new wizards".
//    	layout.addNewWizardShortcut("net.sf.vex.editor.NewDocumentWizard");//$NON-NLS-1$
//    	layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.folder");//$NON-NLS-1$
//    	layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.file");//$NON-NLS-1$
//
//    	layout.addShowViewShortcut(IPageLayout.ID_RES_NAV);
//    	layout.addShowViewShortcut(IPageLayout.ID_OUTLINE);
//    	layout.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);

//    	layout.addActionSet(IPageLayout.ID_NAVIGATE_ACTION_SET);
    }
    
    /**
     * Defines the initial layout for a page.  
     */
    public void defineLayout(IPageLayout layout) {

    	String editorArea = layout.getEditorArea();

    	IFolderLayout topLeft = 
    		layout.createFolder("topLeft", IPageLayout.LEFT, (float)0.25, editorArea);//$NON-NLS-1$
		topLeft.addView("info.textgrid.lab.navigator.view"); //$NON-NLS-1$
       	topLeft.addView("info.textgrid.lab.core.metadataeditor.view"); //$NON-NLS-1$
       	
 
    	IFolderLayout topRight= 
    		layout.createFolder("topRight", IPageLayout.RIGHT, (float)0.7, editorArea);//$NON-NLS-1$
       	topRight.addView(IPageLayout.ID_OUTLINE);

    	IFolderLayout bottomRight= 
    		layout.createFolder("bottomRight", IPageLayout.BOTTOM, (float)0.5, "topRight");//$NON-NLS-1$ //$NON-NLS-2$
       	bottomRight.addView(IPageLayout.ID_PROP_SHEET);

    	layout.setEditorAreaVisible(true);     
    	
    	//IFolderLayout topRight = 
        //	layout.createFolder("topRight", IPageLayout.RIGHT, (float)0.75, editorArea);//$NON-NLS-1$
        //topRight.addView(IPageLayout.ID_OUTLINE);
        
        //IFolderLayout bottomRight = 
        //	layout.createFolder("bottomRight", IPageLayout.BOTTOM, (float)0.5, "topRight");//$NON-NLS-1$ //$NON-NLS-2$
       // bottomRight.addView(IPageLayout.ID_PROP_SHEET);
     //   bottomRight.addView("info.textgrid.lab.core.metadataeditor.view");
        
        
    }

}
