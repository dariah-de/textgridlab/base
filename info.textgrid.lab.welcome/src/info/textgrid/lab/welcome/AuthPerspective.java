package info.textgrid.lab.welcome;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class AuthPerspective implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		// TODO Auto-generated method stub
		
		 // Get the editor area.
		 String editorArea = layout.getEditorArea();

		 // Bottom right: Task List view
		// layout.addView("org.eclipse.ui.internal.introview", IPageLayout.TOP, 
		//		 0.20f, editorArea);

		 
		 // Bottom right: Task List view
		 layout.addView("info.textgrid.lab.authn.views.AuthNView",  //$NON-NLS-1$
				 IPageLayout.TOP, 0.95f, editorArea);
	}

}
