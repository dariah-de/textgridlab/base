package info.textgrid.lab.welcome;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

@Deprecated
public class LinkeditorPerspective implements IPerspectiveFactory {
	// important: if you want to change the id, then you maybe have to modify
	// the OpenHandler.
	public static String ID = "info.textgrid.lab.welcome.LinkeditorPerspective"; //$NON-NLS-1$
	private IPageLayout factory;

	public void createInitialLayout(IPageLayout layout) {
		this.factory = layout;
		addViews();
	}

	private void addViews() {
		// Creates the overall folder layout.
		// Note that each new Folder uses a percentage of the remaining
		// EditorArea.

		// IFolderLayout bottom =
		// factory.createFolder(
		// "bottomRight", //NON-NLS-1
		// IPageLayout.BOTTOM,
		// 0.75f,
		// IPageLayout.ID_EDITOR_AREA);
		// bottom.addView("info.textgrid.lab.linkeditor.rcp_linkeditor.textview");
		// //TextView
		// bottom.addPlaceholder(IConsoleConstants.ID_CONSOLE_VIEW);

		String editorArea = factory.getEditorArea();
		factory.setEditorAreaVisible(true);

		// IFolderLayout topRight =
		// factory.createFolder(
		// "topRight", //NON-NLS-1
		// IPageLayout.RIGHT,
		// 0.5f,
		// editorArea);
		// topRight.addView("edu.uky.mip.views.ImageView"); //ImageView
		// net.sf.vex.editor
		/*
		 * IFolderLayout topLeft = factory.createFolder( "topLeft", //NON-NLS-1
		 * IPageLayout.RIGHT, 0.3f, IPageLayout.ID_EDITOR_AREA);
		 * topLeft.addView("edu.uky.mip.component.views.ThumbView"); //ThumbView
		 */

		IFolderLayout topLeft = factory.createFolder("topleft", // NON-NLS-1 //$NON-NLS-1$
				IPageLayout.LEFT, 0.2f, editorArea);
		topLeft.addView("info.textgrid.lab.navigator.view"); // NaviView //$NON-NLS-1$

		IFolderLayout topRight = factory.createFolder("topright", // NON-NLS-1 //$NON-NLS-1$
				IPageLayout.TOP, 0.5f, editorArea);

		// topRight.addView("edu.uky.mip.views.ImageView");
		topRight.addPlaceholder("edu.uky.mip.views.ImageView:*"); //$NON-NLS-1$

		// factory.addStandaloneView("edu.uky.mip.views.ImageView", true,
		// IPageLayout.TOP, 0.5f, editorArea);
		//		
		// factory.addPlaceholder("edu.uky.mip.views.ImageView:*",
		// IPageLayout.TOP, 0.5f, editorArea);

		// topLeft.addView("edu.uky.mip.views.ImageView");
		// //ImageViewtopRight.addView("edu.uky.mip.views.ImageView");
		// //ImageView
		// ThumbView (deactivated title, so it's not moveable or sizeable -
		// TG-238)

		factory.addStandaloneView("edu.uky.mip.component.views.ThumbView", //$NON-NLS-1$
				false, IPageLayout.BOTTOM, 0.8f, "topleft"); //$NON-NLS-1$
	}
}
