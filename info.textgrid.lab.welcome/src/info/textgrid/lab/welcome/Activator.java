package info.textgrid.lab.welcome;

import info.textgrid.lab.core.browserfix.BrowserFixPlugin;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.welcome"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	private BundleContext context;
	
	private VersionUtils versionUtils;

	/**
	 * The constructor
	 */
	public Activator() {
		BrowserFixPlugin bfp = new BrowserFixPlugin();
		bfp.earlyStartup();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		this.context = context;
		plugin = this;
		new Job("Logging Version Info") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				getVersionInfo().logProductInfo(monitor);
				return Status.OK_STATUS;
			}
			
		}.schedule();
	}
	
	public BundleContext getContext() {
		return context;
	}

	public VersionUtils getVersionInfo() {
		if (versionUtils == null)
			versionUtils = new VersionUtils(context);
		return versionUtils;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		if (versionUtils != null)
			versionUtils.stop();
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

}
