package info.textgrid.lab.welcome;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IBundleGroup;
import org.eclipse.core.runtime.IBundleGroupProvider;
import org.eclipse.core.runtime.IProduct;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.equinox.p2.core.IProvisioningAgent;
import org.eclipse.equinox.p2.core.IProvisioningAgentProvider;
import org.eclipse.equinox.p2.core.ProvisionException;
import org.eclipse.equinox.p2.engine.IProfile;
import org.eclipse.equinox.p2.engine.IProfileRegistry;
import org.eclipse.equinox.p2.engine.query.UserVisibleRootQuery;
import org.eclipse.equinox.p2.metadata.IInstallableUnit;
import org.eclipse.equinox.p2.query.IQuery;
import org.eclipse.equinox.p2.query.IQueryResult;
import org.eclipse.equinox.p2.query.QueryUtil;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;

/**
 * Various functions to retrieve version information about the installed
 * TextGridLab.
 */
public class VersionUtils {

	public static String PRODUCT_P2_ID = "info.textgrid.lab.base.product";

	private Version version = null;
	private Version baseVersion;
	private IProvisioningAgent provisioningAgent;

	private BundleContext context;


	protected VersionUtils(BundleContext context) {
		this.context = context;
	}

	/**
	 * Retrieves the full product name, without version.
	 * 
	 * @return e.g., "TextGridLab Nightly"
	 */
	public String getProductName() {
		final IProduct product = Platform.getProduct();
		final String productName = product.getName().trim();
		return productName;
	}

	/**
	 * Returns the name of the special version line, e.g., 'Nightly' for the
	 * nightly builds.
	 * 
	 * The return value will be an empty string for released versions, and
	 * simply the product name if the current product is not TextGridLab.
	 */
	public String getSpecialVersionName() {
		final String productName = getProductName();
		if (productName.startsWith("TextGridLab")) //$NON-NLS-1$
			return productName.substring(11).trim();
		else
			return productName;
	}

	/**
	 * Returns the version of the TextGridLab's base feature.
	 */
	public Version getBaseVersion() {
		if (baseVersion == null) {
			for (final IBundleGroupProvider provider : Platform.getBundleGroupProviders()) {
				System.out.println(provider.getName());
				for (final IBundleGroup feature : provider.getBundleGroups()) {
					if (feature.getIdentifier().equals("info.textgrid.lab.feature.base")) { //$NON-NLS-1$
						baseVersion = Version.parseVersion(feature.getVersion());
						StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID,
								MessageFormat.format("{0} Version {1}", getProductName(), version)));
						return baseVersion;
					}
				}
			}
			return Version.emptyVersion;
		}
		return baseVersion;
	}

	/**
	 * Returns the version of the TextGridLab product. Falls back to
	 * {@link #getBaseVersion()}.
	 */
	public Version getProductVersion() {
		if (version == null) {
			version = Version.emptyVersion;
			try {
				final IInstallableUnit productIU = getProductIU();
				version = Version.parseVersion(productIU.getVersion().toString());

			} catch (final ProvisionException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
				version = getBaseVersion();
			}
		}
		return version;
	}

	/**
	 * Determines the IU that represents the TextGridLab product.
	 * 
	 * @throws ProvisionException
	 *             if no profile or not found, e.g., because we are running from
	 *             Eclipse.
	 */
	private IInstallableUnit getProductIU() throws ProvisionException {
		final IProfile profile = getProfile();
		final IQuery<IInstallableUnit> productQuery = QueryUtil.createLatestQuery(QueryUtil.createIUQuery(PRODUCT_P2_ID));
		final IQueryResult<IInstallableUnit> productIUs = profile.available(productQuery, new NullProgressMonitor());
		if (productIUs.isEmpty())
			throw new ProvisionException("Product IU not found – running from within Eclipse?");
		final IInstallableUnit productIU = productIUs.iterator().next();
		return productIU;
	}

	/**
	 * Returns the p2 profile for the currently running application.
	 * 
	 * @throws ProvisionException
	 *             if not available, e.g., because we are running from Eclipse.
	 */
	IProfile getProfile() throws ProvisionException {
		final IProvisioningAgent agent = getProvisioningAgent();
		final IProfileRegistry profileRegistry = (IProfileRegistry) agent.getService(IProfileRegistry.SERVICE_NAME);
		final IProfile profile = profileRegistry.getProfile(IProfileRegistry.SELF);
		if (profile == null)
			throw new ProvisionException(new Status(IStatus.WARNING, Activator.PLUGIN_ID,
					"Own installation profile not found (running from Eclipse?)"));
		return profile;
	}

	/**
	 * Returns a provisioning agent. Afterwards, someone should run
	 * {@link #stop()}.
	 */
	private IProvisioningAgent getProvisioningAgent() throws ProvisionException {
		if (this.provisioningAgent == null) {
			final ServiceReference<?> sr = 
					context.getServiceReference(IProvisioningAgentProvider.SERVICE_NAME);
			final IProvisioningAgentProvider agentProvider = (IProvisioningAgentProvider) Activator.getDefault().getContext()
					.getService(sr);
			provisioningAgent = agentProvider.createAgent(null);
		}
		return provisioningAgent;
	}
	
	public String getProductDesc(final boolean full) {
		return MessageFormat.format("{0} {1}", getProductName(), getVersionString(full));
	}
	
	IQueryResult<IInstallableUnit> getRootIUs() throws ProvisionException {
		IProfile profile = getProfile();
		return profile.available(new UserVisibleRootQuery(), new NullProgressMonitor());
	}
	
	protected IStatus logProductInfo(IProgressMonitor monitor) {
		MultiStatus multiStatus = new MultiStatus(Activator.PLUGIN_ID, 0, getProductDesc(true), null);
		try {
			IQueryResult<IInstallableUnit> roots;
			roots = getRootIUs();
			for (IInstallableUnit root: roots)
				multiStatus.add(new Status(IStatus.INFO, root.getId(), describeIU(root, false)));
		} catch (ProvisionException e) {
			multiStatus.add(new Status(IStatus.WARNING, Activator.PLUGIN_ID, "Cannot list root IUs", e));
		}
		StatusManager.getManager().handle(multiStatus, StatusManager.LOG);
		return multiStatus;
	}

	/**
	 * Describes the given installable unit to a one-line string.
	 * @param iu the installable unit
	 * @param withId if <code>true</code>, include the IU's ID.
	 */
	protected static String describeIU(IInstallableUnit iu, boolean withId) {
		String nl = Platform.getNL();
		return MessageFormat.format(withId? "{0} {1} ({3}; by {2})": "{0} {1} ({2})",
				iu.getProperty(IInstallableUnit.PROP_NAME, nl),
				iu.getVersion(),
				iu.getProperty("org.eclipse.equinox.p2.provider", nl),
				iu.getId());
	}

	/**
	 * Returns a human-readable string representing the TextGridLab version. By
	 * default, this will return a two- (3.0) or three-part (3.0.1) version
	 * number, depending on whether the third part is 0.
	 * 
	 * @param full
	 *            if <code>true</code>, returns the full version string
	 *            major.minor.micro.timestamp instead.
	 * @return
	 */
	public String getVersionString(final boolean full) {
		final Version version = getProductVersion();
		if (full)
			return version.toString();
		else if (version.getMicro() == 0)
			return MessageFormat.format("{0}.{1}", version.getMajor(), version.getMinor());
		else
			return MessageFormat.format("{0}.{1}.{2}", version.getMajor(), version.getMinor(), version.getMicro());
	}

	/**
	 * Deallocates the resources aquired by this class.
	 */
	protected void stop() {
		if (provisioningAgent != null) {
			provisioningAgent.stop();
			provisioningAgent = null;
		}
	}

	@Override
	protected void finalize() throws Throwable {
		stop();
		super.finalize();
	}

}
