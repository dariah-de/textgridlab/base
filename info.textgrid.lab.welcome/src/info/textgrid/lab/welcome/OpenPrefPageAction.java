package info.textgrid.lab.welcome;

import java.util.Properties;

import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;

public class OpenPrefPageAction implements IIntroAction {

	public void run(IIntroSite site, Properties params) {
		PreferenceDialog preferenceDialog = PreferencesUtil.createPreferenceDialogOn(site.getShell(), params.getProperty("page"), //$NON-NLS-1$
				null, null);
		preferenceDialog.open();
	}

}
