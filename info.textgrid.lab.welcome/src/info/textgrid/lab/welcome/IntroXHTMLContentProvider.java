package info.textgrid.lab.welcome;

import info.textgrid.lab.authn.AuthBrowser;
import info.textgrid.lab.authn.AuthBrowser.ISIDChangedListener;
import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.client.ConfClient;

import java.io.PrintWriter;
import java.text.MessageFormat;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.intro.config.IIntroContentProviderSite;
import org.eclipse.ui.intro.config.IIntroXHTMLContentProvider;
import org.eclipse.ui.progress.UIJob;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/** more information about welcome-screen content-provider:
 *  http://www.eclipse.org/eclipse/platform-ua/proposals/welcome/intro_xhtml/index.htm
 */
public class IntroXHTMLContentProvider implements IIntroXHTMLContentProvider {

	private IIntroContentProviderSite site;
	private IntroXHTMLContentProvider instance;
	private Boolean apiChanged = false;
	private VersionUtils version = Activator.getDefault().getVersionInfo();
	
	public void createContent(String id, PrintWriter writer) {
	}

	public void createContent(String id, Composite arg1, FormToolkit arg2) {
	}

	public void dispose() {	
		AuthBrowser.removeSIDChangedListener(sidChangedListener);
		ConfClient.removeApiChangedListener(apiChangedListener);
	}

	ISIDChangedListener sidChangedListener = new ISIDChangedListener() {
		public void sidChanged(final String newSID, final String newEPPN) {
			reflowSite();
		}
	};
	
	Listener apiChangedListener = new Listener() {
		public void handleEvent(Event e) {
			if(! apiChanged) {
				apiChanged = true;
				reflowSite();
			}
		}
	};
	
	/**
	 * Reload the welcome ccreen, either from display thread, or with an UIJob
	 */
	private void reflowSite() {
		if(Display.getCurrent() == null){
			UIJob uij = new UIJob (Messages.IntroXHTMLContentProvider_ReloadingWelcomeScreen) {
				@Override
				public IStatus runInUIThread(IProgressMonitor arg0) {
					site.reflow(instance, true);
					return new Status(IStatus.OK,Activator.PLUGIN_ID,
						"reloaded welcome screen"); //$NON-NLS-1$
				}				
			};
			uij.schedule();
			try {
				uij.join();
			} catch (InterruptedException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						Messages.IntroXHTMLContentProvider_EM_CouldNotReloadScreen, e);
				Activator.getDefault().getLog().log(status);
			}
		} else {
			site.reflow(instance, true);
		}
	}

	public void init(IIntroContentProviderSite arg0) {
		this.site = arg0;
		this.instance = this;
		AuthBrowser.addSIDChangedListener(sidChangedListener);
		ConfClient.addApiChangedListener(apiChangedListener);		
	}


    public void createContent(String id, Element parent) {      
        if(id.equals("apiChanged"))  //$NON-NLS-1$
        	addApiStatus(parent);
        else if(id.equals("loginStatus"))  //$NON-NLS-1$
        	addLoginStatusHTML(parent);
        else if(id.equals("revisionID"))  //$NON-NLS-1$
        	addRevisionID(parent);
		else if (id.equals("confInstance")) //$NON-NLS-1$
			addConfStatus(parent);
		else if (id.equals("shortVersion"))
			addText(parent, version.getVersionString(false), true);
		else if (id.equals("specialVersion"))
			addText(parent, version.getSpecialVersionName(), true);
		else if (id.equals("nightlyMode")) {
			addNightlyMode(parent);
		}
    }
    
    /**
     * Display message if middleware-API changed
     * @param parent
     */
    private void addApiStatus(Element parent) {
    	if(apiChanged) {
    		Document dom = parent.getOwnerDocument();
    		Element para = dom.createElement("div"); //$NON-NLS-1$
				para.setAttribute("class", "alert alert-error"); //$NON-NLS-1$ //$NON-NLS-2$
				para.setAttribute("role", "alert"); //$NON-NLS-1$ //$NON-NLS-2$
    		para
					.appendChild(dom
							.createTextNode(Messages.IntroXHTMLContentProvider_IM_VersionOutOfDate));
    		parent.appendChild(para);
    	}
    }
    
	private void addConfStatus(final Element parent) {
		String currentEndpoint = ConfClient.computePreferredEndpoint();
		if (!ConfClient.DEFAULT_ENDPOINT.equals(currentEndpoint)) {
		// TG-754 / #8175: Don't instantiate ConfClient here
				Document document = parent.getOwnerDocument();
				Element div = document.createElement("div"); //$NON-NLS-1$
			div.setAttribute("class", "alert alert-info"); //$NON-NLS-1$ //$NON-NLS-2$
			div.setAttribute("role", "alert"); //$NON-NLS-1$ //$NON-NLS-2$
				div.appendChild(document.createTextNode(NLS.bind(
						Messages.IntroXHTMLContentProvider_IM_DifferentInstance,
					currentEndpoint, ConfClient.DEFAULT_ENDPOINT)));
			div.appendChild(document.createTextNode(" ")); //$NON-NLS-1$
				Element a = document.createElement("a"); //$NON-NLS-1$
				a.setAttribute("href", "http://org.eclipse.ui.intro/openPrefPage?page=info.textgrid.lab.conf.ConfservPrefPage"); //$NON-NLS-1$ //$NON-NLS-2$
				a.setAttribute("title", Messages.IntroXHTMLContentProvider_OpenPreferencePage); //$NON-NLS-1$
				a.setTextContent(Messages.IntroXHTMLContentProvider_ClickHereToChange);
				div.appendChild(a);
				parent.appendChild(div);
			}
	}

    /**
     * Adds html for login-status (Username) to Welcome Screen
     * @param parent
     */
    private void addLoginStatusHTML(Element parent) {
    	Document dom = parent.getOwnerDocument();
        if(RBACSession.getInstance().getEPPN() == "") { //$NON-NLS-1$
        	Element para = dom.createElement("a"); //$NON-NLS-1$
        	para.setAttribute("class", "link"); //$NON-NLS-1$ //$NON-NLS-2$
        	para.setAttribute("href",  //$NON-NLS-1$
              	"http://org.eclipse.ui.intro/runAction?pluginId=info.textgrid.lab.welcome&class=info.textgrid.lab.welcome.OpenAuthPopupAction"); //$NON-NLS-1$
        	para.setAttribute("title", Messages.IntroXHTMLContentProvider_ClickHereToLogin); //$NON-NLS-1$
        	para.appendChild(dom.createTextNode("Login")); //$NON-NLS-1$
        	parent.appendChild(para);
        } else {
        	Element para = dom.createElement("div");        	 //$NON-NLS-1$
        	para.appendChild(dom.createTextNode(Messages.IntroXHTMLContentProvider_LoggedInAs +  RBACSession.getInstance().getEPPN()));
        	para.setAttribute("class", "paragraph_style"); //$NON-NLS-1$ //$NON-NLS-2$
        	para.setAttribute("style", "top:130px;"); //$NON-NLS-1$ //$NON-NLS-2$
        	parent.appendChild(para);       	
        }
    	
    }
    
    /**
     * Adds Revision-ID to Welcome Screen
     * @param parent
     */
    private void addRevisionID(Element parent) {
		// parent.setTextContent("TextGridLab Version 2.2.0, Revision: " + Platform.getProduct().getDefiningBundle().getVersion().getQualifier()); //$NON-NLS-1$
		Document document = parent.getOwnerDocument();
		Element a = document.createElement("a");
		a.setAttribute("href", "http://org.eclipse.ui.intro/runCommand?id=org.eclipse.ui.help.installationDialog");
		a.setTextContent(MessageFormat.format("{0} Version {1}", version.getProductName(), version.getVersionString(true)));
		parent.appendChild(a);
    }
    private void addText(Element parent, String content, boolean inline) {
    	if (inline)
            parent.setAttribute("style", "display:inline;");
    	if (content.isEmpty())
    		content = " ";
    	parent.setTextContent(content);
    }
    
    private void addNightlyMode(Element parent) {
    	if (!version.getSpecialVersionName().isEmpty()) {
    		Element script = parent.getOwnerDocument().createElement("script");
    		script.setTextContent("document.getElementsByTagName('header')[0].className += ' nightly';");
    		parent.appendChild(script);
    	}
    }
    
    // org.eclipse.ui.help.aboutAction
    // org.eclipse.ui.help.installationDialog
    // info.textgrid.lab.feedback.ui.reportBug2

}
