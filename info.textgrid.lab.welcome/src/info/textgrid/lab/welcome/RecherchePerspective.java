
package info.textgrid.lab.welcome;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;


/**
 * Implements the Document perspective.
 */
public class RecherchePerspective implements IPerspectiveFactory {
    
    public void createInitialLayout(IPageLayout layout) {
    	defineLayout(layout);
    }
    
    /**
     * Defines the initial layout for a page.  
     */
    public void defineLayout(IPageLayout layout) {

    	String editorArea = layout.getEditorArea();

    	IFolderLayout topLeft = 
    		layout.createFolder("topLeft", IPageLayout.LEFT, (float)0.5, editorArea);//$NON-NLS-1$
       	topLeft.addView("info.textgrid.lab.search.views.SearchView"); //$NON-NLS-1$
 
    	IFolderLayout topRight= 
    		layout.createFolder("topRight", IPageLayout.RIGHT, (float)0.5, editorArea);//$NON-NLS-1$
       	topRight.addView("info.textgrid.lab.search.views.ResultView"); //$NON-NLS-1$
    	
    	layout.setEditorAreaVisible(false);     
    }

}
