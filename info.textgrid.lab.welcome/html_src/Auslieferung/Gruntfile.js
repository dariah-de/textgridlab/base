module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    less: {
		development: {
			options: {
				// cleancss: true,
				sourceMap: true,
        sourceMapFilename: 'dist/css/app-bootstrap.css.map',
        sourceMapURL: 'app-bootstrap.css.map',
        sourceMapBasepath: 'css/',
			},
			files: {
				"dist/css/app-bootstrap.css": "src/less/app-bootstrap.less"
			}
		}
	},
  autoprefixer: {
        options: {
          browsers: ['last 2 version'],
          map: {
              prev: 'dist/css/app-bootstrap.css.map',
              sourceContent: true,
          }
        },
        development: {
            src: "dist/css/app-bootstrap.css",
            dest: "dist/css/app-bootstrap.css"
        }

  },
  imagemin: {                          // Task
    dynamic: {                         // Another target
      files: [{
        expand: true,                  // Enable dynamic expansion
        cwd: 'src/',                   // Src matches are relative to this path
        src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
        dest: 'dist/'                  // Destination path prefix
      }]
    }
  },
  watch: {
		files: ["src/less/*",'src/img/*'],
		tasks: ["build"]
	}
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
 
  // Default task(s).
  grunt.registerTask('default', ['watch']);
  grunt.registerTask('build', ['less','autoprefixer','imagemin']);

};