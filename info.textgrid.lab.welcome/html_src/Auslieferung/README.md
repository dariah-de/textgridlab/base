# Templates für TextGrid Welcome und Login Screen #

Erstellt am 5.1.2015 von Patrick Heck (patrickheck@dtsi.de)

## Generell ##

Das Paket teilt sich in die Bereiche src/ und dist/ auf. Im dist/ Verzeichnis liegen die Dateien, so wie sie direkt benutzt werden können. Im src/ Verzeichnis liegen die less Quelldateien und die nicht-optimierten Bilder.

Das Paket benutzt ein Standard Bootstrap, dass über Bower im Verzeichnis bower_components installiert ist. 

Die Dateien sind als HTML5 validiert und getestet mit IE 9-11 und Chrome 38-39

### Kompilieren ###

Das kompilieren ist nur notwendig, wenn man mit den less Dateien arbeiten möchte. Es wird über *Grunt* ausgeführt. Zur Vorbereitung ausführen:

* ```npm install```

Danach diesen Befehl ausführen:

* ```grunt build```

Dies erstell die fertigen CSS Dateien im Verzeichnis dist/css/ . Ausserdem werden alle Bilder aus src/img/ in optimierter Form in dist/src/ geschrieben.

### Welcome Screen ###

Der Nightly Modus kann aktviert werden, indem zum ```<header>``` Element die Klasse "nightly" hinzugefügt wird.

Die einblendbaren Hinweise im Kopf sind als auskommentierte Beispiele angelegt und müssten dynamisch gefüllt werden.

Alle ```<contentProvider>``` Felder sind momentan auskommentiert um das HTML validieren zu können. Im produktiven Betrieb müssen diese Felder wieder eingeblendet und die Beispieldaten entfernt werden.

### Login Screen ###

Der **Help-** und **Passwort zurücksetzen-Link** braucht noch einen ```href``` Target.

