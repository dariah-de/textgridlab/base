package info.textgrid.lab.base.extras.preloader;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.tgcrud.client.CrudClientUtilities;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgsearch.client.SearchClient;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.IStartup;

public class StubPreloader extends Job implements IStartup {

	public StubPreloader() {
		super(Messages.StubPreloader_Preloading);
		setPriority(LONG);
		setUser(false);
	}

	@Override
	public void earlyStartup() {
		this.schedule();
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor, 4);
		try {
			progress.subTask(Messages.StubPreloader_Confserv);
			ConfClient.getInstance().getValue(ConfservClientConstants.LAST_API_CHANGE);
			progress.worked(1);
			
			progress.subTask(Messages.StubPreloader_Auth);
			TextGridProject.getStub();
			progress.worked(1);

			progress.subTask(Messages.StubPreloader_Crud);
			CrudClientUtilities.getCrudServiceStub();
			progress.worked(1);

			progress.subTask(Messages.StubPreloader_Search);
			new SearchClient(ConfClient.getInstance().getValue(ConfservClientConstants.TG_SEARCH));
			progress.worked(1);
			
			progress.done();


		} catch (OfflineException e) {
			progress.setCanceled(true);
			return new Status(IStatus.WARNING, "info.textgrid.lab.base.extras", Messages.StubPreloader_Failed, e);
		}
		return Status.OK_STATUS;
	}

}
