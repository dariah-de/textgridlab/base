package info.textgrid.lab.base.extras.preloader;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.base.extras.preloader.messages"; //$NON-NLS-1$
	public static String StubPreloader_Auth;
	public static String StubPreloader_Confserv;
	public static String StubPreloader_Crud;
	public static String StubPreloader_Failed;
	public static String StubPreloader_Preloading;
	public static String StubPreloader_Search;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
