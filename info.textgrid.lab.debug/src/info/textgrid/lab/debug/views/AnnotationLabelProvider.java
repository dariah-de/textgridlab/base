package info.textgrid.lab.debug.views;

import info.textgrid.lab.core.swtutils.annotations.AnnotationContentProvider;

import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * A label provider for {@link Annotation}s
 * 
 * @author tv
 * @see AnnotationContentProvider
 * @see AnnotationView
 * 
 */
public class AnnotationLabelProvider extends ColumnLabelProvider implements IBaseLabelProvider, ITableLabelProvider {

	public static final int TEXT_COLUMN = 0;
	public static final int TYPE_COLUMN = 1;
	public static final int TOSTRING_COLUMN = 2;

	public Image getColumnImage(Object element, int columnIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof Annotation) {
			Annotation annotation = (Annotation) element;
			switch (columnIndex) {
			case TEXT_COLUMN:
				return annotation.getText();
			case TYPE_COLUMN:
				return annotation.getType();
			case TOSTRING_COLUMN:
				return annotation.toString();
			default:
				return "";
			}
		}
		return "Huh? No Annotation in here.";
	}

}
