/**
 * 
 */
package info.textgrid.lab.debug.views;

import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.annotations.ActiveEditorAnnotationViewerController;
import info.textgrid.lab.core.swtutils.annotations.AnnotationContentProvider;

import net.sf.vex.layout.VexAnnotationSupport;

import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.TextStyle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.texteditor.AnnotationPreference;
import org.eclipse.ui.texteditor.IAnnotationImageProvider;

/**
 * A view that displays the current editor's {@link Annotation}s.
 * 
 * @author tv
 */
public class AnnotationView extends ViewPart {

	private final class AnnotationPrefsLabelProvider extends BaseLabelProvider
			implements IStyledLabelProvider {

		public AnnotationPreference getPreference(Object element) {
			if (element instanceof Annotation)
				return annotationSupport.getPreference(((Annotation) element).getType());
			else if (element instanceof String)
				return annotationSupport.getPreference((String) element);
			return null;
		}

		private VexAnnotationSupport annotationSupport;

		@SuppressWarnings("restriction")
		public AnnotationPrefsLabelProvider() {
			annotationSupport = new VexAnnotationSupport();
		}

		public Image getImage(Object element) {
			AnnotationPreference preference = getPreference(element);
			if (preference != null) {
				IAnnotationImageProvider provider = preference
						.getAnnotationImageProvider();
				if (provider != null)
					return provider.getManagedImage((Annotation) element);
			}
			return null;
		}

		public StyledString getStyledText(Object element) {
			AnnotationPreference preference = annotationSupport.getPreference((Annotation) element);
			final String label = annotationSupport.getAccess().getTypeLabel((Annotation) element); // preference.getPreferenceLabel();
			if (preference != null) {
				final RGB rgb = preference.getColorPreferenceValue();
				final String textStyleValue = annotationSupport.getAnnotationDecorationType(((Annotation) element).getType());

				StyledString styledString = new StyledString(
						label == null ? "null" : label,
						new Styler() {
							@Override
							public void applyStyles(TextStyle textStyle) {
								if (rgb != null)
									textStyle.background = new Color(Display
										.getDefault(), rgb);
							}
						});
				if (textStyleValue != null)
				styledString.append(": ").append(textStyleValue);
				return styledString;

			}

			return new StyledString(label != null ? label : element.toString());
		}
	}

	private TableViewer viewer;
	private ActiveEditorAnnotationViewerController controller;

	public AnnotationView() {
		// TODO Auto-generated constructor stub
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		viewer = new TableViewer(parent, SWT.FULL_SELECTION);

		TableViewerColumn textColumn = new TableViewerColumn(viewer, SWT.LEFT,
				AnnotationLabelProvider.TEXT_COLUMN);
		textColumn.getColumn().setText("Text");
		textColumn.getColumn().setWidth(200);
		textColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Annotation) {
					Annotation annotation = (Annotation) element;
					return annotation.getText();
				}
				return super.getText(element);
			}
		});

		TableViewerColumn typeColumn = new TableViewerColumn(viewer, SWT.LEFT,
				AnnotationLabelProvider.TYPE_COLUMN);
		typeColumn.getColumn().setText("Type");
		typeColumn.getColumn().setWidth(200);
		typeColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Annotation) {
					Annotation annotation = (Annotation) element;
					return annotation.getType();
				}
				return super.getText(element);
			}
		});

		TableViewerColumn prefsColumn = new TableViewerColumn(viewer, SWT.LEFT);
		prefsColumn.getColumn().setText("Preferences");
		prefsColumn.getColumn().setWidth(400);
		prefsColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(
				new AnnotationPrefsLabelProvider()));

		// TableColumn toStringColumn = new TableColumn(viewer.getTable(),
		// SWT.LEFT, AnnotationLabelProvider.TOSTRING_COLUMN);
		// toStringColumn.setText("Details");
		// toStringColumn.setWidth(400);

		AnnotationContentProvider contentProvider = new AnnotationContentProvider();
		viewer.setContentProvider(contentProvider);
		// viewer.setLabelProvider(new AnnotationLabelProvider());
		viewer.getTable().setHeaderVisible(true);

		viewer.addDoubleClickListener(new IDoubleClickListener() {

			public void doubleClick(DoubleClickEvent event) {
				ISelection selection = event.getSelection();
				reveal(selection);

			}

			private void reveal(ISelection selection) {
				if (selection != null && selection instanceof IStructuredSelection) {
					Annotation annotation = AdapterUtils.getAdapter(((IStructuredSelection) selection).getFirstElement(),
							Annotation.class);
					if (annotation != null) {
						Position position = controller.getAnnotationModel().getPosition(annotation);
						controller.getTextEditor().selectAndReveal(position.offset, position.length);
						
					}
				}
			}
		});

		controller = new ActiveEditorAnnotationViewerController(viewer);
		getSite().getPage().addPartListener(controller);
		IEditorPart activeEditor = getSite().getPage().getActiveEditor();
		if (activeEditor != null) // TG-617
			controller.partActivated(activeEditor);

	}

	@Override
	public void dispose() {
		if (controller != null)
			getSite().getPage().removePartListener(controller);

		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
