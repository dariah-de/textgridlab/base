package info.textgrid.lab.debug;

import info.textgrid.lab.core.model.TGContentType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

public class ExportContentTypesHandler extends AbstractHandler implements
		IHandler {

	final static String[][] FORMATS = new String[][] {
		// JSON:
			new String[] {
			"contentTypes : {\n",
			"  %s : {\n\tid : \"%1$s\",\n\tmimeType : \"%s\",\n\tdescription : \"%s\",\n\tfile : \"%s\",\n\textension : \"%s\",\n\teclipse : \"%s\",\n\tplugin : \"%s\",\n  },\n",
			"}\n"
			},
		// Confluence:
			new String[] {
				"|| ||MIME Type||Name||Filename Extension||Eclipse Content Type||Contributing Plugin||\n",
				"|!%4$s|alt=%1$s,title=%1$s!|%2$s|%3$s|%5$s|%6$s|%7$s|\n",
				"\n"
			}
			};

	public Object execute(final ExecutionEvent event) throws ExecutionException {

		Shell shell = HandlerUtil.getActiveShell(event);
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		dialog.setText("Save filetype list");
		dialog.setFilterExtensions(new String[] { "json", "wiki" });
		dialog.setFilterNames(new String[] { "JSON", "Confluence Wiki" });
		dialog.setFileName("contentTypes.json");
		String fileName = dialog.open();
		if (fileName == null)
			return null;
		File outputFile = new File(fileName);
		int format = dialog.getFilterIndex();
		try {
			PrintStream out = new PrintStream(outputFile);
			out.print(FORMATS[format][0]);
			ImageLoader loader = new ImageLoader();
			for (TGContentType contentType : TGContentType
					.getContentTypes(false)) {
				String detailId = contentType.getId().substring(
						contentType.getId().indexOf('/') + 1);
				String shortId;
				int plusIndex = detailId.indexOf('+');
				if (plusIndex > 0 && detailId.endsWith("+xml"))
					shortId = detailId.substring(0, plusIndex);
				else
					shortId = detailId;
				if (shortId.startsWith("tg."))
					shortId = shortId.substring(3);
				shortId = shortId.replaceAll("[^a-zA-Z0-9_-]+", "-");

				File imageFile = new File(outputFile.getParent(), shortId
						+ ".png");
				loader.data = new ImageData[] { contentType.getImage(true)
						.getImageData() };
				loader.save(imageFile.getPath(), SWT.IMAGE_PNG);

				out.format(FORMATS[format][1], shortId,
						contentType.getId(), contentType.getDescription(),
						imageFile.getName(), contentType.getExtension(),
						contentType.getEclipseContentType(), contentType
								.getContributor().getName());
			};
			out.print(FORMATS[format][2]);
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
