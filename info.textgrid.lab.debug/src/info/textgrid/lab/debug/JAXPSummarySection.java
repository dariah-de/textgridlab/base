package info.textgrid.lab.debug;

import java.io.PrintWriter;
import java.security.CodeSource;
import java.text.MessageFormat;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.xpath.XPathFactory;

import org.eclipse.ui.about.ISystemSummarySection;

public class JAXPSummarySection implements ISystemSummarySection {

	@Override
	public void write(PrintWriter writer) {
	    writer.println(getJaxpImplementationInfo("DocumentBuilderFactory", DocumentBuilderFactory.newInstance().getClass()));
	    writer.println(getJaxpImplementationInfo("XPathFactory", XPathFactory.newInstance().getClass()));
	    writer.println(getJaxpImplementationInfo("TransformerFactory", TransformerFactory.newInstance().getClass()));
	    writer.println(getJaxpImplementationInfo("SAXParserFactory", SAXParserFactory.newInstance().getClass()));

	}
	
	/**
	 * by Daniel Fortunov, http://stackoverflow.com/a/1804281/1719996
	 */
	private static String getJaxpImplementationInfo(String componentName, Class componentClass) {
		CodeSource source = componentClass.getProtectionDomain().getCodeSource();
		return MessageFormat.format("{0} implementation: {1} loaded from: {2}", componentName, componentClass.getName(),
				source == null ? "Java Runtime" : source.getLocation());
	}

}
