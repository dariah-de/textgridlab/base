package info.textgrid.lab.debug;

import java.text.MessageFormat;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.internal.runtime.Product;
import org.eclipse.core.runtime.IBundleGroup;
import org.eclipse.core.runtime.IBundleGroupProvider;
import org.eclipse.core.runtime.IProduct;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.ProductInfo;
import org.osgi.framework.Bundle;

public class DebugBundleGroupHandler extends AbstractHandler implements
		IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IBundleGroupProvider[] bundleGroupProviders = Platform.getBundleGroupProviders();
		System.out.println("Bundle Group Hierarchy\n======================");
		for (IBundleGroupProvider provider : bundleGroupProviders) {
			System.out.println(MessageFormat.format("{0} ({1})", provider.getName(), provider.getClass().getSimpleName()));
			for (IBundleGroup group : provider.getBundleGroups()) {
				System.out.println(MessageFormat.format("  {0} ({1} {2}), by {3}, class: {4}", group.getName(), group.getIdentifier(),
						group.getVersion(), group.getProviderName(), group.getClass().getSimpleName()));
//				Bundle[] bundles = group.getBundles();
//				for (Bundle bundle : bundles) {
//					System.out.println(MessageFormat.format("    {0} {1} ({2})", bundle.getSymbolicName(), bundle.getVersion(),
//							bundle.getClass().getSimpleName()));
//				}
			}
			
		}
		
		// TODO Auto-generated method stub
		return null;
	}

}
