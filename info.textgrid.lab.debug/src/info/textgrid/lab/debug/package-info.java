/**
 * This plugin is intended to provide various debugging and development tools
 * for the TextGridLab.
 * 
 * <dl>
 * <dt>{@link info.textgrid.lab.debug.views}</dt>
 * <dd>Various views and accompanying classes</dd>
 * </dl>
 */
package info.textgrid.lab.debug;

