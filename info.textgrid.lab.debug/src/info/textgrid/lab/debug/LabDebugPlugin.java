package info.textgrid.lab.debug;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class LabDebugPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.debug";

	// The shared instance
	private static LabDebugPlugin plugin;
	
	/**
	 * The constructor
	 */
	public LabDebugPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		initializeSharedImages();
	}

	protected void initializeSharedImages() {
		// Due to an Eclipse bug, these aren't available via
		// PlatformUI.getWorkbench().getSharedImages() ...
		registerImage(ISharedImages.IMG_DEC_FIELD_ERROR, "resources/error_co.gif");
		registerImage(ISharedImages.IMG_DEC_FIELD_WARNING, "resources/warning_co.gif");
	}

	private void registerImage(final String identifier, final String resourcePath) {
		final ImageRegistry registry = getImageRegistry();
		final URL url = FileLocator.find(getBundle(), new Path(resourcePath), null);
		final ImageDescriptor imageDescriptor = ImageDescriptor.createFromURL(url);
		registry.put(identifier, imageDescriptor);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static LabDebugPlugin getDefault() {
		return plugin;
	}

}
