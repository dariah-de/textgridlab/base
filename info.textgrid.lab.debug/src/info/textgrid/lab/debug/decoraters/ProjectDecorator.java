package info.textgrid.lab.debug.decoraters;

import info.textgrid.lab.core.model.TextGridProject;

import java.text.MessageFormat;

import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;

import com.google.common.base.Joiner;

public class ProjectDecorator extends BaseLabelProvider implements ILightweightLabelDecorator {

	public void decorate(Object element, IDecoration decoration) {
		if (element instanceof TextGridProject) {
			TextGridProject project = (TextGridProject) element;

			String id = project.getId();
			String leaders = Joiner.on(", ").join(project.getLeaders());
			decoration.addSuffix(MessageFormat.format(" {0} (by {1})", id, leaders));
		}
	}

}
