package info.textgrid.lab.debug.decoraters;

import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.ITextGridPermission;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.ui.statushandlers.StatusManager;

public class TGOPermissionsDecorator extends BaseLabelProvider implements ILightweightLabelDecorator {
	
	public void decorate(Object element, IDecoration decoration) {
		TextGridObject object = AdapterUtils.getAdapter(element, TextGridObject.class);
		if (object == null)
			return;				

		try {
			int permissions = object.retrievePermissions(object.getURI().toString());
			if(!((permissions & ITextGridPermission.READ) == ITextGridPermission.READ))
				decoration.addOverlay(ImageDescriptor.createFromFile(this.getClass(), "/resources/private_co.gif"), 
										IDecoration.BOTTOM_RIGHT);
			else if(!((permissions & ITextGridPermission.UPDATE) == ITextGridPermission.UPDATE))
				decoration.addOverlay(ImageDescriptor.createFromFile(this.getClass(), "/resources/protected_co.gif"),
										IDecoration.BOTTOM_RIGHT);	
		} catch (CoreException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
		}
	}

}
