package info.textgrid.lab.debug.decoraters;

import info.textgrid.lab.core.model.TGObjectReference;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;

public class TGOPublicSandboxDecorator extends BaseLabelProvider implements ILightweightLabelDecorator {

	public void decorate(Object element, IDecoration decoration) {
		if (!(element instanceof TGObjectReference)) 
			return;

		TGObjectReference tgoRef = (TGObjectReference)element;
		try {
			//TODO isSandboxObject()???
			if (tgoRef.getTgo().isPublic()) {
				ImageDescriptor overlay = ImageDescriptor.createFromFile(this.getClass(), "/resources/public_sandbox.gif");
				decoration.addOverlay(overlay, IDecoration.BOTTOM_RIGHT);
			}
		} catch (CoreException e) {
			return;
		}	
	}

}