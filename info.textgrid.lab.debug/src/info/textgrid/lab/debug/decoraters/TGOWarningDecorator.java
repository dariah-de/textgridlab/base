package info.textgrid.lab.debug.decoraters;

import info.textgrid.lab.core.model.TGObjectReference;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;

public class TGOWarningDecorator extends BaseLabelProvider implements ILightweightLabelDecorator {

	public void decorate(Object element, IDecoration decoration) {
		if (!(element instanceof TGObjectReference)) 
			return;

		TGObjectReference tgoRef = (TGObjectReference)element;
		try {
			if (!tgoRef.getTgo().getCrudWarning().equals("")) {
				ImageDescriptor overlay = ImageDescriptor.createFromFile(this.getClass(), "/resources/warning_co.gif");
				decoration.addOverlay(overlay, IDecoration.TOP_RIGHT);
			}
		} catch (CoreException e) {
			return;
		}	
	}

}