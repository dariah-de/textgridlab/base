package info.textgrid.lab.debug.decoraters;

import info.textgrid.lab.core.model.TGObjectReference;

import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;

public class TGORevisionDecorator extends BaseLabelProvider implements ILightweightLabelDecorator {

	public void decorate(Object element, IDecoration decoration) {
		if (!(element instanceof TGObjectReference)) 
			return;

		TGObjectReference tgoRef = (TGObjectReference)element;
		
		String revision = tgoRef.getRevisionNumber();
		if (!revision.equals("0") && !revision.equals(""))
			decoration.addSuffix(" (Revision " + revision + ")");
	}
}

