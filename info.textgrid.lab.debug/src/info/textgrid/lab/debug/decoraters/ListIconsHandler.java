package info.textgrid.lab.debug.decoraters;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.MessageFormat;
import java.util.regex.Pattern;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.RegistryFactory;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.handlers.HandlerUtil;

public class ListIconsHandler extends AbstractHandler implements IHandler {

	private PrintStream out;

	public Object execute(ExecutionEvent event) throws ExecutionException {

		FileDialog dialog = new FileDialog(HandlerUtil.getActiveShell(event),
				java.awt.FileDialog.SAVE);
		dialog.setFileName("icons.tsv");
		dialog.setFilterExtensions(new String[] { "tsv" });
		dialog.setFilterIndex(0);
		dialog.setFilterPath(System.getProperty("user.home", "/"));
		dialog.setText("Generate icon list");
		String filename = dialog.open();

		if (filename != null) {
			try {
				out = new PrintStream(filename);
			} catch (FileNotFoundException e) {
				out = System.out;
			}
		} else
			out = System.out;

		try {
			out.println("Contributor\tExtension Point\tElement\tAttribute\tFile");
			IExtensionRegistry registry = RegistryFactory.getRegistry();
			for (String namespace : registry.getNamespaces()) {
				for (IExtension extension : registry.getExtensions(namespace)) {
					for (IConfigurationElement config : extension
							.getConfigurationElements()) {
						handleConfigurationElement("", config);
					}
				}
			}
		} finally {
			if (out != System.out) {
				out.close();
			}
		}

		return null;
	}

	private static Pattern ICON = Pattern.compile(".*(icon|image)$");

	private void handleConfigurationElement(String path,
			IConfigurationElement config) {
		if (ICON.matcher(config.getName()).matches()
				&& config.getValue() != null && !config.getValue().isEmpty()) {
			handleMatch(path, config, "", config.getValue());
		}
		for (String attr : config.getAttributeNames()) {
			String value = config.getAttribute(attr);
			if ((ICON.matcher(attr).matches() && value != null && !value
					.isEmpty())
					|| value != null
					&& (value.endsWith(".png") || value.endsWith(".gif")))
				handleMatch(path, config, attr, value);
		}
		for (IConfigurationElement child : config.getChildren()) {
			handleConfigurationElement(path + "/" + config.getName(), child);
		}
	}

	private void handleMatch(String path, IConfigurationElement config,
			String attr, String value) {
		out.println(MessageFormat.format("{0}\t{1}\t{2}/{3}\t{4}\t{5}", config
				.getContributor().getName(), config.getDeclaringExtension()
				.getExtensionPointUniqueIdentifier(), path, config.getName(),
				attr, value));
	}
}