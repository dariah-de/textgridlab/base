package info.textgrid.lab.debug.decoraters;

import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

public class TGODecorator extends BaseLabelProvider implements ILightweightLabelDecorator {

	public void decorate(Object element, IDecoration decoration) {
		TextGridObject object = AdapterUtils.getAdapter(element, TextGridObject.class);
		if (object == null)
			return;

		StringBuilder label = new StringBuilder(" ");
		List<Character> flags = Lists.newArrayList();
		if (object.isNew())
			flags.add('N');
		if (!object.isComplete())
			flags.add('I');
		else {
			try {
				if (object.getAdaptor() != null)
					flags.add('A');
				if (object.getSchemaURI() != null)
					flags.add('S');
			} catch (CrudServiceException e) {
				Activator.handleError(e, null);
			}
		}

		if (flags.size() > 0)
			label.append('[').append(Joiner.on("").join(flags)).append("] ");

		try {
			label.append(object.getContentTypeID());
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		decoration.addSuffix(label.toString());
	}

}
