package info.textgrid.lab.debug.decoraters;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.debug.LabDebugPlugin;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.base.Joiner;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Decorates items that are associated with resources that have a problem
 * marker.
 * 
 * @author vitt
 */
public class ResourceProblemDecorator extends BaseLabelProvider implements ILightweightLabelDecorator {

	public static final String MOVE_TGOBJECT = "MOVE_TGOBJECT";

	private IResourceChangeListener resourceChangeListener = new IResourceChangeListener() {

		public void resourceChanged(IResourceChangeEvent event) {
			IMarkerDelta[] deltas = event.findMarkerDeltas(IMarker.MARKER, true);
			List<IResource> resources = Lists.newArrayListWithCapacity(deltas.length);
			for (IMarkerDelta delta : deltas) {
				resources.add(delta.getResource());
			}
			fireLabelProviderChanged(new LabelProviderChangedEvent(ResourceProblemDecorator.this, deltas));
		}
	};

	public ResourceProblemDecorator() {
		super();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(resourceChangeListener, IResourceChangeEvent.POST_BUILD);
	}

	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(resourceChangeListener);
		super.dispose();
	}

	public void decorate(Object element, IDecoration decoration) {
		// is a Move-Object-Job active?
		IResource resource = null;
		IJobManager jobMan = Job.getJobManager();
		Job[] jobs = jobMan.find(MOVE_TGOBJECT);
		
		if (jobs.length == 0) 
			resource = getAdapter(element, IFile.class);
		
		if (resource != null) {
			try {
				IMarker[] markers = resource.findMarkers(null, true, IResource.DEPTH_INFINITE);

				int severity = IMarker.SEVERITY_INFO;
				List<String> messages = Lists.newLinkedList();

				for (IMarker marker : markers) {
					if (marker.exists()) {
						severity = Math.max(severity, marker.getAttribute(IMarker.SEVERITY, IMarker.SEVERITY_INFO));
						messages.add(marker.getAttribute(IMarker.MESSAGE, null));
					}
				}

				if (severity > IMarker.SEVERITY_INFO) {
					String joinedMessage = Joiner.on("; ").join(Iterables.filter(messages, Predicates.notNull()));
					decoration.addSuffix(joinedMessage);
					ImageDescriptor overlay = LabDebugPlugin.getDefault().getImageRegistry().getDescriptor(
							severity == IMarker.SEVERITY_ERROR ? ISharedImages.IMG_DEC_FIELD_ERROR
									: ISharedImages.IMG_DEC_FIELD_WARNING);
					decoration.addOverlay(overlay, IDecoration.BOTTOM_LEFT);
				}

			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}
		}
	}
}
