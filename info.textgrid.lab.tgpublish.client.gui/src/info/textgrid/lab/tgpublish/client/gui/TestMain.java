/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.tgpublish.client.gui;

import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.client.PublishClient;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXB;


public class TestMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// get a sid: https://textgridlab.org/WebAuthN/WebAuthN.php?authZinstance=textgrid-ws3.sub.uni-goettingen.de	
		String sid = "";
		String uri = "textgrid:2bd7.1";
		
		PublishClient pc = new PublishClient("http://textgrid-ws3.sub.uni-goettingen.de/tgpublish", sid);
		
//		boolean bres = pc.publish(uri).getStatus() == Response.Status.OK.getStatusCode();
//		JAXB.marshal(bres, System.out);
		
		PublishResponse pres = pc.getStatus(uri);

    	JAXB.marshal(pres, System.out);
    	
    	//System.out.println("\npublishStatus : " + res.status);
    	
	}

}
