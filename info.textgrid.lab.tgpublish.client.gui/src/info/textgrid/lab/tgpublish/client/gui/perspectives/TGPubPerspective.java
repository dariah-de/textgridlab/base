package info.textgrid.lab.tgpublish.client.gui.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;


/**
 *  This class is meant to serve as an example for how various contributions 
 *  are made to a perspective. Note that some of the extension point id's are
 *  referred to as API constants while others are hardcoded and may be subject 
 *  to change. 
 */
public class TGPubPerspective implements IPerspectiveFactory {
	
	public static String ID = "info.textgrid.lab.tgpublish.client.gui.perspectives.TGPubPerspective";
	private IPageLayout factory;

	public TGPubPerspective() {
		super();
	}

	public void createInitialLayout(IPageLayout factory) {
		this.factory = factory;
		addViews();
	}

	private void addViews() {
		// Creates the overall folder layout. 
		// Note that each new Folder uses a percentage of the remaining EditorArea.
		
		IFolderLayout topLeft =
			factory.createFolder(
				"left", //NON-NLS-1
				IPageLayout.LEFT,
				0.25f,
				factory.getEditorArea());
		topLeft.addView("info.textgrid.lab.navigator.view"); //NON-NLS-1

		IFolderLayout topRight =
			factory.createFolder(
				"right", //NON-NLS-1
				IPageLayout.RIGHT,
				0.75f,
				factory.getEditorArea());
		topRight.addView("info.textgrid.lab.tgpublish.client.gui.tgpview"); //NON-NLS-1
		
		factory.setEditorAreaVisible(false);
		
	}

}
