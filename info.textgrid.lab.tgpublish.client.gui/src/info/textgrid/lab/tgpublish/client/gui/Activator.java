/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.tgpublish.client.gui;

import java.net.URL;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.tgsearch.client.gui"; //$NON-NLS-1$

	public static final String PUBLIZIERE_025_IMAGE_ID 	= "publiziere_025"; //$NON-NLS-1$
	public static final String ERROR_IMAGE_ID 			= "error"; //$NON-NLS-1$
	public static final String GREEN16_IMAGE_ID 		= "green16"; //$NON-NLS-1$
	public static final String YELLOW_IMAGE_ID 			= "yellow"; //$NON-NLS-1$
	public static final String RED16_IMAGE_ID 			= "red16"; //$NON-NLS-1$
	public static final String NWARNING16_IMAGE_ID 		= "nwarning16"; //$NON-NLS-1$
	public static final String SYSTEMINFO16_IMAGE_ID 	= "systeminfo16"; //$NON-NLS-1$
	public static final String WARNING16_IMAGE_ID 		= "warning16"; //$NON-NLS-1$

	
	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	public static void handleWarning(CoreException e, String string) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	
	public static IStatus handleError(Throwable e, String message, Object... args) {
		Status status = new Status(IStatus.ERROR, PLUGIN_ID, NLS.bind(message, args));
		StatusManager.getManager().handle(status);
		return status;
	}
	
	protected void initializeImageRegistry(ImageRegistry registry) {
		registerImage(registry, PUBLIZIERE_025_IMAGE_ID , "icons/025-publiziere-Objekt.gif");
		registerImage(registry, ERROR_IMAGE_ID 			, "icons/error.gif");
		registerImage(registry, GREEN16_IMAGE_ID 		, "icons/green16.gif");
		registerImage(registry, YELLOW_IMAGE_ID 		, "icons/yellow.gif");
		registerImage(registry, RED16_IMAGE_ID 			, "icons/red16.gif");
		registerImage(registry, NWARNING16_IMAGE_ID 	, "icons/nwarning16.gif");
		registerImage(registry, SYSTEMINFO16_IMAGE_ID 	, "icons/systeminfo16.gif");
		registerImage(registry, WARNING16_IMAGE_ID 		, "icons/warning16.gif");
    }
	
	/**
	 * Registers the image in the image registry.
	 * 
	 * @param registry
	 *            the image registry to use
	 * @param key
	 *            the key by which the image will be adressable
	 * @param fullPath
	 *            the full path to the image, relative to the plugin directory.
	 */
	private void registerImage(ImageRegistry registry, String key, String fullPath) {
		URL imageURL = FileLocator.find(getBundle(), new Path(fullPath), null);
		registry.put(key, ImageDescriptor.createFromURL(imageURL));
	}

}
