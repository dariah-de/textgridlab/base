package info.textgrid.lab.tgpublish.client.gui.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.tgpublish.client.gui.views.messages"; //$NON-NLS-1$
	public static String HelpDialog_Message_MESSAGE_INFO;
	public static String HelpDialog_Message_TITLE_INFO;
	public static String HelpDialog_Message_TEXT_INFO_FULL_PUBLISH;
	public static String HelpDialog_Message_TEXT_INFO_FULL_PUBLISH_2;
	public static String HelpDialog_Message_TEXT_INFO_LIMITED_PUBLISH;
	public static String HelpDialog_Message_TEXT_INFO_LIMITED_PUBLISH_2;
	public static String TGPublishView_OK_FOR_PUBLICATION;
	public static String TGPublishView_NOT_OK_FOR_PUBLICATION;
	public static String TGPublishView_TableColumn_Status;
	public static String TGPublishView_WARNINGs;
	public static String TGPublishView_NoStatusInfo;
	public static String TGPublishView_PublishWarning;
	public static String TGPublishView_ReloadingMetadata;
	public static String TGPublishView_NoMetadata;
	public static String TGPublishView_JobName_1;
	public static String TGPublishView_TGPublishView;
	public static String TGPublishView_TechnicalObject;
	public static String TGPublishView_Status;
	public static String TGPublishView_SUCCESSFULL_PUBLISHED;
	public static String TGPublishView_OK;
	public static String TGPublishView_Warning;
	public static String TGPublishView_Error;
	public static String TGPublishView_TableColumn_Name;
	public static String TGPublishView_SUCCESSFULL_PUBLISHED_2;
	public static String TGPublishView_CallAction;
	public static String TGPublishView_PROOF_FOR_PUBLICATION;
	public static String TGPublishView_BtnProof;
	public static String TGPublishView_IgnoreWarnings;
	public static String TGPublishView_NotReversible;
	public static String TGPublishView_NOT_REVERSABLE_ACTION;
	public static String TGPublishView_BtnPublish;
	public static String TGPublishView_BtnCancel;
	public static String TGPublishView_ACTIVATE_PUBLISH_INFO;
	public static String TGPublishView_PUBLISH_STATUS_ERROR;
	public static String TGPublishView_PUBLISH_STATUS_ERROR_MESSAGE;
	public static String TGPublishView_The;
	public static String TGPublishView_NoTGOContent;
	public static String TGPublishView_TableColumn_Actions;
	public static String TGPublishView_OKs;
	public static String TGPublishView_ERRORs;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
