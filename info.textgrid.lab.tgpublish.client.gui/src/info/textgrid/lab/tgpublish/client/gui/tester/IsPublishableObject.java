/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.tgpublish.client.gui.tester;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.tgpublish.client.gui.Activator;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgpublish.api.jaxb.WorldReadableMimetypes;
import info.textgrid.middleware.tgpublish.client.PublishClient;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

/**
 * This class is a property tester class for tgpublishable objects
 * 
 * @author Leuk
 * 
 */
public class IsPublishableObject extends PropertyTester {
	
//	private final String tgPublishEndpoint = "http://textgrid-ws3.sub.uni-goettingen.de/tgpublish";
	private PublishClient publishClient = null;
	private String SID = "";
	private WorldReadableMimetypes wRMimeTypes = null;

	/**
	 * Constructor detects the possible mime types.
	 */
	public IsPublishableObject() {
		SID = RBACSession.getInstance().getSID(false);
		if (!SID.equals("")) {
			try {
				publishClient = new PublishClient(ConfClient.getInstance().getValue(ConfservClientConstants.TGPUBLISH), SID);
				wRMimeTypes = publishClient.getWorldReadables();
			} catch (Exception e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
 "System offline, can not connect to TG-Publish.", e);
				Activator.getDefault().getLog().log(status);
			}
		}
	}
	
	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		
		if (wRMimeTypes == null)
			return false;

		if (receiver != null && receiver instanceof TGObjectReference) {
			return isOK(AdapterUtils.getAdapter(receiver, TextGridObject.class));
		}

		return false;
	}

	private boolean isOK(TextGridObject tgo) {
		String contentTypeId = "";
//		reloadMetadata(tgo);
		try {
			contentTypeId = tgo.getContentTypeID();
			if ((isWorldReadable(contentTypeId) || 	//"technical"-Object
				contentTypeId.contains("tg.edition") || 
				contentTypeId.contains("tg.collection")) && !tgo.isPublic()) {
				return true;
			}
		} catch (CoreException e) {
			info.textgrid.lab.tgpublish.client.gui.Activator
					.handleWarning(e,
							"Couldn't get the content type id of the selected textgrid object");
		}

		return false;
	}
	
	
	/**
	 * proof if the content types matches
	 * @param contentTypeId
	 * @return
	 */
	private boolean isWorldReadable(String contentTypeId) {
		ArrayList<String> contTypeList = (ArrayList<String>) wRMimeTypes.getWorldReadableList();
		for (Iterator<String> iterator = contTypeList.iterator(); iterator.hasNext();) {
			String wRcontType = iterator.next();
			if (contentTypeId.matches(wRcontType)) {
				//System.out.println("...match!");
				return true;
			}
		}
		return false;
	}

}
