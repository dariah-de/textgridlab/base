/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.tgpublish.client.gui.views;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import com.swtdesigner.SWTResourceManager;
import org.eclipse.swt.widgets.Text;


/**
 * Dialog class for giving informations about "publishable"-objects.
 * 
 * @author Leuk
 */
public class HelpDialog extends TitleAreaDialog {
	private static final String  MESSAGE_INFO = Messages.HelpDialog_Message_MESSAGE_INFO;
	private static final String  TITLE_INFO = Messages.HelpDialog_Message_TITLE_INFO;
	private static final String  TEXT_INFO_FULL_PUBLISH = Messages.HelpDialog_Message_TEXT_INFO_FULL_PUBLISH;
	private static final String  TEXT_INFO_FULL_PUBLISH_2 = Messages.HelpDialog_Message_TEXT_INFO_FULL_PUBLISH_2;
	private static final String  TEXT_INFO_LIMITED_PUBLISH = Messages.HelpDialog_Message_TEXT_INFO_LIMITED_PUBLISH;
	private static final String  TEXT_INFO_LIMITED_PUBLISH_2 = Messages.HelpDialog_Message_TEXT_INFO_LIMITED_PUBLISH_2;
	/**
	 * Create the dialog.
	 * @param parentShell
	 */
	public HelpDialog(Shell parentShell) {
		super(parentShell);
		setHelpAvailable(false);
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		setMessage(MESSAGE_INFO);
		setTitle(TITLE_INFO);
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		Label lblFullPublish = new Label(container, SWT.NONE);
		lblFullPublish.setFont(SWTResourceManager
				.getFont("Tahoma", 9, SWT.BOLD)); //$NON-NLS-1$
		lblFullPublish.setText(TEXT_INFO_FULL_PUBLISH);

		Text fullPublishText = new Text(container, SWT.READ_ONLY | SWT.WRAP);
		fullPublishText.setText(TEXT_INFO_FULL_PUBLISH_2);
		GridData gd_styledText = new GridData(SWT.FILL, SWT.FILL, true, true,
				1, 1);
		gd_styledText.heightHint = 66;
		fullPublishText.setLayoutData(gd_styledText);

		Label lblLimitedPublishworldreadable = new Label(container, SWT.NONE);
		lblLimitedPublishworldreadable.setFont(SWTResourceManager.getFont(
				"Tahoma", 9, SWT.BOLD)); //$NON-NLS-1$
		lblLimitedPublishworldreadable.setText(TEXT_INFO_LIMITED_PUBLISH);

		Text limitedPublishText = new Text(container, SWT.READ_ONLY | SWT.WRAP);
		limitedPublishText.setText(TEXT_INFO_LIMITED_PUBLISH_2);
		limitedPublishText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));

		return area;
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
//		createButton(parent, IDialogConstants.CANCEL_ID,
//				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(600, 350);
	}
}
