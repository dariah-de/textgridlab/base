/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.tgpublish.client.gui.model;

import info.textgrid.lab.tgpublish.client.gui.controller.TGPublishController;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the content provider of the used TableViever
 *
 * @author leuk
 *
 */
public class TGPublishContentProvider {
	private static TGPublishContentProvider content;
	private List<EditionObjectDataModel> allData;
	private TGPublishController controller = TGPublishController.getInstance();

	
	private TGPublishContentProvider() {
		allData = controller.getDataModel();
//		System.out.println("xxxx: "+allData.toString());
	}

	public static synchronized TGPublishContentProvider getInstance() {
		if (content != null) {
			return content;
		}
		content = new TGPublishContentProvider();
		return content;
	}

	public List<EditionObjectDataModel> getAllData() {
		return allData;
	}
}
