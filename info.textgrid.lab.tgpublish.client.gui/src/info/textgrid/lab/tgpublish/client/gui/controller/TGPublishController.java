/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.tgpublish.client.gui.controller;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditor;
import info.textgrid.lab.core.aggregations.ui.views.AggregationComposerEditorInput;
import info.textgrid.lab.core.metadataeditor.MetaDataView;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.util.ModelUtil.InUIThreadException;
import info.textgrid.lab.tgpublish.client.gui.Activator;
import info.textgrid.lab.tgpublish.client.gui.model.EditionObjectDataModel;
import info.textgrid.lab.tgpublish.client.gui.views.TGPublishView;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.middleware.tgpublish.api.jaxb.Module;
import info.textgrid.middleware.tgpublish.api.jaxb.ProcessStatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishError;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishWarning;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.WorldReadableMimetypes;
import info.textgrid.middleware.tgpublish.client.PublishClient;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXB;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

/**
 * The controller class for the tgpublish service.
 * 
 * @author Leuk
 * 
 */
public class TGPublishController {

	// private final String tgPublishEndpoint =
	// "http://textgrid-ws3.sub.uni-goettingen.de/tgpublish";
	private static final String SUCCESSFULL_PUBLISHED = " has been successful published!";
	private PublishResponse publishResponse = null;
	private PublishClient publishClient = null;
	private EditionObjectDataModel data = null;
	private List<EditionObjectDataModel> allData = new ArrayList<EditionObjectDataModel>();
	private TextGridObject tgObj = null;
	private String uri = "";
	private String sid = "";
	private String faildMessage = "";
	public boolean isReadyForPublishing = false;
	public boolean isCanceld = false;
	public boolean isWorldReadable = false;
	public boolean ignoreWarnings = false;
	public boolean publishStatusFailed = false;
	public boolean showLogs = false;
	public ProcessStatusType statusInfo = null;

	private int okCounts = 0;
	private int errorCounts = 0;
	private int warningCounts = 0;

	private enum ActionError {
		NOT_SPECIFIED, AUTH, WRONG_CONTENT_TYPE, NO_PUBLISH_RIGHT, PID_GENERATION_FAILED, MISSING_METADATA, ALREADY_PUBLISHED, METADATA_WARNINGS_EXIST,
		SERVER_ERROR
	}

	private Job job = null;
	private boolean simulateRun;

	public Job getJob() {
		return job;
	}

	private static TGPublishController instance = new TGPublishController();

	/**
	 * Default-Constructor
	 */
	private TGPublishController() {
	}

	/**
	 * Static Method - getting the instance.
	 */
	public static TGPublishController getInstance() {
		return instance;
	}

	public TextGridObject getTGObj() {
		return tgObj;
	}

	public void setTGObj(TextGridObject uri) {
		this.tgObj = uri;
	}

	public String getUri() {
		return uri;
	}

	public String getFaildMessage() {
		return faildMessage;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public int getOKCounts() {
		return okCounts;
	}

	public int getErrorCounts() {
		return errorCounts;
	}

	public int getWarningCounts() {
		return warningCounts;
	}

	public static String getSuccessfullPublished() {
		return SUCCESSFULL_PUBLISHED;
	}

	public PublishResponse getPublishResponse() {
		return publishResponse;
	}

	public void setIgnoreWarnings(boolean ignoreWarnings) {
		this.ignoreWarnings = ignoreWarnings;
	}

	public boolean getWorldReadable() {
		return isWorldReadable;
	}

	public void setWorldReadable(boolean isWorldReadable) {
		this.isWorldReadable = isWorldReadable;
	}

	public void setPublishStatusFailed(boolean status) {
		this.publishStatusFailed = status;
	}

	/**
	 * call TGPublish client
	 * 
	 * @param client
	 */
	public void callTGPublishClient(final String client) {
		statusInfo = null;
		isCanceld = false;
		simulateRun = true;
		job = new Job("call tgpublish") {

			String message = "";
//			@SuppressWarnings("unused")
//			boolean bres = true;
			//TODO : use the response Status
//			int bres = Response.Status.OK.getStatusCode();
			Object bres = null;
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				Log("bres: "+bres);
				// TODO Auto-generated method stub
				if (monitor.isCanceled()) {
					isCanceld = true;
					return Status.CANCEL_STATUS;
				}
				if (client.equals("WorldReadable")) {
					Log("---->WorldReadable: ");
					message = "An error occured while calling TGPublish-Client(WR)!";
					simulateRun = false;
//					bres = publishClient.publishWorldReadable(uri).getStatus() == Response.Status.OK.getStatusCode();
					bres = publishClient.publishWorldReadable(uri).getEntity();
				} else if (client.equals("WordReadableSimulate")) {
					Log("---->WordReadableSimulate: ");
					message = "An error occured while calling TGPublish-Client(WR-Simulate)!";
//					bres = publishClient.publishWorldReadableSimulate(uri).getStatus() == Response.Status.OK.getStatusCode();
					bres = publishClient.publishWorldReadableSimulate(uri).getEntity();
				} else if (client.equals("Simulate")) {
					Log("---->Simulate: ");
					message = "An error occured while calling TGPublish-Client(Simulate)!";
//					bres = publishClient.publishSimulate(uri).getStatus() == Response.Status.OK.getStatusCode();
					bres = publishClient.publishSimulate(uri).getEntity();
				} else if (client.equals("IgnoreWarnings")) {
					Log("---->IgnoreWarnings: ");
					message = "An error occured while calling the TGPublish-Client(IW)!";
					simulateRun = false;
//					bres = publishClient.publishIgnoreWarnings(uri).getStatus() == Response.Status.OK.getStatusCode();
					bres = publishClient.publishIgnoreWarnings(uri).getEntity();
				} else if (client.equals("Publish")) {
					Log("---->Publish: ");
					message = "An error occured while calling the TGPublish-Client!";
					simulateRun = false;
//					bres = publishClient.publish(uri).getStatus() == Response.Status.OK.getStatusCode();
					bres = publishClient.publish(uri).getEntity();
				}
				

				ProcessStatusType status = null;
				int elapsedTime = 3000;
				int elapsedTime4NOT_QUEUED = 10000;
				do {
					if (monitor.isCanceled()) {
						isCanceld = true;
						return Status.CANCEL_STATUS;
					}
					
					try {
						Thread.sleep(elapsedTime);	//after the first call --> elapsedTime = 0
						elapsedTime = 0;
						
						publishResponse = publishClient.getStatus(uri);

						status = getPublishResponse().getPublishStatus().processStatus;
						Log("---->Status: " + status.toString());
						statusInfo = status;
						
						//TODO
						int progressInfo = publishResponse.getPublishStatus().progress;
						if (progressInfo != 0) {
							Log("progressInfo: " + progressInfo);
							monitor.beginTask("TG-Publish is proofing...", 100);
							monitor.worked(progressInfo);
						}
						
						if (status.toString().equals("NOT_QUEUED")) {
							Thread.sleep(elapsedTime4NOT_QUEUED);	//wait 10 sec.

							status = getPublishResponse().getPublishStatus().processStatus;
							Log("---->Status: " + status.toString());
							statusInfo = status;
						}
						
						if (!status.toString().equals("RUNNING") && !status.toString().equals("NOT_QUEUED")) {
							break;
						}
						proofPublishStatus(status.toString());
					} catch (InterruptedException e) {
						Activator.handleError(e, message);
					} catch (Exception e) {
						Activator.handleError(e, message);
					}

				} while (status.toString().equals("RUNNING"));

				monitor.done();
				fillDataModel();

				return Status.OK_STATUS;
			}
		};
		job.addJobChangeListener(new IJobChangeListener() {

			@Override
			public void sleeping(IJobChangeEvent event) {
				// TODO Auto-generated method stub

			}

			@Override
			public void scheduled(IJobChangeEvent event) {
				// TODO Auto-generated method stub

			}

			@Override
			public void running(IJobChangeEvent event) {
				// TODO Auto-generated method stub

			}

			@Override
			public void done(IJobChangeEvent event) {
				// TODO Auto-generated method stub

				UIJob uiJob = new UIJob("...tgpublish") {

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						// TODO Auto-generated method stub
						if (monitor.isCanceled()) {
							isCanceld = true;
							return Status.CANCEL_STATUS;
						}

						try {
							((TGPublishView) PlatformUI
									.getWorkbench()
									.getActiveWorkbenchWindow()
									.getActivePage()
									.showView(
											"info.textgrid.lab.tgpublish.client.gui.tgpview",
											null, IWorkbenchPage.VIEW_ACTIVATE))
									.updateMessageCounts();

							proofIsReadyForPublishing();

						} catch (PartInitException e) {
							Activator.handleError(e,
									"Couldn't open the TGPublish View!");
						}
						return Status.OK_STATUS;
					}
				};
				uiJob.addJobChangeListener(new IJobChangeListener() {

					@Override
					public void sleeping(IJobChangeEvent event) {
						// TODO Auto-generated method stub

					}

					@Override
					public void scheduled(IJobChangeEvent event) {
						// TODO Auto-generated method stub

					}

					@Override
					public void running(IJobChangeEvent event) {
						// TODO Auto-generated method stub

					}

					@Override
					public void done(IJobChangeEvent event) {
						// TODO Auto-generated method stub
						try {
							((TGPublishView) PlatformUI
									.getWorkbench()
									.getActiveWorkbenchWindow()
									.getActivePage()
									.showView(
											"info.textgrid.lab.tgpublish.client.gui.tgpview",
											null, IWorkbenchPage.VIEW_ACTIVATE))
									.refreshViewer();

							((TGPublishView) PlatformUI
									.getWorkbench()
									.getActiveWorkbenchWindow()
									.getActivePage()
									.showView(
											"info.textgrid.lab.tgpublish.client.gui.tgpview",
											null, IWorkbenchPage.VIEW_ACTIVATE))
									.proofIsReadyForPublishing();

							if (!simulateRun && !publishResponse.dryRun) {
								((TGPublishView) PlatformUI
										.getWorkbench()
										.getActiveWorkbenchWindow()
										.getActivePage()
										.showView(
												"info.textgrid.lab.tgpublish.client.gui.tgpview",
												null,
												IWorkbenchPage.VIEW_ACTIVATE))
										.forceTGPublishButtons();
							}
						} catch (PartInitException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void awake(IJobChangeEvent event) {
						// TODO Auto-generated method stub

					}

					@Override
					public void aboutToRun(IJobChangeEvent event) {
						// TODO Auto-generated method stub

					}
				});
				uiJob.setUser(true);
				uiJob.setName("...processing tgpublish");
				uiJob.schedule();

			}

			@Override
			public void awake(IJobChangeEvent event) {
				// TODO Auto-generated method stub

			}

			@Override
			public void aboutToRun(IJobChangeEvent event) {
				// TODO Auto-generated method stub

			}
		});
		job.setName("get result(s) from tgpublish...");
		job.schedule();
	}

	private void proofPublishStatus(String status) {
		if (status.equals("FAILED")) {
			this.publishStatusFailed = true;
			// System.out.println("1111"+publishStatusFailed);
			// TODO extract message for status ERROR and show it.
			List<?> statusListOfModules = publishClient.getStatus(uri)
					.getPublishStatus().getModules();
			for (Iterator<?> iterator = statusListOfModules.iterator(); iterator
					.hasNext();) {
				Module obj = (Module) iterator.next();

				List<?> modules = obj.getMessages();
				for (Iterator<?> iterator2 = modules.iterator(); iterator2
						.hasNext();) {
					String message = (String) iterator2.next();
					this.faildMessage += message;
					this.faildMessage += "\n";
				}
			}
		}
	}

	/**
	 * 
	 * @param contentTypeId
	 * @return
	 */
	public void testIfWorldReadableObject() {
		this.setWorldReadable(false);
		String contentTypeId = "";
		try {
			contentTypeId = this.tgObj.getContentTypeID();
		} catch (CoreException e) {
			Activator.handleError(e,
					"An error occured while getting the ContentType!");
		}
		// PublishClient pc = new PublishClient(tgPublishEndpoint,
		// RBACSession.getInstance().getSID(true));
		WorldReadableMimetypes wRMimeTypes = publishClient.getWorldReadables();
		ArrayList<String> contTypeList = (ArrayList<String>) wRMimeTypes
				.getWorldReadableList();
		for (Iterator<String> iterator = contTypeList.iterator(); iterator
				.hasNext();) {
			String wRcontType = iterator.next();
			if (contentTypeId.matches(wRcontType)) {
				// System.out.println("...match!");
				this.setWorldReadable(true);
			}
		}
	}

	/**
	 * call tgpuplish
	 */
	public void resetDataModel() {
		try {
			this.allData.clear();
		} catch (UnsupportedOperationException e) {
			Activator.handleError(e, "Couldn't clear the DataModel.");
		}
	}

	public void fillDataModelPublished(final String message) {
		resetDataModel();
		printResponse();
		String title = "";
		for (Iterator<PublishObject> iterator = publishResponse
				.getPublishObjects().iterator(); iterator.hasNext();) {
			PublishObject obj = iterator.next();
			// get URI
			String uri = obj.uri;
			if (obj.status.equals(StatusType.OK)) {
				TextGridObject tgo = null;
				try {
					tgo = TextGridObject.getInstance(
							new URI(uri), false);
					title = tgo.getTitle();
					// function not longer available TG-1500
					// tgo.setIsPublic();
				} catch (CrudServiceException e) {
					Activator.handleError(e,
							"An error occured while calling the CRUD-Service!");
				} catch (CoreException e) {
					Activator.handleError(e,
							"An error occured (CoreException)!");
				} catch (URISyntaxException e) {
					Activator.handleError(e, "An error occured (URI Syntax)!");
				} catch (NullPointerException e) {
					Activator.handleError(e,
							"An error occured (NullPointerException)!");
				}

				data = new EditionObjectDataModel(title, message, "", "", uri);
				allData.add(data);
				//Bug #9960
				reloadMetadata(tgo);
			} else if (obj.status.equals(StatusType.ERROR)) {
				Log("...it seems, something is wrong with tgpublish!");
			} else {
				// TODO
			}

		}
		// Forces decorator to mark the new public objects.
		UIJob uiJob2 = new UIJob("update Decorator Manager") {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				// TODO Auto-generated method stub
				if (monitor.isCanceled()) {
					isCanceld = true;
					return Status.CANCEL_STATUS;
				}
				PlatformUI
						.getWorkbench()
						.getDecoratorManager()
						.update("info.textgrid.lab.debug.decorators.TGOPublicDecorator");
				return Status.OK_STATUS;
			}
		};
		uiJob2.setName("update Decorator manager...");
		uiJob2.schedule();

	}

	/**
	 * 
	 */
	private void reloadMetadata(final TextGridObject tgo) {
		new Job("Reloading metadata...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					tgo.reloadMetadata(true);
					Log("----->  reloadMetadata:"+ tgo);
				} catch (CoreException e) {
					info.textgrid.lab.tgpublish.client.gui.Activator
							.handleWarning(e,
									"Couldn't reload the Metadata ot the textgrid object");
					return Status.CANCEL_STATUS;
				}
				return Status.OK_STATUS;
			}
		}.schedule();
	}

	public void fillDataModel() {
		String title = "";
		String uri = "";
		String message = "";
		String type = "";
		okCounts = 0;
		errorCounts = 0;
		warningCounts = 0;

		resetDataModel(); // make clear
		printResponse();
		for (Iterator<?> iterator = this.publishResponse.getPublishObjects()
				.iterator(); iterator.hasNext();) {
			PublishObject obj = (PublishObject) iterator.next();
			boolean merk = false;
			title = "";
			message = "";
			type = "";
			errorCounts += obj.getErrors().size();
			warningCounts += obj.getWarnings().size();
			// get URI
			try {
				uri = obj.uri;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				ErrorLog("uri" + uri);
				Activator.handleError(e1,
						"An error occured while filling the DataModel!");
			}

			// get Title
			// TODO getTitle()...
			try {
				title = TextGridObject.getInstance(new URI(uri), false)
						.getTitle();

			} catch (CrudServiceException e) {
				Activator
						.handleError(e,
								"An error occured while calling the CRUD-Service in <fillDataModel>!");
			} catch (CoreException e) {
				Activator.handleError(e,
						"An error occured (CoreException) in <fillDataModel>!");
			} catch (URISyntaxException e) {
				Activator.handleError(e,
						"An error occured (URI Syntax) in <fillDataModel>!");
			} catch (NullPointerException e) {
				Activator
						.handleError(e,
								"An error occured (NullPointerException) in <fillDataModel>!");
			} catch (InUIThreadException e) {
				Activator
						.handleError(e,
								"An error occured (UIThread-Exception) in <fillDataModel>!");
			} catch (Exception e) {
				Activator
						.handleError(e, "An error occured in <fillDataModel>!");
			} finally {
				if (title == null) {
					title = "";
				}
			}

			// add uri string to title
			if (title.equals("")) {
				title = "... (" + uri + " )";
			}

			// get errors
			// System.out.println("errors Count: "+obj.getErrors().size());
			for (Iterator<?> iteratorError = obj.getErrors().iterator(); iteratorError
					.hasNext();) {
				PublishError error = (PublishError) iteratorError.next();
				message = error.getMessage();
				type = error.getType().toString();

				data = new EditionObjectDataModel(title, message,/*
																 * ErrorMessage.
																 * getMessage
																 * (type)
																 */type, "red",
						uri);
				allData.add(data);
				merk = true;
			}

			// get warnings
			// System.out.println("warning Count: "+obj.getWarnings().size());
			for (Iterator iteratorWarnings = obj.getWarnings().iterator(); iteratorWarnings
					.hasNext();) {
				PublishWarning warnings = (PublishWarning) iteratorWarnings
						.next();
				message = warnings.getMessage();
				type = warnings.getType().toString();
				data = new EditionObjectDataModel(title, message, type,
						"yellow", uri);
				allData.add(data);
				merk = true;
			}

			if (!merk) {
				if (obj.status.toString().equals("ALREADY_PUBLISHED")) {
					data = new EditionObjectDataModel(title,
							"Object is already published.", "---", "info", uri);
				} else if (obj.status.toString().equals("OK")) {
					okCounts += 1;
					data = new EditionObjectDataModel(title, "ok", "---",
							"green", uri);
				} else if (obj.status.toString().equals("NOT_YET_PUBLISHED")) {
					data = new EditionObjectDataModel(title, "Not yet published, please try again.", "---",
							"info", uri);
				} else {
					data = new EditionObjectDataModel(title, "ok", "---",
							"green", uri);
				}

				allData.add(data);
			}
		}

		// proofIsReadyForPublishing();
	}

	private void printResponse() {
		if (showLogs) {
			try {
				JAXB.marshal(this.publishResponse, System.out);
			} catch (NullPointerException e) {
				// TODO: handle exception
				e.getStackTrace();
			} catch (Exception e) {
				Activator.handleError(e,
						"An error occured while marshalling object!");
			}
			Log("uri: " + this.uri);
			Log("sid: " + sid);
		}
	}

	/**
	 * proof if Object is ready for publishing
	 */
	public void proofIsReadyForPublishing() {
		Log("proofIsReadyForPublishing:--->" + statusInfo.toString());
		if (!isCanceld && !statusInfo.toString().equals("FAILED") && !statusInfo.toString().equals("NOT_QUEUED")) {
			if ((this.errorCounts == 0 && this.warningCounts == 0)
					&& !statusInfo.toString().equals("FAILED")) {
				this.isReadyForPublishing = true;
			} else if (this.errorCounts == 0
					&& this.publishStatusFailed == false) {
				setIgnoreWarnings(true);
				this.isReadyForPublishing = true;
			} else if (this.publishStatusFailed = false && this.errorCounts != 0) {
				this.isReadyForPublishing = false;
				// System.out.println("2222222"+publishStatusFailed);
			}
			// this.isReadyForPublishing = false;
		} else {
			this.isReadyForPublishing = false;
		}
	}

	public List<EditionObjectDataModel> getDataModel() {
		return this.allData;
	}

	/**
	 * 
	 * @param strArr
	 *            1. param is the action type 2. param is the XPATH term
	 * @return
	 */
	public Object callRightAction(String[] strArr) {
		String message = strArr[0];
		String xPath = strArr[1];

		switch (ActionError.valueOf(message)) {
		case NOT_SPECIFIED:
			// "Error not specified"
			return "...error not specified";
		case AUTH:
			// "Object not existing or authorized"
			return "...Object not existing or authorized.";
		case WRONG_CONTENT_TYPE:
			// Object is no Edition or Collection, or not one of the types
			// allowed for
			// setting worldReadable.
			return "...Object is no Edition or Collection... setting worldreadable.";
		case NO_PUBLISH_RIGHT:
			// "No right to publish object"
			//
			try {
				return call_COMPOSER_VIEW(message);
			} catch (PartInitException e) {
				Activator.handleError(e,
						"An error occured while calling the Composer View!");
			}
			return "...no right to publish object.";
		case METADATA_WARNINGS_EXIST :
			// required metadata field is missing.
			return call_METADATA_VIEW(xPath, message);

		case PID_GENERATION_FAILED:
			// No PID could be generated.
			return "...no PID could be generated.";
		case MISSING_METADATA:
			// required metadata field is missing.
			return call_METADATA_VIEW(xPath, message);
			// return "...required metadata field is missing.";
		case ALREADY_PUBLISHED:
			return "...the object is already published";
		case SERVER_ERROR:
			return "...Server Error occured.";
		default:
			return "???";
		}

	}

	/**
	 * 
	 * @param xpath
	 * @param messageType
	 * @return
	 */
	private Object call_METADATA_VIEW(String xpath, final String messageType) {
		MetaDataView metadataView;
		xpath = xpath.substring(6, xpath.length()).trim();
		Log("MDE  : " + xpath);
		try {
			metadataView = (MetaDataView) PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.showView(MetaDataView.ID);
			metadataView.triggerCheckField(xpath);
			// metadataView.setMetadataInView(tgObj, true);
			// TODO
			return messageType;
		} catch (PartInitException e) {
			Activator.handleError(e,
					"An error occured while calling MetaData-View!");
			return false;
		}
	}

	/**
	 * 
	 * @param message
	 * @return
	 * @throws PartInitException
	 */
	private Object call_COMPOSER_VIEW(String message) throws PartInitException {
		// -->EditAggregationHandler
		// TODO: the aggregation view is now an editor

		IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();
		try {
			AggregationComposerEditorInput input = new AggregationComposerEditorInput(
					tgObj);
			AggregationComposerEditor editor = (AggregationComposerEditor) activePage
					.openEditor(input,
							"info.textgrid.lab.core.aggregations.ui.editor");
		} catch (PartInitException e) {
			Activator.handleError(e,
					"An error occured while calling Composer-Editor!");
		} //$NON-NLS-1$

		return message;
	}

	/*
	 * create TGPublishClient
	 */
	public void setTGPublishClient() {
		try {
			publishClient = new PublishClient(ConfClient.getInstance()
					.getValue(ConfservClientConstants.TGPUBLISH), sid);
		} catch (OfflineException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"System offline, can not connect to TG-Publish.");
			Activator.getDefault().getLog().log(status);
		}

	}

	public void Log(String logString) {
		// Print Log-message
		if (showLogs) {
			System.out.println(logString);
		}
	}

	public void ErrorLog(String logString) {
		// Print Log-message
		if (showLogs) {
			System.err.println(logString);
			;
		}
	}

	@Override
	public String toString() {
		return "";
	}

	public void initialize() {
		this.allData.clear();
		this.isReadyForPublishing = false;
		this.isWorldReadable = false;
		this.ignoreWarnings = false;
		this.okCounts = 0;
		this.warningCounts = 0;
		this.errorCounts = 0;
		this.tgObj = null;
		this.uri = "";
	}

	public void toggleAll() {
		this.isReadyForPublishing = true;
	}

	public void test() {
		// Print OutPutStream of tgPublish Response
		JAXB.marshal(this.publishResponse, System.out);
		// Print Content of Status Attribut
		Log("\npublishStatus : "
				+ this.publishResponse.getPublishStatus().processStatus);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// get a sid:
		// https://textgridlab.org/WebAuthN/WebAuthN.php?authZinstance=textgrid-ws3.sub.uni-goettingen.de
		TGPublishController controller = TGPublishController.getInstance();
		@SuppressWarnings("unused")
		String sid = "";
		@SuppressWarnings("unused")
		String uri = "textgrid:2bd7.1";
		// PublishClient pc = new
		// PublishClient("http://textgrid-ws3.sub.uni-goettingen.de/tgpublish",
		// sid);
		// PublishResponse res = pc.publish(uri);
		controller.callTGPublishClient("Publish");
		controller.test();

		// JAXB.marshal(controller.getPublishRes(), System.out);
		// System.out.println("\npublishStatus : " +
		// controller.getPublishRes().status);
	}

}
