/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.tgpublish.client.gui.handler;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.tgpublish.client.gui.views.TGPublishView;
import info.textgrid.lab.ui.core.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * 
 * @author Leuk
 * 
 */
public class OpenTGPublishHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {

			Object obj = ((IStructuredSelection) selection).getFirstElement();

			if (obj instanceof IAdaptable) {
				TextGridObject tgo = (TextGridObject) ((IAdaptable) obj)
						.getAdapter(TextGridObject.class);
				try {
					// first reset the perspective...
					IWorkbench wb = PlatformUI.getWorkbench();
					wb.getActiveWorkbenchWindow().getActivePage().setPerspective(
							wb.getPerspectiveRegistry().findPerspectiveWithId(
									"info.textgrid.lab.tgpublish.client.gui.perspectives.TGPubPerspective"));

					// show it when necessary
					try {
						PlatformUI.getWorkbench().showPerspective(
								"info.textgrid.lab.tgpublish.client.gui.perspectives.TGPubPerspective",
								PlatformUI.getWorkbench().getActiveWorkbenchWindow());
					} catch (WorkbenchException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());
					
					((TGPublishView) PlatformUI
							.getWorkbench()
							.getActiveWorkbenchWindow()
							.getActivePage()
							.showView(
									"info.textgrid.lab.tgpublish.client.gui.tgpview",
									null, IWorkbenchPage.VIEW_ACTIVATE))
							.setTGPublishInView(tgo);

				} catch (PartInitException e) {
					Activator.handleError(e, "Couldn't open the TGPublish View!");
				}
			}
		}

		return null;

	}
}
