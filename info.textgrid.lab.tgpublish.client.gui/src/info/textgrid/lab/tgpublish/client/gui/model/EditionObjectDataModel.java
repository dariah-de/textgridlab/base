/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.tgpublish.client.gui.model;

import java.net.URI;
import java.net.URISyntaxException;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.runtime.IAdaptable;

/**
 * This class represents the object data modell of the used TableViewer
 * 
 * @author leuk
 * 
 */
public class EditionObjectDataModel implements IAdaptable {

	private String objName;
	private String status;
	private String actionInfo;
	private String statusColor;
	private String objUri;

	public EditionObjectDataModel() {
	}

	public EditionObjectDataModel(String objName, String status,
			String actionInfo, String statusColor, String objUri) {
		super();
		this.objName = objName;
		this.status = status;
		this.actionInfo = actionInfo;
		this.statusColor = statusColor;
		this.objUri = objUri;
	}

	public String getObjUri() {
		return objUri;
	}

	public String getObjName() {
		return objName;
	}

	public void setObjName(String objName) {
		this.objName = objName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActionInfo() {
		return actionInfo;
	}

	public void setActionInfo(String actionInfo) {
		this.actionInfo = actionInfo;
	}

	public String getStatusColor() {
		return statusColor;
	}

	public void setStatusColor(String statusColor) {
		this.statusColor = statusColor;
	}

	@Override
	public String toString() {
		return objName + " - " + status + " - " + actionInfo;
	}

	@Override
	public Object getAdapter(Class adapter) {
		if (adapter == TextGridObject.class) {
			try {
				return TextGridObject.getInstance(new URI(objUri), false);
			} catch (CrudServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
	}

}
