/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.tgpublish.client.gui.views;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.tgpublish.client.gui.controller.TGPublishController;
import info.textgrid.lab.tgpublish.client.gui.model.EditionObjectDataModel;
import info.textgrid.lab.tgpublish.client.gui.model.TGPublishContentProvider;
import info.textgrid.lab.ui.core.Activator;

import java.util.Calendar;
import java.util.NoSuchElementException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;

import com.swtdesigner.SWTResourceManager;

/**
 * View class for managing of publishable-objects.
 * 
 * @author Leuk
 */
public class TGPublishView extends ViewPart {

	private static final String OK_FOR_PUBLICATION = Messages.TGPublishView_OK_FOR_PUBLICATION;
	private static final String NOT_OK_FOR_PUBLICATION = Messages.TGPublishView_NOT_OK_FOR_PUBLICATION;
	private static final String SUCCESSFULL_PUBLISHED = Messages.TGPublishView_SUCCESSFULL_PUBLISHED;
	private static final String SUCCESSFULL_PUBLISHED_2 = Messages.TGPublishView_SUCCESSFULL_PUBLISHED_2;
	private static final String PROOF_FOR_PUBLICATION = Messages.TGPublishView_PROOF_FOR_PUBLICATION;
	private static final String NOT_REVERSABLE_ACTION = Messages.TGPublishView_NOT_REVERSABLE_ACTION;
	private static final String ACTIVATE_PUBLISH_INFO = Messages.TGPublishView_ACTIVATE_PUBLISH_INFO;
//	private static final String IGNORE_WARNINGS_TOOLTIP = "If you select this Button, warnings be ignored.";
	private static final String PUBLISH_STATUS_ERROR = Messages.TGPublishView_PUBLISH_STATUS_ERROR;
	private static final String PUBLISH_STATUS_ERROR_MESSAGE = Messages.TGPublishView_PUBLISH_STATUS_ERROR_MESSAGE;
	private TGPublishController controller = TGPublishController.getInstance();

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "info.textgrid.lab.tgpublish.client.gui.tgpview"; //$NON-NLS-1$

	private static final String COLLECTION = "Collection"; //$NON-NLS-1$
	private static final String EDITION = "Edition"; //$NON-NLS-1$

	private static final String TG_COLLECTION = "tg.collection"; //$NON-NLS-1$
	private static final String TG_EDITION = "tg.edition"; //$NON-NLS-1$

	private boolean ignoreWarnings = false;

	private String infoMessage = ""; //$NON-NLS-1$
	private TableViewer viewer;
	private Action action1;
	private Action action2;
	private Action doubleClickAction;

	private Composite top;
	private Label lblInfoMessage;
	private CLabel label_status_icon_error_count;
	private CLabel label_status_icon_error;
	private CLabel label_status_icon_warning;
	private CLabel label_status_icon_warning_count;
	private CLabel label_status_icon_ok_count;
	private CLabel label_status_icon_ok;
	private CLabel label_message_icon;
	private Button btnProof;
	private Button btnPublish;
	private Button btnCancel;
	private Label defaultLabel;

	/*
	 * The content provider class is responsible for providing objects to the
	 * view. It can wrap existing objects in adapters or simply return objects
	 * as-is. These objects may be sensitive to the current input of the view,
	 * or ignore it and always show the same content (like Task List, for
	 * example).
	 */

	class ViewContentProvider implements IStructuredContentProvider {
		@Override
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}

		@Override
		public void dispose() {
		}

		@Override
		public Object[] getElements(Object parent) {
			return new String[] { "One", "Two", "Three" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
	}

	class ViewLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		@Override
		public String getColumnText(Object obj, int index) {
			return getText(obj);
		}

		@Override
		public Image getColumnImage(Object obj, int index) {
			return getImage(obj);
		}

		@Override
		public Image getImage(Object obj) {
			return PlatformUI.getWorkbench().getSharedImages()
					.getImage(ISharedImages.IMG_OBJ_ELEMENT);
		}
	}

	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public TGPublishView() {
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(final Composite parent) {

		top = new Composite(parent, SWT.NONE);
		top.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				// System.out.println("--->"+"resetDataModel()");
				controller.resetDataModel();
			}
		});
		top.setLayout(new GridLayout(1, false));

		Composite composite_message = new Composite(top, SWT.NONE);
		composite_message.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		composite_message.setLayout(new GridLayout(2, false));
		GridData gd_composite_message = new GridData(SWT.FILL, SWT.CENTER,
				true, true, 1, 1);
		gd_composite_message.widthHint = 578;
		composite_message.setLayoutData(gd_composite_message);

		label_message_icon = new CLabel(composite_message, SWT.NONE);
		label_message_icon.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		GridData gd_label_message_icon = new GridData(SWT.CENTER, SWT.TOP,
				false, false, 1, 1);
		gd_label_message_icon.widthHint = 22;
		label_message_icon.setLayoutData(gd_label_message_icon);
		// label_message_icon.setImage(ResourceManager.getPluginImage(
		// "info.textgrid.lab.tgpublish.client.gui",
		// "icons/error.gif"));
//		this.label_message_icon
//				.setImage(info.textgrid.lab.tgpublish.client.gui.Activator
//						.getDefault()
//						.getImageRegistry()
//						.get(info.textgrid.lab.tgpublish.client.gui.Activator.ERROR_IMAGE_ID));
//		label_message_icon.setText("");
//		label_message_icon.setVisible(true);
		
		this.label_message_icon
		.setImage(info.textgrid.lab.tgpublish.client.gui.Activator
				.getDefault()
				.getImageRegistry()
				.get(info.textgrid.lab.tgpublish.client.gui.Activator.SYSTEMINFO16_IMAGE_ID));
		label_message_icon.setVisible(true);

		lblInfoMessage = new Label(composite_message, SWT.WRAP);
		lblInfoMessage.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		lblInfoMessage.setFont(SWTResourceManager.getFont("Tahoma", 10, //$NON-NLS-1$
				SWT.BOLD));
		GridData gd_lblEditionIsNot = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_lblEditionIsNot.widthHint = 526;
		lblInfoMessage.setLayoutData(gd_lblEditionIsNot);
		lblInfoMessage.setText(ACTIVATE_PUBLISH_INFO);
		lblInfoMessage.setImage(null);

		Label seperator_0 = new Label(top, SWT.SEPARATOR | SWT.HORIZONTAL);
		GridData gd_seperator_0 = new GridData(SWT.FILL, SWT.CENTER, true,
				true, 1, 1);
		gd_seperator_0.widthHint = 579;
		seperator_0.setLayoutData(gd_seperator_0);

		Composite composite_status = new Composite(top, SWT.NONE);
		GridData gd_composite_status = new GridData(SWT.FILL, SWT.CENTER, true,
				true, 1, 1);
		gd_composite_status.widthHint = 577;
		composite_status.setLayoutData(gd_composite_status);
		composite_status.setLayout(new GridLayout(18, false));

		CLabel lblStatus = new CLabel(composite_status, SWT.NONE);
		lblStatus.setText(Messages.TGPublishView_Status);

		label_status_icon_ok = new CLabel(composite_status, SWT.NONE);
		GridData gd_label_status_icon_ok = new GridData(SWT.FILL, SWT.CENTER,
				false, false, 1, 1);
		gd_label_status_icon_ok.widthHint = 22;
		label_status_icon_ok.setLayoutData(gd_label_status_icon_ok);
		label_status_icon_ok.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		// label_status_icon_ok.setImage(ResourceManager.getPluginImage(
		// "info.textgrid.lab.tgpublish.client.gui", "icons/green16.gif"));
		label_status_icon_ok
				.setImage(info.textgrid.lab.tgpublish.client.gui.Activator
						.getDefault()
						.getImageRegistry()
						.get(info.textgrid.lab.tgpublish.client.gui.Activator.GREEN16_IMAGE_ID));

		label_status_icon_ok_count = new CLabel(composite_status, SWT.NONE);
		// GridData gd_label_status_icon_ok_count = new GridData(SWT.FILL,
		// SWT.CENTER, false, false, 1, 1);
		// gd_label_status_icon_ok_count.widthHint = 46;
		label_status_icon_ok_count.setLayoutData(new GridData(SWT.FILL,
				SWT.CENTER, true, false, 1, 1));
		label_status_icon_ok_count
				.setText(new Integer(controller.getOKCounts()).toString()
						+ Messages.TGPublishView_OK);
		new Label(composite_status, SWT.NONE);
		new Label(composite_status, SWT.NONE);

		label_status_icon_warning = new CLabel(composite_status, SWT.NONE);
		GridData gd_label_status_icon_warning = new GridData(SWT.FILL,
				SWT.CENTER, false, false, 1, 1);
		gd_label_status_icon_warning.widthHint = 22;
		label_status_icon_warning.setLayoutData(gd_label_status_icon_warning);
		label_status_icon_warning.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		// label_status_icon_warning.setImage(ResourceManager.getPluginImage(
		// "info.textgrid.lab.tgpublish.client.gui", "icons/yellow.gif"));
		label_status_icon_warning
				.setImage(info.textgrid.lab.tgpublish.client.gui.Activator
						.getDefault()
						.getImageRegistry()
						.get(info.textgrid.lab.tgpublish.client.gui.Activator.YELLOW_IMAGE_ID));

		label_status_icon_warning_count = new CLabel(composite_status, SWT.NONE);
		label_status_icon_warning_count.setLayoutData(new GridData(SWT.FILL,
				SWT.CENTER, true, false, 1, 1));
		label_status_icon_warning_count.setText(new Integer(controller
				.getWarningCounts()).toString() + Messages.TGPublishView_Warning);

		new Label(composite_status, SWT.NONE);
		new Label(composite_status, SWT.NONE);

		label_status_icon_error = new CLabel(composite_status, SWT.NONE);
		GridData gd_label_status_icon_error = new GridData(SWT.FILL,
				SWT.CENTER, false, false, 1, 1);
		gd_label_status_icon_error.widthHint = 22;
		label_status_icon_error.setLayoutData(gd_label_status_icon_error);
		label_status_icon_error.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		// label_status_icon_error.setImage(ResourceManager.getPluginImage(
		// "info.textgrid.lab.tgpublish.client.gui", "icons/red16.gif"));
		label_status_icon_error
				.setImage(info.textgrid.lab.tgpublish.client.gui.Activator
						.getDefault()
						.getImageRegistry()
						.get(info.textgrid.lab.tgpublish.client.gui.Activator.RED16_IMAGE_ID));
		label_status_icon_error.setText(""); //$NON-NLS-1$

		label_status_icon_error_count = new CLabel(composite_status, SWT.NONE);
		label_status_icon_error_count.setLayoutData(new GridData(SWT.FILL,
				SWT.CENTER, true, false, 1, 1));
		label_status_icon_error_count.setText(new Integer(controller
				.getErrorCounts()).toString() + Messages.TGPublishView_Error);
		new Label(composite_status, SWT.NONE);
		new Label(composite_status, SWT.NONE);
		new Label(composite_status, SWT.NONE);
		new Label(composite_status, SWT.NONE);
		new Label(composite_status, SWT.NONE);
		new Label(composite_status, SWT.NONE);
		new Label(composite_status, SWT.NONE);

//		final Button btnIgnoreWarnings = new Button(composite_status, SWT.CHECK);
//		btnIgnoreWarnings.setToolTipText(IGNORE_WARNINGS_TOOLTIP);
//		btnIgnoreWarnings.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				if (btnIgnoreWarnings.getSelection()) {
//					// System.out.println("--->checked");
//					ignoreWarnings = true;
//				} else {
//					ignoreWarnings = false;
//				}
//				controller.setIgnoreWarnings(ignoreWarnings);
//			}
//		});
//		btnIgnoreWarnings.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
//				true, false, 1, 1));
//		btnIgnoreWarnings.setText("Ignore Warnings");

		Label seperator_1 = new Label(top, SWT.SEPARATOR | SWT.HORIZONTAL);
		GridData gd_seperator_1 = new GridData(SWT.FILL, SWT.CENTER, true,
				true, 1, 1);
		gd_seperator_1.widthHint = 579;
		seperator_1.setLayoutData(gd_seperator_1);
		viewer = new TableViewer(top, SWT.FULL_SELECTION);
		Table table = viewer.getTable();
//		table.addPaintListener(new PaintListener() {
//			public void paintControl(PaintEvent e) {
				// System.out.println("paintControl: "+controller.getDataModel().iterator().next().getStatus());
//				try {
//					if (!controller.getDataModel().isEmpty()) {
//						if (controller
//								.getDataModel()
//								.iterator()
//								.next()
//								.getStatus()
//								.equals(TGPublishController
//										.getSuccessfullPublished())) {
//							setInfoMessagePublished();
//						}
//					}
//				} catch (NoSuchElementException e2) {
//					// TODO: handle exception
//					controller.ErrorLog(e2.toString());
//				} catch (Exception e2) {
//					// TODO: handle exception
//					controller.ErrorLog(e2.toString());
//				}
//			}
//		});
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		GridData gd_table = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		gd_table.heightHint = 297;
		gd_table.widthHint = 562;
		table.setLayoutData(gd_table);

		TableViewerColumn tableViewerColumn = new TableViewerColumn(viewer,
				SWT.NONE);
		TableColumn tableColumnName = tableViewerColumn.getColumn();
		tableColumnName.setWidth(150);
		tableColumnName.setText(Messages.TGPublishView_TableColumn_Name);
		// LabelProvider fuer jede Spalte setzen
		tableViewerColumn.setLabelProvider(new CellLabelProvider() {
			@Override
			public void update(ViewerCell cell) {
				Color bg = null;
				Color fg = SWTResourceManager.getColor(SWT.COLOR_BLACK);
				cell.setText(((EditionObjectDataModel) cell.getElement())
						.getObjName());
				if (((EditionObjectDataModel) cell.getElement())
						.getStatusColor() == "red") { //$NON-NLS-1$
					bg = SWTResourceManager.getColor(SWT.COLOR_RED);
					fg = SWTResourceManager.getColor(SWT.COLOR_WHITE);
				} else if (((EditionObjectDataModel) cell.getElement())
						.getStatusColor() == "yellow") { //$NON-NLS-1$
					bg = SWTResourceManager.getColor(SWT.COLOR_YELLOW);
				} else if (((EditionObjectDataModel) cell.getElement())
						.getStatusColor() == "info") { //$NON-NLS-1$
					bg = SWTResourceManager.getColor(SWT.COLOR_GRAY);
				} else {
					bg = SWTResourceManager.getColor(SWT.COLOR_GREEN);
				}
				cell.setBackground(bg);
				cell.setForeground(fg);
			}
		});

		TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(viewer,
				SWT.NONE);
		TableColumn tableColumnStatus = tableViewerColumn_1.getColumn();
		tableColumnStatus.setWidth(300);
		tableColumnStatus.setText(Messages.TGPublishView_TableColumn_Status);
		// LabelProvider fuer jede Spalte setzen
		tableViewerColumn_1.setLabelProvider(new CellLabelProvider() {
			@Override
			public void update(ViewerCell cell) {
				cell.setText(((EditionObjectDataModel) cell.getElement())
						.getStatus());
			}
		});

		final TableViewerColumn tableViewerColumn_2 = new TableViewerColumn(
				viewer, SWT.NONE);
		TableColumn tableColumnAction = tableViewerColumn_2.getColumn();
		tableColumnAction.setWidth(119);
		tableColumnAction.setText(Messages.TGPublishView_TableColumn_Actions);

		// LabelProvider fuer jede Spalte setzen
		tableViewerColumn_2.setLabelProvider(new CellLabelProvider() {
			@Override
			public void update(ViewerCell cell) {
				cell.setText(((EditionObjectDataModel) cell.getElement())
						.getActionInfo());
			}
		});

		// -->http://www.ralfebert.de/rcpbuch/jface_tableviewer/
		tableViewerColumn_2.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected boolean canEdit(Object element) {
				// System.out.println("element: "+"#"+((EditionObjectDataModel)element).getActionInfo()+"#");
				if (((EditionObjectDataModel) element).getActionInfo().equals("---")
						|| ((EditionObjectDataModel) element).getActionInfo()
								.equals("") //$NON-NLS-1$
						|| ((EditionObjectDataModel) element).getActionInfo()
								.equals("WRONG_CONTENT_TYPE") //$NON-NLS-1$
//						|| ((EditionObjectDataModel) element).getActionInfo()
//								.equals("NO_PUBLISH_RIGHT")
						|| ((EditionObjectDataModel) element).getActionInfo()
								.equals("PID_GENERATION_FAILED") //$NON-NLS-1$
						|| ((EditionObjectDataModel) element).getActionInfo()
								.equals("AUTH") //$NON-NLS-1$
						|| ((EditionObjectDataModel) element).getActionInfo()
								.equals("ALREADY_PUBLISHED") //$NON-NLS-1$
						|| ((EditionObjectDataModel) element).getActionInfo()
								.equals("NOT_SPECIFIED")) { //$NON-NLS-1$
					return false;
				}
				return true;
			}

			@Override
			protected CellEditor getCellEditor(final Object element) {
				// return new TextCellEditor(viewer.getTable());
				return new DialogCellEditor(viewer.getTable()) {

					/**
					 * Creates the button for this cell editor under the given
					 * parent control.
					 * <p>
					 * The default implementation of this framework method
					 * creates the button display on the right hand side of the
					 * dialog cell editor. Subclasses may extend or reimplement.
					 * </p>
					 * -->
					 * http://www.javadocexamples.com/java_source/org/eclipse
					 * /jface/viewers/DialogCellEditor.java.html
					 * 
					 * @param parent
					 *            the parent control
					 * @return the new button control
					 */
					@Override
					protected Button createButton(Composite parent) {
						Button result = new Button(parent, SWT.DOWN);
						result.setToolTipText(Messages.TGPublishView_CallAction);
						result.setText("...call Action"); //$NON-NLS-1$
						return result;
					}

					/**
					 * Creates the controls used to show the value of this cell
					 * editor.
					 * <p>
					 * The default implementation of this framework method
					 * creates a label widget, using the same font and
					 * background color as the parent control.
					 * </p>
					 * <p>
					 * Subclasses may reimplement. If you reimplement this
					 * method, you should also reimplement
					 * <code>updateContents</code>.
					 * </p>
					 * 
					 * @param cell
					 *            the control for this cell editor
					 * @return the underlying control
					 */
					@Override
					protected Control createContents(Composite cell) {
						defaultLabel = new Label(cell, SWT.LEFT);
						defaultLabel.setFont(cell.getFont());
						defaultLabel.setBackground(cell.getBackground());
						return defaultLabel;
					}

					/**
					 * Updates the controls showing the value of this cell
					 * editor.
					 * <p>
					 * The default implementation of this framework method just
					 * converts the passed object to a string using
					 * <code>toString</code> and sets this as the text of the
					 * label widget.
					 * </p>
					 * <p>
					 * Subclasses may reimplement. If you reimplement this
					 * method, you should also reimplement
					 * <code>createContents</code>.
					 * </p>
					 * 
					 * @param value
					 *            the new value of this cell editor
					 */


					@Override
					protected Object openDialogBox(Control cellEditorWindow) {
						// TODO Auto-generated method stub
						String[] arr = new String[2];
						try {
							arr[0] = ((EditionObjectDataModel) element)
									.getActionInfo();
							arr[1] = ((EditionObjectDataModel) element)
									.getStatus();
							return controller.callRightAction(arr);
						} catch (NoSuchElementException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return null;
					}
				};

			}

			@Override
			protected Object getValue(Object element) {
				return ((EditionObjectDataModel) element).getActionInfo();
			}

			@Override
			protected void setValue(Object element, Object value) {
				((EditionObjectDataModel) element).setActionInfo(String
						.valueOf(value));
				viewer.refresh(element);
			}

		});

		Label seperator_2 = new Label(top, SWT.SEPARATOR | SWT.HORIZONTAL);
		GridData gd_seperator_2 = new GridData(SWT.FILL, SWT.CENTER, true,
				true, 1, 1);
		gd_seperator_2.widthHint = 579;
		seperator_2.setLayoutData(gd_seperator_2);

		Composite composite_button = new Composite(top, SWT.NONE);
		composite_button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				true, 1, 1));
		composite_button.setLayout(new GridLayout(13, false));

		Label lblInfo = new Label(composite_button, SWT.NONE);
		lblInfo.setImage(SWTResourceManager.getImage(TGPublishView.class,
				"/org/eclipse/jface/dialogs/images/help.gif")); //$NON-NLS-1$
		lblInfo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				HelpDialog helpDialog = new HelpDialog(PlatformUI
						.getWorkbench().getDisplay().getActiveShell());
				helpDialog.open();
			}
		});
		GridData gd_lblInfo = new GridData(SWT.CENTER, SWT.FILL, false, false,
				1, 1);
		gd_lblInfo.widthHint = 28;
		lblInfo.setLayoutData(gd_lblInfo);

		final ProgressBar progressBar = new ProgressBar(composite_button,
				SWT.NONE);
		GridData gd_progressBar = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		gd_progressBar.widthHint = 53;
		progressBar.setLayoutData(gd_progressBar);
		gd_progressBar.grabExcessVerticalSpace = true;
		progressBar.setLayoutData(gd_progressBar);
		progressBar.setVisible(false);
		// progressBar.setMinimum(0);
		// progressBar.setMaximum(30);
		// Start the first progress bar

		new Label(composite_button, SWT.NONE);
		new Label(composite_button, SWT.NONE);
		new Label(composite_button, SWT.NONE);

		btnProof = new Button(composite_button, SWT.NONE);
		btnProof.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				UIJob uiJob = new UIJob("") { //$NON-NLS-1$
					
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						// TODO Auto-generated method stub
						if (monitor.isCanceled()) {
							controller.setIgnoreWarnings(false);
							btnProof.setEnabled(false);
							top.setCursor(
									new Cursor(PlatformUI.getWorkbench().getDisplay(), SWT.CURSOR_ARROW));
							setTGPublishInView(controller.getTGObj());
							controller.getJob().cancel();
							return Status.CANCEL_STATUS;
						}
						callTGPublishTryRun();

						return Status.OK_STATUS;
					}
				};
				uiJob.addJobChangeListener(new IJobChangeListener() {
					
					@Override
					public void sleeping(IJobChangeEvent event) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void scheduled(IJobChangeEvent event) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void running(IJobChangeEvent event) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void done(IJobChangeEvent event) {
						// TODO Auto-generated method stub
//						callTGPublish();
						controller.setIgnoreWarnings(false);
						btnProof.setEnabled(false);
						top.setCursor(
								new Cursor(PlatformUI.getWorkbench().getDisplay(), SWT.CURSOR_ARROW));
					}
					
					@Override
					public void awake(IJobChangeEvent event) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void aboutToRun(IJobChangeEvent event) {
						// TODO Auto-generated method stub
						
					}
				});
				uiJob.setName("...tgpublish"); //$NON-NLS-1$
				uiJob.schedule();

				btnProof.setEnabled(true);
				setInfoMessage(controller.isReadyForPublishing);
				top.setCursor(
						new Cursor(PlatformUI.getWorkbench().getDisplay(), SWT.CURSOR_ARROW));
			}

			private void callTGPublishTryRun() {
				if (controller.isWorldReadable) {
					controller.callTGPublishClient("WordReadableSimulate"); //$NON-NLS-1$
				} else if (ignoreWarnings) {
					controller.callTGPublishClient("IgnoreWarnings"); //$NON-NLS-1$
				} else {
					controller.callTGPublishClient("Simulate"); //$NON-NLS-1$
				}
			}
		
		});
		btnProof.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				
			}
		});


		
		btnProof.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
				4, 1));
		btnProof.setText(Messages.TGPublishView_BtnProof);
		this.btnProof.setEnabled(false);

		new Label(composite_button, SWT.NONE);

		btnPublish = new Button(composite_button, SWT.NONE);
		btnPublish.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
			}
		});
		btnPublish.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				// #####################################################
				// START
				//

				// Forces decorator to mark the new public objects.
				boolean tgObjIsAlreadyPublished = updateNavigatorTGOPublicDecorator();
				String messageBoxText = ""; //$NON-NLS-1$
				String ignoreWarningMessage = ""; //$NON-NLS-1$
				// System.out.println("tgObjIsAlreadyPublished:  "+tgObjIsAlreadyPublished);
				if (!tgObjIsAlreadyPublished) {
					int style = SWT.ICON_WARNING | SWT.OK | SWT.CANCEL;
					MessageBox messageBox = new MessageBox(PlatformUI
							.getWorkbench().getDisplay().getActiveShell(),
							style);
					if (controller.ignoreWarnings) {
						System.out.println("ignoreWarnings: "+controller.ignoreWarnings); //$NON-NLS-1$
						ignoreWarningMessage = Messages.TGPublishView_IgnoreWarnings;
						messageBoxText+=ignoreWarningMessage;
					}
					messageBoxText += Messages.TGPublishView_NotReversible;
					messageBox.setText(Messages.TGPublishView_PublishWarning);
					messageBox
							.setMessage(messageBoxText);

					int rc = messageBox.open();
					switch (rc) {
					case SWT.OK:
//						top.setCursor(
//								new Cursor(PlatformUI.getWorkbench().getDisplay(), SWT.CURSOR_WAIT));
//						callTGPublish();
//						
//						forceTGPublishButtons();
//						
//						top.setCursor(
//								new Cursor(PlatformUI.getWorkbench().getDisplay(), SWT.CURSOR_ARROW));

						UIJob uiJob = new UIJob("") { //$NON-NLS-1$
							
							@Override
							public IStatus runInUIThread(IProgressMonitor monitor) {
								// TODO Auto-generated method stub
								if (monitor.isCanceled()) {
									controller.setIgnoreWarnings(false);
									btnProof.setEnabled(false);
									top.setCursor(
											new Cursor(PlatformUI.getWorkbench().getDisplay(), SWT.CURSOR_ARROW));
									setTGPublishInView(controller.getTGObj());
									controller.getJob().cancel();
									return Status.CANCEL_STATUS;
								}
								callTGPublish();

								return Status.OK_STATUS;
							}
						};
						uiJob.addJobChangeListener(new IJobChangeListener() {
							
							@Override
							public void sleeping(IJobChangeEvent event) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void scheduled(IJobChangeEvent event) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void running(IJobChangeEvent event) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void done(IJobChangeEvent event) {
								// TODO Auto-generated method stub
//								callTGPublish();
								controller.setIgnoreWarnings(false);
								btnProof.setEnabled(false);
								btnPublish.setEnabled(false);
								top.setCursor(
										new Cursor(PlatformUI.getWorkbench().getDisplay(), SWT.CURSOR_ARROW));
							}
							
							@Override
							public void awake(IJobChangeEvent event) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void aboutToRun(IJobChangeEvent event) {
								// TODO Auto-generated method stub
								
							}
						});
						uiJob.setName(Messages.TGPublishView_JobName_1);
						uiJob.schedule();

						btnProof.setEnabled(true);
						setInfoMessage(controller.isReadyForPublishing);
						top.setCursor(
								new Cursor(PlatformUI.getWorkbench().getDisplay(), SWT.CURSOR_ARROW));
					
						
						
						
//						break;
					case SWT.CANCEL:
						break;
					}
				} else {
					setInfoMessagePublished_2();
					btnCancel.setEnabled(false);
					btnPublish.setEnabled(false);
					controller.fillDataModelPublished(SUCCESSFULL_PUBLISHED_2);
					viewer.refresh();
				}

			}

			private void callTGPublish() {
				if (controller.isWorldReadable) {
					controller.callTGPublishClient("WorldReadable"); //$NON-NLS-1$
				} else if (controller.ignoreWarnings) {
					controller.callTGPublishClient("IgnoreWarnings"); //$NON-NLS-1$
				} else {
					controller.callTGPublishClient("Publish"); //$NON-NLS-1$
				}
			}

		});
		
		btnPublish.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
				1, 1));
		btnPublish.setEnabled(false);
		btnPublish.setText(Messages.TGPublishView_BtnPublish);
		btnPublish.setToolTipText(NOT_REVERSABLE_ACTION);
		new Label(composite_button, SWT.NONE);

		btnCancel = new Button(composite_button, SWT.NONE);
		btnCancel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
			}
		});
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// progressBar.setSelection(0);
				// progressBar.setVisible(false);
				controller.getJob().cancel();
				setTGPublishInView(controller.getTGObj());
			}
		});
		btnCancel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
				1, 1));
		btnCancel.setText(Messages.TGPublishView_BtnCancel);
		this.btnCancel.setEnabled(false);

		// viewer.setContentProvider(new ViewContentProvider());
		// viewer.setLabelProvider(new ViewLabelProvider());

		// **************************
		// ArrayContentProvider kann verwendet werden, da Input-Objekt eine Java
		// Collection ist
		viewer.setContentProvider(ArrayContentProvider.getInstance());

		// **************************

		viewer.setSorter(new NameSorter());
		// viewer.setInput(getViewSite());
		viewer.setInput(TGPublishContentProvider.getInstance().getAllData());

		// Create the help context id for the viewer's control
//		PlatformUI
//				.getWorkbench()
//				.getHelpSystem()
//				.setHelp(viewer.getControl(),
//						"info.textgrid.lab.tgpublish.ui2.viewer");
		makeActions();
		// hookContextMenu();
		// hookDoubleClickAction();
		// contributeToActionBars();

		getSite().setSelectionProvider(viewer);
		try {
			controller.Log("xxxxxxxxx:" //$NON-NLS-1$
					+ ((EditionObjectDataModel) (viewer).getSelection())
							.getObjUri());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "info.textgrid.lab.tgpublish.client.gui.PublishView"); //$NON-NLS-1$
		
		
//		if (controller.getTGObj() == null) {
//			lblInfoMessage.setText(ACTIVATE_PUBLISH_INFO);
//		}
//		lblInfoMessage.setText("");
		lblInfoMessage.setText(ACTIVATE_PUBLISH_INFO);
		
	}


	/*
	 * 
	 */
	private boolean updateNavigatorTGOPublicDecorator() {
		boolean tgObjIsAlreadyPublished = false;

		// Forces decorator to mark the new public objects.
		PlatformUI
				.getWorkbench()
				.getDecoratorManager()
				.update("info.textgrid.lab.debug.decorators.TGOPublicDecorator"); //$NON-NLS-1$

		try {
			// controller.getTGObj().refreshWorkspaceIfNeccessary();
			reloadMetadata(controller.getTGObj());
			tgObjIsAlreadyPublished = controller.getTGObj().isPublic();
			// System.out.println("tgObjIsAlreadyPublished ???: "+tgObjIsAlreadyPublished);
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return tgObjIsAlreadyPublished;
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu"); //$NON-NLS-1$
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(IMenuManager manager) {
				TGPublishView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(action1);
		manager.add(new Separator());
		manager.add(action2);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(action1);
		manager.add(action2);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(action1);
		manager.add(action2);
	}

	private void makeActions() {
		action1 = new Action() {
			@Override
			public void run() {
				showMessage("Action 1 executed"); //$NON-NLS-1$
			}
		};
		action1.setText("Action 1"); //$NON-NLS-1$
		action1.setToolTipText("Action 1 tooltip"); //$NON-NLS-1$
		action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));

		action2 = new Action() {
			@Override
			public void run() {
				showMessage("Action 2 executed"); //$NON-NLS-1$
			}
		};
		action2.setText("Action 2"); //$NON-NLS-1$
		action2.setToolTipText("Action 2 tooltip"); //$NON-NLS-1$
		action2.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));

		doubleClickAction = new Action() {
			@Override
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection)
						.getFirstElement();
				showMessage("Double-click detected on \n\n" + "Name \t:" //$NON-NLS-1$ //$NON-NLS-2$
						+ ((EditionObjectDataModel) obj).getObjName() + "\n" //$NON-NLS-1$
						+ "Status\t:" //$NON-NLS-1$
						+ ((EditionObjectDataModel) obj).getStatus() + "\n" //$NON-NLS-1$
						+ "Action\t:" //$NON-NLS-1$
						+ ((EditionObjectDataModel) obj).getActionInfo());
			}
		};
	}
	
	/**
	 * 
	 */
	public void forceTGPublishButtons() {
		btnCancel.setEnabled(false);
		btnPublish.setEnabled(false);
		btnProof.setEnabled(false);
		controller.Log("forceTGPublishButtons: -->"+controller.getPublishResponse().dryRun); //$NON-NLS-1$
		if (!controller.getPublishResponse().dryRun && controller.statusInfo.toString().equals("FINISHED")) { //$NON-NLS-1$
			setInfoMessagePublished();
			controller.fillDataModelPublished(SUCCESSFULL_PUBLISHED);
		} else {
			controller.fillDataModel();
		}
		
		viewer.refresh();
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(),
				Messages.TGPublishView_TGPublishView, message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
//		viewer.getControl().setFocus();
		if(top != null) 
			top.setFocus();
	}
	
	/**
	 * 
	 */
	public void refreshViewer() {
		this.viewer.refresh();
	}


	private void setInfoMessage(TextGridObject tgo) {
		if (tgo != null) {
			try {
				if (tgo.getContentTypeID().contains(TG_EDITION)) {
					this.infoMessage = EDITION;
				} else if (tgo.getContentTypeID().contains(TG_COLLECTION)) {
					this.infoMessage = COLLECTION;
				} else {
					this.infoMessage = Messages.TGPublishView_TechnicalObject;
				}
				String title = tgo.getTitle();
				title = title.replaceAll("&", "&&"); //$NON-NLS-1$ //$NON-NLS-2$
				//			lblInfoMessage.setText("The " + infoMessage + " \"" + title + "\""
				//					+ PROOF_FOR_PUBLICATION);
				lblInfoMessage.setText(NLS.bind(Messages.TGPublishView_The
						+ PROOF_FOR_PUBLICATION, infoMessage, title));

			} catch (CoreException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				Activator.handleError(e,
						Messages.TGPublishView_NoTGOContent);
			}
		} else {
			lblInfoMessage.setText(ACTIVATE_PUBLISH_INFO);
		}
			
	}

	/**
	 * 
	 * @param ready4Publishing
	 */
	private void setInfoMessage(boolean ready4Publishing) {
		try {
			if (ready4Publishing) {
				String title = controller.getTGObj().getTitle();
				title = title.replaceAll("&", "&&"); //$NON-NLS-1$ //$NON-NLS-2$
//				lblInfoMessage.setText("The " + infoMessage + " \"" + title
// + "\"" + OK_FOR_PUBLICATION + getPidWarning());
				lblInfoMessage.setText(NLS.bind(Messages.TGPublishView_The + OK_FOR_PUBLICATION + getPidWarning(), infoMessage, title)); 
			} else {
				String title = controller.getTGObj().getTitle();
				title = title.replaceAll("&", "&&"); //$NON-NLS-1$ //$NON-NLS-2$
//				lblInfoMessage.setText("The " + infoMessage + " \"" + title
// + "\"" + NOT_OK_FOR_PUBLICATION + getPidWarning());
				lblInfoMessage.setText(NLS.bind(Messages.TGPublishView_The+ NOT_OK_FOR_PUBLICATION + getPidWarning(), infoMessage, title)); 
				if (controller.publishStatusFailed) {
					int style = SWT.ICON_ERROR | SWT.OK;
					MessageBox messageBox = new MessageBox(PlatformUI
							.getWorkbench().getDisplay().getActiveShell(),
							style);
					messageBox.setText(PUBLISH_STATUS_ERROR);
					messageBox
							.setMessage(controller.getFaildMessage() + "\n" + PUBLISH_STATUS_ERROR_MESSAGE); //$NON-NLS-1$
//					int rc = messageBox.open();
				}
				
			}
			top.layout();

		} catch (CoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Activator.handleError(e, "Couldn't read textgrid object content."); //$NON-NLS-1$
		}
	}

	/*
	 * 
	 */
	private void setInfoMessagePublished() {
		if (!lblInfoMessage.isDisposed()) {
			try {
				String title = controller.getTGObj().getTitle();
				title = title.replaceAll("&", "&&"); //$NON-NLS-1$ //$NON-NLS-2$
//				lblInfoMessage.setText("The " + infoMessage + " \"" + title
//						+ "\"" + SUCCESSFULL_PUBLISHED);
				lblInfoMessage.setText(NLS.bind(Messages.TGPublishView_The+ SUCCESSFULL_PUBLISHED, infoMessage, title)); 

			} catch (CoreException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				Activator.handleError(e,
						"Couldn't read textgrid object content."); //$NON-NLS-1$
			}
		}
	}

	/*
	 * 
	 */
	private void setInfoMessagePublished_2() {
		if (!lblInfoMessage.isDisposed()) {
			try {
				String title = controller.getTGObj().getTitle();
				title = title.replaceAll("&", "&&"); //$NON-NLS-1$ //$NON-NLS-2$
//				lblInfoMessage.setText("The " + infoMessage + " \"" + title
//						+ "\"" + SUCCESSFULL_PUBLISHED_2);
				lblInfoMessage.setText(NLS.bind(Messages.TGPublishView_The+ SUCCESSFULL_PUBLISHED_2, infoMessage, title)); 

			} catch (CoreException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				Activator.handleError(e,
						Messages.TGPublishView_NoTGOContent);
			}
		}
	}

	/**
	 * update the message counts
	 */
	public void updateMessageCounts() {
		this.label_status_icon_ok_count.setText(new Integer(controller
				.getOKCounts()).toString() + Messages.TGPublishView_OKs);
		this.label_status_icon_error_count.setText(new Integer(controller
				.getErrorCounts()).toString() + Messages.TGPublishView_ERRORs);
		this.label_status_icon_warning_count.setText(new Integer(controller
				.getWarningCounts()).toString() + Messages.TGPublishView_WARNINGs);
	}

	/**
	 * proof if Object is ready for publishing
	 */
	public void proofIsReadyForPublishing() {
		String title = ""; //$NON-NLS-1$
		try {
			if (controller.statusInfo.toString().equals("FINISHED") || !controller.isCanceld || controller.isReadyForPublishing) { //$NON-NLS-1$
				try {
					title = controller.getTGObj().getTitle();
					title = title.replaceAll("&", "&&"); //$NON-NLS-1$ //$NON-NLS-2$
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// this.label_message_icon.setImage(ResourceManager.getPluginImage(
				// "info.textgrid.lab.tgpublish.client.gui",
				// "icons/systeminfo16.gif"));
				this.label_message_icon
						.setImage(info.textgrid.lab.tgpublish.client.gui.Activator
								.getDefault()
								.getImageRegistry()
								.get(info.textgrid.lab.tgpublish.client.gui.Activator.SYSTEMINFO16_IMAGE_ID));

				if (controller.isReadyForPublishing && !controller.isCanceld) {
					this.btnProof.setEnabled(false);
					this.btnPublish.setEnabled(true);
//					lblInfoMessage.setText("The " + infoMessage + " \"" + title + "\""
// + OK_FOR_PUBLICATION + getPidWarning());
					lblInfoMessage.setText(NLS.bind(Messages.TGPublishView_The+ OK_FOR_PUBLICATION + getPidWarning(), infoMessage, title)); 
				} else {
					this.btnProof.setEnabled(true);
					this.btnPublish.setEnabled(false);
//					lblInfoMessage.setText("The " + infoMessage + " \"" + title + "\""
// + NOT_OK_FOR_PUBLICATION + getPidWarning());
					lblInfoMessage.setText(NLS.bind(Messages.TGPublishView_The+ NOT_OK_FOR_PUBLICATION + getPidWarning(), infoMessage, title)); 
				}
				

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Activator.handleError(e, Messages.TGPublishView_NoStatusInfo);
		}
	}

	private String getPidWarning() {
		// FIXME remove if no longer applicable
		final String pidWarning;
		Calendar now = Calendar.getInstance();
		Calendar deadline = Calendar.getInstance();
		deadline.set(2011, 7, 25);
		deadline.getTime();
		if (now.before(deadline)) {
			pidWarning = "\nAttention: Because TG-publish is a very new component, it needs further testing before we declare it finished. So you may need to publish this again in the end of August 2011. Please check back on www.textgrid.de."; //$NON-NLS-1$
		} else
			pidWarning = ""; //$NON-NLS-1$
		return pidWarning;
	}

	/**
	 * 
	 * @param tgo
	 */
	private void reloadMetadata(final TextGridObject tgo) {
		new Job(Messages.TGPublishView_ReloadingMetadata) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					tgo.reloadMetadata(true);
				} catch (CoreException e) {

					info.textgrid.lab.tgpublish.client.gui.Activator
							.handleWarning(e,
									Messages.TGPublishView_NoMetadata);
					return Status.CANCEL_STATUS;
				}
				return Status.OK_STATUS;
			}
		}.schedule();
	}

	/**
	 * 
	 */
	private void initializeButton() {
		// this.label_message_icon.setImage(ResourceManager
		// .getPluginImage("info.textgrid.lab.tgpublish.client.gui",
		// "icons/warning16.gif"));
		this.label_message_icon
				.setImage(info.textgrid.lab.tgpublish.client.gui.Activator
						.getDefault()
						.getImageRegistry()
						.get(info.textgrid.lab.tgpublish.client.gui.Activator.WARNING16_IMAGE_ID));
		label_message_icon.setVisible(true);
		this.btnProof.setEnabled(true);
		this.btnPublish.setEnabled(false);
		this.btnCancel.setEnabled(true);
	}

	/**
	 * 
	 * @param tgo
	 *            TextGridObject
	 */
	public void setTGPublishInView(TextGridObject tgo) {
		controller.initialize(); // initialize the controller fields
		updateMessageCounts();
		initializeButton();
		viewer.refresh();
		controller.setSid(RBACSession.getInstance().getSID(true));
		controller.setTGObj(tgo);
		controller.setUri(tgo.getURI().toString());
		controller.setTGPublishClient();
		controller.testIfWorldReadableObject();

		setInfoMessage(tgo);
	}

}
